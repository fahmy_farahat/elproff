"use strict";

/**
 * @ngdoc overview
 * @name elproffApp
 * @description
 * # elproffApp
 *
 * Main module of the application.
 */
var modules = [
    "ngAnimate",
    "ngEnter",
    "angular-loading-bar",
    "ngCookies",
    "restangular",
    "ngRoute",
    "ngSanitize",
    "ngTouch",
    "ui.bootstrap",
    "ngMessages",
    "ngDraggable",
    "ui.select",
    "hc.marked",
    "reCAPTCHA",
    "facebook",
    "angularMoment",
    "ui.tree",
    "highcharts-ng",
    "720kb.socialshare",
    "timer",
    "cgPrompt",
    "ngTagsInput",
    "bd.sockjs",
    ];

var elproffApp = angular.module("elproffApp", modules);

elproffApp.constant("URLS", {
    "upgrade77": "http://www.upgrade77.com/"
});

elproffApp.config(function($routeProvider, RestangularProvider, uiSelectConfig, reCAPTCHAProvider, markedProvider, FacebookProvider) {
    markedProvider.setOptions({
        gfm: true
    });
    reCAPTCHAProvider.setPublicKey("6Le2lf4SAAAAAAHbX7sVKx4yNIaQBOTh7flJ-74T");
    reCAPTCHAProvider.setOptions({
        theme: "clean"
    });
    uiSelectConfig.theme = "bootstrap";
    uiSelectConfig.resetSearchInput = true;

    //RestangularProvider.setBaseUrl("http://dev.elproff.com:9000/ar/api/v1/");
    RestangularProvider.setBaseUrl("/ar/api/v1/");
    // TODO: add API key here
    RestangularProvider.setDefaultHeaders({"Cache-Control": "no-cache"});
    RestangularProvider.setDefaultRequestParams({format: "json"});
    RestangularProvider.setDefaultHttpFields({cache: true});

    // FacebookProvider.init("1396947150606524");
    // Production APP ID: 1424725767822551
    // Dev APP ID: 1437636636531464
    FacebookProvider.init("1424725767822551");

    $routeProvider
        .when("/"                     , { templateUrl: "views/main.html"              , controller: "MainCtrl" })
        .when("/forgotpassword"       , { templateUrl: "views/forgotpassword.html"    , controller: "ForgotpasswordCtrl"   , title: "forgotpassword" })
        .when("/login"                , { templateUrl: "views/login.html"             , controller: "LoginCtrl"            , title: "login" })
        .when("/register"             , { templateUrl: "views/register.html"          , controller: "RegisterCtrl"         , title: "register" })
        .when("/register/thankyou"    , { templateUrl: "views/register/thankyou.html" , controller: "RegisterThankyouCtrl" , title: "thankyou" })
        .when("/invitation/:token"    , { templateUrl: "views/invitation.html"        , controller: "InvitationCtrl"       , title: ["invitation"           , "acceptance"] })
        .when("/activate/:token"      , { templateUrl: "views/activate.html"          , controller: "ActivateCtrl"         , title: ["mail"                 , "activation"] })
        .when("/resetpassword/:token" , { templateUrl: "views/resetpassword.html"     , controller: "ResetpasswordCtrl"    , title: "resetpasswordtitle" })
        .when("/fblogin"              , { templateUrl: "views/fblogin.html"           , controller: "FBLoginCtrl"          , title: "fblogin" })
        .when("/fbregister"           , { templateUrl: "views/fbregister.html"        , controller: "FBRegisterCtrl"       , title: "fbregister" })

        // Roles : admin                                                                                               , sme                                                                                           , author                                                        , reviewer               , parent                                                                                        , manager                 , schooladmin   , schoolteacher , privateteacher , student
        .when("/admin"                                                                                                 , { templateUrl: "views/admin.html"                                                             , controller: "AdminCtrl"                                       , role: "admin"          , title: "dashboard" })
        .when("/admin/author"                                                                                          , { templateUrl: "views/admin/author.html"                                                      , controller: "AdminAuthorCtrl"                                 , role: "admin"          , title: ["authors"                                                                             , "list"] })
        .when("/admin/author/create"                                                                                   , { templateUrl: "views/admin/author/create.html"                                               , controller: "AdminAuthorCreateCtrl"                           , role: "admin"          , title: "create" })
        .when("/admin/author/:id"                                                                                      , { templateUrl: "views/admin/author/view.html"                                                 , controller: "AdminAuthorViewCtrl"                             , role: "admin"          , title: ["admin/author"                                                                        , "view"] })
        .when("/admin/author/:authorid/question"                                                                       , { templateUrl: "views/admin/author/question.html"                                             , controller: "AdminAuthorQuestionCtrl"                         , role: "admin"          , title: ["list"                                                                                , "questions"] })
        .when("/admin/author/:authorid/question/:questionid"                                                           , { templateUrl: "views/admin/author/question/view.html"                                        , controller: "AdminAuthorQuestionViewCtrl"                     , role: "admin"          , title: "question" })
        .when("/admin/author/:authorid/task"                                                                           , { templateUrl: "views/admin/author/task.html"                                                 , controller: "AdminAuthorTaskCtrl"                             , role: "admin"          , title: ["list"                                                                                , "tasks"] })
        .when("/admin/author/:authorid/task/:taskid"                                                                   , { templateUrl: "views/admin/author/task/view.html"                                            , controller: "AdminAuthorTaskViewCtrl"                         , role: "admin"          , title: "task" })
        .when("/admin/element_type"                                                                                    , { templateUrl: "views/admin/element/type.html"                                                , controller: "AdminCourseElementTypeCtrl"                      , role: "admin"          , title: ["courseelementtype"                                                              ] })
        .when("/admin/users_list"                                                                                      , { templateUrl: "views/admin/user/list.html"                                                   , controller: "AdminUserListCtrl"                               , role: "admin"          , title: ["users"                                                                               , "list"] })
        .when("/admin/element_type/create"                                                                             , { templateUrl: "views/admin/element/type/create.html"                                         , controller: "AdminCourseElementTypeCreateCtrl"                , role: "admin"          , title: ["create"                                                                              , "courseelementtype"] })
        .when("/admin/element_type/update/:id"                                                                         , { templateUrl: "views/admin/element/type/update.html"                                         , controller: "AdminCourseElementTypeUpdateCtrl"                , role: "admin"          , title: ["course"                                                                              , "element"               , "type"        , "update"] })
        .when("/admin/country"                                                                                         , { templateUrl: "views/admin/country.html"                                                     , controller: "AdminCountryCtrl"                                , role: "admin"          , title: ["countries"                                                                           , "list"] })
        .when("/admin/country/create"                                                                                  , { templateUrl: "views/admin/country/create.html"                                              , controller: "AdminCountryCreateCtrl"                          , role: "admin"          , title: ["country"                                                                             , "create"] })
        .when("/admin/country/update/:id"                                                                              , { templateUrl: "views/admin/country/update.html"                                              , controller: "AdminCountryUpdateCtrl"                          , role: "admin"          , title: ["country"                                                                             , "update"] })
        .when("/admin/country/educationalsystem"                                                                       , { templateUrl: "views/admin/country/educationalsystem.html"                                   , controller: "AdminEducationalsystemCtrl"                      , role: "admin"          , title: ["educationalsystems"                                                                  , "list"] })
        .when("/admin/country/:countryid"                                                                              , { templateUrl: "views/admin/country/view.html"                                                , controller: "AdminCountryViewCtrl"                            , role: "admin"          , title: ["country"                                                                             , "view"] })
        .when("/admin/country/:countryid/educationalsystem/create"                                                     , { templateUrl: "views/admin/country/educationalsystem/create.html"                            , controller: "AdminEducationalsystemCreateCtrl"                , role: "admin"          , title: ["create"                                                                              , "educationalsystem"] })
        .when("/admin/country/:countryid/educationalsystem/update/:id"                                                 , { templateUrl: "views/admin/country/educationalsystem/update.html"                            , controller: "AdminEducationalsystemUpdateCtrl"                , role: "admin"          , title: ["update"                                                                              , "educationalsystem"] })
        .when("/admin/country/:countryid/educationalsystem/grade"                                                      , { templateUrl: "views/admin/country/educationalsystem/grade.html"                             , controller: "AdminGradeCtrl"                                  , role: "admin"          , title: ["grades"                                                                              , "list"] })
        .when("/admin/country/:countryid/educationalsystem/:systemid"                                                  , { templateUrl: "views/admin/country/educationalsystem/view.html"                              , controller: "AdminCountryEducationalsystemViewCtrl"           , role: "admin"          , title: ["educationalsystem"                                                                   , "view"] })
        .when("/admin/country/:countryid/educationalsystem/:systemid/grade/create"                                     , { templateUrl: "views/admin/country/educationalsystem/grade/create.html"                      , controller: "AdminGradeCreateCtrl"                            , role: "admin"          , title: ["create"                                                                              , "grade"] })
        .when("/admin/country/:countryid/educationalsystem/:systemid/grade/update/:id"                                 , { templateUrl: "views/admin/country/educationalsystem/grade/update.html"                      , controller: "AdminGradeUpdateCtrl"                            , role: "admin"          , title: ["update"                                                                              , "grade"] })
        .when("/admin/country/:countryid/educationalsystem/:systemid/grade/:gradeid"                                   , { templateUrl: "views/admin/country/educationalsystem/grade/view.html"                        , controller: "AdminCountryEducationalsystemGradeViewCtrl"      , role: "admin"          , title: ["grade"                                                                               , "view"] })
        .when("/admin/country/:countryid/educationalsystem/:systemid/grade/:gradeid/viewdeleted"                       , { templateUrl: "views/admin/country/educationalsystem/grade/viewdeleted.html"                 , controller: "AdminCountryEducationalsystemGradeViewdelCtrl"   , role: "admin"          , title: ["grade"                                                                               , "view"] })
        .when("/admin/country/:countryid/educationalsystem/:systemid/grade/:gradeid/course"                            , { templateUrl: "views/admin/country/educationalsystem/grade/course.html"                      , controller: "AdminCourseCtrl"                                 , role: "admin"          , title: ["courses"                                                                             , "list"] })
        .when("/admin/country/:countryid/educationalsystem/:systemid/grade/:gradeid/course/create"                     , { templateUrl: "views/admin/country/educationalsystem/grade/course/create.html"               , controller: "AdminCourseCreateCtrl"                           , role: "admin"          , title: ["create"                                                                              , "course"] })
        .when("/admin/country/:countryid/educationalsystem/:systemid/grade/:gradeid/course/:courseid/update"           , { templateUrl: "views/admin/country/educationalsystem/grade/course/update.html"               , controller: "AdminCourseUpdateCtrl"                           , role: "admin"          , title: ["update"                                                                              , "course"] })
        .when("/admin/country/:countryid/educationalsystem/:systemid/grade/:gradeid/course/:courseid"                  , { templateUrl: "views/admin/country/educationalsystem/grade/course/view.html"                 , controller: "AdminCourseViewCtrl"                             , role: "admin"          , title: ["course"                                                                              , "view"] })
        .when("/admin/country/:countryid/educationalsystem/:systemid/grade/:gradeid/subscription"                      , { templateUrl: "views/admin/country/educationalsystem/grade/subscription.html"                , controller: "AdminSubscriptionCtrl"                           , role: "admin"          , title: ["subscription"                                                                        , "view"] })
        .when("/admin/country/:countryid/educationalsystem/:systemid/grade/:gradeid/subscription/term"                 , { templateUrl: "views/admin/country/educationalsystem/grade/subscription/term.html"           , controller: "AdminSubscriptionTermCtrl"                       , role: "admin"          , title: ["listSubscriptionTerms"] })
        .when("/admin/country/:countryid/educationalsystem/:systemid/grade/:gradeid/subscription/term/create"          , { templateUrl: "views/admin/country/educationalsystem/grade/subscription/term/create.html"    , controller: "AdminSubscriptionTermCreateCtrl"                 , role: "admin"          , title: ["createSubscriptionTerm"] })
        .when("/admin/country/:countryid/educationalsystem/:systemid/grade/:gradeid/subscription/term/update/:id"      , { templateUrl: "views/admin/country/educationalsystem/grade/subscription/term/update.html"    , controller: "AdminSubscriptionTermUpdateCtrl"                 , role: "admin"          , title: ["updateSubscriptionTerm"] })
        .when("/admin/country/:countryid/educationalsystem/:systemid/grade/:gradeid/subscription/plan"                 , { templateUrl: "views/admin/country/educationalsystem/grade/subscription/plan.html"           , controller: "AdminSubscriptionPlanCtrl"                       , role: "admin"          , title: ["list"                                                                                , "subscription"          , "plans"] })
        .when("/admin/country/:countryid/educationalsystem/:systemid/grade/:gradeid/subscription/plan/create"          , { templateUrl: "views/admin/country/educationalsystem/grade/subscription/plan/create.html"    , controller: "AdminSubscriptionPlanCreateCtrl"                 , role: "admin"          , title: ["create"                                                                              , "subscription"          , "plan"] })
        .when("/admin/country/:countryid/educationalsystem/:systemid/grade/:gradeid/subscription/plan/update/:planid"  , { templateUrl: "views/admin/country/educationalsystem/grade/subscription/plan/update.html"    , controller: "AdminSubscriptionPlanUpdateCtrl"                 , role: "admin"          , title: ["update"                                                                              , "subscription"          , "plan"] })
        .when("/admin/country/:countryid/educationalsystem/:systemid/grade/:gradeid/subscription/coupons"              , { templateUrl: "views/admin/country/educationalsystem/grade/subscription/coupons.html"        , controller: "AdminSubscriptionCouponsCtrl"                    , role: "admin"          , title: "coupons" })
        .when("/admin/country/:countryid/educationalsystem/:systemid/grade/:gradeid/subscription/coupons/create"       , { templateUrl: "views/admin/country/educationalsystem/grade/subscription/coupons/create.html" , controller: "AdminSubscriptionCouponsCreateCtrl"              , role: "admin"          , title: "createCoupons" })
        .when("/admin/country/:countryid/educationalsystem/:systemid/grade/:gradeid/subscription/:studentid"           , { templateUrl: "views/admin/country/educationalsystem/grade/subscription/view.html"           , controller: "AdminCountryEducationalsystemGradeSubscriptionViewCtrl" , role: "admin"   , title: ["student subscription"                                                                , "view"] })
        .when("/admin/country/:countryid/educationalsystem/:systemid/grade/:gradeid/course/:courseid/prize/create"     , { templateUrl: "views/admin/country/educationalsystem/grade/course/prize/create.html"         , controller: "AdminPrizeCreateCtrl"                            , role: "admin"          , title: ["create"                                                                              , "prize"] })
        .when("/admin/country/:countryid/educationalsystem/:systemid/grade/:gradeid/course/:courseid/prize/update/:prizeid" , { templateUrl: "views/admin/country/educationalsystem/grade/course/prize/update.html"    , controller: "AdminPrizeUpdateCtrl"                            , role: "admin"          , title: ["update"                                                                              , "prize"] })
        .when("/admin/help"                                                                                            , { templateUrl: "views/admin/help.html"                                                        , controller: "AdminHelpCtrl"                                   , role: "admin"          , title: ["help"] })
        .when("/admin/help_pages"                                                                                      , { templateUrl: "views/admin/helppage.html"                                                    , controller: "AdminHelppageCtrl"                               , role: "admin"          , title: ["help"                                                                                , "pages"] })
        .when("/admin/help_pages/create"                                                                               , { templateUrl: "views/admin/helppages/create.html"                                            , controller: "AdminHelppagesCreateCtrl"                        , role: "admin"          , title: ["create"                                                                              , "help"] })
        .when("/admin/help_pages/author"                                                                               , { templateUrl: "views/admin/helppages/author/view.html"                                       , controller: "AdminHelppagesAuthorViewCtrl"                    , role: "admin"          , title: ["help"                                                                                , "authors"] })
        .when("/admin/help_pages/reviewer"                                                                             , { templateUrl: "views/admin/helppages/reviewer/view.html"                                     , controller: "AdminHelppagesReviewerViewCtrl"                  , role: "admin"          , title: ["help"                                                                                , "reviewers"] })
        .when("/admin/help_pages/sme"                                                                                  , { templateUrl: "views/admin/helppages/sme/view.html"                                          , controller: "AdminHelppagesSmeViewCtrl"                       , role: "admin"          , title: ["help"                                                                                , "sme"] })
        .when("/admin/help_pages/student"                                                                              , { templateUrl: "views/admin/helppages/student/view.html"                                      , controller: "AdminHelppagesStudentViewCtrl"                   , role: "admin"          , title: ["help"                                                                                , "students"] })
        .when("/admin/help_pages/update/:pageid"                                                                       , { templateUrl: "views/admin/helppages/update.html"                                            , controller: "AdminHelppagesUpdateCtrl"                        , role: "admin"          , title: ["help"                                                                                , "update"] })
        .when("/admin/invitations"                                                                                     , { templateUrl: "views/admin/invitations.html"                                                 , controller: "AdminInvitationsCtrl"                            , role: "admin"          , title: "invitations" })
        .when("/admin/notifications"                                                                                   , { templateUrl: "views/admin/notifications.html"                                               , controller: "AdminNotificationsCtrl"                          , role: "admin"          , title: ["notifications"] })
        .when("/admin/profile"                                                                                         , { templateUrl: "views/admin/profile.html"                                                     , controller: "AdminProfileCtrl"                                , role: "admin"          , title: ["profile"] })
        .when("/admin/profile/changepassword"                                                                          , { templateUrl: "views/admin/profile/changepassword.html"                                      , controller: "AdminProfileChangepasswordCtrl"                  , role: "admin"          , title: "changepassword" })
        .when("/admin/profile/edit"                                                                                    , { templateUrl: "views/admin/profile/edit.html"                                                , controller: "AdminProfileEditCtrl"                            , role: "admin"          , title: ["profile"                                                                             , "edit"] })
        .when("/admin/reviewer"                                                                                        , { templateUrl: "views/admin/reviewer.html"                                                    , controller: "AdminReviewerCtrl"                               , role: "admin"          , title: ["reviewers"                                                                           , "list"] })
        .when("/admin/reviewer/create"                                                                                 , { templateUrl: "views/admin/reviewer/create.html"                                             , controller: "AdminReviewerCreateCtrl"                         , role: "admin"          , title: "create" })
        .when("/admin/reviewer/:id"                                                                                    , { templateUrl: "views/admin/reviewer/view.html"                                               , controller: "AdminReviewerViewCtrl"                           , role: "admin"          , title: ["reviewer"                                                                            , "view"] })
        .when("/admin/sme"                                                                                             , { templateUrl: "views/admin/sme.html"                                                         , controller: "AdminSmeCtrl"                                    , role: "admin"          , title: ["smes"                                                                                , "list"] })
        .when("/admin/sme/create"                                                                                      , { templateUrl: "views/admin/sme/create.html"                                                  , controller: "AdminSmeCreateCtrl"                              , role: "admin"          , title: ["smes"                                                                                , "invitation"] })
        .when("/admin/sme/:id"                                                                                         , { templateUrl: "views/admin/sme/view.html"                                                    , controller: "AdminSmeViewCtrl"                                , role: "admin"          , title: ["view"                                                                                , "sme"] })
        .when("/author"                                                                                                , { templateUrl: "views/author.html"                                                            , controller: "AuthorCtrl"                                      , role: "author"         , title: ["dashboard"] })
        .when("/author/course"                                                                                         , { templateUrl: "views/author/course.html"                                                     , controller: "AuthorCourseCtrl"                                , role: "author"         , title: ["courses"                                                                             , "list"] })
        .when("/author/course/:courseid"                                                                               , { redirectTo: "/author/course/:courseid/task" })
        .when("/author/course/:courseid/task/:taskid/question"                                                         , { templateUrl: "views/author/course/task/question.html"                                       , controller: "AuthorQuestionCtrl"                              , role: "author"         , title: ["questions"                                                                           , "list"] })
        .when("/author/course/:courseid/task/:taskid/question/create"                                                  , { templateUrl: "views/author/course/task/question/create.html"                                , controller: "AuthorQuestionCreateCtrl"                        , role: "author"         , title: ["create"                                                                              , "question"] })
        .when("/author/course/:courseid/task/create"                                                                   , { templateUrl: "views/author/course/task/create.html"                                         , controller: "AuthorTaskCreateCtrl"                            , role: "author"         , title: ["select"                                                                              , "task"] })
        .when("/author/course/:courseid/task"                                                                          , { templateUrl: "views/author/course/tasks.html"                                               , controller: "AuthorTasksCtrl"                                 , role: "author"         , title: "tasks" })
        .when("/author/course/update/:courseid"                                                                        , { templateUrl: "views/author/course/tasks.html"                                               , controller: "AuthorTasksCtrl"                                 , role: "author"         , title: ["tasks"                                                                               , "list"] })
        .when("/author/help"                                                                                           , { templateUrl: "views/author/help.html"                                                       , controller: "AuthorHelpCtrl"                                  , role: "author"         , title: ["author"                                                                              , "help"] })
        .when("/author/help/:pageid"                                                                                   , { templateUrl: "views/author/help.html"                                                       , controller: "AuthorHelpCtrl"                                  , role: "author"         , title: ["author"                                                                              , "help"] })
        .when("/author/notifications"                                                                                  , { templateUrl: "views/author/notifications.html"                                              , controller: "AuthorNotificationsCtrl"                         , role: "author"         , title: "notifications" })
        .when("/author/profile"                                                                                        , { templateUrl: "views/author/profile.html"                                                    , controller: "AuthorProfileCtrl"                               , role: "author"         , title: ["profile"] })
        .when("/author/profile/changepassword"                                                                         , { templateUrl: "views/author/profile/changepassword.html"                                     , controller: "AuthorProfileChangepasswordCtrl"                 , role: "author"         , title: "changepassword" })
        .when("/author/profile/edit"                                                                                   , { templateUrl: "views/author/profile/edit.html"                                               , controller: "AuthorProfileEditCtrl"                           , role: "author"         , title: "edit" })
        .when("/manager"                                                                                               , { templateUrl: "views/manager.html"                                                           , controller: "ManagerCtrl"                                     , role: "manager"        , title: "dashboard" })
        .when("/manager/notifications"                                                                                 , { templateUrl: "views/manager/notifications.html"                                             , controller: "ManagerNotificationsCtrl"                        , role: "manager"        , title: "notifications" })
        .when("/manager/profile"                                                                                       , { templateUrl: "views/manager/profile.html"                                                   , controller: "ManagerProfileCtrl"                              , role: "manager"        , title: ["profile"] })
        .when("/manager/profile/changepassword"                                                                        , { templateUrl: "views/manager/profile/changepassword.html"                                    , controller: "ManagerProfileChangepasswordCtrl"                , role: "manager"        , title: ["changepassword"] })
        .when("/manager/profile/edit"                                                                                  , { templateUrl: "views/manager/profile/edit.html"                                              , controller: "ManagerProfileEditCtrl"                          , role: "manager"        , title: ["profile"                                                                             , "update"] })
        .when("/parent"                                                                                                , { templateUrl: "views/parent.html"                                                            , controller: "ParentCtrl"                                      , role: "parent"         , title: "dashboard" })
        .when("/parent/add"                                                                                            , { templateUrl: "views/parent/add.html"                                                        , controller: "ParentAddCtrl"                                   , role: "parent"         , title: ["add"] })
        .when("/parent/help"                                                                                           , { templateUrl: "views/parent/help.html"                                                       , controller: "ParentHelpCtrl"                                  , role: "parent"         , title: ["parent"                                                                              , "help"] })
        .when("/parent/notifications"                                                                                  , { templateUrl: "views/parent/notifications.html"                                              , controller: "ParentNotificationsCtrl"                         , role: "parent"         , title: "notifications" })
        .when("/parent/profile"                                                                                        , { templateUrl: "views/parent/profile.html"                                                    , controller: "ParentProfileCtrl"                               , role: "parent"         , title: "profile" })
        .when("/parent/profile/changepassword"                                                                         , { templateUrl: "views/parent/profile/changepassword.html"                                     , controller: "ParentProfileChangepasswordCtrl"                 , role: "parent"         , title: "changepassword" })
        .when("/parent/profile/edit"                                                                                   , { templateUrl: "views/parent/profile/edit.html"                                               , controller: "ParentProfileEditCtrl"                           , role: "parent"         , title: "edit" })
        .when("/parent/son/:studentid"                                                                                 , { templateUrl: "views/parent/son.html"                                                        , controller: "ParentSonCtrl"                                   , role: "parent"         , title: "sondetails" })
        .when("/parent/son/:studentid/course/"                                                                         , { redirectTo: "/parent/son/:studentid/" })
        .when("/parent/son/:studentid/course/payment"                                                                  , { templateUrl: "views/parent/son/payment.html"                                                , controller: "ParentPaymentCtrl"                               , role: "parent"         , title: "payment" })
        .when("/parent/son/:studentid/course/:courseid/"                                                               , { redirectTo: "/parent/son/:studentid/course/:courseid/report" })
        .when("/parent/son/:studentid/course/:courseid/exam/:examid"                                                   , { templateUrl: "views/parent/son/course/exam.html"                                            , controller: "ParentSonCourseExamCtrl"                         , role: "parent"         , title: "exam" })
        .when("/parent/son/:studentid/course/:courseid/subscribe"                                                      , { templateUrl: "views/parent/son/course/subscribe.html"                                       , controller: "ParentSonCourseSubscribeCtrl"                    , role: "parent"         , title: "subscribe" })
        .when("/parent/son/:studentid/course/:courseid/report"                                                         , { templateUrl: "views/parent/son/course/report.html"                                          , controller: "ParentSonCourseReportCtrl"                       , role: "parent"         , title: "detailedreport" })
        .when("/parent/son/:studentid/exam/:examid"                                                                    , { templateUrl: "views/parent/son/exam.html"                                                   , controller: "ParentSonExamCtrl"                               , role: "parent"         , title: "sonexamreport" })
        .when("/privateteacher"                                                                                        , { templateUrl: "views/privateteacher.html"                                                    , controller: "PrivateteacherCtrl"                              , role: "privateteacher" , title: ["dashboard"] })
        .when("/privateteacher/course/:courseid"                                                                       , { templateUrl: "views/privateteacher/course.html"                                             , controller: "PrivateteacherCourseCtrl"                        , role: "privateteacher" , title: ["course"] })
        .when("/privateteacher/course/:courseid/group/create"                                                          , { templateUrl: "views/privateteacher/course/group/create.html"                                , controller: "PrivateteacherGroupCreateCtrl"                   , role: "privateteacher" , title: ["create"                                                                              , "group"] })
        .when("/privateteacher/course/:courseid/group/:groupid"                                                        , { templateUrl: "views/privateteacher/course/group.html"                                       , controller: "PrivateteacherGroupCtrl"                         , role: "privateteacher" , title: ["view"                                                                                , "group"] })
        .when("/privateteacher/course/:courseid/group/:groupid/exams"                                                  , { templateUrl: "views/privateteacher/course/group/examlist.html"                              , controller: "PrivateteacherCourseGroupExamlistCtrl"           , role: "privateteacher" , title: ["view"                                                                                , "examlist"] })
        .when("/privateteacher/course/:courseid/group/:groupid/exams/:examid"                                          , { templateUrl: "views/privateteacher/course/group/exams/view.html"                            , controller: "PrivateteacherCourseGroupExamsViewCtrl"          , role: "privateteacher" , title: ["view"                                                                                , "examreport"] })
        .when("/privateteacher/course/:courseid/group/:groupid/student/:studentid/exam/:examid/report"                 , { templateUrl: "views/privateteacher/course/group/exams/report.html"                          , controller: "PrivateteacherCourseGroupExamsReportCtrl"        , role: "privateteacher" , title: ["report"                                                                                , "examreport"] })
        .when("/privateteacher/course/:courseid/group/update/:groupid"                                                 , { templateUrl: "views/privateteacher/course/group/update.html"                                , controller: "PrivateteacherGroupUpdateCtrl"                   , role: "privateteacher" , title: ["group"                                                                               , "update"] })
        .when("/privateteacher/course/:courseid/group/:groupid/student/:studentid"                                     , { templateUrl: "views/privateteacher/course/group/studentview.html"                           , controller: "PrivateteacherCourseGroupStudentviewCtrl"        , role: "privateteacher" , title: ["student"                                                                             , "view"] })
        .when("/privateteacher/invite"                                                                                 , { templateUrl: "views/privateteacher/invitation.html"                                         , controller: "PrivateteacherInvitationCtrl"                    , role: "privateteacher" , title: ["invite"                                                                              , "student"] })
        .when("/privateteacher/course/:courseid/exam/create"                                                           , { templateUrl: "views/privateteacher/course/exam/create.html"                                 , controller: "PrivateteacherCourseExamCreateCtrl"              , role: "privateteacher" , title: ["create"                                                                              , "exam"] })
        .when("/privateteacher/course/:courseid/exam/:buildid/review"                                                  , { templateUrl: "views/privateteacher/course/exam/review.html"                                 , controller: "PrivateteacherCourseExamReviewCtrl"              , role: "privateteacher" , title: ["review"                                                                              , "exam"] })
        .when("/privateteacher/course/exam/reports"                                                                    , { templateUrl: "views/privateteacher/course/exam/reports.html"                                , controller: "PrivateteacherCourseExamReportsCtrl"             , role: "privateteacher" , title: ["exam"                                                                                , "reports"] })
        .when("/privateteacher/course/exam/reports/group"                                                              , { templateUrl: "views/privateteacher/course/exam/reports/group.html"                          , controller: "PrivateteacherCourseExamReportsGroupCtrl"        , role: "privateteacher" , title: ["group"                                                                               , "exam"                  , "reports"] })
        .when("/privateteacher/course/exam/reports/student"                                                            , { templateUrl: "views/privateteacher/course/exam/reports/student.html"                        , controller: "PrivateteacherCourseExamReportsStudentCtrl"      , role: "privateteacher" , title: ["course"                                                                              , "exam"                  , "student"] })
        .when("/privateteacher/course/homework/create"                                                                 , { templateUrl: "views/privateteacher/course/homework/create.html"                             , controller: "PrivateteacherCourseHomeworkCreateCtrl"          , role: "privateteacher" , title: "create" })
        .when("/privateteacher/help"                                                                                   , { templateUrl: "views/privateteacher/help.html"                                               , controller: "PrivateteacherHelpCtrl"                          , role: "privateteacher" , title: ["privateteacher"                                                                      , "help"] })
        .when("/privateteacher/notifications"                                                                          , { templateUrl: "views/privateteacher/notifications.html"                                      , controller: "PrivateteacherNotificationsCtrl"                 , role: "privateteacher" , title: "notifications" })
        .when("/privateteacher/profile"                                                                                , { templateUrl: "views/privateteacher/profile.html"                                            , controller: "PrivateteacherProfileCtrl"                       , role: "privateteacher" , title: "profile" })
        .when("/privateteacher/profile/changepassword"                                                                 , { templateUrl: "views/privateteacher/profile/changepassword.html"                             , controller: "PrivateteacherProfileChangepasswordCtrl"         , role: "privateteacher" , title: "changepassword" })
        .when("/privateteacher/profile/edit"                                                                           , { templateUrl: "views/privateteacher/profile/edit.html"                                       , controller: "PrivateteacherProfileEditCtrl"                   , role: "privateteacher" , title: "edit" })
        .when("/reviewer"                                                                                              , { templateUrl: "views/reviewer.html"                                                          , controller: "ReviewerCtrl"                                    , role: "reviewer"       , title: "dashboard" })
        .when("/reviewer/course"                                                                                       , { templateUrl: "views/reviewer/course.html"                                                   , controller: "ReviewerCourseCtrl"                              , role: "reviewer"       , title: "courses" })
        .when("/reviewer/course/:courseid"                                                                             , { redirectTo: "/reviewer/course/:courseid/task" })
        .when("/reviewer/course/:courseid/task"                                                                        , { templateUrl: "views/reviewer/course/tasks.html"                                             , controller: "ReviewerCourseTasksCtrl"                         , role: "reviewer"       , title: "tasks" })
        .when("/reviewer/course/:courseid/task/create"                                                                 , { templateUrl: "views/reviewer/course/task/create.html"                                       , controller: "ReviewerCourseTaskCreateCtrl"                    , role: "reviewer"       , title: ["task"                                                                                , "select"] })
        .when("/reviewer/course/:courseid/task/:taskid/questions"                                                      , { templateUrl: "views/reviewer/course/task/questions.html"                                    , controller: "ReviewerTaskQuestionsViewCtrl"                   , role: "reviewer"       , title: ["questions"                                                                           , "list"] })
        .when("/reviewer/course/:courseid/task/:taskid/question/:questionid"                                           , { templateUrl: "views/reviewer/course/task/question/view.html"                                , controller: "ReviewerQuestionViewCtrl"                        , role: "reviewer"       , title: ["question"                                                                            , "edit"] })
        .when("/reviewer/course/:courseid/task/:taskid/question/:questionid/reviewlist"                                           , { templateUrl: "views/reviewer/course/task/question/reviewlist.html"                                , controller: "QuestionReviewListCtrl"         , role: "reviewer"       , title: ["reviewlist"                                                                            , "reviewlist"] })
        .when("/reviewer/help"                                                                                         , { templateUrl: "views/reviewer/help.html"                                                     , controller: "ReviewerHelpCtrl"                                , role: "reviewer"       , title: ["reviewer"                                                                            , "help"] })
        .when("/reviewer/help/:pageid"                                                                                 , { templateUrl: "views/reviewer/help.html"                                                     , controller: "ReviewerHelpCtrl"                                , role: "reviewer"       , title: ["reviewer"                                                                            , "help"] })
        .when("/reviewer/notifications"                                                                                , { templateUrl: "views/reviewer/notifications.html"                                            , controller: "ReviewerNotificationsCtrl"                       , role: "reviewer"       , title: "notifications" })
        .when("/reviewer/profile"                                                                                      , { templateUrl: "views/reviewer/profile.html"                                                  , controller: "ReviewerProfileCtrl"                             , role: "reviewer"       , title: "profile" })
        .when("/reviewer/profile/changepassword"                                                                       , { templateUrl: "views/reviewer/profile/changepassword.html"                                   , controller: "ReviewerProfileChangepasswordCtrl"               , role: "reviewer"       , title: "changepassword" })
        .when("/reviewer/profile/edit"                                                                                 , { templateUrl: "views/reviewer/profile/edit.html"                                             , controller: "ReviewerProfileEditCtrl"                         , role: "reviewer"       , title: "edit" })
        .when("/schooladmin"                                                                                           , { templateUrl: "views/schooladmin.html"                                                       , controller: "SchooladminCtrl"                                 , role: "schooladmin"    , title: ["dashboard"] })
        .when("/schooladmin/course"                                                                                    , { templateUrl: "views/schooladmin/course.html"                                                , controller: "SchooladminCourseCtrl"                           , role: "schooladmin"    , title: ["course"] })
        .when("/schooladmin/course/add"                                                                                , { templateUrl: "views/schooladmin/course/add.html"                                            , controller: "SchooladminCourseAddCtrl"                        , role: "schooladmin"    , title: ["course"                                                                              , "add"] })
        .when("/schooladmin/help"                                                                                      , { templateUrl: "views/schooladmin/help.html"                                                  , controller: "SchooladminHelpCtrl"                             , role: "schooladmin"    , title: ["schooladmin"                                                                         , "help"] })
        .when("/schooladmin/manager"                                                                                   , { templateUrl: "views/schooladmin/manager.html"                                               , controller: "SchooladminManagerCtrl"                          , role: "schooladmin"    , title: ["manager"] })
        .when("/schooladmin/manager/add"                                                                               , { templateUrl: "views/schooladmin/manager/add.html"                                           , controller: "SchooladminManagerAddCtrl"                       , role: "schooladmin"    , title: ["manager"                                                                             , "add"] })
        .when("/schooladmin/notifications"                                                                             , { templateUrl: "views/schooladmin/notifications.html"                                         , controller: "SchooladminNotificationsCtrl"                    , role: "schooladmin"    , title: "notifications" })
        .when("/schooladmin/profile"                                                                                   , { templateUrl: "views/schooladmin/profile.html"                                               , controller: "SchooladminProfileCtrl"                          , role: "schooladmin"    , title: ["profile"] })
        .when("/schooladmin/profile/edit"                                                                              , { templateUrl: "views/schooladmin/profile/edit.html"                                          , controller: "SchooladminProfileEditCtrl"                      , role: "schooladmin"    , title: ["profile"                                                                             , "edit"] })
        .when("/schooladmin/profile/changepassword"                                                                    , { templateUrl: "views/schooladmin/profile/changepassword.html"                                , controller: "SchooladminProfileChangepasswordCtrl"            , role: "schooladmin"    , title: ["profile"                                                                             , "changepassword"] })
        .when("/schooladmin/profile/school/:schoolid"                                                                  , { templateUrl: "views/schooladmin/profile/school.html"                                        , controller: "SchooladminProfileSchoolCtrl"                    , role: "schooladmin"    , title: ["profile"                                                                             , "school"] })
        .when("/schooladmin/school/grade"                                                                              , { templateUrl: "views/schooladmin/school/grade.html"                                          , controller: "SchooladminSchoolGradeCtrl"                      , role: "schooladmin"    , title: ["school"                                                                              , "grade"] })
        .when("/schooladmin/school/grade/add"                                                                          , { templateUrl: "views/schooladmin/school/grade/add.html"                                      , controller: "SchooladminSchoolGradeAddCtrl"                   , role: "schooladmin"    , title: ["school"                                                                              , "grade"                 , "add"] })
        .when("/schooladmin/educationalsystems"                                                                        , { templateUrl: "views/schooladmin/student.html"                                               , controller: "SchooladminStudentCtrl"                          , role: "schooladmin"    , title: ["educationalsystem"] })
        .when("/schooladmin/educationalsystems/:systemid"                                                              , { templateUrl: "views/schooladmin/student/educationalsystem.html"                             , controller: "SchooladminStudentEducationalsystemCtrl"         , role: "schooladmin"    , title: ["grade"] })
        .when("/schooladmin/educationalsystems/:systemid/grade/:gradeid"                                               , { templateUrl: "views/schooladmin/student/grade/view.html"                                    , controller: "SchooladminStudentGradeViewCtrl"                 , role: "schooladmin"    , title: ["classes"] })
        .when("/schooladmin/educationalsystems/:systemid/grade/:gradeid/class/add"                                     , { templateUrl: "views/schooladmin/student/grade/addclass.html"                                , controller: "SchooladminStudentGradeAddclassCtrl"             , role: "schooladmin"    , title: ["addclass"] })
        .when("/schooladmin/educationalsystems/:systemid/grade/:gradeid/class/:classid"                                , { templateUrl: "views/schooladmin/student/grade/class/view.html"                              , controller: "SchooladminStudentGradeClassViewCtrl"            , role: "schooladmin"    , title: ["student"] })
        .when("/schooladmin/educationalsystems/:systemid/grade/:gradeid/class/:classid/invite"                         , { templateUrl: "views/schooladmin/student/grade/class/invite.html"                            , controller: "SchooladminStudentGradeClassInviteCtrl"          , role: "schooladmin"    , title: ["student"                                                                              , "add"] })
        .when("/schooladmin/teacher"                                                                                   , { templateUrl: "views/schooladmin/teacher.html"                                               , controller: "SchooladminTeacherCtrl"                          , role: "schooladmin"    , title: ["teacher"] })
        .when("/schooladmin/teacher/add"                                                                               , { templateUrl: "views/schooladmin/teacher/add.html"                                           , controller: "SchooladminTeacherAddCtrl"                       , role: "schooladmin"    , title: ["teacher"                                                                             , "add"] })
        .when("/schooladmin/teacher/course"                                                                            , { templateUrl: "views/schooladmin/teacher/course.html"                                        , controller: "SchooladminTeacherCourseCtrl"                    , role: "schooladmin"    , title: ["teacher"                                                                             , "course"] })
        .when("/schooladmin/teacher/course/add"                                                                        , { templateUrl: "views/schooladmin/teacher/course/add.html"                                    , controller: "SchooladminTeacherCourseAddCtrl"                 , role: "schooladmin"    , title: ["teacher"                                                                             , "course"                , "add"] })
        .when("/schooladmin/teacher/:teacherid"                                                                        , { templateUrl: "views/schooladmin/teacher/view.html"                                          , controller: "SchooladminTeacherViewCtrl"                      , role: "schooladmin"    , title: ["teacher"                                                                             , "view"] })
        .when("/schoolmanager"                                                                                         , { templateUrl: "views/schoolmanager.html"                                                     , controller: "SchoolmanagerCtrl"                               , role: "schoolmanager"  , title: ["dashboard"] })
        .when("/schoolmanager/help"                                                                                    , { templateUrl: "views/schoolmanager/help.html"                                                , controller: "SchoolmanagerHelpCtrl"                           , role: "schoolmanger"   , title: ["schoolmanager"                                                                       , "help"] })
        .when("/schoolteacher"                                                                                         , { templateUrl: "views/schoolteacher.html"                                                     , controller: "SchoolteacherCtrl"                               , schoolteacher: "admin" , title: ["dashboard"] })
        .when("/schoolteacher/course"                                                                                  , { templateUrl: "views/schoolteacher/course.html"                                              , controller: "SchoolteacherCourseCtrl"                         , role: "schoolteacher"  , title: "course" })
        .when("/schoolteacher/course/:courseid"                                                                        , { templateUrl: "views/schoolteacher/course/view.html"                                         , controller: "SchoolteacherCourseViewCtrl"                     , role: "schoolteacher"  , title: ["view"                                                                                , "classes"]})
        .when("/schoolteacher/course/:courseid/class/:classid"                                                         , { templateUrl: "views/schoolteacher/course/classes/view.html"                                 , controller: "SchoolteacherCourseClassesViewCtrl"              , role: "schoolteacher"  , title: ["view"                                                                                , "students"]})
        .when("/schoolteacher/course/:courseid/class/:classid/exams"                                                   , { templateUrl: "views/schoolteacher/course/classes/exams.html"                                , controller: "SchoolteacherCourseClassesExamsCtrl"             , role: "schoolteacher"  , title: "exams" })
        .when("/schoolteacher/course/:courseid/class/:classid/student/:studentid"                                      , { templateUrl: "views/schoolteacher/course/student/examview.html"                             , controller: "SchoolteacherCourseStudentExamviewCtrl"          , role: "schoolteacher"  , title: "exams" })
        .when("/schoolteacher/course/:courseid/exams/:buildid/review"                                                  , { templateUrl: "views/schoolteacher/course/exams/review.html"                                 , controller: "SchoolteacherCourseExamsReviewCtrl"              , role: "schoolteacher"  , title: ["review"                                                                              , "exam"] })
        .when("/schoolteacher/course/:courseid/exams/:examid/questions"                                                , { templateUrl: "views/schoolteacher/course/exams/questions.html"                              , controller: "SchoolteacherCourseExamsQuestionsCtrl"           , role: "schoolteacher"  , title: "questions" })
        .when("/schoolteacher/course/:courseid/exams/:examid/questions/create"                                         , { templateUrl: "views/schoolteacher/course/exams/questions/create.html"                       , controller: "SchoolteacherCourseExamsQuestionsCreateCtrl"     , role: "schoolteacher"  , title: "create" })
        .when("/schoolteacher/course/:courseid/exams/:examid/reports"                                                  , { templateUrl: "views/schoolteacher/course/exams/reports.html"                                , controller: "SchoolteacherCourseExamsReportsCtrl"             , role: "schoolteacher"  , title: "reports" })
        .when("/schoolteacher/course/:courseid/exams/create"                                                           , { templateUrl: "views/schoolteacher/course/exams/create.html"                                 , controller: "SchoolteacherCourseExamsCreateCtrl"              , role: "schoolteacher"  , title: "create" })
        .when("/schoolteacher/course/:courseid/grade"                                                                  , { templateUrl: "views/schoolteacher/course/grade.html"                                        , controller: "SchoolteacherCourseGradeCtrl"                    , role: "schoolteacher"  , title: "grade" })
        .when("/schoolteacher/course/:courseid/homeworks"                                                              , { templateUrl: "views/schoolteacher/course/homeworks.html"                                    , controller: "SchoolteacherCourseHomeworksCtrl"                , role: "schoolteacher"  , title: "homeworks" })
        .when("/schoolteacher/course/:courseid/homeworks/:homeworkid/questions"                                        , { templateUrl: "views/schoolteacher/course/homeworks/questions.html"                          , controller: "SchoolteacherCourseHomeworksQuestionsCtrl"       , role: "schoolteacher"  , title: "questions" })
        .when("/schoolteacher/course/:courseid/homeworks/:homeworkid/questions/create"                                 , { templateUrl: "views/schoolteacher/course/homeworks/questions/create.html"                   , controller: "SchoolteacherCourseHomeworksQuestionsCreateCtrl" , role: "schoolteacher"  , title: "create" })
        .when("/schoolteacher/course/:courseid/homeworks/:homeworkid/reports"                                          , { templateUrl: "views/schoolteacher/course/homeworks/reports.html"                            , controller: "SchoolteacherCourseHomeworksReportsCtrl"         , role: "schoolteacher"  , title: "reports" })
        .when("/schoolteacher/course/:courseid/homeworks/create"                                                       , { templateUrl: "views/schoolteacher/course/homeworks/create.html"                             , controller: "SchoolteacherCourseHomeworksCreateCtrl"          , role: "schoolteacher"  , title: "create" })
        .when("/schoolteacher/course/:courseid/reports"                                                                , { templateUrl: "views/schoolteacher/course/reports.html"                                      , controller: "SchoolteacherCourseReportsCtrl"                  , role: "schoolteacher"  , title: "reports" })
        .when("/schoolteacher/help"                                                                                    , { templateUrl: "views/schoolteacher/help.html"                                                , controller: "SchoolteacherHelpCtrl"                           , role: "schoolteacher"  , title: ["schoolteacher"                                                                       , "help"] })
        .when("/schoolteacher/notifications"                                                                           , { templateUrl: "views/schoolteacher/notifications.html"                                       , controller: "SchoolteacherNotificationsCtrl"                  , role: "schoolteacher"  , title: "notifications" })
        .when("/schoolteacher/profile"                                                                                 , { templateUrl: "views/schoolteacher/profile.html"                                             , controller: "SchoolteacherProfileCtrl"                        , role: "schoolteacher"  , title: "profile" })
        .when("/schoolteacher/profile/changepassword"                                                                  , { templateUrl: "views/schoolteacher/profile/changepassword.html"                              , controller: "SchoolteacherProfileChangepasswordCtrl"          , role: "schoolteacher"  , title: "changepassword" })
        .when("/schoolteacher/profile/edit"                                                                            , { templateUrl: "views/schoolteacher/profile/edit.html"                                        , controller: "SchoolteacherProfileEditCtrl"                    , role: "schoolteacher"  , title: ["profile"                                                                             , "edit"] })
        .when("/sme"                                                                                                   , { templateUrl: "views/sme.html"                                                               , controller: "SmeCtrl"                                         , role: "sme"            , title: "dashboard" })
        .when("/sme/author"                                                                                            , { templateUrl: "views/sme/author.html"                                                        , controller: "SmeAuthorCtrl"                                   , role: "sme"            , title: "authors" })
        .when("/sme/author/create"                                                                                     , { templateUrl: "views/sme/author/create.html"                                                 , controller: "SmeAuthorCreateCtrl"                             , role: "sme"            , title: "inviteauthor" })
        .when("/sme/author/:authorid"                                                                                  , { templateUrl: "views/sme/author/view.html"                                                   , controller: "SmeAuthorViewCtrl"                               , role: "sme"            , title: "author" })
        .when("/sme/author/:authorid/question"                                                                         , { templateUrl: "views/sme/author/question.html"                                               , controller: "SmeAuthorQuestionCtrl"                           , role: "sme"            , title: ["list"                                                                                , "questions"] })
        .when("/sme/author/:authorid/question/:questionid"                                                             , { templateUrl: "views/sme/author/question/view.html"                                          , controller: "SmeAuthorQuestionViewCtrl"                       , role: "sme"            , title: "question" })
        .when("/sme/author/:authorid/task"                                                                             , { templateUrl: "views/sme/author/task.html"                                                   , controller: "SmeAuthorTaskCtrl"                               , role: "sme"            , title: "task" })
        .when("/sme/author/:authorid/task/:taskid"                                                                     , { templateUrl: "views/sme/author/task/view.html"                                              , controller: "SmeAuthorTaskViewCtrl"                           , role: "sme"            , title: "task" })
        .when("/sme/course"                                                                                            , { templateUrl: "views/sme/course.html"                                                        , controller: "SmeCourseCtrl"                                   , role: "sme"            , title: ["list"                                                                                , "courses"] })
        .when("/sme/course/:courseid/element/create"                                                                   , { templateUrl: "views/sme/course/element/create.html"                                         , controller: "SmeCourseElementCreateCtrl"                      , role: "sme"            , title: ["course"                                                                              , "element"               , "create"] })
        .when("/sme/course/:courseid/element/:elementid/update"                                                        , { templateUrl: "views/sme/course/element/update.html"                                         , controller: "SmeCourseElementUpdateCtrl"                      , role: "sme"            , title: ["course"                                                                              , "element"               , "update"] })
        .when("/sme/course/:courseid/element/:elementid"                                                               , { templateUrl: "views/sme/course/element/view.html"                                           , controller: "SmeCourseElementViewCtrl"                        , role: "sme"            , title: ["course"                                                                              , "element"] })
        .when("/sme/course/:courseid/element/:elementid/objective/"                                                    , { redirectTo: "/sme/course/:courseid/element/:elementid/" })
        .when("/sme/course/:courseid"                                                                                  , { templateUrl: "views/sme/course/view.html"                                                   , controller: "SmeCourseViewCtrl"                               , role: "sme"            , title: ["course"                                                                              , "view"] })
        .when("/sme/course/:courseid/duplications"                                                                     , { templateUrl: "views/sme/course/duplication/view.html"                                       , controller: "SmeCourseDuplicationViewCtrl"                    , role: "sme"            , title: ["course"                                                                              , "duplications"] })
        .when("/sme/course/:courseid/duplications/:questionid"                                                         , { templateUrl: "views/sme/course/duplication/question/view.html"                              , controller: "SmeCourseDuplicationQuestionViewCtrl"            , role: "sme"            , title: ["course"                                                                              , "duplicated question"] })
        .when("/sme/course/:courseid/elements"                                                                         , { templateUrl: "views/sme/course/elements.html"                                               , controller: "SmeCourseElementsCtrl"                           , role: "sme"            , title: ["course"                                                                              , "view"] })
        .when("/sme/help"                                                                                              , { templateUrl: "views/sme/help.html"                                                          , controller: "SmeHelpCtrl"                                     , role: "sme"            , title: ["sme"                                                                                 , "help"] })
        .when("/sme/help/:pageid"                                                                                      , { templateUrl: "views/sme/help.html"                                                          , controller: "SmeHelpCtrl"                                     , role: "sme"            , title: ["sme"                                                                                 , "help"] })
        .when("/sme/invitations"                                                                                       , { templateUrl: "views/sme/invitations.html"                                                   , controller: "SmeInvitationsCtrl"                              , role: "sme"            , title: "invitations" })
        .when("/sme/notifications"                                                                                     , { templateUrl: "views/sme/notifications.html"                                                 , controller: "SmeNotificationsCtrl"                            , role: "sme"            , title: "notifications" })
        .when("/sme/course/:courseid/element/:elementid/objective"                                                     , { templateUrl: "views/sme/course/element/objective.html"                                      , controller: "SmeObjectiveCtrl"                                , role: "sme"            , title: ["objective"                                                                           , "list"] })
        .when("/sme/course/:courseid/element/:elementid/objective/create"                                              , { templateUrl: "views/sme/course/element/objective/create.html"                               , controller: "SmeObjectiveCreateCtrl"                          , role: "sme"            , title: ["objective"                                                                           , "create"] })
        .when("/sme/course/:courseid/element/:elementid/objective/:objectiveid"                                        , { templateUrl: "views/sme/course/element/objective/view.html"                                 , controller: "SmeObjectiveViewCtrl"                            , role: "sme"            , title: ["objective"                                                                           , "update"] })
        .when("/sme/profile"                                                                                           , { templateUrl: "views/sme/profile.html"                                                       , controller: "SmeProfileCtrl"                                  , role: "sme"            , title: "profile" })
        .when("/sme/profile/changepassword"                                                                            , { templateUrl: "views/sme/profile/changepassword.html"                                        , controller: "SmeProfileChangepasswordCtrl"                    , role: "sme"            , title: "changepassword" })
        .when("/sme/profile/edit"                                                                                      , { templateUrl: "views/sme/profile/edit.html"                                                  , controller: "SmeProfileEditCtrl"                              , role: "sme"            , title: "edit" })
        .when("/sme/reviewer"                                                                                          , { templateUrl: "views/sme/reviewer.html"                                                      , controller: "SmeReviewerCtrl"                                 , role: "sme"            , title: "reviewers" })
        .when("/sme/reviewer/create"                                                                                   , { templateUrl: "views/sme/reviewer/create.html"                                               , controller: "SmeReviewerCreateCtrl"                           , role: "sme"            , title: "invitereviewer" })
        .when("/sme/reviewer/:reviewerid"                                                                              , { templateUrl: "views/sme/reviewer/view.html"                                                 , controller: "SmeReviewerViewCtrl"                             , role: "sme"            , title: "reviewer" })
        .when("/student"                                                                                               , { templateUrl: "views/student.html"                                                           , controller: "StudentCtrl"                                     , role: "student"        , title: "dashboard" })
        .when("/student/course/:courseid"                                                                              , { templateUrl: "views/student/course.html"                                                    , controller: "StudentCourseCtrl"                               , role: "student"        , title: "course" })
        //.when("/student/course/:courseid/exam"                                                                         , { templateUrl: "views/student/course/exam.html"                                             , controller: "StudentCourseExamCtrl"                           , role: "student"      ,title: ["exams", "list"] })
        .when("/student/course/:courseid/exam/:examid/report"                                                          , { templateUrl: "views/student/course/exam/report.html"                                        , controller: "StudentCourseExamReportCtrl"                     , role: "student"        , title: "examreport" })
        .when("/student/course/:courseid/exam/report"                                                                  , { templateUrl: "views/student/course/exam/detailed_reports.html"                              , controller: "StudentCourseExamDetailedreportsCtrl"            , role: "student"        , title: "detailedreport" })
        .when("/student/course/:courseid/exam/:examid/take"                                                            , { templateUrl: "views/student/course/exam/take.html"                                          , controller: "StudentCourseExamTakeCtrl"                       , role: "student"        , title: "exam" })
        .when("/student/course/:courseid/exam/:examid/take/:questionIndex"                                             , { templateUrl: "views/student/course/exam/take/q.html"                                        , controller: "StudentCourseExamTakeQCtrl"                      , role: "student"        , title: "question" })
        .when("/student/course/:courseid/exam/create"                                                                  , { templateUrl: "views/student/course/exam/generate.html"                                      , controller: "StudentCourseExamGenerateCtrl"                   , role: "student"        , title: "generateExam" })
        .when("/student/course/:courseid/exam/difficulty"                                                              , { templateUrl: "views/student/course/exam/difficulty.html"                                    , controller: "StudentCourseExamDifficultyCtrl"                 , role: "student"        , title: "difficulty" })
        .when("/student/course/:courseid/exam/load/:difficulty/:questionsCount"                                        , { templateUrl: "views/student/course/exam/load.html"                                          , controller: "StudentCourseExamLoadCtrl"                       , role: "student"        , title: "load" })
        .when("/student/course/:courseid/exam/privateteachersuggested"                                                 , { templateUrl: "views/student/course/exam/privateteachersuggested.html"                       , controller: "StudentCourseExamPrivateteachersuggestedCtrl"    , role: "student"        , title: "suggestedteacherexams" })
        .when("/student/course/:courseid/exam/schoolteachersuggested"                                                  , { templateUrl: "views/student/course/exam/schoolteachersuggested.html"                        , controller: "StudentCourseExamSchoolteachersuggestedCtrl"     , role: "student"        , title: "" })
        .when("/student/course/:courseid/exam/suggested"                                                               , { templateUrl: "views/student/course/exam/suggested.html"                                     , controller: "StudentCourseExamSuggestedCtrl"                  , role: "student"        , title: "suggestedexams" })
        .when("/student/course/:courseid/exam/:examid/result"                                                          , { templateUrl: "views/student/course/exam/result.html"                                        , controller: "StudentCourseExamResultCtrl"                     , role: "student"        , title: "examresult" })
        .when("/student/course/:courseid/exam/:examid/answers/"                                                        , { redirectTo: "/student/course/:courseid/exam/:examid/answers/0" })
        .when("/student/course/:courseid/exam/:examid/answers/:questionIndex"                                          , { templateUrl: "views/student/course/exam/answers.html"                                       , controller: "StudentCourseExamAnswersCtrl"                    , role: "student"        , title: ["perfectanswers"] })
        .when("/student/course/:courseid/practice/create"                                                              , { templateUrl: "views/student/course/practice/generate.html"                                  , controller: "StudentCoursePracticeGenerateCtrl"               , role: "student"        , title: ["generate"                                                                            , "practice"] })
        .when("/student/course/:courseid/practice/suggested"                                                           , { templateUrl: "views/student/course/practice/suggested.html"                                 , controller: "StudentCoursePracticeSuggestedCtrl"              , role: "student"        , title: "suggestedpractices" })
        .when("/student/course/:courseid/subscribe"                                                                    , { templateUrl: "views/student/course/subscribe.html"                                          , controller: "StudentCourseSubscribeCtrl"                      , role: "student"        , title: "subscribe" })
        .when("/student/course/exam/load"                                                                              , { templateUrl: "views/student/course/exam/load.html"                                          , controller: "StudentCourseExamLoadCtrl"                       , role: "student"        , title: "" })
        .when("/student/course/exam/question"                                                                          , { templateUrl: "views/student/course/exam/question.html"                                      , controller: "StudentCourseExamQuestionCtrl"                   , role: "student"        , title: "" })
        .when("/student/course/practice"                                                                               , { templateUrl: "views/student/course/practice.html"                                           , controller: "StudentCoursePracticeCtrl"                       , role: "student"        , title: "" })
        .when("/student/help"                                                                                          , { templateUrl: "views/student/help.html"                                                      , controller: "StudentHelpCtrl"                                 , role: "student"        , title: "" })
        .when("/student/help/:pageid"                                                                                  , { templateUrl: "views/student/help.html"                                                      , controller: "StudentHelpCtrl"                                 , role: "student"        , title: "" })
        .when("/student/notifications"                                                                                 , { templateUrl: "views/student/notifications.html"                                             , controller: "StudentNotificationsCtrl"                        , role: "student"        , title: "" })
        .when("/student/parent"                                                                                        , { templateUrl: "views/student/parent.html"                                                    , controller: "StudentParentCtrl"                               , role: "student"        , title: "" })
        .when("/student/practice"                                                                                      , { templateUrl: "views/student/practice.html"                                                  , controller: "StudentPracticeCtrl"                             , role: "student"        , title: "" })
        .when("/student/private_teacher/exams"                                                                         , { templateUrl: "views/student/private_teacher/exams.html"                                     , controller: "StudentPrivateTeacherExamsCtrl"                  , role: "student"        , title: "" })
        .when("/student/private_teacher/practices"                                                                     , { templateUrl: "views/student/private_teacher/practices.html"                                 , controller: "StudentPrivateTeacherPracticesCtrl"              , role: "student"        , title: "" })
        .when("/student/profile"                                                                                       , { templateUrl: "views/student/profile.html"                                                   , controller: "StudentProfileCtrl"                              , role: "student"        , title: "" })
        .when("/student/profile/addparent"                                                                             , { templateUrl: "views/student/profile/addparent.html"                                         , controller: "StudentProfileAddparentCtrl"                     , role: "student"        , title: "" })
        .when("/student/profile/changepassword"                                                                        , { templateUrl: "views/student/profile/changepassword.html"                                    , controller: "StudentProfileChangepasswordCtrl"                , role: "student"        , title: "" })
        .when("/student/profile/edit"                                                                                  , { templateUrl: "views/student/profile/edit.html"                                              , controller: "StudentProfileEditCtrl"                          , role: "student"        , title: "" })
        .when("/student/school_teacher/exams"                                                                          , { templateUrl: "views/student/school_teacher/exams.html"                                      , controller: "StudentSchoolTeacherExamsCtrl"                   , role: "student"        , title: "" })
        .when("/student/school_teacher/practices"                                                                      , { templateUrl: "views/student/school_teacher/practices.html"                                  , controller: "StudentSchoolTeacherPracticesCtrl"               , role: "student"        , title: "" })
        .when("/student/system/exams"                                                                                  , { templateUrl: "views/student/system/exams.html"                                              , controller: "StudentSystemExamsCtrl"                          , role: "student"        , title: "" })
        .when("/student/system/practices"                                                                              , { templateUrl: "views/student/system/practices.html"                                          , controller: "StudentSystemPracticesCtrl"                      , role: "student"        , title: "" })
        .when("/pre"                                                , { templateUrl: "views/pre.html"                                     , controller: "PreCtrl" })
        .when("/profile"                                            , { redirectTo: "/author" })
        .when("/staffmember"                                        , { redirectTo: "/author" })

        .otherwise({
            redirectTo: "/"
        });
});

elproffApp.factory("sock", function (socketFactory) {
    return socketFactory({
        url: "/notes/ws"
    });
});

elproffApp.run(["$rootScope", "User", "$location", "Lang", "$route", "Restangular", "Common", "$cookies", "HTTPCache", "$timeout", "sock",
        function(root, User, $location, Lang, $route, Restangular, Common, $cookies, HTTPCache, $timeout, sock) {
            Lang.setLang("en");
            root.lang = Lang.getTranslation();
            root.ws_opened = false;
            var setSockJS = function(channel, id) {
                sock.setHandler("open", function() {
                    var data = {
                        "channel": channel,
                        "id": String(id)
                    };
                    sock.send(JSON.stringify(data));
                    root.ws_opened = true;
                });
                sock.setHandler("message", function(e) {
                    var data = JSON.parse(e.data);
                    if (data.message === "clear_notification"){
                        root.$broadcast("reload_notifications");
                    }
                });
            };
            root.setSockJS = setSockJS;
            // add a response intereceptor
            var paginable = ["my", "available", "custom_list", "courses", 
            "objective_customlist", "courses/available_subscription", 
            "objective_customlist", "son_subscribable_courses",
            "son_courses","duplications","list_help"];
            Restangular.addFullRequestInterceptor(function(element, operation, what, url, headers, params) {
                var query = params;
                // Do not remove
                if (operation === "getList" || paginable.indexOf(what) > -1) {
                    if (Common.query) {
                        query = _.cloneDeep(Common.query);
                        angular.extend(query, params);
                        if (query.limit === 0) {
                            delete query.page;
                            delete query.offset;
                        }
                        // No need to query the db for more than one item if all
                        // what we want is meta data
                        if (query.meta_only) {
                            query.limit = 1;
                        }
                    }
                } else {
                    headers["X-CSRFToken"] = $cookies.csrftoken;
                }
                return {
                    element: element,
                    params: query,
                    headers: headers
                };
            });
            
            var checkUserType = function checkUserType(){
                var current = $route.current.role;
                var user = User.getUser();
                if (user){
                    if (current !== user.type) {
                        $location.path("/" + user.type);
                    }
                }
            };
            
            function cleanRestangular(element) {

                _.each(element, function(v, k) {
                    if(_.isString(k) && k.match(/\_promise/)){
                        delete element[k];
                    }
                    if (_.isObject(v)) {
                        if (v.resource_uri) {
                            element[k] = v.resource_uri;
                            // For Course Element update
                        } else if (k === "children") {
                            _.each(element.children, cleanRestangular);
                        } else if(_.isArray(v)) {
                            cleanRestangular(v);
                        } else {
                            delete v.parentResource;
                            delete v.reqParams;
                            delete v.fromServer;
                            delete v.route;
                            delete v.restangularCollection;
                        }
                    }
                });
            }
            // add a request intereceptor
            Restangular.addRequestInterceptor(function(element, operation, what, url) {
                cleanRestangular(element);
                return element;
            });

            // add a response intereceptor
            // add custom calls that needed to be paginable here
            Restangular.addResponseInterceptor(function(data, operation, what, url, response, deferred) {
                var extractedData = data;
                if (operation === "getList" && data.meta && !_.isArray(data)) {
                    extractedData = data.objects || [];
                    extractedData.meta = data.meta;
                } else {
                    // If paginable GET operation
                    if (paginable.indexOf(what) > -1) {
                        Common.pagination = data.meta;
                        root.$broadcast("list_response");
                    }
                }
                // Don"t move this inside the above if statement
                // because data might be altered in cache service Interceptors
                if (["profile", "notification"].indexOf(what) < 0 &&
                        extractedData.meta &&
                        [0, 1, 1000].indexOf(extractedData.meta.limit) < 0) {
                    Common.pagination = data.meta;
                    root.$broadcast("list_response");
                }
                return extractedData;
            });

            root.setTitle = function(pageTitle) {
                root.pageTitle = pageTitle;
            };

            function updateRouteTitle() {
                if (_.isString($route.current.title)) {
                    root.setTitle(root.lang[$route.current.title]);
                } else {
                    var str = _.map($route.current.title, function(k) {
                        return root.lang[k];
                    });
                    if (root.LANG_DIRECTION === "rtl" && _.last($route.current.title) === "list") {
                        root.setTitle(str.reverse().join(" "));
                    } else {
                        root.setTitle(str.join(" "));
                    }
                }
            }

            function updateSorters() {
                root.NAME_SORTER = "name_" + Lang.getLang();
                root.TITLE_SORTER = "title_" + Lang.getLang();
                root.DESCRIPTION_SORTER = "description_" + Lang.getLang();
            }

            root.$on("lang_updated", function() {
                root.lang = Lang.getTranslation();
                root.LANG_DIRECTION = Lang.getLang() === "ar" ? "rtl" : "ltr";
                Restangular.setBaseUrl("/" + Lang.getLang() + "/api/v1/");
                updateRouteTitle();
                updateSorters();
            });

            var onUserUpdated = _.throttle(function () {
                //$route.reload();
                root.USER = User.getUser();
                if (root.USER) {
                    $("body").removeClass("body2");
                    Lang.setLang(root.USER.lang);
                } else {
                    $("body").addClass("body2");
                    if ($route.current && $route.current.role) {
                        $location.path("/login");
                    }
                }
            }, 200);
            root.$on("user_updated", onUserUpdated);
            root.hookRouteChange = function() {
                root.$on("$routeChangeStart", function(event, next, current) {
                    onUserUpdated();
                    if (!((next || current).$$route)) {
                        console.log("Error: Incorrect route - ", $route.current);
                        return;
                    }
                    if (!((next || current).$$route.role)) {
                        return;
                    }
                    var currentUser = User.getUser();
                    if (currentUser) {
                        root.USER = currentUser;
                        var role = next && next.$$route.role;
                        if (!role) {
                            return;
                        }
                        if (_.isArray(role) && !_.contains(role, currentUser.type)) {
                            event.preventDefault();
                            return;
                        }
                        if (_.isString(role) && role !== currentUser.type) {
                            event.preventDefault();
                            return;
                        }
                    } else {
                        if (next && next.role) {
                            $location.path("/login");
                        }
                    }
                });
            };

            User.loggedin().then(function(result) {
                User.setUser(result);
                if (!root.ws_opened) {
                    setSockJS(result.channel,root.USER.user_id);
                }
                checkUserType();
                root.hookRouteChange();
            }, function() {
                User.setUser(null);
                root.hookRouteChange();
                if ($route.current.role) {
                    $location.path("/login");
                }
            });

            root.$on("$routeChangeSuccess", function(event, next, current) {
                Common.pagination = null;
                Common.query.q = "";
                Common.query.page = 1;
                Common.query.offset = 0;
                var path = $location.path();
                // If not one of these, save it
                if (["/login"].indexOf(path) < 0) {
                    localStorage.setItem("last_path", $location.path());
                }
                updateRouteTitle();
                root.$broadcast("reload_notifications");

                $timeout(function(){
                    $(".btn").on("click", function(e){
                        var x = e.pageX;
                        var y = e.pageY;
                        var clickY = y - $(this).offset().top;
                        var clickX = x - $(this).offset().left;
                        var button = this;

                        var setX = parseInt(clickX);
                        var setY = parseInt(clickY);
                        $(button).find("svg").remove();
                        $(button).append('<svg class="ripple"><circle cx="' + setX + '" cy="' + setY + '" r="' + 0 + '"></circle></svg>');
                        var c = $(button).find("circle");
                        c.animate({
                            "r" : $(button).outerWidth()
                        }, {
                            easing: "easeOutQuad",
                            duration: 400,
                            step : function(val){
                                c.attr("r", val);
                            },
                            complete: function() {
                                $(button).find("svg").fadeOut(150, function(){ $(this).remove(); });
                            }
                        }
                        );
                    });
                }, 5000);
            });
        }
]);
