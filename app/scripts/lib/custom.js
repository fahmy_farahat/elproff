// Show/hide student fields on signup
$(".signup_form .radio-inline input").change(function(){
	if($(".radio-inline #student").is(":checked")) {
		$(".student-inputs").fadeIn();
	}else{
		$(".student-inputs").fadeOut(100);
	}
});

// jQuery function to get current position of an element while moving
// Needed to calculate draggable on revert
jQuery.fn.onPositionChanged = function (trigger, millis) {
    if (millis == null) millis = 100;
    var o = $(this[0]); // our jquery object
    if (o.length < 1) return o;

    var lastPos = null;
    var lastOff = null;
    setInterval(function () {
        if (o == null || o.length < 1) return o; // abort if element is non existend eny more
        if (lastPos == null) lastPos = o.position();
        if (lastOff == null) lastOff = o.offset();
        var newPos = o.position();
        var newOff = o.offset();
        if (lastPos.top != newPos.top || lastPos.left != newPos.left) {
            $(this).trigger("onPositionChanged", { lastPos: lastPos, newPos: newPos });
            if (typeof (trigger) == "function") trigger(lastPos, newPos);
            lastPos = o.position();
        }
        if (lastOff.top != newOff.top || lastOff.left != newOff.left) {
            $(this).trigger("onOffsetChanged", { lastOff: lastOff, newOff: newOff});
            if (typeof (trigger) == "function") trigger(lastOff, newOff);
            lastOff= o.offset();
        }
    }, millis);

    return o;
};

// Custom droppable
function customDrop(x) {
    $( "#droppable-"+x ).droppable({
        activeClass: "ui-state-default",
        hoverClass: "ui-state-hover",
        drop: function( event, ui ) {
            $( this ).addClass( "ui-state-highlight" );
            $(".droppedIn-"+x).animate({left:0,top:0});
            $(".droppedIn-"+x).removeClass("droppedIn-"+x);
            $("#"+$(ui.draggable).attr('id')).addClass("droppedIn-"+x);
        },
        out: function (event, ui) {
            $(ui.draggable).removeClass("droppedIn-"+x);
        }
    });
}

// Link drop
function linkDrop(x) {
    $( "#droppable-"+x ).droppable({
        tolerance: "fit",
        activeClass: "ui-state-default",
        hoverClass: "ui-state-hover",
        drop: function( event, ui ) {
            $(".lineIn-"+x).remove();
            $(".line").removeClass(".lineIn-"+x);
            $(".lastmod").addClass("lineIn-"+x);
            $(".lastmod").addClass("lineIn");
            $(".line").removeClass(".lastmod");
            $(".droppedIn-"+x).removeClass("droppedIn-"+x);
            $("#"+$(ui.draggable).attr('id')).addClass("droppedIn-"+x);
        },
    })
}

function linkDropMulti(x) {
    $( "#droppable-"+x ).droppable({
        tolerance: "touch",
        activeClass: "ui-state-default",
        hoverClass: "ui-state-hover",
        drop: function( event, ui ) {
            $(".line").removeClass(".lineIn-"+x);
            $(".lastmod").addClass("lineIn-"+x);
            $(".lastmod").addClass("lineIn");
            $(".line").removeClass(".lastmod");
        }
    })
}


function createLine(x1,y1, x2,y2, lineId){
    var length = Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
    var angle  = Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI;
    var transform = "rotate("+angle+"deg)";

    $(".lastmod").removeClass("lastmod");

    var line = $("<div>")
        .appendTo("body")
        .addClass("line")
        .addClass("lastmod")
        .attr("id", lineId)
        .css({
          "position": "absolute",
          "transform": transform
        })
        .width(length)
        .offset({left: x1, top: y1});

    return line;
}

function modifyLine(x1,y1,x2,y2, lineId){
    // var x1 = $(".line").position().left;
    // var x1 = 869.5;
    // var y1 = 386;
    var length = Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
    var angle  = Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI;
    var transform = "rotate("+angle+"deg)";

    // $(".line")
    $("#"+lineId)
        .css({"transform":transform})
        .width(length);

}

//Payment page with paying options clickable
$(".method").click(function() { //when click on flip radio button
    $(".method").removeClass("selected");
    $(this).addClass("selected").find('input:radio').prop("checked", true);
    //.prop("checked", true);
    
});

// Close modal
$(".modal .close").click(function(){
    $(this).parents(".modal").fadeOut();
});