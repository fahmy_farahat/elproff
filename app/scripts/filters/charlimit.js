"use strict";

/**
 * @ngdoc filter
 * @name elproffApp.filter:charlimit
 * @function
 * @description
 * # charlimit
 * Filter in the elproffApp.
 */
angular.module("elproffApp")
  .filter("charlimit", function () {
    return function (input, limit) {
        if (!_.isString(input)) {
            return input;
        }
        if (!limit) {
            limit = 100;
        }
        if (input.length > limit) {
            input = input.substring(0, limit - 3) + "...";
        }
        return input;
    };
  });
