"use strict";

/**
 * @ngdoc filter
 * @name elproffApp.filter:examinstance
 * @function
 * @description
 * # examinstance
 * Filter in the elproffApp.
 */
angular.module("elproffApp").filter("examinstance", ["$rootScope", function(root) {
    return function (instance) {
        var course,
            course_element,
            text;
        if (instance && instance.course) {
            course = instance.course;
            course_element = instance.course_element;
        } else if (instance && instance.exam && instance.exam.course) {
            course = instance.exam.course;
            course_element = instance.exam.course_element;
        } else {
            return "...";
        }

        // Check that course name is translated
        if (_.has(course, "name_" + root.USER.lang)) {
            text = course["name_" + root.USER.lang];
        } else {
            text = course.name;
        }

        text += " - ";

        // Check that elements names is translated
        if (!_.isEmpty(_.pluck(course_element, "name_" + root.USER.lang)[0])) {
            text += _.pluck(course_element, "name_" + root.USER.lang).join(" , ");
        } else {
            text += _.pluck(course_element, "name").join(" , ");
        }
        return text;
    };
}]);
