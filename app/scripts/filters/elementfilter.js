"use strict";

/**
 * @ngdoc filter
 * @name elproffApp.filter:elementFilter
 * @function
 * @description
 * # elementFilter
 * Filter in the elproffApp.
 */
angular.module("elproffApp")
  .filter("elementFilter", [ "$rootScope", function ($root) {
        return function (element) {
            if (_.isString(element) || _.isString(element.element_type)){
                return $root.lang.courseelement;
            } else if (element && element.element_type) {
                var elementTypeName =  $root.lang.i18n(element.element_type, "name");
                var elementName     =  $root.lang.i18n(element, "name");
                return elementTypeName +" "+ elementName;
            } else {
                return $root.lang.courseelementtype;
            }
        };
  }]);
