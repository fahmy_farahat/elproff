"use strict";

/**
 * @ngdoc filter
 * @name elproffApp.filter:durationبFormat
 * @function
 * @description
 * # durationبFormat
 * Filter in the elproffApp.
 */
angular.module("elproffApp")
  .filter("durationFormat", function () {
    return function (value, unit, format) {
        unit = unit ? unit : "seconds";
        format = format ? format : "HH:mm:ss";
        if (value && value > 1 && format && format.indexOf(":") > -1) {
            return moment.utc(moment.duration(value, unit).asMilliseconds()).format(format);
        } else if (value && value < 1 && format && format.indexOf(":") > -1){
            return moment.utc(moment.duration(value*-1, unit).asMilliseconds()).format(format);
        } else {
            return "..:..";
        }
    };
  });
