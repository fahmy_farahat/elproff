"use strict";

/**
 * @ngdoc filter
 * @name elproffApp.filter:ordinal
 * @function
 * @description
 * # ordinal
 * Filter in the elproffApp.
 */
//TODO: Write documentation and test
// Setup the filter
angular.module("elproffApp")
.filter("ordinal", function() {

  // Create the return function
  // set the required parameter name to **number**
  return function(number) {

    // Ensure that the passed in data is a number
    if(isNaN(number) || number < 1) {

      // If the data is not a number or is less than one (thus not having a cardinal value) return it unmodified.
      return number;

    } else {

      // If the data we are applying the filter to is a number, perform the actions to check it"s ordinal suffix and apply it.
      var lastDigit = number % 10;
      // var ordinalNumbers = { 
      //   1: "First",
      //   2: "Second",
      //   3: "Third",
      //   4: "Fourth",
      //   5: "Fifth",
      //   6: "Sixth",
      //   7: "Seventh", 
      //   8: "Eighth",
      //   9: "Ninth",
      //   10: "Tenth",
      //   11: "Eleventh",
      //   12: "Twelfth",
      //   13: "Thirteenth",
      //   14: "Fourteenth",
      //   15: "Fifteenth",
      //   16: "Sixteenth",
      //   17: "Seventeenth",
      //   18: "Eighteenth",
      //   19: "Nineteenth",
      //   20: "Twentieth",
      //   30: "Thirtieth",
      //   40: "Fortieth",
      //   60: "Sixtieth",
      //   70: "Seventieth",
      //   80: "Eightieth",
      //   90: "Ninetieth",
      //   100: "Hundredth", 
      //   1000: "Thousandth",
      //   1000000: "Millionth"
      // }

      // var numbers = {
      //   1: "One",
      //   2: "Two",
      //   3: "Three",
      //   4: "Four",
      //   5: "Five",
      //   6: "Six",
      //   7: "Seven",
      //   8: "Eighth",
      //   9: "Nine",
      //   10: "Ten",
      //   11: "Eleven",
      //   12: "Twelve",
      //   13: "Thirteen",
      //   14: "Fourteen",
      //   15: "Fifteen",
      //   16: "Sixteen",
      //   17: "Seventeen",
      //   18: "Eighteen",
      //   19: "Nineteen",
      //   20: "Twenty",
      //   30: "Thirty",
      //   40: "Forty",
      //   50: "Fifty",
      //   60: "Sixty",
      //   70: "Seventy",
      //   80: "Eighty",
      //   90: "Ninety",
      //   100: "Hundred",
      //   1000: "Thousand",
      //   1000000: "Million",
      // } 

      // if ( number < 20 || lastDigit == 0) {
      //   return ordinalNumbers[lastDigit];
      // } else if ( number < 100 && lastDigit > 0 ) {
      //   return numbers[lastDigit * 10] + "-" + ordinalNumbers[lastDigit];
      // } else if (numbers > 100 && lastDigit > 0) {
      //   // TODO: Write Algorithm
      // }
      return number;
    }
  };
});
