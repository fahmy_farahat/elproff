'use strict';

/**
 * @ngdoc filter
 * @name elproffApp.filter:dateFormatter
 * @function
 * @description
 * # dateFormatter
 * Filter in the elproffApp.
 */
angular.module('elproffApp')
  .filter('dateFormatter', function () {
    return function (cDate) {
      	
      	if (cDate) {
    		var localTime  = moment.utc(cDate.toString()).toDate();
    		localTime = moment(localTime).format('YYYY-MM-DD hh:mm a');

    		return localTime;
        }
    };
  });
