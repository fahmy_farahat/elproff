"use strict";

/**
 * @ngdoc service
 * @name elproffApp.question
 * @description
 * # question
 * Service in the elproffApp.
 */
(function() {

    function Question(Restangular, Cache) {
        var question = Cache.wrap("question", "task");
    	question.addRestangularMethod("duplications", "get", "duplications");
		return question;
    }

    angular.module("elproffApp")
    .service("Question", ["Restangular", "HTTPCache", Question]);
}).call(null);
