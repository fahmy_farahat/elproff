"use strict";

/**
 * @ngdoc service
 * @name elproffApp.Help
 * @description
 * # Help
 * Service in the elproffApp.
 */

(function() {

    function Help(Restangular, Cache) {
    	var service = Cache.wrap("help");
    	service.addRestangularMethod("list_help", "get", "list_help");
        return service;
    }

    angular.module("elproffApp")
    .service("Help", ["Restangular", "HTTPCache", Help]);
}).call(null);
