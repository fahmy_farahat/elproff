"use strict";

/**
 * @ngdoc service
 * @name elproffApp.SubscriptionPlan
 * @description
 * # SubscriptionPlan
 * Service in the elproffApp.
 */
(function() {

    function SubscriptionPlan(Restangular, Cache) {
        var plan = Cache.wrap("subscription_plan");
        plan.addRestangularMethod("listForStudent", "get", "list_for_student");
        return plan;
    }

    angular.module("elproffApp")
    .service("SubscriptionPlan", ["Restangular", "HTTPCache", SubscriptionPlan]);
}).call(null);
