"use strict";

/**
 * @ngdoc service
 * @name elproffApp.CourseElement
 * @description
 * # CourseElement
 * Service in the elproffApp.
 */
(function() {

    function CourseElement(Restangular, Cache) {
        var service = Cache.wrap("course_element");
        service.addRestangularMethod("tree_sort", "post", "tree_sort");
        service.addRestangularMethod("change_type", "post", "change_type");
        service.addRestangularMethod("custom_list", "get", "custom_list");
        service.addRestangularMethod("reactivate_element", "get", "reactivate_element");
        return service;
    }

    angular.module("elproffApp")
    .service("CourseElement", ["Restangular", "HTTPCache", CourseElement]);
}).call(null);
