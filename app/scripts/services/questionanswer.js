"use strict";

/**
 * @ngdoc service
 * @name elproffApp.QuestionAnswer
 * @description
 * # QuestionAnswer
 * Service in the elproffApp.
 */
(function() {

    function QuestionAnswer(Restangular, Cache) {
        this.qa = Cache.wrap("question_answer");
        this.qa.addRestangularMethod("start", "post", "start");
        this.qa.addRestangularMethod("end", "post", "end");
        this.qa.addRestangularMethod("next", "post", "next");
        this.qa.addRestangularMethod("list_skipped", "get", "list_skipped");
        this.qa.addRestangularMethod("list_answers", "get", "list_answers");
        // this.qa.addRestangularMethod("skip", "post", "skip");
        return this.qa;
    }

    angular.module("elproffApp")
    .service("QuestionAnswer", ["Restangular", "HTTPCache", QuestionAnswer]);
}).call(null);
