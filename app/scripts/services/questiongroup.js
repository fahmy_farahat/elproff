"use strict";

/**
 * @ngdoc service
 * @name elproffApp.QuestionGroup
 * @description
 * # QuestionGroup
 * Service in the elproffApp.
 */
(function() {

    function QuestionGroup(Restangular, Cache) {
        var questionGroup = Cache.wrap("question_group");
        questionGroup.addRestangularMethod("reject", "post", "reject");
        return questionGroup;
    }

    angular.module("elproffApp")
    .service("QuestionGroup", ["Restangular", "HTTPCache", QuestionGroup]);
}).call(null);
