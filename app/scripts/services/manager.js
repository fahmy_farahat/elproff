"use strict";

/**
 * @ngdoc service
 * @name elproffApp.Manager
 * @description
 * # Manager
 * Service in the elproffApp.
 */
(function() {

    function Service(Restangular, Cache) {
        return Cache.wrap("manager");
    } 
    angular.module("elproffApp")
    .service("Manager", ["Restangular", "HTTPCache", Service]);
}).call(null);
