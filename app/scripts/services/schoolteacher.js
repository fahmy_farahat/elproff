"use strict";

/**
 * @ngdoc service
 * @name elproffApp.SchoolTeacher
 * @description
 * # SchoolTeacher
 * Service in the elproffApp.
 */
(function() {

    function SchoolTeacher(Restangular, Cache) {
        return Cache.wrap("schoolteacher");
    }
    angular.module("elproffApp")
    .service("SchoolTeacher", ["Restangular", "HTTPCache", SchoolTeacher]);
}).call(null);
