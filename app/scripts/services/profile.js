"use strict";

/**
 * @ngdoc service
 * @name elproffApp.Profile
 * @description
 * # Profile
 * Service in the elproffApp.
 */
(function() {

    function service(Restangular, Cache) {
        var profile = Cache.wrap("profile");
        profile.addRestangularMethod("switchProfile", "post", "switch");
        profile.addRestangularMethod("add", "post", "add");
        profile.addRestangularMethod("remove_profile", "post", "remove");
        profile.addRestangularMethod("myprofiles", "get", "myprofiles");
        profile.addRestangularMethod("available", "get", "available_profiles");
        profile.addRestangularMethod("reactivate", "post", "reactivate");
        return profile;
    }

    angular.module("elproffApp")
    .service("Profile", ["Restangular", "HTTPCache", service]);
}).call(null);
