"use strict";

/**
 * @ngdoc service
 * @name elproffApp.Task
 * @description
 * # Task
 * Service in the elproffApp.
 */
(function() {

    function Task(Restangular, Cache) {
        var task = Cache.wrap("task");
        task.addRestangularMethod("my", "get", "my");
        task.addRestangularMethod("available", "get", "available");
        task.addRestangularMethod("shouldbe_reviewed", "post", "shouldbe_reviewed");
        task.addRestangularMethod("selfAssign", "post", "selfassign");
        task.addRestangularMethod("selfUnassign", "post", "selfunassign");
        task.addRestangularMethod("mytask_questions", "post", "mytask_questions");
        
        task.addRestangularMethod("author_rejected_precentage", "get", "author_rejected_precentage");
        task.addRestangularMethod("reviewer_total_reviewed", "get", "reviewer_total_reviewed");
        return task;
    }

    angular.module("elproffApp")
    .service("Task", ["Restangular", "HTTPCache", Task]);
}).call(null);
