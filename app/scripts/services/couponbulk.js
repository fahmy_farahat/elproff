"use strict";

/**
 * @ngdoc service
 * @name elproffApp.CouponBulk
 * @description
 * # CouponBulk
 * Service in the elproffApp.
 */
(function() {

    function CouponBulk(Restangular, Cache) {
        return Cache.wrap("coupon_bulk");
    }

    angular.module("elproffApp")
    .service("CouponBulk", ["Restangular", "HTTPCache", CouponBulk]);
}).call(null);
