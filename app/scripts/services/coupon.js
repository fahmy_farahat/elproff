"use strict";

/**
 * @ngdoc service
 * @name elproffApp.coupon
 * @description
 * # coupon
 * Service in the elproffApp.
 */
(function() {

    function Coupon(Restangular, Cache) {
        var service = Cache.wrap("coupon");
        service.addRestangularMethod("createBulkCoupons", "post", "bulk_coupons_create");
        service.addRestangularMethod("downloadCoupons", "post", "download_csv");
        service.addRestangularMethod("couponConsume", "post", "coupon_consume");
        return service;
    }

    angular.module("elproffApp")
    .service("Coupon", ["Restangular", "HTTPCache", Coupon]);
}).call(null);
