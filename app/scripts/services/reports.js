"use strict";

/**
 * @ngdoc service
 * @name elproffApp.reports
 * @description
 * # reports
 * Service in the elproffApp.
 */
(function() {

    function reports() {

        /** Pads time seconds and minutes
         */
        this.pad = function (n) {
            if (!n) {
                return "00";
            }
            if (n<10) {
                return "0" + n;
            }
            return n;
        };

        /** Convert period in seconds to string in format hh:mm:ss */
        this.duration = function(time) {
            var fix = _.compose(this.pad, Math.floor);
            var timestr = fix(time/60) + ":" + fix(time%60);
            if (time/60/60 >= 1) {
                timestr = fix(time/60/60) + ":" + timestr;
            }
            return timestr;
        };

        /**
         * @param fractionGrade grade percentage , vaue should be between 0 and 1
         * @return a strig representing the level ( fail, perfect, excellent,
         * vgood, good, pass )
         */
        this.calcLevel = function (fractionGrade) {
            var fg = fractionGrade;
            var level = "grade_f";
            if (fg <=1 && fg >= 0.96) {
                level = "grade_a";
            }
            if (fg <0.96 && fg >= 0.86) {
                level = "grade_b";
            }
            if (fg <0.86 && fg >= 0.71) {
                level = "grade_c";
            }
            if (fg <0.71 && fg >= 0.56) {
                level = "grade_d";
            }
            if (fg <0.56 && fg >= 0.45) {
                level = "grade_e";
            }
            if (fg <0.45 && fg >= 0) {
                level = "grade_f";
            }
            return level;
        };
    }

    angular.module("elproffApp").service("Reports", reports);

}).call(null);
