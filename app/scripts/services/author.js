"use strict";

/**
 * @ngdoc service
 * @name elproffApp.Author
 * @description
 * # Author
 * Service in the elproffApp.
 */
(function() {

    function Author(Restangular, Cache) {
        this.member = Cache.wrap("author");
        this.member.addRestangularMethod("questions", "get", "author_questions");
        this.member.addRestangularMethod("questionsCount", "get", "questions_count");
        this.member.addRestangularMethod("authorsQuestions", "get", "authors_questions");
        this.member.addRestangularMethod("tasksCompletedCount", "get", "tasks_completed_count");
        return this.member;
    }

    angular.module("elproffApp")
    .service("Author", ["Restangular", "HTTPCache", Author]);
}).call(null);
