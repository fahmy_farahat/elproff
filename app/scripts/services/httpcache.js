"use strict";

/**
 * @ngdoc service
 * @name elproffApp.HTTPCache
 * @description
 * # HTTPCache
 * Service in the elproffApp.
 */
(function() {

    function HTTPCache(Restangular, $cacheFactory, root, Common, $http) {

        var reload = _.throttle(function() {
            root.$broadcast("relist");
        }, 5000);

        this.wrap = function(endpoint, relatedEndpoints) {
            var cache = $cacheFactory("http_cache_" + endpoint, {
                maxAge: 15 * 60 * 1000, // Items added to this cache expire after 15 minutes
                cacheFlushInterval: 60 * 60 * 1000, // This cache will clear itself every hour
                deleteOnExpire: "aggressive", // Items will be deleted from this cache when they expire
                // storageMode: "localStorage"
                storageMode: "sessionStorage"
            });
            var rest = Restangular.all(endpoint);

            rest.getUri = function(uri) {
                var id = Common.uriToID(uri);
                return rest.get(id);
            };

            rest.clearCache = function() {
                var cached = $cacheFactory.get("http_cache_" + endpoint);
                if (cached) {
                    cached.removeAll();
                }
            };

            Restangular.addFullRequestInterceptor(function(element, operation, what, url, headers, params) {
                var urlEndpoint = url.split("/")[4];
                var ret = {
                    element: element,
                    params: params,
                    headers: headers
                };
                if (urlEndpoint === endpoint) {
                    ret.httpConfig = {cache: cache};
                }
                return ret;
            });

            Restangular.addResponseInterceptor(function(data, operation, what, url, response, deferred) {
                function clearRelatedCache(endPoint) {
                    var cached = $cacheFactory.get("http_cache_" + endPoint);
                    if (cached) {
                        cached.removeAll();
                    }
                }
                var urlEndpoint = url.split("/")[4];
                if ((urlEndpoint === endpoint && 
                    ["put", "post", "delete", "remove"].indexOf(operation) > -1) || 
                    what === "logout" || what === "switch") {
                    // TODO: Try adding specific IDs caching
                    cache.removeAll();
                    if (relatedEndpoints && _.isArray(relatedEndpoints)) {
                        _.each(relatedEndpoints, clearRelatedCache);
                    }
                    if (relatedEndpoints && _.isString(relatedEndpoints)) {
                        clearRelatedCache(relatedEndpoints);
                    }
                    if(what !== "logout") {
                        reload();    
                    }
                    
                }
                return data;
            });
            return rest;
        };
        return this;
    }

    angular.module("elproffApp")
    .service("HTTPCache", ["Restangular", "$cacheFactory", "$rootScope",
                           "Common", "$http", HTTPCache]);
})();
