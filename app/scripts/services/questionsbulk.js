"use strict";

/**
 * @ngdoc service
 * @name elproffApp.QuestionsBulk
 * @description
 * # QuestionsBulk
 * Service in the elproffApp.
 */
(function() {

    function QuestionsBulk(Restangular, Cache) {
        return Cache.wrap("questions_bulk");
    }

    angular.module("elproffApp")
    .service("QuestionsBulk", ["Restangular", "HTTPCache", QuestionsBulk]);
}).call(null);
