"use strict";

/**
 * @ngdoc service
 * @name elproffApp.Student
 * @description
 * # Student
 * Service in the elproffApp.
 */
(function() {

    function service(Restangular, Cache) {
        // AngularJS will instantiate a singleton by calling "new" on this function
        var student = Cache.wrap("student");
        student.addRestangularMethod("listCourses", "get", "courses");
        student.addRestangularMethod("listSubscribableCourses", "get", "courses/available_subscription");
        return student;
    }

    angular.module("elproffApp")
    .service("Student", ["Restangular", "HTTPCache", service]);
}).call(null);
