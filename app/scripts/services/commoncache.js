"use strict";

/**
 * @ngdoc service
 * @name elproffApp.CommonCache
 * @description
 * # CommonCache
 * Service in the elproffApp.
 */
(function() {

    function CommonCache($q, Course, Grade, System, Country, Element, QuestionsBulk, Objective, Exam, Question, CourseElementType, Classes) {

        function systemRelated(system) {
            if (_.isString(system.country)) {
                system.country = Country.getUri(system.country).$object;
            }
            return system;
        }

        function gradeRelated(grade) {
            if (_.isString(grade.system)) {
                System.getUri(grade.system).then(function(system) {
                    grade.system = system;
                    systemRelated(system);
                });
            } else if (_.isObject(grade.system)) {
                systemRelated(grade.system);
            } else if (String(grade).indexOf("/") >= 0 ) {
                grade = Grade.getUri(grade).$object;
            }
            return grade;
        }

        function courseRelated(course) {
            if (_.isString(course.grade)) {
                Grade.getUri(course.grade).then(function(grade) {
                    course.grade = grade;
                    gradeRelated(grade);
                });
            } else if (_.isObject(course.grade)) {
                gradeRelated(course.grade);
            }
            return course;
        }

        function groupRelated(group) {
            if (_.isString(group.course)) {
                Course.getUri(group.course).then(function(course) {
                    group.course = course;
                    courseRelated(course);
                });
            } else if (_.isObject(group.course)) {
                courseRelated(group.course);
            }
            return group;
        }

        function elementRelated(element) {
            if (_.isString(element.course)) {
                Course.getUri(element.course).then(function(course) {
                    element.course = course;
                    courseRelated(course);
                });
            } else if (_.isObject(element.course)) {
                courseRelated(element.course);
            }
            if (_.isString(element.element_type)) {
                CourseElementType.getUri(element.element_type).then(function(element_type){
                    element.element_type = element_type;
                });
            }
            _.each(element.children, function(child, i) {
                if (_.isString(child)) {
                    Element.getUri(child).then(function(childElement) {
                        element.children[i] = childElement;
                        elementRelated(childElement);
                    });
                } else if (_.isObject(child)) {
                    elementRelated(child);
                }
            });
            return element;
        }

        function questionRelated(question) {
            if (_.isString(question.course_element)) {
                Element.getUri(question.course_element).then(function(element) {
                    question.course_element = element;
                    elementRelated(element);
                });
            } else if (_.isObject(question.course_element)) {
                elementRelated(question.course_element);
            }
            return question;
        }

        function prizeRelated(prize) {
            if (_.isString(prize.course)) {
                Course.getUri(prize.course).then(function(course) {
                    prize.course = course;
                    courseRelated(course);
                });
            } else if (_.isObject(prize.course)) {
                courseRelated(prize.course);
            }
            return prize;
        }

        function questionBulkRelated(bulk) {
            bulk.objective_promise = Objective.getUri(bulk.objective);
            bulk.objective = bulk.objective_promise.$object;
            return bulk;
        }

        function taskRelated(task) {
            if (_.isString(task.course_element)) {
                Element.getUri(task.course_element).then(function(element) {
                    task.course_element = element;
                    elementRelated(element);
                });
            } else if (_.isObject(task.course_element)) {
                elementRelated(task.course_element);
            }
            if (_.isString(task.questions_bulk)) {
                task.questions_bulk_promise = QuestionsBulk.getUri(task.questions_bulk);
                task.questions_bulk_promise.then(function(bulk){
                    task.questions_bulk = bulk;
                    questionBulkRelated(bulk);
                });
            } else if (_.isObject(task.questions_bulk)) {
                questionBulkRelated(task.questions_bulk);
            }
            return task;
        }

        function staffMemberRelated(staffMember) {
            _.map(staffMember.courses, function(course, i) {
                if (_.isString(course)) {
                    Course.getUri(course).then(function(course) {
                        staffMember.courses[i] = course;
                        courseRelated(course);
                    });
                } else if (_.isObject(course)) {
                    courseRelated(course);
                }
            });
            if(staffMember.classes) {
                _.map(staffMember.classes, function(classObj, i) {
                    if(_.isString(classObj)){
                        Classes.getUri(classObj).then(function(fullClassObj){
                            staffMember.classes[i] = fullClassObj;
                        });
                    }
                });
            }
            return staffMember;
        }

        function gradeRelatedSync(gradeUri) {
            var deffered = $q.defer();
            var promise;
            if (String(gradeUri).indexOf("/") >= 0 ) {
                promise = Grade.getUri(gradeUri);
            } else {
                promise = Grade.get(gradeUri);
            }
            promise.then(function(grade) {
                systemRelatedSync(grade.system).then(function(system){
                    grade.system = system;
                    deffered.resolve(grade);
                });
            });
            return deffered.promise;
        }

        function systemRelatedSync(systemUri) {
            var deffered = $q.defer();
            System.getUri(systemUri).then(function(system) {
                countryRelatedSync(system.country).then(function(country){
                    system.country = country;
                    deffered.resolve(system);
                });
            });
            return deffered.promise;
        }
        
        function countryRelatedSync(countryUri) {
            var deffered = $q.defer();
            Country.getUri(countryUri).then(function(country) {
                deffered.resolve(country);
                });
            return deffered.promise;
        }

        function examRelatedSync(exam) {
            var deffered = $q.defer();
            if (exam.type === "exam") {
                var afterAllQuestions = _.after(exam.questions.length, function(){
                    deffered.resolve(exam);
                });
                _.each(exam.questions, function(questionUri, i){
                    Question.getUri(questionUri).then(function(question){
                        exam.questions[i] = question;
                        afterAllQuestions();    
                    });
                });
            } else if (exam.type === "practice") {
                deffered.resolve(exam);
            }
            return deffered.promise;
        }

        function examInstanceRelatedSync(examInstance) {
            var deffered = $q.defer();
            Exam.getUri(examInstance.exam).then(function(exam){
                examRelatedSync(exam).then(function(exam){
                    examInstance.exam = exam;
                    deffered.resolve(examInstance);
                });
            });
            return deffered.promise;
        }

        function examInstanceRelated(examInstance) {
            if (!_.isString(examInstance.exam)) {
                return;
            }
            Exam.getUri(examInstance.exam).then(function(exam){
                examInstance.exam = exam;
            });
            return examInstance;
        }

        function reviewerRelated(reviewer) {
            _.each(reviewer.courses, function(course, i) {
                if(_.isString(course)){
                    Course.getUri(course).then(function(courseObj){
                        reviewer.courses[i] = courseObj;
                    });
                }
            });
            return reviewer;
        }

        this.system = systemRelated;
        this.element = elementRelated;
        this.course = courseRelated;
        this.grade = gradeRelated;
        this.question = questionRelated;
        this.prize = prizeRelated;
        this.task = taskRelated;
        this.questionBulk = questionBulkRelated;
        this.staffMember = staffMemberRelated;
        this.gradeSync = gradeRelatedSync;
        this.systemSync = systemRelatedSync;
        this.countrySync = countryRelatedSync;
        this.examSync = examRelatedSync;
        this.examInstanceSync = examInstanceRelatedSync;
        this.group = groupRelated;
        this.examInstance = examInstanceRelated;
        this.reviewer = reviewerRelated;
    }

    angular.module("elproffApp")
    .service("CommonCache", ["$q", "Course", "Grade", "EducationalSystem",
        "Country", "CourseElement", "QuestionsBulk", "Objective", "Exam",
        "Question", "CourseElementType", "Classes", CommonCache] );
}).call(null);
