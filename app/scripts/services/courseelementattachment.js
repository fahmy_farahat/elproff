"use strict";

/**
 * @ngdoc service
 * @name elproffApp.CourseElementAttachment
 * @description
 * # CourseElementAttachment
 * Service in the elproffApp.
 */
(function() {

    function service(Restangular, Cache) {
        return Cache.wrap("course_element_attachment");
    }

    angular.module("elproffApp")
    .service("CourseElementAttachment", ["Restangular", "HTTPCache", service]);
}).call(null);
