"use strict";

/**
 * @ngdoc service
 * @name elproffApp.Prize
 * @description
 * # Prize
 * Service in the elproffApp.
 */
(function() {

    function Prize(Restangular, Cache) {
        return Cache.wrap("prize");
    }

    angular.module("elproffApp")
    .service("Prize", ["Restangular", "HTTPCache", Prize]);
}).call(null);
