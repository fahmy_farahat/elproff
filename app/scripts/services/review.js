"use strict";

/**
 * @ngdoc service
 * @name elproffApp.Review
 * @description
 * # Review
 * Service in the elproffApp.
 */
(function() {

    function Review(Restangular, Cache) {
        this.member = Cache.wrap("review", ["question"]);
        this.member.addRestangularMethod("question_accept", "post", "question_accept");
        this.member.addRestangularMethod("question_change", "post", "question_change");
        return this.member;
    }

    angular.module("elproffApp")
    .service("Review", ["Restangular", "HTTPCache", Review]);
}).call(null);
