"use strict";

/**
 * @ngdoc service
 * @name elproffApp.invitation
 * @description
 * # invitation
 * Service in the elproffApp.
 */
(function() {

    function Service(Restangular, Cache) {
        this.invitation = Cache.wrap("invitation");
        this.invitation.addRestangularMethod("schooladminManager", "post", "schooladmin/manager");
        this.invitation.addRestangularMethod("schooladminSchoolteacher", "post", "schooladmin/schoolteacher");
        this.invitation.addRestangularMethod("schooladminStudent", "post", "schooladmin/student");
        this.invitation.addRestangularMethod("parentStudent", "post", "parent/student");
        this.invitation.addRestangularMethod("studentParent", "post", "student/parent");
        this.invitation.addRestangularMethod("adminSME", "post", "admin/sme");
        this.invitation.addRestangularMethod("adminAuthor", "post", "admin/author");
        this.invitation.addRestangularMethod("adminReviewer", "post", "admin/reviewer");
        this.invitation.addRestangularMethod("smeAuthor", "post", "sme/author");
        this.invitation.addRestangularMethod("smeReviewer", "post", "sme/reviewer");
        this.invitation.addRestangularMethod("accept", "post", "accept");
        this.invitation.addRestangularMethod("load", "post", "load");
        this.invitation.addRestangularMethod("resend", "post", "resend");
        this.invitation.addRestangularMethod("privateteacherStudent", "post", "privateteacher/student");
        return this.invitation;
    }

    angular.module("elproffApp")
    .service("Invitation", ["Restangular", "HTTPCache", Service]);
}).call(null);
