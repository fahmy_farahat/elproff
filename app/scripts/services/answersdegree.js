"use strict";

/**
 * @ngdoc service
 * @name elproffApp.AnswersDegree
 * @description Calculating whole answers degree 
 * # AnswersDegree
 * Service in the elproffApp.
 */
(function() {

    function service() {
        this.calculate = function(answers) {
            if (!answers || !_.isArray(answers) || _.isEmpty(answers)) {
                // console.warn("AnswersDegree: Invalid answers");
                return;
            } else {
                var answersDegrees = _.pluck(_.filter(answers), "degree");
                var resultByPercentage = ( _.sum(answersDegrees) / answers.length ) * 100;
                return resultByPercentage;
            }
        };
    }

    angular.module("elproffApp")
    .service("AnswersDegree", [service]);
}).call(null);
