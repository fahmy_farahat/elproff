"use strict";

/**
 * @ngdoc service
 * @name elproffApp.School
 * @description
 * # School
 * Service in the elproffApp.
 */
(function() {

    function School(Restangular, Cache) {
        var school = Cache.wrap("school");
        school.addRestangularMethod("updateSchool", "post", "update");
        school.addRestangularMethod("deleteGrade", "post", "delete_grade");
        school.addRestangularMethod("courses", "get", "courses");
        return school;
    }

    angular.module("elproffApp")
    .service("School", ["Restangular", "HTTPCache", School]);
}).call(null);
