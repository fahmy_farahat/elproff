"use strict";

/**
 * @ngdoc service
 * @name elproffApp.Reviewer
 * @description
 * # Reviewer
 * Service in the elproffApp.
 */
(function() {

    function Reviewer(Restangular, Cache) {
        this.member = Cache.wrap("reviewer");
        this.member.addRestangularMethod("questionsCount", "get", "questions_count");
        this.member.addRestangularMethod("tasksCompletedCount", "get", "tasks_completed_count");
        return this.member;
    }

    angular.module("elproffApp")
    .service("Reviewer", ["Restangular", "HTTPCache", Reviewer]);
}).call(null);
