"use strict";

/**
 * @ngdoc service
 * @name elproffApp.SubscriptionTerm
 * @description
 * # SubscriptionTerm
 * Service in the elproffApp.
 */
(function() {

    function SubscriptionTerm(Restangular, Cache) {
        return Cache.wrap("subscription_term");
    }

    angular.module("elproffApp")
    .service("SubscriptionTerm", ["Restangular", "HTTPCache", SubscriptionTerm]);
}).call(null);
