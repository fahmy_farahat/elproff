"use strict";

/**
 * @ngdoc service
 * @name elproffApp.QuestionAttachment
 * @description
 * # QuestionAttachment
 * Service in the elproffApp.
 */
(function() {
    function QuestionAttachment(Restangular, Cache) {
        return Cache.wrap("question_attachment");
    }
    angular.module("elproffApp")
    .service("QuestionAttachment", ["Restangular", "HTTPCache", QuestionAttachment]);
}).call(null);
