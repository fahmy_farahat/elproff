"use strict";

/**
 * @ngdoc service
 * @name elproffApp.ValidateAnswer
 * @description
 * # ValidateAnswer
 * Service in the elproffApp.
 */
(function() {

    function service() {
        this.answer = function(answer, question_type) {
            if (answer && _.isObject(answer) && !_.isEmpty(answer) || // If answer type is object
                answer && _.isArray(answer) && answer.length || // If answer type is array
                answer && !_.isEmpty(answer) || // If answer type is string or anything else
                _.isNumber(answer)) { // If answer type is number (in mcq case)
                return true;
            } else {
                return false;
            }
        };
    }

    angular.module("elproffApp")
    .service("Validate", [service]);
}).call(null);
