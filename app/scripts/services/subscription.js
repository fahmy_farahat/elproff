"use strict";

/**
 * @ngdoc service
 * @name elproffApp.subscription
 * @description
 * # subscription
 * Service in the elproffApp.
 */
(function() {

    function Subscription(Restangular, Cache) {
    	var service = Cache.wrap("subscription");
    	service.addRestangularMethod("listSubscription", "get", "list_subscriptions");
    	service.addRestangularMethod("listSubscriptionStudent", "get", "list_subscriptions_student");
        return service;
    }

    angular.module("elproffApp")
    .service("Subscription", ["Restangular", "HTTPCache", Subscription]);
}).call(null);
