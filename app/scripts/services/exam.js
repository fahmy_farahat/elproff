"use strict";

/**
 * @ngdoc service
 * @name elproffApp.exam
 * @description
 * # exam
 * Service in the elproffApp.
 */
(function() {

    function Exam(Restangular, Cache) {
        this.exam = Cache.wrap("exam");
        this.exam.addRestangularMethod("studentGenerate", "post", "generate/student"); 
        this.exam.addRestangularMethod("studentGeneratePractice", "post", "generate_practice_student");
        this.exam.addRestangularMethod("groupExamList", "post", "group_exam_list");
        this.exam.addRestangularMethod("classExamList", "post", "class_exam_list");

        //this.exam.addRestangularMethod("generate", "post", "generate");
        return this.exam;
    }

    angular.module("elproffApp")
    .service("Exam", ["Restangular", "HTTPCache", Exam]);
}).call(null);
