"use strict";

/**
 * @ngdoc service
 * @name elproffApp.QuestionFeedback
 * @description
 * # QuestionFeedback
 * Service in the elproffApp.
 */
(function() {

    function QuestionFeedback(Restangular, Cache) {
        var service = Cache.wrap("question_feedback");
        return service;
    }

    angular.module("elproffApp")
    .service("QuestionFeedback", ["Restangular", "HTTPCache", QuestionFeedback]);
}).call(null);
