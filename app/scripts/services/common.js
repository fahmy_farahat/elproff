"use strict";

/**
 * @ngdoc service
 * @name elproffApp.Common
 * @description
 * # Common
 * Service in the elproffApp.
 */
(function() {

    function Common(root, Lang, prompt, Restangular, $q, $modal) {
        this.questionImgWidth = 400;
        this.questionImgHeight = 100;
        var alerts = {
            error: {},
            success: {},
            info: {}
        };
        function alertsBuild() {
            var lang = Lang.getTranslation();
              _.each(lang, function(v, k) {
                  _.each(["error", "success", "info"], function(type) {
                    if (k && k.indexOf(type + "_") === 0) {
                        var cls = type === "error"? "danger": type;
                        alerts[type][k.substring(k.indexOf("_") + 1)] = [{ msg: lang[k], type: cls}];
                    }
                });
            });
        }
        alertsBuild();
        root.$on("lang_updated", alertsBuild);
        this.alerts = alerts;
        this.Lang = Lang;
        this.languages = ["ar", "en"];
        this.copyTranslation = function copyTranslation(model, fields) {
            var that = this;
            if (!fields) {
                fields = ["name", "title", "description", "message"];
            }
            _.each(fields, function(field) {
                // NOTE: Do not use _.isUndefined
                if (!(field in model)) {
                    return;
                }
                var current = field + "_" + that.Lang.getLang();
                if (!model[current]) {
                    model[current] = model[field];
                }
                _.each(that.languages, function(lang) {
                    if (lang === that.Lang.getLang()) {
                        return;
                    }
                    // Language field (eg. name_ar )
                    var lfield = field + "_" + lang;
                    if (!model[lfield] || (model[lfield] && model[lfield].trim() === "")) {
                        // Setting language field
                        model[lfield] = model[current];
                    }
                });
            });
            return model;
        };

        this.onError = function onError(scope, handler) {
            var that = this;
            return function errorHandler(err) {
                scope.isLoading = false;
                var e = err.error || err.data.error;
                console.log(e);
                //var errorMsg = this.alerts.error["api_" + e];
                var errorMsg = that.alerts.error[e];
                if (errorMsg) {
                    scope.alerts = errorMsg;
                }
                if (handler) {
                    handler(err);
                }
                console.log(err);
                //TODO: save errors in logstash
            };
        };

        this.onSuccess = function onSuccess(result) {
            console.log(result);
        };

        this.uriToID  = function uriToID(uri){
            var id = uri.substring(uri.lastIndexOf("/")+1);
            return parseInt(id);
        };
        // The query sent to API ( for pagination offset and filtering q)
        this.query = {};
        // Holds the pagination meta data
        // to be used in PaginatorCtrl
        this.pagination = {};

        /**
        /* @params title: if not set the dialog title will be Remove.
        /* otherwise the dialoge will be set to title.
        /* @params msg: if not set the dialog message 
        /* will be This action cannot be undone, Do you want to continue?
        /* other wise it will be set as msg variable.
        /* @params buttons: if not set the buttons will be cancel and ok.
        /* otherwise it should be an array of dictionary. each element should have
        /* label, and wether it was cancel or primary.
        /* this function returns a promise should be use 
        /* like .then(function success(){}, function error() {} )
        */
        this.promptDialog = function promptDialog(title, msg, buttons){
            if(!title || title === ""){
                title = root.lang.remove;
            }
            if (!msg || msg === ""){
                msg = root.lang.removecoursemessage;
            }
            if(typeof buttons === "undefined" || typeof buttons.length === "undefined" || buttons.length <= 0){
                buttons = [
                    {
                        "label": root.lang.cancel,
                        "cancel": true
                    },
                    {
                        "label": root.lang.ok,
                        "primary": true
                    }
                ];
            }
            var data = {
                "title": title,
                "message": msg,
                "buttons": buttons,
            };
            return prompt(data);

        };
        /**
        /* @params header: should be set as array for titles
        /*  header ex. [name, email]
        /* @params data: should be set as array of array as each element
        /*  represents a row in csv file.
        /* @filename: the filename to be downloaded ex. "report"
        /*  default of filename "data report"
        /* @return: downloads the file. 
        */

        this.exportCSV = function(header, data, filename){
            var csvContent = "data:text/csv;charset=utf-8,";
            if(typeof header !== "undefined" && typeof header.length !== "undefined" && header.length > 0){
                csvContent += header.join(",") + "\n";
            }
            if(typeof data !== "undefined" && typeof data.length !== "undefined" && data.length > 0){
                data.forEach(function(object){
                    csvContent += object.join(",") + "\n";
                });
            }
            if (!filename || filename === ""){
                filename = "data report";
            }
            var encodedUri = encodeURI(csvContent);
            // this will not be shown on UI
            var link = document.createElement("a");
            link.setAttribute("href", encodedUri);
            link.setAttribute("download", filename + ".csv");
            link.click();
        };
        /***
        * this function is to load Image and make a promise after loading
        * @params: imageFile: is the clone copy of file-model attribute.
        * Note: using imageFile not clonned can raise error "Illegal Invocation"
        ***/
        this.imageLoadPromise = function(imageFile){
            var deferred = $q.defer();
            var loadImage = function(deferred) {
                var image = new Image();
                function loaded() {
                  unbindEvents();
                  // Calling resolve means the image loaded sucessfully and is ready to use.
                  deferred.resolve(image);
                }
                function errored() {
                  unbindEvents();
                  // Calling reject means we failed to load the image (e.g. 404, server offline, etc).
                  deferred.reject(image);
                }
                function unbindEvents() {
                  // Ensures the event callbacks only get called once.
                  image.onload = null;
                  image.onerror = null;
                  image.onabort = null;
                }
                // Set up event handlers to know when the image has loaded
                // or fails to load due to an error or abort.
                image.onload = loaded;
                image.onerror = errored; // URL returns 404, etc
                image.onabort = errored; // IE may call this if user clicks "Stop"
                 
                // Setting the src property begins loading the image.
                var reader = new FileReader();
                reader.readAsDataURL(imageFile);
                reader.onload = function(file) {
                   image.src = file.target.result;
                };
                 
                return deferred.promise;
            };

            return loadImage(deferred);
        };
        /***
        * This function for custom restangular call with
        * with HttpConfig. Usually this function is used for
        * uploading files.
        * @ serviceName: for api call. eg. question_attachments.
        * @ data: is for data that will be saved and uploaded.
        *
        *
        ***/
        this.customRestangular = function (serviceName, data, path) {
            if(path){
                return Restangular.all(serviceName)
                    .withHttpConfig({transformRequest: angular.identity})
                    .customPOST(data, path, {}, {"Content-Type": undefined});    
            }
            return Restangular.all(serviceName)
                    .withHttpConfig({transformRequest: angular.identity})
                    .post(data, {}, {"Content-Type": undefined});
        };


    }

    angular.module("elproffApp")
        .service("Common", [
                "$rootScope", "Lang", "prompt", "Restangular", "$q", "$modal", Common]);
}).call(null);
