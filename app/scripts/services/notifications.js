"use strict";

/**
 * @ngdoc service
 * @name elproffApp.notifications
 * @description
 * # notifications
 * Service in the elproffApp.
 */
(function() {

    function service(Restangular, Cache) {
        // AngularJS will instantiate a singleton by calling "new" on this function
        // var notifications = Cache.wrap("notification");
        var notifications = Restangular.all("notification");
        notifications.addRestangularMethod("accept", "post", "accept");
        notifications.addRestangularMethod("reject", "post", "reject");
        return notifications;
    }

    angular.module("elproffApp")
    .service("Notifications", ["Restangular", "HTTPCache", service]);
}).call(null);
