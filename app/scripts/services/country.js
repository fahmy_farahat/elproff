"use strict";

/**
 * @ngdoc service
 * @name elproffApp.country
 * @description
 * # country
 * Service in the elproffApp.
 */
(function() {

    function Country(Restangular, Cache) {
    	var service = Cache.wrap("country");
    	service.addRestangularMethod("disable", "post", "disable");
    	service.addRestangularMethod("enable", "post", "enable");
        return service;
    }

    angular.module("elproffApp")
    .service("Country", ["Restangular", "HTTPCache", Country]);
}).call(null);
