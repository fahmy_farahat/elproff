"use strict";

/**
 * @ngdoc service
 * @name elproffApp.user
 * @description
 * # user
 * Service in the elproffApp.
 */
(function() {

    function service(Restangular, $location, root, Cache) {

        this.users = Cache.wrap("user", ["student"]);
        this.users.addRestangularMethod("logout", "post", "logout");
        this.users.addRestangularMethod("login", "post", "login");
        this.users.addRestangularMethod("loggedin", "post", "loggedin");
        this.users.addRestangularMethod("checkEmail", "post", "email_check");
        this.users.addRestangularMethod("updatePassword", "post", "update_password");
        this.users.addRestangularMethod("confirmEmail", "post", "confirm_email");
        this.users.addRestangularMethod("lang", "post", "lang");

        this.users.addRestangularMethod("forgotPassword", "post", "forget_password");
        this.users.addRestangularMethod("resetPassword", "post", "new_password");
        this.users.addRestangularMethod("update_profile", "post", "update_profile");
        //this.users.addRestangularMethod("activate", "post", "activate");
        // I"m not sure where is that used
        this.create = function create(data) {
            return this.users.post(data);
        };

        this.setLang = function setLang(key) {
            return this.users.lang({"lang": key});
        };

        this.activate = function activate(userid) {
            return this.users.activate({"userid": userid});
        };

        this.logout = function logout(data) {
            return this.users.logout(data);
        };

        this.login = function login(data) {
            return this.users.login(data);
        };

        this.loggedin = function loggedin() {
            return this.users.loggedin();
        };

        this.forgotPassword = function forgotPassword(data) {
            return this.users.forgotPassword(data);
        };

        this.resetPassword = function resetPassword(data) {
            return this.users.resetPassword(data);
        };

        this.setUser = function setUser(user) {
            this.user = user;
            root.USER = user;
            root.$broadcast("user_updated");
        };

        this.getUser = function getUser(user) {
            return this.user;
        };

        this.checkEmail = function checkEmail(email) {
            return this.users.checkEmail(email);
        };

        this.activate = function activate(data) {
            return this.users.activate(data);
        };

        this.goToDashboard = function goToProfile() {
            $location.path("/" + this.getUser().user_type);
        };

        this.goToProfile = function goToProfile(email) {
            $location.path("/" + this.getUser().user_type + "/profile");
        };

        this.updatePassword = function updatePassword(data) {
            return this.users.updatePassword(data);
        };

        this.confirmEmail = function updatePassword(data) {
            return this.users.confirmEmail(data);
        };

        this.updateProfile = function updateProfile(data) {
            return this.users.withHttpConfig({transformRequest: angular.identity})
                   .customPOST(data, "update_profile", {}, {"Content-Type": undefined});
        };
    }

    angular.module("elproffApp")
    .service("User", ["Restangular", "$location", "$rootScope", "HTTPCache", service]);
}).call(null);
