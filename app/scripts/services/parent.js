"use strict";

/**
 * @ngdoc service
 * @name elproffApp.Parent
 * @description
 * # Parent
 * Service in the elproffApp.
 */
(function() {

    function Parent(Restangular, Cache) {
        var service = Cache.wrap("parent");
        service.addRestangularMethod("listSons", "get", "sons");
        service.addRestangularMethod("listSonCourses", "get", "son_courses");
        service.addRestangularMethod("listSonSubscribableCourses", "get", "son_subscribable_courses");
        service.addRestangularMethod("subscribe", "post", "subscribe");
        service.addRestangularMethod("invite", "post", "invite");
        service.addRestangularMethod("courseReport", "post", "course_report");
        service.addRestangularMethod("listExamsReport", "post", "list_exams_report");
        return service;
    }

    angular.module("elproffApp")
    .service("Parent", ["Restangular", "HTTPCache", Parent]);
}).call(null);
