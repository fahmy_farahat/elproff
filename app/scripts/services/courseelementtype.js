"use strict";

/**
 * @ngdoc service
 * @name elproffApp.CourseElementType
 * @description
 * # CourseElementType
 * Service in the elproffApp.
 */
(function() {

    function CourseElementType(Restangular, Cache) {
        var service = Cache.wrap("course_element_type");
        service.addRestangularMethod("delete_type", "post", "delete_type");
        return service;
    }

    angular.module("elproffApp")
    .service("CourseElementType", ["Restangular", "HTTPCache", CourseElementType]);
}).call(null);
