"use strict";

/**
 * @ngdoc service
 * @name elproffApp.EducationalSystem
 * @description
 * # EducationalSystem
 * Service in the elproffApp.
 */
(function() {

    function EducationalSystem(Restangular, Cache) {
    	var service = Cache.wrap("educational_system");
        return service;
    }

    angular.module("elproffApp")
    .service("EducationalSystem",
        ["Restangular", "HTTPCache", EducationalSystem]);
}).call(null);
