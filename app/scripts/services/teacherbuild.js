"use strict";

/**
 * @ngdoc service
 * @name elproffApp.TeacherBuild
 * @description
 * # TeacherBuild
 * Service in the elproffApp.
 */
(function() {

    function TeacherBuild(Restangular, Cache) {
        var teacherBuild = Cache.wrap("teacher_build");
        teacherBuild.addRestangularMethod("generateBuild", "post", "generate_build");
        teacherBuild.addRestangularMethod("verifyExam", "post", "verify_exam");
        return teacherBuild;
    }

    angular.module("elproffApp")
    .service("TeacherBuild", ["Restangular", "HTTPCache", TeacherBuild]);
}).call(null);
