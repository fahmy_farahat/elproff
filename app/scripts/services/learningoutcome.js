"use strict";

/**
 * @ngdoc service
 * @name elproffApp.LearningOutcome
 * @description
 * # LearningOutcome
 * Service in the elproffApp.
 */
(function() {

    function service(Restangular, $root) {

        this.getEntities = function() {
            return [
                { name: "analyze",    lang: $root.lang.outcome_analyze },
                { name: "apply",      lang: $root.lang.outcome_apply },
                { name: "correct",    lang: $root.lang.outcome_correct },
                { name: "innovate",   lang: $root.lang.outcome_innovate },
                { name: "memorize",   lang: $root.lang.outcome_memorize },
                { name: "understand", lang: $root.lang.outcome_understand },
            ];
        };

    }

    angular.module("elproffApp")
    .service("LearningOutcome", ["Restangular", "$rootScope", service]);
}).call(null);
