"use strict";

/**
 * @ngdoc service
 * @name elproffApp.CustomEditor
 * @description
 * # CustomEditor
 * Service in the elproffApp.
 */
(function() {

    function CustomEditor(root, $modal, Common, $timeout) {

        /*
        * This function returns colors button for custom editor
        * directive. 
        * @returns array of configuration that is used in editor
        * configuration.
        */
        this.colorsBtns = function colorsBtnsConfiguration() {
            var buttons = {
                name: "groupColors",
                data: [{
                    name: "cmdRed",
                    toggle: false, // this param only take effect if you load bootstrap.js
                    title: root.lang.red,
                    icon: "fa fa-tint red",
                    callback: function(e){
                    // Replace selection with some drinks
                    var chunk, cursor,
                        selected = e.getSelection(), content = e.getContent();

                    if (!selected) {
                        return;
                    }
                    chunk = '<span style="color:red">' + selected.text + '</span>';

                    // transform selection and set the cursor into chunked text
                    e.replaceSelection(chunk);
                    cursor = selected.start;

                    // Set the cursor
                    e.setSelection(cursor,cursor+chunk.length);
                    }
                }, {
                    name: "cmdBlue",
                    toggle: false, // this param only take effect if you load bootstrap.js
                    title: root.lang.blue,
                    icon: "fa fa-tint blue",
                    callback: function(e){
                    // Replace selection with some drinks
                    var chunk, cursor,
                        selected = e.getSelection(), content = e.getContent();

                    if (!selected) {
                        return;
                    }
                    chunk = '<span style="color:blue">' + selected.text + '</span>';

                    // transform selection and set the cursor into chunked text
                    e.replaceSelection(chunk);
                    cursor = selected.start;

                    // Set the cursor
                    e.setSelection(cursor,cursor+chunk.length);
                    }
                }, {
                    name: "cmdGreen",
                    toggle: false, // this param only take effect if you load bootstrap.js
                    title: root.lang.green,
                    icon: "fa fa-tint green",
                    callback: function(e){
                    // Replace selection with some drinks
                    var chunk, cursor,
                        selected = e.getSelection(), content = e.getContent();

                    if (!selected) {
                        return;
                    }
                    chunk = '<span style="color:green">' + selected.text + '</span>';

                    // transform selection and set the cursor into chunked text
                    e.replaceSelection(chunk);
                    cursor = selected.start;

                    // Set the cursor
                    e.setSelection(cursor,cursor+chunk.length);
                    }
                }]
            };
            return buttons;            
        };

        /***
        * This function returns image attachment button for custom editor
        * directive. 
        * @ returns array of configuration that is used in editor
        * configuration.
        * @input: attachmentRelation ==> {"help", "course"} to identify editor behaviour
        * when uploading the attachment.
        * element: course element in case of attachmentRelation = "course"
        ***/
        this.imageAttachmentBtns = function imageAttachmentBtnsConfiguration(attachmentRelation, model) {
            var imageBtn = {
                name: "groupResources",
                data: [{
                    name: "cmdResource",
                    toggle: false, // this param only take effect if you load bootstrap.js
                    title: root.lang.attachmentInsert,
                    icon: "fa fa-paperclip fa-lg",
                    callback: function(e){

                        var modalInstance = $modal.open({
                            templateUrl: "views/modals/imageattachment.html",
                            controller: "ModalsImageattachmentCtrl",
                            size: "lg",
                            resolve: {
                                    attachment_relation: function() {
                                        return attachmentRelation;
                                    },
                                    crsElement: function(){
                                        if(_.isUndefined(model) || _.isUndefined(model.course_element)) {
                                            return null;
                                        }
                                        return model.course_element;
                                    }
                                }
                        });

                        modalInstance.result.then(function (selectedItem) {
                            var chunk, cursor,
                            selected = e.getSelection(), content = e.getContent();

                            if (!selected) {
                                return;
                            }

                            chunk = '<img src="' + selectedItem + '"/> ';
                            e.replaceSelection(chunk);
                            cursor = selected.start;
                            // Set the cursor
                            e.setSelection(cursor,cursor+chunk.length);
                            // timout to solve $digest in process error
                            // why not $timout(e.change)? it didn't work !!
                            $timeout(function(){
                                e.change();
                            });
                            
                            //e.$editor.enableSelection();

                        }, Common.onError);

                    }
                }]
            };
            return imageBtn;
        };

        /***
        * This function returns equation attachment button for custom editor
        * directive. 
        * @ returns array of configuration that is used in editor
        * configuration.
        * @input: attachmentRelation ==> {"help", "course"} to identify editor behaviour
        * when uploading the attachment
        * element: course element in case of attachmentRelation = "course"
        ***/

        this.equationAttachmentBtn = function equationAttachmentBtnConfiguration(attachmentRelation, model) {
            var equationBtn =     {
                name: "groupEquation",
                data: [{
                    name: "cmdEquation",
                    toggle: false, // this param only take effect if you load bootstrap.js
                    title: root.lang.equationInsert,
                    icon: "fa fa-superscript fa-lg",
                    callback: function(e){

                        var modalInstance = $modal.open({
                            templateUrl: 'views/modals/equationattachment.html',
                            controller: 'ModalsequationattachmentCtrl',
                            size: 'lg',
                            resolve: {
                                    attachment_relation: function() {
                                        return attachmentRelation;
                                    },
                                    crsElement: function(){
                                        if(_.isUndefined(model) || _.isUndefined(model.course_element)) {
                                            return null;
                                        }
                                        return model.course_element;
                                    }
                                }
                        });

                        modalInstance.result.then(function (selectedItem) {
                            var chunk, cursor,
                            selected = e.getSelection(), content = e.getContent();

                            if (!selected) {
                                return;
                            }

                            chunk = '<img src="'+selectedItem+'"/>';
                            e.replaceSelection(chunk);
                            cursor = selected.start;

                            // Set the cursor
                            e.setSelection(cursor,cursor+chunk.length);
                            // timout to solve $digest in process error
                            // why not $timout(e.change)? it didn't work !!
                            $timeout(function(){
                                e.change();
                            });

                        }, Common.onError);

                    }
                }]  
            };
            return equationBtn;
        };

        /***
        * This function returns course element attachment button for custom editor
        * directive. 
        * @ returns array of configuration that is used in editor
        * configuration.
        * @input: course element object
        ***/
        this.courseElementAttachment = function elementAttachmentBtnConfiguration(model) {
            var crsAttachmentBtn = {
                name: "groupElementResources",
                data: [{
                    name: "cmdElementResources",
                    toggle: false, // this param only take effect if you load bootstrap.js
                    title: root.lang.green,
                    icon: "fa fa-paperclip fa-lg",
                    callback: function(e){

                        var modalInstance = $modal.open({
                            templateUrl: "views/modals/addcourseelementresource.html",
                            controller: "ModalsAddCourseElementResourceCtrl",
                            size: "lg",
                            resolve: {
                                element: function() {
                                    return model.course_element;

                                }
                            }
                        });

                        modalInstance.result.then(function (selectedItem) {
                            model.attachment = selectedItem;
                        }, Common.onError);

                    }
                }]
            };
            return crsAttachmentBtn;            
        };
    }

    angular.module('elproffApp')
        .service('CustomEditor', ["$rootScope", "$modal", "Common", "$timeout", CustomEditor]);
}).call(null);
