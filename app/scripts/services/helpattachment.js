"use strict";

/**
 * @ngdoc service
 * @name elproffApp.HelpAttachment
 * @description
 * # HelpAttachment
 * Service in the elproffApp.
 */

(function() {

    function HelpAttachment(Restangular, Cache) {
    	var service = Cache.wrap("help_attachment");
        return service;
    }

    angular.module("elproffApp")
    .service("HelpAttachment", ["Restangular", "HTTPCache", HelpAttachment]);
}).call(null);