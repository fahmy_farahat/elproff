"use strict";

/**
 * @ngdoc service
 * @name elproffApp.Crumbs
 * @description
 * # Crumbs
 * Service in the elproffApp.
 */
(function() {

    function Crumbs(Course, CourseElement, Student, Group, Country, EducationalSystem, Grade, Prize, CourseElementType, Author, Classes) {

        /**
         * @param Entity is the Restangular Service object
         * @param entityName is the name to set on the scope & scope.model 
         * @param langTitle If present, as string will be used as the name of 
         * the attribute to get from the received entity, and set to the crumb
         * If present as function will be called, and passed the received 
         * entity and it should return a string to be used to set the curmbs
         * title
         */
        function commonSet(Entity, entityName, langTitle) {
            /**
             * @param crumb The crumb object to set the title in
             * @param id, The entity id used to get the entity from server 
             * @param scope [Optional] The controller scope to set the entityName to
             * @param setModel [Optional] A boolean or a function,
             * if present as boolean, will set scope.model[entityName] = entity
             * if present as function, will call the function with the entity
             */
            return function(crumb, id, scope, setModel) {
                if (scope && typeof setModel === "undefined") {
                    setModel = true;
                }

                function update(entity) {
                    if (!entity) {
                        return;
                    }
                    if (scope) {
                        scope[entityName] = entity;
                        // If we can set model 
                        if (setModel && scope.model) {
                            // if set model is a function
                            if (_.isFunction(setModel)) {
                                // Call the function to save a call to the server
                                setModel(entity);
                            } else {
                                scope.model[entityName] = entity;
                            }
                        }
                    }
                    if (langTitle) {
                        if (_.isString(langTitle))  {
                            crumb.title = entity[langTitle];
                            crumb.title_ar = entity[langTitle];
                            crumb.title_en = entity[langTitle];
                        }
                        if (_.isFunction(langTitle)) {
                            crumb.title = langTitle(entity);
                            crumb.title_ar = langTitle(entity, "ar");
                            crumb.title_en = langTitle(entity, "en");
                        }
                    } else {
                        crumb.title = entity.name;
                        crumb.title_ar = entity.name_ar;
                        crumb.title_en = entity.name_en;
                    }
                }

                if (_.isNumber(id) || _.isString(id)) {
                    Entity.get(id).then(update);
                } else if (_.isObject(id)) {
                    // Where currently, id is the actual entity object.
                    update(id);
                }

            };
        }

        this.setGrade = commonSet(Grade, "grade");
        this.setSystem = commonSet(EducationalSystem, "system");
        this.setCountry = commonSet(Country, "country");
        this.setStudent = commonSet(Student, "student", function(entity) {
            return entity.user.full_name;
        });
        this.setCourse = commonSet(Course, "course", "name");
        this.setElement = commonSet(CourseElement, "element", "name");
        this.setGroup = commonSet(Group, "group", "name");
        this.setClass = commonSet(Classes, "classObj", "name");
        this.setPrize = commonSet(Prize, "prize");
        this.setElementType = commonSet(CourseElementType, "elementtype", "name");
        this.setAuthor = commonSet(Author, "author", function(entity) {
            return entity.user.full_name;
        });

    }

    angular.module("elproffApp")
        .service("Crumbs",
                ["Course", "CourseElement", "Student", "Group", "Country",
                "EducationalSystem", "Grade", "Prize", "CourseElementType", 
                "Author", "Classes", Crumbs]);
}).call(null);
