"use strict";

/**
 * @ngdoc service
 * @name elproffApp.address
 * @description
 * # address
 * Service in the elproffApp.
 */
(function() {

    function Address(Restangular, Cache) {
        var address = Cache.wrap("address");
        
        return address;
    }

    angular.module("elproffApp")
    .service("Address", ["Restangular", "HTTPCache", Address]);
}).call(null);