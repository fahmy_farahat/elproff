"use strict";

/**
 * @ngdoc service
 * @name elproffApp.Classes
 * @description
 * # Classes
 * Service in the elproffApp.
 */
(function() {

    function Service(Restangular, Cache) {
    	var service = Cache.wrap("classes"); 
    	service.addRestangularMethod("course_classes", "get", "course_classes");
        return service;
    }

    angular.module("elproffApp")
    .service("Classes", ["Restangular", "HTTPCache", Service]);
}).call(null);
