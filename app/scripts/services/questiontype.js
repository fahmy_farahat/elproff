"use strict";

/**
 * @ngdoc service
 * @name elproffApp.QuestionType
 * @description
 * # QuestionType
 * Service in the elproffApp.
 */
(function() {

    function service(Restangular, $root) {

        this.getEntities = function() {
            return [
                // categorize
                { name: "dragdrop",            lang: $root.lang.type_dragdrop },
                //mcq
                { name: "mcq",                 lang: $root.lang.type_mcq },
                //mcq multi
                { name: "multi_mcq",           lang: $root.lang.type_multi_mcq },
                //true or false
                { name: "truefalse",           lang: $root.lang.type_truefalse },
    //fill
    //fill multi
    //connect
    //connect multi

                //{ name: "reorder",             lang: $root.lang.type_reorder },
                { name: "connect",             lang: $root.lang.type_connect },
                { name: "fillblanks",          lang: $root.lang.type_fillblanks },
                //{ name: "multi_truefalse",     lang: $root.lang.type_multi_truefalse },

                //{ name: "calculate",           lang: $root.lang.type_calculate },
                //{ name: "essay",               lang: $root.lang.type_essay },
                //{ name: "graph",               lang: $root.lang.type_graph },
                //{ name: "hotspot",             lang: $root.lang.type_hotspot },
                //{ name: "problemsolving",      lang: $root.lang.type_problemsolving },
            ];
        };

    }

    angular.module("elproffApp")
    .service("QuestionType", ["Restangular", "$rootScope", service]);
}).call(null);
