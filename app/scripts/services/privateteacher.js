"use strict";

/**
 * @ngdoc service
 * @name elproffApp.privateteacher
 * @description
 * # privateteacher
 * Service in the elproffApp.
 */
(function() {

  function service(Restangular, Cache) {

    var privateteacher = Cache.wrap("private_teacher");
    privateteacher.addRestangularMethod("addCourse", "post", "add_course");
    privateteacher.addRestangularMethod("unassignCourse", "post", "course/remove");
    return privateteacher;
  }

  angular.module("elproffApp")
    .service("PrivateTeacher", ["Restangular", "HTTPCache" , service]);
}).call(null);
