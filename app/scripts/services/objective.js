"use strict";

/**
 * @ngdoc service
 * @name elproffApp.Objective
 * @description
 * # Objective
 * Service in the elproffApp.
 */
(function() {

    function Objective(Restangular, Cache) {
        var service = Cache.wrap("objective");
        service.addRestangularMethod("objective_customlist", "get", "objective_customlist");
        service.addRestangularMethod("reactivate_objective", "get", "reactivate_objective");
        return service;        
    }

    angular.module("elproffApp")
    .service("Objective", ["Restangular", "HTTPCache", Objective]);
}).call(null);
