"use strict";

/**
 * @ngdoc service
 * @name elproffApp.FeedbackType
 * @description
 * # FeedbackType
 * Service in the elproffApp.
 */
(function() {

    function service(Restangular, $root) {

        this.getEntities = function() {
            return [
                { name: "positive",      lang: $root.lang.feedback_positive },
                { name: "negative",      lang: $root.lang.feedback_negative },
            ];
        };

    }

    angular.module("elproffApp")
    .service("FeedbackType", ["Restangular", "$rootScope", service]);
}).call(null);
