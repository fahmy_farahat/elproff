"use strict";

/**
 * @ngdoc service
 * @name elproffApp.Group
 * @description
 * # Group
 * Service in the elproffApp.
 */
(function () {

  function service(Restangular, Cache) {
    var group = Cache.wrap("group");
    group.addRestangularMethod("groupStudents", "get", "students");
    group.addRestangularMethod("studentRemove", "post", "student/remove");
    return group;
  }

  angular.module("elproffApp")
    .service("Group", ["Restangular", "HTTPCache", service]);
}).call(null);
