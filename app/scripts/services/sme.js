"use strict";

/**
 * @ngdoc service
 * @name elproffApp.SME
 * @description
 * # SME
 * Service in the elproffApp.
 */
(function() {

    function SME(Restangular, Cache) {
        this.member = Cache.wrap("sme");
        this.member.addRestangularMethod("questionsCount", "get", "questions_count");
        this.member.addRestangularMethod("tasksCompletedCount", "get", "tasks_completed_count");
        return this.member;
    }

    angular.module("elproffApp")
    .service("SME", ["Restangular", "HTTPCache", SME]);
}).call(null);
