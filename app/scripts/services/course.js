"use strict";

/**
 * @ngdoc service
 * @name elproffApp.Course
 * @description
 * # Course
 * Service in the elproffApp.
 */
(function() {

    function Course(Restangular, Cache) {
        var service = Cache.wrap("course");
        service.addRestangularMethod("assignSME", "post", "assign/sme");
        service.addRestangularMethod("unassignSME", "post", "unassign/sme");
        service.addRestangularMethod("assignAuthor", "post", "assign/author");
        service.addRestangularMethod("unassignAuthor", "post", "unassign/author");
        service.addRestangularMethod("assignReviewer", "post", "assign/reviewer");
        service.addRestangularMethod("unassignReviewer", "post", "unassign/reviewer");
        service.addRestangularMethod("unassignTeacher", "post", "unassign/teacher");
        service.addRestangularMethod("assignTeacher", "post", "assign/teacher");
        service.addRestangularMethod("enable", "post", "enable");
        service.addRestangularMethod("disable", "post", "disable");
        service.addRestangularMethod("studentStats", "get", "student_stats");
        service.addRestangularMethod("stats", "get", "stats");
        service.addRestangularMethod("deleted_list", "get", "deleted_list");
        service.addRestangularMethod("undodelete", "post", "undodelete");
        service.addRestangularMethod("allCourses", "get", "all_courses");

        return service;
    }

    angular.module("elproffApp")
    .service("Course", ["Restangular", "HTTPCache", Course]);
}).call(null);
