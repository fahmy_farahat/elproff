"use strict";

/**
 * @ngdoc service
 * @name elproffApp.ExamInstance
 * @description
 * # ExamInstance
 * Service in the elproffApp.
 */
(function() {

    function ExamInstance(Restangular, $q, $timeout, Cache) {
        this.examInstance = Cache.wrap("exam_instance");
        this.examInstance.addRestangularMethod("take", "post", "take");

        // seems not to be used
        //this.examInstance.addRestangularMethod("question", "post", "question");

        this.examInstance.addRestangularMethod("result", "get", "result");
        this.examInstance.addRestangularMethod("finish", "post", "finish");
        this.examInstance.addRestangularMethod("list_instances", "post", "list_instances");
        this.examInstance.addRestangularMethod("studentGroupExams", "post", "student_group_exams");
        this.examInstance.addRestangularMethod("studentClassExams", "post", "student_class_exams");
        this.examInstance.addRestangularMethod("recommend", "post", "recommend");
        this.examInstance.addRestangularMethod("retake", "post", "retake");
        this.examInstance.addRestangularMethod("totals", "post", "totals");

      return this.examInstance;
    }

    angular.module("elproffApp")
    .service("ExamInstance", ["Restangular", "$q", "$timeout", "HTTPCache", ExamInstance]);
}).call(null);
