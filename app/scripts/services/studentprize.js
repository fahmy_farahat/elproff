"use strict";

/**
 * @ngdoc service
 * @name elproffApp.StudentPrize
 * @description
 * # StudentPrize
 * Service in the elproffApp.
 */
(function() {

    function studentPrize(Restangular, Cache) {
        var service = Cache.wrap("prize_student");
        service.addRestangularMethod("studentExamPrizes", "get", "student_exam_prizes");
        service.addRestangularMethod("studentPrizes", "get", "student_prizes");
        return service;
    }

    angular.module("elproffApp")
    .service("StudentPrize", ["Restangular", "HTTPCache", studentPrize]);
}).call(null);
