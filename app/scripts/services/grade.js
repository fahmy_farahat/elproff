"use strict";

/**
 * @ngdoc service
 * @name elproffApp.Grade
 * @description
 * # Grade
 * Service in the elproffApp.
 */
(function() {

    function service(Restangular, Cache) {
        return Cache.wrap("grade");
    }

    angular.module("elproffApp")
    .service("Grade", ["Restangular", "HTTPCache", service]);
}).call(null);
