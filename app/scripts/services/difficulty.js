"use strict";

/**
 * @ngdoc service
 * @name elproffApp.Difficulty
 * @description
 * # Difficulty
 * Service in the elproffApp.
 */
(function() {

    function service(Restangular, $root) {

        this.getEntities = function() {
            return [
                { name: "easy",      lang: $root.lang.difficulty_easy },
                { name: "medium",    lang: $root.lang.difficulty_medium },
                { name: "difficult", lang: $root.lang.difficulty_difficult },
            ];
        };

    }

    angular.module("elproffApp")
    .service("Difficulty", ["Restangular", "$rootScope", service]);
}).call(null);
