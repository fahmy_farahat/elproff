"use strict";

/**
 * @ngdoc service
 * @name elproffApp.plumbEndPoint
 * Service in the elproffApp.
 */
(function() {

    var commonJsplumbConfig = {
        paintStyle:{ fillStyle: "white", outlineColor: "steelblue", outlineWidth: 2},
        hoverPaintStyle:{ fillStyle:"lightblue" },
        connectorStyle:{ strokeStyle:"blue", lineWidth: 1 },
        connectorHoverStyle:{ lineWidth: 2 },
        connector: ["Straight"],
    };
    var customConfigurations = {};
    customConfigurations.end = angular.extend({}, commonJsplumbConfig, {
        anchor: [0, 0.5, -1, 0],
        isSource: false,
        isTarget: true,
        maxConnections:3,
    });
    customConfigurations.choice = angular.extend({}, commonJsplumbConfig, {
        anchor: [1, 0.5, 1, 0],
        isSource: true,
        isTarget: false,
    });
    /*
     * Create JSPlumb end points
     * @param  {integer} index          [The index of targeted Question Model]
     * @param  {string} prefix          [End Model type (Choices || Ends)]
     * @return {object}                 [JSPlumb Points for connect question]
     */
    function service($timeout) {
        this.create = function(index, prefix) {
            jsPlumb.ready(function(){
                // Angular timeout makes a callback when all the elements
                // is rendered before jsPlumb build on it, to prevent any error.
                $timeout(function(){
                    // We decide which configurations it"s gonna be, based on the given prefix
                    // if the custom configurations doesn"t exists, we will use the common one
                    var config = customConfigurations[prefix] ? customConfigurations[prefix] : commonJsplumbConfig;
                    var id = prefix + "-" + index,
                        el = $("#" + id),
                        $container = $(".question_body");
                    // Setting a unique ui to the end point
                    config.uuid = id;
                    if ($container.length) {
                        // Setting the right container
                        jsPlumb.setContainer($container);
                    }
                    // If the end point doesn"t exit
                    if (!jsPlumb.getEndpoints(el)) {
                        // Create new end point for the end model
                        jsPlumb.addEndpoint(id, config);
                    }
                });
            });
        };
        this.connect = function(source, target) {
            jsPlumb.ready(function(){
                // Angular timeout makes a callback when all the elements
                // is rendered before jsPlumb build on it, to prevent any error.
                $timeout(function(){
                    jsPlumb.connect({ 
                        uuids: [source, target]
                    });
                });
            });
        };
        this.update = function(source, target) {
            jsPlumb.ready(function(){
                // Angular timeout makes a callback when all the elements
                // is rendered before jsPlumb build on it, to prevent any error.
                $timeout(function(){
                    jsPlumb.repaintEverything();
                });
            });
        };
    }

    angular.module("elproffApp")
    .service("plumbEndPoint", ["$timeout", service]);
}).call(null);
