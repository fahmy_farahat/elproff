"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:questionMakeFooter
 * @description
 * # questionMakeFooter
 */
(function() {

    function controller(scope) {

        scope.removePerfectAnswer = function removePerfectAnswer(index) {
            scope.model.correct_answer.perfect_answers.splice(index, 1);
        };

        scope.removeHint = function removeHint(index) {
            scope.model.correct_answer.hints.splice(index, 1);
        };

    }

    angular.module("elproffApp")
    .directive("questionMakeFooter", function () {
        return {
            templateUrl: "views/directives/question-make-footer.html",
            restrict: "E",
            controller: ["$scope", "$rootScope", controller]
        };
    });
}).call(null);
