"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:schoolCourse
 * @description
 * # schoolCourse
 */
angular.module("elproffApp")
  .directive("schoolCourse", function () {
    return {
      template: "<div></div>",
      restrict: "E",
      link: function postLink(scope, element, attrs) {
        element.text("this is the schoolCourse directive");
      }
    };
  });
