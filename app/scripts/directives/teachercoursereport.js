"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:teacherCourseReport
 * @description
 * # teacherCourseReport
 */
(function() {

    function controller(scope) {

    }

    angular.module("elproffApp")
    .directive("teacherCourseReport", function () {
        return {
            templateUrl: "views/directives/teacher-course-report.html",
            restrict: "E",
            controller: ["$scope", "$rootScope", controller]
        };
    });
}).call(null);
