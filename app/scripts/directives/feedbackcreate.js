"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:FeedbackCreate
 * @description
 * # FeedbackCreate
 */

(function () {

  function controller (scope) {

  }

angular.module("elproffApp")
    .directive("feedbackCreate", function () {
      return {
        templateUrl: "views/directives/feedback-create.html",
        restrict: "E",
        controller: ["$scope", controller]
      };
    });


}).call(null);