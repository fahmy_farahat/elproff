"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:questions/takeMcqMulti
 * @description
 * # questions/takeMcqMulti
 */
(function() {

    function controller(scope) {

        if (!scope.model.user) {
            scope.model.user = {
                answer: []
            };
        }

    }

    angular.module("elproffApp")
    .directive("takeMcqMulti", function () {
        return {
            templateUrl: "views/directives/questions/take-mcq_multi.html",
            restrict: "E",
            controller: ["$scope", "$rootScope", controller]
        };
    });
}).call(null);
