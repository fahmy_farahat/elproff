"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:questions/takeTruefalse
 * @description
 * # questions/takeTruefalse
 */
(function() {

    function controller(scope) {
        if (!scope.model.user) {
            scope.model.user = {
                answer: ""
            };
        }
    }

    angular.module("elproffApp")
        .directive("takeTruefalse", function () {
            return {
                templateUrl: "views/directives/questions/take-true_false.html",
                restrict: "E",
                //replace: true,
                controller: ["$scope", "$rootScope", controller]
            };
        });
}).call(null);
