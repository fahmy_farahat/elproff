"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:questions/takeDragDrop
 * @description
 * # questions/takeDragDrop
 */
(function() {

    function controller(scope) {

        if (!scope.model.user) {
            scope.model.user = {
                answer: {}
            };
        }

        scope.onDragComplete = function(data,evt){
            // console.log("Drag success:: data, evt", data);
        };

        scope.$parent.onDropComplete = function(data,evt, box){
            if (!scope.model.user.answer[box.text]) {
                scope.model.user.answer[box.text] = [];
            }
            if (!_.isNull(data)) {
                var i = _.findIndex(scope.model.user.answer[box.text], function(item) {
                    return item.text === data.text;
                });           
                // If not found before
                if (i < 0) {
                    scope.model.user.answer[box.text].push(data);
                }
            }
        };

        scope.onRemove = function(choice, end){
            var indexEnd = _.findIndex(scope.model.user.answer[end.text], function(item) {
                return item.text === choice.text;
            });
            scope.model.user.answer[end.text].splice(indexEnd, 1);
        };
    }

    angular.module("elproffApp")
    .directive("takeDragDrop", function () {
        return {
            templateUrl: "views/directives/questions/take-dnd.html",
            restrict: "E",
            controller: ["$scope", "$rootScope", controller]
        };
    });
}).call(null);