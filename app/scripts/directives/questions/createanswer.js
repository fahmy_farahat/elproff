"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:createAnswer
 * @description
 * # createAnswer
 */
(function() {

    function controller(scope, $timeout, Common) {
        scope.isLoading = false;
        scope.model.type = "composite";
        scope.onSuccess = function onSuccess(result) {
            if (result.attachment_type === "image") {
                scope.model.image = result.attachment;
            } else if (result.attachment_type === "equation") {
                scope.model.equation = result.attachment;
            }
            scope.isLoading = false;
        };
        
        // This watcher watchs the answer
        // And validate it if it"s not empty
        // No matter the answer"s type
        var answerWatcher = scope.$watch("model", function(newValue, oldValue) {
            if (_.isUndefined(scope.model) || _.isEmpty(scope.model)) {
                return;
            }
            scope.answer_validate = !_.isEmpty(scope.model[scope.model.type]);
            // Making use of the watcher already existing, if this watcher
            // to be removed for any reason, move the call to onchange to an 
            // event
            if (_.isFunction(scope.model.onchange)) {
                if (newValue.index <= oldValue.index) {
                    scope.model.onchange(newValue.text, oldValue.text);
                } else if (newValue.index > oldValue.index) {
                    scope.model.onchange(oldValue.text, newValue.text);
                }
            }
        }, true);
    }

    angular.module("elproffApp")
    .directive("createAnswer", function () {
        return {
            templateUrl: "views/directives/questions/createanswer.html",
            restrict: "E",
            scope: {
                model: "=",
                name: "@",
                index: "@",
                remove: "&",
                add: "&"
            },
            controller: ["$scope", "$timeout", "Common", controller]
        };
    });
}).call(null);
