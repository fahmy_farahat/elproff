"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:questions/createAnswerContainer
 * @description
 * # questions/createAnswerContainer
 */
(function() {

    function controller(scope, $timeout) {

    }

    angular.module("elproffApp")
    .directive("createAnswerContainer", function () {
        return {
            templateUrl: "views/directives/questions/createanswercontainer.html",
            restrict: "E",
            controller: ["$scope", "$timeout", controller]
        };
    });
}).call(null);
