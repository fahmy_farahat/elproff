"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:questions/viewAnswer
 * @description
 * # questions/viewAnswer
 */
(function() {

    function controller(scope) {

    }

    angular.module("elproffApp")
    .directive("viewAnswer", function () {
        return {
            templateUrl: "views/directives/questions/viewanswer.html",
            restrict: "E",
            scope: {
                model: "="
            },
            controller: ["$scope", controller]
        };
    });
}).call(null);
