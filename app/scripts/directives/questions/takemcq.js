"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:questions/takeMcq
 * @description
 * # questions/takeMcq
 */
(function() {

    function controller(scope) {

        if (!scope.model.user) {
            scope.model.user = {
                answer: null
            };
        }

    }

    angular.module("elproffApp")
    .directive("takeMcq", function () {
        return {
            templateUrl: "views/directives/questions/take-mcq.html",
            restrict: "E",
            controller: ["$scope", "$rootScope", controller]
        };
    });
}).call(null);
