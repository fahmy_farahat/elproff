"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:questions/takeconnect
 * @description
 * # questions/takeconnect
 */
(function() {

    function controller(scope, plumbEndPoint) {

        if (!scope.model.user) {
            scope.model.user = {
                answer: {}
            };
        }
        function IsJsonString(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        };
        // Initializing jsplumb in case the question is already answered 
        function init() {
            jsPlumb.ready(function(){
                // Removing any previous end point
                jsPlumb.selectEndpoints().remove();
                // Setting the global prefix(s), we are about to use them
                var end_prefix = "end", choice_prefix = "choice";
                // Checking if the question model is valid
                if (scope.model && scope.model.question && !_.isEmpty(scope.model.question)){
                    // check if end points is a json object
                    if(IsJsonString(scope.model.question)) {
                        scope.model.question = JSON.parse(scope.model.question);
                    }
                    // Checking if the end points array isn"t empty
                    if (!_.isUndefined(scope.model.question[end_prefix + "s"]) && 
                        scope.model.question[end_prefix + "s"].length) {
                        // Looping inside question model and extracting end points
                        _.each(scope.model.question[end_prefix + "s"], function(question_end, end_index){
                            // Creating question end point
                            plumbEndPoint.create(end_index, end_prefix);
                        });
                    }
                    // Checking if question points array isn"t empty
                    if (!_.isUndefined(scope.model.question[choice_prefix + "s"]) &&
                        scope.model.question[choice_prefix + "s"].length) {
                        // Looping inside end model and extracting choices
                        _.each(scope.model.question[choice_prefix + "s"], function(question_choice, choice_index) {
                            // Creating question choice point
                            plumbEndPoint.create(choice_index, choice_prefix);
                        });
                    }
                    // If the answer model array isn"t empty
                    if (!_.isEmpty(scope.model.user.answer)){
                        // Looping inside answers model and extracting answers
                        _.each(scope.model.user.answer, function(answer_choices, end_key) {
                            // We need to found this answer end point index in question model
                            var end_index = _.findIndex(scope.model.question[end_prefix + "s"], function(question_end){
                                return end_key === question_end[question_end.type];
                            });
                            // End points can have more than one connection, we need to go through them all
                            _.each(answer_choices, function(answer_choice, choice_key){
                                var choice_index = _.findIndex(scope.model.question[choice_prefix + "s"], function(question_choice){
                                    return answer_choice[answer_choice.type] === question_choice[question_choice.type];
                                });
                                // Check if we have found the two points
                                if (end_index >= 0 && choice_index >= 0){
                                    // Make the connection
                                    plumbEndPoint.connect(
                                        end_prefix + "-" + end_index,
                                        choice_prefix + "-" + choice_index
                                    );
                                }
                            });
                        });
                    }
                }
            });
        }
        init();
        scope.$on("question_init", init);

        // Building Answer model
        scope.rebuildModel = function(){
            if (scope.model.question &&
                scope.model.question.choices) {
                scope.model.user.answer = {};
                _.each(jsPlumb.getConnections(), function(connection, index){
                    // Preparing answer object
                    var answerContainer = connection.target.innerText.replace(/(\r\n|\n|\r)/gm,"");
                    scope.model.user.answer[answerContainer] = [];
                    // Find the equalivant answer in the answers model
                    var answerData = _.find(scope.model.question.choices, function(choice) {
                        var sourceText = connection.source.innerText.replace(/(\r\n|\n|\r)/gm,"");
                        return choice.description === sourceText;
                    });
                    // Injecting answer in the right place (choice in the end point)
                    scope.model.user.answer[answerContainer].push(answerData);
                });
            } else {
                // TODO: Make some alert to control this case of 
                // not having a valid question model
            }
        }

        // On making or deatching a connection event
        jsPlumb.bind("connection", scope.rebuildModel);
        jsPlumb.bind("connectionDetached", scope.rebuildModel);
        // On changing in ends||choice inputs event
        var modelWatcher = scope.$watch("model.question", function(newValue) {
            if ( _.isUndefined(scope.model.question) || 
                _.isEmpty(scope.model.question)) {
                return;
            }
            $('[ng-model="model.text"]').bind("change paste keyup", scope.rebuildModel);
            // Repainting end points
            plumbEndPoint.update();
        }, true);
        // On resizing the window (responsive case)
        $(window).resize(function(){
            jsPlumb.repaintEverything();
        });
    }

    angular.module("elproffApp")
    .directive("takeConnect", function () {
        return {
            templateUrl: "views/directives/questions/take-link.html",
            restrict: "E",
            controller: ["$scope", "plumbEndPoint", controller]
        };
    });
}).call(null);
