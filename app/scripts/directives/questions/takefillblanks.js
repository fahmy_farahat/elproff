"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:questions/takeFillBlanks
 * @description
 * # questions/takeFillBlanks
 */
(function() {

    function controller(scope) {

        if (!scope.model.user) {
            scope.model.user = {
                answer: []
            };
        }

        scope.onDragComplete = function(data, evt) {
            console.log("drag success, data:", data);
        };

        /*
         * Fixes the incorrect positioning happens while dragging and scroll
         * exists
         * Fixes only Y position, no X scoll exists so fr
         */
        scope.onDrag = function(data, evt) {
            try {
                var trans = evt.element.css("-webkit-transform");
                // "matrix(1, 0, 0, 1, 279.71875, 567)"
                var transAttrs = trans.split(", ");
                if (transAttrs.length > 0) {
                    var currentPos = transAttrs[5].substring(0, transAttrs[5].length - 1);
                    var newPos = parseFloat(currentPos) - window.scrollY;
                    transAttrs[5] = newPos + ")";
                    trans = transAttrs.join(", ");
                    evt.element.css("-webkit-transform", trans);

                }
                trans = evt.element.css("transform");
                // matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, -11.28125, 377, 0, 1)
                transAttrs = trans.split(", ");
                if (transAttrs.length > 0) {
                    currentPos = transAttrs[13]
                    newPos = parseFloat(currentPos) - window.scrollY;
                    transAttrs[13] = newPos;
                    trans = transAttrs.join(", ");
                    evt.element.css("transform", trans);
                }
            } catch(e) {
                console.log(e);
            }
        };
        scope.$on("draggable:move", scope.onDrag);

        scope.$parent.onDropComplete = function(data,evt, index){
            scope.model.user.answer[index] = data;
            // _.remove(scope.model.choices, {text: data.text});
        };

        scope.$watch("model.question.choices", function(newValue) {
            // If there"s an answer
            if (_.find(scope.model.user.answer, "text")) {
                // Looping answers
                _.forEach(scope.model.user.answer, function(answer, index){
                    // Checking if answer choice still exists
                    var currentChoiceAvailability = _.find(newValue, function(choice) {
                        return choice.text === answer.text;
                    });
                    // If there"s no choices matching answer then:
                    // Clear answer
                    if (typeof currentChoiceAvailability === "undefined") {
                        scope.model.user.answer[index] = "";
                    }
                });
            }
        }, true);

        scope.$watch("model.description", function(newValue) {
            // If the 4 dots are completly removed
            if (!/\.{4}/.test(newValue)) {
                // Clear all answers
                scope.model.user.answer = [];
            } 
            // There"s 4 dots somewhere, then we need
            // to check that all dots groups are valid
            else {
                // Testing description value
                var testValue = newValue;
                // Removing any chrachters other than dots
                testValue = newValue.replace(/\w/g, "");
                // Spliting the value into array to detect dots place
                testValue = testValue.split(" ");
                // Removing any empty value from the splitted array
                testValue = _.remove(testValue, function(n) {
                  return /\./.test(n);
                });
                // Detecting if and which of the dots group isn"t complete
                _.forEach(scope.model.user.answer, function(answer, index){
                    // If the dots length isn"t 4, remove the answer
                    if (testValue[index] !== "....") {
                        scope.model.user.answer[index] = "";
                    }
                });
            }
        }, true);
    }

    angular.module("elproffApp")
    .directive("takeFillBlanks", function () {
        return {
            templateUrl: "views/directives/questions/take-fill-blanks.html",
            restrict: "E",
            controller: ["$scope", "$rootScope", controller]
        };
    });
}).call(null);
