"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:footer
 * @description
 * # footer
 */
angular.module("elproffApp")
  .directive("footerdir", function () {
    return {
      templateUrl: "views/directives/footer.html",
      restrict: "E",
      link: function postLink(scope, element, attrs) {
      }
    };
  });
