"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:linkitem
 * @description
 * # linkitem
 */
(function() {

    function controller(scope, Common) {

        scope.onSuccess = function onSuccess(result) {
            scope.$root.$broadcast("relist");
        };

        scope.remove = function(entity) {
            entity.remove().then(this.onSuccess, Common.onError(scope));
        };

    }

    angular.module("elproffApp")
    .directive("linkItem", function () {
        return {
            templateUrl: "views/directives/linkitem.html",
            restrict: "E",
            transclude: true,
            scope: {
                title: "@",
                description: "@",
                imgsrc: "@",
                href: "@",
                entity: "="
            },
            link: function(scope, element) {
                scope.transcluding = $(element).find("ng-transclude *").length > 0;
            },
            controller: ["$scope", "Common", controller]
        };
    });
}).call(null);
