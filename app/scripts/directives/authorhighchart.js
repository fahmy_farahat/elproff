"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:authorHighChart
 * @description
 * # authorHighChart
 */
(function() {

    function controller(scope, $location, root) {
        var watcher = scope.$watch("model", function(newValue) {
            if (!scope.model || !_.isArray(scope.model)) {
                return;
            }
            watcher();

            scope.data = _.map(scope.model, function(array, key) {
                // Splitting Date, e.g "1/1/2015" will be ["1","1","2015"]
                var dateArray = array.creation.split(/\/|\-|\\/g);
                // Sorting date, moment takes date in this order [YYYY,MM,DD]
                if (dateArray[2].length === 4) {
                    dateArray = dateArray.reverse();
                }
                // Converting date array to useble Date object
                var creationDateUTC = moment.utc(dateArray).toDate();
                // Converting server model to useble plugin data
                return {
                    "count": array.count,
                    "creation": array.creation,
                    "x": Date.UTC(creationDateUTC.getUTCFullYear(), creationDateUTC.getUTCMonth() - 1, creationDateUTC.getUTCDate()),
                    "y": parseInt(array.count)
                };
            });

            function config() {
                // Highcharts confgiurations
                scope.chartConfig = {
                    options: {
                        tooltip: {
                            formatter: function(){
                                var point = this.points[0].point;
                                return point.creation + "<br>" +"<b>" + root.lang.questions + "</b>: " + point.count;
                            },
                            style: {
                                padding: 10,
                            },
                            useHTML: true
                        },
                        navigator: {
                            enabled: false
                        },
                        rangeSelector: {
                            enabled: false
                        },
                        exporting: {
                            enabled: false
                        }
                    },
                    xAxis: {
                        type: "datetime",
                        dateTimeLabelFormats: { // don"t display the dummy year
                            day: "%e. %b",
                            year: "%b"
                        },
                        title: {
                            text: root.lang.creationDate,
                            useHTML: true
                        },
                        // RTL SUPPRT
                        reversed: root.USER.lang === "ar",
                    },
                    yAxis: {
                        min: 0,
                        labels: {
                            x: root.USER.lang === "ar" ? -8 : -2,
                            y: 4
                        },
                        title: {
                            text: root.lang.questions,
                            useHTML: true
                        },
                        // RTL SUPPRT
                        opposite: root.USER.lang === "ar",
                    },
                    series: [{
                        name: root.lang.questions,
                        // Sorting data descending by number of questions
                        data: _.sortBy(scope.data, "x"),
                        pointWidth: 32,
                        pointInterval: 24 * 3600 * 1000,
                        pointStart: Date.UTC(2015, 0, 1),
                        dataLabels: {
                            enabled: true,
                            color: "#FFFFFF",
                            align: "right",
                            format: "{point.y:.0f}", 
                            y: 10,
                            x: root.USER.lang === "ar" ? 14 : 0,
                            style: {
                                fontSize: "11px",
                            }
                        }
                    }],
                    scrollbar: {
                        enabled: true,
                        minWidth: 20,
                        barBackgroundColor: "gray",
                        barBorderRadius: 7,
                        barBorderWidth: 0,
                        buttonBackgroundColor: "gray",
                        buttonBorderWidth: 0,
                        buttonArrowColor: "yellow",
                        buttonBorderRadius: 7,
                        rifleColor: "yellow",
                        trackBackgroundColor: "white",
                        trackBorderWidth: 1,
                        trackBorderColor: "silver",
                        trackBorderRadius: 7
                    },
                    title: {
                        text: root.lang.author + " " + root.lang.questions,
                        useHTML: Highcharts.hasBidiBug
                    },
                    legend: {
                        useHTML: Highcharts.hasBidiBug
                    },    
                    useHighStocks: true,
                    credits: {
                        enabled: false
                    }
                };
            }
            config();
            scope.$on("lang_updated", config);
        });

    }

    angular.module("elproffApp")
        .directive("authorHighChart", function() {
            return {
                templateUrl: "views/directives/author-high-chart.html",
                restrict: "E",
                scope: {
                    model: "="
                },
                controller: ["$scope", "$location", "$rootScope", controller]
            };
        });
}).call(null);
