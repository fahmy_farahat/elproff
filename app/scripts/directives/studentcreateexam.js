"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:studentCreateExam
 * @description
 * # studentCreateExam
 */
(function() {

    function controller(scope, Exam) {

    }

    angular.module("elproffApp")
        .directive("studentCreateExam", function() {
            return {
                templateUrl: "views/directives/student-create-exam.html",
                restrict: "E",
                controller: ["$scope", "Exam", controller]
            };
        });
}).call(null);
