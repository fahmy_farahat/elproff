"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:listprofiles
 * @description
 * # listprofiles
 */
(function() {


    function controller(scope, route, Common, Profile, root, User, $location) {

        scope.isLoading = false;

        function onLogout(result) {
            User.setUser(null);
            $location.path("/login");
        }

        scope.onSuccess = function onSuccess(logout) {
            scope.isLoading = false;
            if(logout){
                User.logout().then(onLogout, onLogout);
            }
            else {
                load();
                route.reload();
            }
        };
        scope.onProcessSuccess = function onProcessSuccess(result) {
            scope.isLoading = false;
            load();
            route.reload();
        };
        scope.onProfilesSuccess = function onProfilesSuccess(result){
            scope.profiles = result;
            var data = {};
            if(scope.$root.USER.type === "admin"){
                data.data = scope.userlist;
            }
            Profile.available(data).then(scope.onAvailableSuccess,Common.onError(scope));
        };
        scope.onAvailableSuccess = function onAvailableSuccess(result){
            scope.availables = result;
            _.forEach(scope.availables,function($item){
                var obj = {
                    "profile_type": $item,
                    "is_add": true,
                };
                scope.profiles.push(obj);
            });
        };
        function load() {
            scope.isLoading = true;
            var data = {};
            if(scope.$root.USER.type === "admin"){
                data.data = scope.userlist;
            }
            Profile.myprofiles(data).then(scope.onProfilesSuccess,Common.onError(scope));
            
        }

        load();
        scope.remove_profile = function remove_profile(profile){
            scope.isLoading = true;
            var data = {"profile": profile.profile_type,};
            var filteredprofiles = _.filter(scope.profiles, function(profile){
                return !profile.is_deleted && !profile.is_add;
            });

            if(scope.$root.USER.type === "admin"){
                data.data =  scope.userlist;
                Profile.remove_profile(data).then(scope.onProcessSuccess, Common.onError(scope));
            }
            else {
                var title = root.lang.deactivate;
                var msg = root.lang.removeprofile;
                var logout = false;
                if(filteredprofiles.length === 1){
                    msg = root.lang.removelastprofile;
                    logout = true;
                }
                var prompt =  Common.promptDialog(title, msg);
                prompt.then(function(){
                    Profile.remove_profile(data).then(scope.onSuccess(logout),Common.onError(scope));
                });
            }
        };
        scope.activate_profile = function activate(profile){
            scope.isLoading = true;
            var data = {};
            data.profile = profile.profile_type;
            if(scope.$root.USER.type === "admin"){  
                data.data = scope.userlist;
            }
            Profile.reactivate(data).then(this.onProcessSuccess,Common.onError(scope));   
        };
        scope.add_profile = function add_profile(profile){
            scope.isLoading = true;
            var data = {"profile": profile};
            if(scope.$root.USER.type === "admin"){
                data.data = scope.userlist;
            }
            Profile.add(data).then(this.onProcessSuccess,Common.onError(scope));
            
        };
    }
    angular.module("elproffApp")
    .directive("listprofiles", function () {
        return {
            templateUrl: "views/directives/listprofiles.html",
            restrict: "E",
            scope:{
                userlist: "="
            },
            controller: ["$scope", "$route", "Common", "Profile", "$rootScope", "User", "$location", controller]
        };

    });
}).call(null);

