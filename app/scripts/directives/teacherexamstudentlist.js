"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:teacherExamStudentList
 * @description
 * # teacherExamStudentList
 */
(function() {

    function controller(scope) {

    }

    angular.module("elproffApp")
    .directive("teacherExamStudentList", function () {
        return {
            templateUrl: "views/directives/teacher-exams-student-list.html",
            restrict: "E",
            controller: ["$scope", "$rootScope", controller]
        };
    });
}).call(null);
