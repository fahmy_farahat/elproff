"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:objectiveView
 * @description
 * # objectiveView
 */
(function() {

    function controller(scope, Common, Objective, LearningOutcome, QuestionType,
                        Difficulty, $modal, $location, QuestionsBulk) {

        // Viewing objective
        scope.types = QuestionType.getEntities();
        scope.outcomes = LearningOutcome.getEntities();
        scope.difficulties = Difficulty.getEntities();
        var watcher = scope.$watch("model.questions_bulks", function() {
            if (scope.model && scope.model.questions_bulks) {
                scope.bulks = [];
                _.each(scope.model.questions_bulks, function(bulkUri, index) {
                    QuestionsBulk.getUri(bulkUri).then(function(obj){
                        var bulk = obj;
                        bulk.questions_type = _.find(scope.types, {name: obj.questions_type});
                        bulk.learning_outcome = _.find(scope.outcomes, {name: obj.learning_outcome});
                        bulk.difficulty = _.find(scope.difficulties, {name: obj.difficulty});
                        scope.bulks.push(bulk);
                        //scope.model.questions_bulks[index] = bulk; 
                    });
                });
                var query = {name: scope.model.learning_outcome};
                scope.model.learning_outcome = _.find(scope.outcomes, query);
            }
        });

        // Adding question bulk
        scope.addBulk = function addBulk() {
            var modalInstance = $modal.open({
                templateUrl: "views/modals/createquestionsbulk.html",
                controller: "ModalsCreatequestionsbulkCtrl",
                size: "lg",
                resolve: { }
            });

            modalInstance.result.then(function (bulk) {
                // Questions Bulks
                scope.bulks.push(bulk);
            });
        };

        // On Relist, reload bulks
        scope.$on("relist", function() {
            if (_.isUndefined(scope.model.id)) {
                return;
            }
            Objective.get(scope.model.id).then(function(objective) {
                // this should trigger the watcher above
                scope.model.questions_bulks = objective.questions_bulks;
            });
        });
        scope.dialogBox = function dialogBox(node) {
            var modalInstance = $modal.open({
                templateUrl: "views/modals/list-child-course-element.html",
                controller: "ModalsListchildcourseelementCtrl",
                size: "lg",
                resolve: {
                  courseElementNode: function () {
                        return node;
                    }
                }
            });
        };

        // Saving Objective
        scope.onEditSuccess = function onSuccess(result){
            scope.isLoading = false;
            scope.model = result;
            scope.cancel();
            $location.path(scope.$parent.crumbs[3].link.substring(1));
        };

        scope.save = function save() {
            if (!scope.model.learning_outcome) {
                if (Common.alerts.errors) {
                    scope.alerts = Common.alerts.errors.learning_outcome_required;
                }
                return;
            }
            scope.isLoading = true;
            var model = _.cloneDeep(scope.model);
            delete model.parentResource;
            // model.questions
            model.learning_outcome = model.learning_outcome.name;
            model.questions_bulks = _.map(scope.bulks, function(originalBulk) {
                var bulk = _.cloneDeep(originalBulk);
                bulk.questions_type = bulk.questions_type.name;
                bulk.difficulty = bulk.difficulty.name;
                if (!bulk.tasks){
                    bulk.tasks = [];
                    var savable = {
                        duration: bulk.count * 5,
                        description: scope.$root.lang.createquestions,
                        description_en: scope.$root.lang.trans_obj.en.createquestions,
                        description_ar: scope.$root.lang.trans_obj.ar.createquestions,
                        count: bulk.count,
                        course_element: scope.model.course_element
                    };
                    bulk.tasks.push(savable);
                }
                return bulk;
            });
            Objective.post(model)
                .then(this.onEditSuccess, Common.onError(scope, this.onError));
        };
    }
    angular.module("elproffApp")
        .directive("objectiveView", function () {
            return {
                templateUrl: "views/directives/objectiveview.html",
                restrict: "E",
                scope: {
                    model: "=",
                    cancel: "&"
                },
                controller:
                    ["$scope", "Common", "Objective", "LearningOutcome",
                     "QuestionType", "Difficulty", "$modal", "$location", "QuestionsBulk", controller]
            };
        });
}).call(null);
