"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:taskListItem
 * @description
 * # taskListItem
 */
(function() {

    function controller(scope, Common, Task, Restangular) {

        scope.unassign = function(task) {
            Task.selfUnassign({task_id: task.id}).then(function(result) {
                scope.model.mine = false;
                scope.$root.$broadcast("relist");
            }, Common.onError(scope));
        };

        scope.assignToMe = function(task) {
            Task.selfAssign({task_id: task.id}).then(function(result) {
                scope.model.mine = true;
                scope.$root.$broadcast("relist");
                scope.afterAssign()(scope.model);
            }, Common.onError(scope));
        };

        var watcher = scope.$watch("model", function() {
            if (!scope.model) {
                return;
            }
            watcher();
            var parent = scope.model.course_element.parent;
            if (parent) {
                // Update the parent uri with the real object
                scope.model.course_element.parent = Restangular.oneUrl("course_element", parent).get().$object;
            }
        });

    }

    angular.module("elproffApp")
    .directive("taskListItem", function () {
        return {
            templateUrl: "views/directives/tasklistitem.html",
            restrict: "E",
            replace:true,
            scope: {
                model: "=",
                courseid: "=",
                questionMode: "@",
                afterAssign: "&",
            },
            controller: ["$scope", "Common", "Task", "Restangular", controller]
        };
    });
}).call(null);
