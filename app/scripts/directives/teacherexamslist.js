"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:teacherExamsList
 * @description
 * # teacherExamsList
 */
(function() {

    function controller(scope) {

    }

    angular.module("elproffApp")
    .directive("teacherExamsList", function () {
        return {
            templateUrl: "views/directives/teacher-exams-list.html",
            restrict: "E",
            controller: ["$scope", "$rootScope", controller]
        };
    });
}).call(null);
