"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:smallLanguageBtns
 * @description
 * # smallLanguageBtns
 */
/*angular.module("elproffApp")
  .directive("smallLanguageBtns", function () {
    return {
      template: "<div></div>",
      restrict: "E",
      link: function postLink(scope, element, attrs) {
        element.text("this is the smallLanguageBtns directive");
      }
    };
  });*/
(function() {

    function controller(scope, Lang) {
        

    }

    angular.module("elproffApp")
    .directive("smallLanguageBtns", function () {
        return {
            templateUrl: "views/directives/smalllanguagebtns.html",
            restrict: "E",
            replace: true,
            controller: ["$scope", "Lang", controller]
        };
    });
}).call(null);
