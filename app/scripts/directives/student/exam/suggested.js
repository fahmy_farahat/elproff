"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:student/exam/suggested
 * @description
 * # student/exam/suggested
 */
(function() {

    function controller(scope) {
    }

    angular.module("elproffApp")
    .directive("studentexamsuggested", function () {
        return {
            templateUrl: "views/directives/student/exam/suggested.html",
            restrict: "E",
            replace: true,
            controller: ["$scope", controller]
        };
    });

}).call(null);
