"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:notifications
 * @description
 * # notifications
 */
(function() {

    function controller(scope, Common, Notifications, Restangular, $location, ExamInstance, CommonCache, Parent) {

        //scope.searchable = true;
        scope.crumbs = [
        {title: "notifications", link: ""},
        ];

        function load() {
            Notifications.getList(Common.query).then(function(notifications){
                _.each(notifications, function(notification, i){
                    var senderId = Common.uriToID(notification.sender);
                    Restangular.one("user", senderId).get().then(function(sender){
                        notifications[i].sender = sender;
                    });
                    if (notification.attrs && notification.attrs.exam_instance) {
                        var examInstanceID = notification.attrs.exam_instance;
                        ExamInstance.get(examInstanceID).then(function(examInstanceObj){
                            notifications[i].examInstance = CommonCache.examInstance(examInstanceObj);
                        });
                    }
                });
                scope.notifications = notifications;

            });
            scope.$root.$broadcast("reload_notifications");
        }

        scope.onError = function onError(result) {
            scope.alerts = Common.alerts.error.notification;
        };

        scope.onAcceptSuccess = function onAcceptSuccess(result) {
            var idx = _.findIndex(scope.notifications, {id: result.id});
            var examInstanceID;
            var courseID;
            if(scope.notifications[idx].notification_type === "privateteacher_exam_student"){
                examInstanceID = scope.notifications[idx].attrs.exam_instance;
                courseID = scope.notifications[idx].attrs.course;
                ExamInstance.get(examInstanceID).then(function(examInstance){
                    if (!examInstance.start_time && !examInstance.end_time && !examInstance.degree) {
                        $location.path("/student/course/" + courseID + "/exam/" + examInstanceID + "/take");
                    }
                });
            } else if(scope.notifications[idx].notification_type === "schoolteacher_exam_class") {
                examInstanceID = scope.notifications[idx].attrs.exam_instance;
                courseID = scope.notifications[idx].attrs.course;
                ExamInstance.get(examInstanceID).then(function(examInstance){
                    if (!examInstance.start_time && !examInstance.end_time && !examInstance.degree) {
                        $location.path("/student/course/" + courseID + "/exam/" + examInstanceID + "/take");
                    }
                });
            } else if (scope.notifications[idx].notification_type === "student_parent_invitation") {
                Parent.clearCache();
            }
            scope.notifications.splice(idx, 1);
            load();
        };
        scope.onRejectSuccess = function onRejectSuccess(result) {
            var idx = _.findIndex(scope.notifications, {id: result.id});
            scope.notifications.splice(idx, 1);
            load();
        };

        load();
        scope.$on("relist", load);

        scope.accept = function accept(notification) {
            Notifications.accept({id: notification.id}).then(
                    scope.onAcceptSuccess, Common.onError(scope, this.onError));
        };


        scope.reject = function reject(notification) {
            Notifications.reject({id: notification.id}).then(
                    scope.onRejectSuccess, Common.onError(scope, this.onError));
        };

        scope.timeOf = function timeOf(notification) {
            if (notification) {
                var time = moment.utc(notification.creation_date);
                var res  = moment(time).fromNow();
                return res;
            }
        };

    }
    angular.module("elproffApp")
        .directive("notifications", function () {
            return {
                templateUrl: "views/directives/notifications.html",
                restrict: "E",
                transclude:true,
                controller: ["$scope", "Common", "Notifications", "Restangular", "$location", "ExamInstance", "CommonCache", "Parent", controller]
            };
        });
}).call(null);
