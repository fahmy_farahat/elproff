"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:plan
 * @description
 * # plan
 */
(function() {

    function controller(scope) {
        var watcher = scope.$watch("model", function(newValue){
            scope.planDetails = newValue.planDetails;
        }, true);
    }

    angular.module("elproffApp")
    .directive("plan", function () {
        return {
            templateUrl: "views/directives/plan.html",
            restrict: "E",
            scope: {
                model: "=",
                subscribe: "&",
                showdetails: "&"
            },
            controller: ["$scope", "$rootScope", controller]
        };
    });
}).call(null);
