"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:staffMember
 * @description
 * # staffMember
 */
(function() {

    function controller(scope, SME, Author, Reviewer, Course, Common, $modal, $log, Question, Task) {

        scope.addCourse = function addCourse() {

            if (!scope.model) {
                return;
            }
            var modalInstance = $modal.open({
                templateUrl: "views/modals/assignstaffmembercourse.html",
                controller: "ModalsAssignstaffmembercourseCtrl",
                resolve: {
                    targetUser: function () { return scope.model; },
                    userType: function () { return scope.type; }
                }
            });


            modalInstance.result.then(function (courses) {
                var params = {id: scope.model.id};
                _.each(courses, function(course){
                    var obj = _.find(scope.model.courses, function(obj) { 
                        return obj.id === course.id; 
                    });
                    if(!obj){
                        scope.model.courses.push(course);
                    }
                });


            }, function () {
                $log.info("Modal dismissed at: " + new Date());
            });
        };
        var watcher = scope.$watch("model", function() {
            if (!scope.model || !scope.model.user) {
                return;
            }
            watcher();
            scope.questionsCreated = 0;
            scope.rejectedPrecentage = 0;
            scope.reviewed_questions_count = 0;
            var tasksCompleted;
            var query = {user_id: scope.model.user.id};
            Author.questionsCount(query).then(function(result) {
                scope.questionsCreated = result.objects;
            });
            var params = {member_id: scope.model.user.id};
            var data;
            if (scope.type === "author") {
                tasksCompleted = Author.tasksCompletedCount(params);
                data = {author_id: scope.model.id};
                Task.author_rejected_precentage(data).then(function(result) {
                    scope.rejectedPrecentage = (result.rejected / result.total)*100;
                    scope.rejectedPrecentage = Math.round(scope.rejectedPrecentage*100)/100;
                    scope.reviewedCount = result.total_reviewed;
                }, Common.onError(scope));
            }
            if (scope.type === "sme") {
                tasksCompleted = SME.tasksCompletedCount(params);

            }
            if (scope.type === "reviewer") {
                tasksCompleted = Reviewer.tasksCompletedCount(params);
                data = {reviewer_id: scope.model.id};
                Task.reviewer_total_reviewed(data).then(function(result) {
                    scope.reviewed_questions_count = result.reviewed_count;
                });
            }
            if(tasksCompleted){
                tasksCompleted.then(function(result) {
                    scope.completedTasks = result.objects;
                }, Common.onError(scope));
            }
        });
        scope.onSuccess  = function (course){
            _.remove(scope.model.courses, function(c) {
                var id;
                if(c.id){
                    id = c.id; 
                }
                else {
                    id = Common.uriToID(c);
                }
                return id === course.id;
            });
        };
        scope.remove = function (course) {
            if (!course) {
                return;
            }
            var model = {
                "course_id": course.id,
                "target_user": scope.model.user.id
            };
            scope.isLoading = true;
            if(scope.type === "author"){
                Course.unassignAuthor(model).then(scope.onSuccess(course), Common.onError(scope));    
            }
            else if(scope.type === "sme"){
                Course.unassignSME(model).then(scope.onSuccess(course), Common.onError(scope));    
            }
            else if(scope.type === "reviewer"){
                Course.unassignReviewer(model).then(scope.onSuccess(course), Common.onError(scope));    
            }
            else if(scope.type === "schoolteacher"){
                Course.unassignTeacher(model).then(scope.onSuccess(course), Common.onError(scope));    
            }
        };

    }

    angular.module("elproffApp")
    .directive("staffMember", function () {
        return {
            templateUrl: "views/directives/staff-member.html",
            restrict: "E",
            replace:true,
            scope: {
                model: "=",
                type: "@"
            },
            controller:
                ["$scope", "SME", "Author", "Reviewer", "Course", "Common",
                 "$modal", "$log", "Question", "Task", controller]
        };
    });
}).call(null);
