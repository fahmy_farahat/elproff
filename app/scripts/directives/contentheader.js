"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:contentHeader
 * @description
 * # contentHeader
 */
(function() {

    function controller(scope) {

        scope.closeAlert = function closeAlert(index) {
            scope.alerts = [];
        };

        scope.$watch("alerts", function() {
            // Scroll to the top of the page
            // Don"t change to jquery unless you test it 
            document.body.scrollTop = document.documentElement.scrollTop = 0;
        });
    }

    angular.module("elproffApp")
    .directive("contentHeader", function () {
        return {
            templateUrl: "views/directives/content-header.html",
            restrict: "E",
            transclude:true,
            controller: ["$scope", controller]
        };
    });
}).call(null);
