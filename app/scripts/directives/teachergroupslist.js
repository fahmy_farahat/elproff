"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:teacherGroupsList
 * @description
 * # teacherGroupsList
 */
(function() {

    function controller(scope) {

    }

    angular.module("elproffApp")
    .directive("teacherGroupsList", function () {
        return {
            templateUrl: "views/directives/teacher-groups-list.html",
            restrict: "E",
            controller: ["$scope", "$rootScope", controller]
        };
    });
}).call(null);
