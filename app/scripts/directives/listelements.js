"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:listelements
 * @description
 * # listelements
 */
(function() {


    function controller(scope, route, Common, CourseElement, CourseElementType) {
    	scope.model = {};

        scope.save = function save(element, item) {
            if(element.element_type.id === item.id){
                return;
            }
            element.element_type = item;
            element.save().then(function() {
                var index = _.findIndex(scope.elements, function(e) {
                    return e.id === element.id;
                });
                scope.elements.splice(index, 1);
            }, Common.onError(scope));
        };

        function load() {
            var query = {limit:0, q:""};
            scope.types = CourseElementType.getList(query).$object;
        }
        load();
    }

    angular.module("elproffApp")
    .directive("listelements", function () {
        return {
            templateUrl: "views/directives/listelements.html",
            restrict: "E",
            scope:{
                elements: "=",
                hiddenType: "="
            },
            controller: ["$scope", "$route", "Common", "CourseElement",
                        "CourseElementType", controller]
        };
    });
}).call(null);
