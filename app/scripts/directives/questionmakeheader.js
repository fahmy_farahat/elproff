"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:questionMakeHeader
 * @description
 * # questionMakeHeader
 */
(function() {

    function controller(scope, $timeout, $modal, Common) {

        
        // Time inputs validation
        $(".time-input").bind("change paste keyup", function() {
            var intVal = parseInt($(this).val());
            var maxNum = parseInt($(this).attr("max"));
            if (intVal) {
                if (maxNum && intVal > maxNum) {
                    scope.$parent.$parent.alerts = Common.alerts.error.max_value;
                    $(this).val(maxNum);
                }
            } else {
                scope.$parent.$parent.alerts = Common.alerts.error.not_valid_number;
                $(this).val(0);
            }
        });
    }

    angular.module("elproffApp")
        .directive("questionMakeHeader", function () {
            return {
                templateUrl: "views/directives/question-make-header.html",
                restrict: "E",
                controller: ["$scope", "$timeout", "$modal","Common", controller]
            };
        });
}).call(null);
