"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:CourseElementAttachmentListItem
 * @description
 * # CourseElementAttachmentListItem
 */
(function() {

    function postLink(marked) {
        return function(scope, element, attrs, ngModel) {
            if (scope.item.attachment_type === "text") {
                scope.item.preview = marked(scope.item.text || "");
            } else {
                scope.item.preview = '<img src="' + scope.item.attachment + '">';
            }
            scope.use = function() {
                scope.model.selected = scope.item;
            };
        };
    }

    angular.module("elproffApp")
    .directive("courseElementAttachmentListItem", ["marked", function (marked) {
        return {
            templateUrl: "views/directives/course-element-attachment-list-item.html",
            restrict: "E",
            scope: {
                item: "=",
                model: "=",
                remove: "&",
            },
            link: postLink(marked)
        };
    }]);
}).call(null);
