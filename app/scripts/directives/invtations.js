"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:invtations
 * @description
 * # invtations
 */
(function() {

    function controller(scope, Common, Invitation) {
        scope.crumbs = [
            {title: "dashboard", link: "#/" + scope.type + "/"},
            {title: "invitations", link: ""},
        ];
        function load() {
            scope.invitations = Invitation.getList(Common.query).$object;
        }
        load();
        scope.$on("relist", load);
        scope.timeOf = function timeOf(time) {
            if (time) {
                var timeFormat = moment.utc(time.creation_date);
                var res  = moment(timeFormat).format();
                return res;
            }
        };
        scope.hideAnswered = false;
        scope.sending = {};
        scope.alerts = [];
        scope.onSuccess = function onSuccess(result) {
            scope.sending["invitation_" + result.id] = false;
            scope.alerts = Common.alerts.success["invite_" + result.invitation_type];
        };

        scope.isSending = function isSending(invitation, value) {
            if (value) {
                scope.sending["invitation_" + invitation.id] = value;
            }
            return [scope.sending["invitation_" + invitation.id], invitation.id];
        };

        scope.onError = function onError(result) {
            if (result && result.id){
                scope.sending["invitation_" + result.id] = false;
            }
            if (result.error === "not_exist") {
                scope.alerts = Common.alerts.error.invitationnotexist;
            } else {
                scope.alerts = Common.alerts.error.generic;
            }
        };

        scope.resend = function resend(invitation) {
            var res = scope.isSending(invitation, true);
            Invitation.resend({id: res[1]}).then(this.onSuccess, Common.onError(scope, this.onError));
        };

        scope.delete_invitation = function delete_invitation(invitation) {
            invitation.remove().then(load, Common.onError(scope, this.onError));
        };

        scope.closeAlert = function(index) {
            scope.alerts = [];
        };
    }

    angular.module("elproffApp")
    .directive("invitations", function () {
        return {
            templateUrl: "views/directives/invitations.html",
            restrict: "E",
            scope: {
                type: "@"
            },
            controller: ["$scope", "Common", "Invitation", controller]
        };
    });
}).call(null);
