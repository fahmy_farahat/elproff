"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:QuestionsBulk
 * @description
 * # QuestionsBulk
 */
(function() {

    function controller(scope, QuestionsBulk, $modal) {

        scope.removeBulk = function removeBulk() {
            if (scope.model) {
                var bulkHashKey = scope.model.$$hashKey;
                _.each(scope.$parent.$parent.bulks, function(bulk, i){
                    if (bulk.$$hashKey === bulkHashKey){
                        scope.$parent.$parent.bulks.splice(i ,1);
                    }
                });
            }
        };

        scope.update = function() {
            var modalInstance = $modal.open({
                templateUrl: "views/modals/updatequestionsbulk.html",
                controller: "ModalsUpdateQuestionsbulkCtrl",
                size: "lg",
                resolve: {
                    bulk: function() {
                        return scope.model;
                    }
                }
            });

            modalInstance.result.then(function (bulk) {
                // Questions Bulks
                scope.$root.$broadcast("relist");
            });
        };

    }

    angular.module("elproffApp")
    .directive("questionsBulk", function () {
        return {
            templateUrl: "views/directives/questions_bulk.html",
            restrict: "E",
            scope: {
                bulks: "=",
                model: "=",
                index: "@",
            },
            controller: ["$scope", "QuestionsBulk", "$modal", controller]
        };
    });
}).call(null);
