"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:courseElementBulk
 * @description
 * # courseElementBulk
 */
(function() {

    function controller(scope, root) {
        scope.removeElement = function removeElement() {
            var elementId = scope.model.id;
            _.each(scope.$parent.$parent.elements, function(element, i){
                if (element.id === elementId){
                    scope.$parent.$parent.elements.splice(i ,1);
                }
            });
        };
    }

    angular.module("elproffApp")
    .directive("courseElement", function () {
        return {
            templateUrl: "views/directives/course-element.html",
            restrict: "E",
            scope: {
                model: "="
            },
            controller: ["$scope", "$rootScope", controller]
        };
    });
}).call(null);

