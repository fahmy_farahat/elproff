"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:nav
 * @description
 * # nav
 */
(function() {

    function controller(scope, cookies, Common, User, URLS, Lang, $location, Notifications, Profile, sock) {
        scope.urls = URLS;

        scope.changelang = function changelang(lang) {
            if (User.getUser()) {
                User.setLang(lang).then(function() {
                    User.getUser().lang = lang;
                    Lang.setLang(lang);
                }, Common.onError(scope));
            } else {
                Lang.setLang(lang);
            }
        };

        scope.logout = function logout() {

            function onSuccess(result) {
                // Facebook logout.
                // FB.logout();
                removeCookie();
                $location.path("/login");
                sock.close();
            }
            localStorage.removeItem("last_path");
            User.setUser(null);
            User.logout().then(onSuccess, Common.onError(scope));
        };

        var removeCookie = function removeCookie(){
            delete cookies.email;
            delete cookies.password;
            localStorage.clear();
        };

        scope.notificationsCount = 0 ;
        scope.$on("reload_notifications", _.throttle(function() {
            if (User.getUser()) {
                Notifications.getList({meta_only: true}).then(function(res) {
                    scope.notificationsCount = res.meta.total_count;
                });
                scope.loadProfiles();
            }
        }, 300)); // Reload only every 10 minutes
        /*** Note to Diaa: You told me to do so
             Try Again loading profile for one more time if failing 
        ***/
        // TODO: We should be using caching here
        scope.tryAgain = function tryAgain(error) { 
            scope.isLoading = false;
            Profile.getList().then(function(profiles) {
                scope.profiles = profiles;
            });

            Notifications.getList({meta_only: true}).then(function(res) {
                scope.notificationsCount = res.meta.total_count;
            });
        };
        scope.loadProfiles = function loadProfiles() {
            Profile.clearCache();
            Profile.getList().then(function(profiles) {
                scope.profiles = profiles;
                if (!scope.$root.ws_opened) {
                    scope.$root.setSockJS(profiles[0].channel, scope.$root.USER.user_id);
                }
            }, Common.onError(scope, scope.tryAgain));
        };

        scope.switchProfile = function switchProfile(profile) {
            if (profile !== scope.$root.USER.type) {
                Profile.switchProfile({"profile": profile}).then(function(result) {
                    User.setUser(result);
                    $location.path("/" + result.type);
                });
            }
        };

        scope.$on("user_updated", function() { 
            if (scope.$root.USER) {
                scope.TYPE = scope.$root.USER.type;
                scope.loadProfiles();
            }
        });

    }

    angular.module("elproffApp")
    .directive("navheader", function () {
        return {
            templateUrl: "views/directives/nav.html",
            restrict: "E",
            replace: true,
            controller: ["$scope", "$cookies", "Common", "User", "URLS", "Lang",
                         "$location", "Notifications", "Profile", "sock",
                         controller]
        };
    });
}).call(null);
