"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:report/examGrade
 * @description
 * # report/examGrade
 */
(function() {

    function controller(scope) {

    }

    angular.module("elproffApp")
    .directive("examGrade", function () {
        return {
            templateUrl: "views/directives/report/examgrade.html",
            restrict: "E",
            scope: {
                model: "="
            },
            controller: ["$scope", controller]
        };
    });
}).call(null);
