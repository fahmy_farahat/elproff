"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:report/examTime
 * @description
 * # report/examTime
 */
(function() {

    function controller(scope, $timeout) {

        var options4 = {
            scaleColor: false,
            trackColor: "#b5d033",
            barColor: "#e4eef0",
            lineWidth: 10,
            lineCap: "butt",
            size: 110
        };

        var examtimeChart = new EasyPieChart($(".circle4")[0], options4);
        // NEEDED TO INITIALIZE UPDATE
        $timeout(function() {examtimeChart.update(0);}, 10);

        var watcher2 = scope.$watch("time", onWatch);
        var watcher1 = scope.$watch("examtime", onWatch);
        function onWatch() {
            if (_.isUndefined(scope.time)) {
                return;
            }
            if (_.isUndefined(scope.examtime)) {
                return;
            }
            scope.examstars = 1;
            scope.timediff = (scope.time - scope.examtime) / 60;
            if (scope.timediff >= 0) {
                scope.elapsedtimepercentage = Math.floor(scope.examtime / scope.time * 100);
            } else {
                scope.elapsedtimepercentage = 0;
            }

            examtimeChart.update(scope.elapsedtimepercentage);

            watcher2();
            watcher1();
        }
    }

    angular.module("elproffApp")
    .directive("examTime", function () {
        return {
            templateUrl: "views/directives/report/examtime.html",
            restrict: "E",
            scope: {
                time: "=",
                examtime: "="
            },
            controller: ["$scope", "$timeout", controller]
        };
    });
}).call(null);
