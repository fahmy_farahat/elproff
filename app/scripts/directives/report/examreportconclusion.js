"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:report/examReportConclusion
 * @description
 * # report/examReportConclusion
 */
(function(){

  function controller(scope){

  }

  angular.module("elproffApp")
    .directive("examReportConclusion", function () {
      return {
        templateUrl: "views/directives/report/exam-report-conclusion.html",
        restrict: "E",
        controller: ["$scope", controller]
      };
    });

}).call(null);
