"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:report/questionsCount
 * @description
 * # report/questionsCount
 */
(function() {

    function controller(scope, $timeout) {
        var options = {
            scaleColor: false,
            trackColor: "#b5d033",
            barColor: "#e4eef0",
            lineWidth: 20,
            lineCap: "butt",
            size: 170
        };
        var options2 = {
            scaleColor: false,
            trackColor: "#dc6652",
            barColor: "#e4eef0",
            lineWidth: 20,
            lineCap: "butt",
            size: 128
        };
        // Creating new charts
        var correctChart = new EasyPieChart($(".correct")[0], options);
        var wrongChart = new EasyPieChart($(".wrong")[0], options2);
        // Initialize update is needed
        $timeout(function() {
            correctChart.update(0);
            wrongChart.update(0);
        }, 10);
        scope.$watch("model", function() {
            if (scope.model) {
                var correct = 100 - (scope.model.correct * 100 / scope.model.total);
                var wrong = 100 - (scope.model.wrong * 100 / scope.model.total);
                correctChart.update(correct);
                wrongChart.update(wrong);
            }
        });
    }

    angular.module("elproffApp")
    .directive("questionsCount", function () {
        return {
            templateUrl: "views/directives/report/questionscount.html",
            restrict: "E",
            scope: {
                model: "="
            },
            controller: ["$scope", "$timeout", controller]
        };
    });
}).call(null);
