"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:report/courseReportExamItem
 * @description
 * # report/courseReportExamItem
 */
(function() {

    function controller(scope, CourseElement) {
        var watcher = scope.$watch("model", function(examsList) {
            if (!scope.model) {
                return;
            } 
            watcher();
            scope.instances = examsList.reverse();
        }, true);

        scope.timeOf = function timeOf(time) {
            if (time) {
                var timeFormat = moment.utc(time);
                var res  = moment(timeFormat).format();
                return res;
            }
        };
    }

    angular.module("elproffApp")
        .directive("courseReportExamItem", function() {
            return {
                templateUrl: "views/directives/report/course-report-exam-item.html",
                restrict: "E",
                scope: {
                    model: "=",
                    courseid: "=",
                    studentid: "="
                },
                controller: ["$scope", "CourseElement", controller]
            };
        });
}).call(null);