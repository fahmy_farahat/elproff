"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:reports/recommendations
 * @description
 * # reports/recommendations
 */
(function() {

    function controller(scope, $root, params, ExamInstance, Common, Exam, AnswersDegree, CourseElement, CourseElementType) {

        function loadRecommendations() {

            function onExamSuccess(result) {
                if (result.no_recommended){
                    scope.no_recommended = true;
                } else {
                    scope.exam = ExamInstance.get(result.recommendation.exam).then(function(examInstanceResult){
                        Exam.getUri(examInstanceResult.exam).then(function(examResult){
                            scope.recommendedExam = examResult;
                            scope.recommendedExamInstance = examInstanceResult;
                        });
                    });
                    scope.practice = ExamInstance.get(result.recommendation.practice).then(function(practiceInstanceResult){
                        Exam.getUri(practiceInstanceResult.exam).then(function(practiceResult){
                            scope.recommendedPractice = practiceResult;
                            scope.recommendedPracticeInstance = practiceInstanceResult;
                        });
                    });
                    scope.study = CourseElement.get(result.recommendation.study).then(function(studyResult){
                        scope.recommendedStudy = studyResult;
                        CourseElementType.getUri(studyResult.element_type).then(function(studyElementType){
                            scope.recommendedStudy.element_type = studyElementType;
                        });
                    });
                }
            }

            var query = {
                instance_id: scope.instanceid,
            };

            ExamInstance.recommend(query).then(onExamSuccess, Common.onError(scope));
        }

        function init() {
            if (_.isUndefined(scope.instanceid) || scope.instanceid === "" ||
                    _.isUndefined(scope.answers) || scope.answers === "") {
                return;
            }
            
            loadRecommendations()

            examWatcher();
            answersWatcher();
        }
        scope.courseid = params.courseid;
        var examWatcher = scope.$watch("instanceid", init);
        var answersWatcher = scope.$watch("answers", init);
    }

    angular.module("elproffApp")
        .directive("examRecommendations", function() {
            return {
                templateUrl: "views/directives/report/recommendations.html",
                restrict: "E",
                scope: {
                    instanceid: "@",
                    model: "=",
                    answers: "="
                },
                controller: ["$scope", "$rootScope", "$routeParams", "ExamInstance", "Common",
                            "Exam", "AnswersDegree", "CourseElement", "CourseElementType", controller]
            };
        });
}).call(null);
