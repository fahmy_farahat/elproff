"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:report/courseTotalReportExam
 * @description
 * # report/courseTotalReportExam
 */
(function(){

    function controller(scope, ExamInstance, Common) {

        scope.reportHours   = "00";
        scope.reportMinutes = "00";
        scope.reportSeconds = "00";
        scope.reportDays = "0";

        scope.onSuccess = function onSuccess(result) {
            scope.loaded = true;
            scope.model = result;
            scope.model.max_degree *=100;
            scope.model.min_degree *=100;
            
            // Declaring regex for the valid date charachters and we will use later
            var dateValidCharachtersRegex = new RegExp("[^0-9:.]", "g");
            // Extracts any extra values than time ex. "Days"
            var totalDuration = result.total_duration.split(",");
            // If Duration contains days
            if (totalDuration.length > 1 || /days/.test(totalDuration)) {
                scope.reportDays = totalDuration.shift();
                // Removing any extra unwanted charachters to extract just numbers
                scope.reportDays = scope.reportDays.replace(dateValidCharachtersRegex, "");
            }
            var totalTime = totalDuration.pop();
            // Removing any extra unwanted charachters in time
            totalTime = totalTime.replace(dateValidCharachtersRegex, "");
            // Spliting time elements (hours,mins,seconds)
            totalTime = totalTime.split(":");
            // Setting the time elements
            scope.reportHours   = totalTime[0];
            scope.reportMinutes = totalTime[1];
            scope.reportSeconds = Math.floor(totalTime[2]); // Seconds comes in fractional form (float), we need an Integer
        };

        scope.onError = function onError(error) {
            console.log(error);
            if (error.data.error === "not_exist") {
                scope.alerts = Common.alerts.error.parent_son_noexams;
            }
        };

        var query = {"course_id": scope.course};
        if (scope.student) {
            query.student_id = scope.student
        }
        ExamInstance.totals(query).then(scope.onSuccess, Common.onError(scope, scope.onError));
    }

    angular.module("elproffApp")
        .directive("courseTotalReportExam", function () {
        return {
            templateUrl: "views/directives/report/course-total-report-exam.html",
            restrict: "E",
            scope: {
                course: "=",
                student: "=" // Used by parent
            },
            controller: ["$scope", "ExamInstance", "Common", controller]
      };
    });
}).call(null);
