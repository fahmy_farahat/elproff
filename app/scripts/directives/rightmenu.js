"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:rightMenu
 * @description
 * # rightMenu
 */
(function() {

    function controller(scope, User) {

        function init() {
            var user = User.getUser();
            if (!user) {
                return;
            }
            var type = user.type;
            
            var base = "#/" + type;
            scope.links = [{
                name: "changepassword",
                title: "changepassword",
                href: base + "/profile/changepassword"
            }, {
                name: "editprofile",
                title: "editprofile",
                href: base +  "/profile/edit"
            }, {
                name: "view",
                title: "profile",
                href: base + "/profile"
            }];

            if (type === "student") {
                scope.links.push({
                    name: "addparent",
                    title: "addparent",
                    href: base + "/profile/addparent"
                });
            }

            if (type === "schooladmin") {
                scope.links.push({
                    name: "school",
                    title: "school",
                    href: base + "/profile/school/" + user.school
                });
            }

        }

        scope.$on("user_updated", init);
        init();

    }

    angular.module("elproffApp")
    .directive("rightMenu", function () {
        return {
            templateUrl: "views/directives/right-menu.html",
            restrict: "E",
            scope: {
                current: "@"
            },
            controller: ["$scope", "User", controller]
        };
    });
}).call(null);
