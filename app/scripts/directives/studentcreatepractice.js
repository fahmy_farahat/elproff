"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:studentCreatePractice
 * @description
 * # studentCreatePractice
 */
(function() {

    function controller(scope, Exam) {

    }

    angular.module("elproffApp")
        .directive("studentCreatePractice", function() {
            return {
                templateUrl: "views/directives/student-create-practice.html",
                restrict: "E",
                controller: ["$scope", "Exam", controller]
            };
        });
}).call(null);
