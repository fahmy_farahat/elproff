"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:teacherExamReport
 * @description
 * # teacherExamReport
 */
(function() {

    function controller(scope) {

    }

    angular.module("elproffApp")
    .directive("teacherExamReport", function () {
        return {
            templateUrl: "views/directives/teacher-exam-report.html",
            restrict: "E",
            controller: ["$scope", "$rootScope", controller]
        };
    });
}).call(null);
