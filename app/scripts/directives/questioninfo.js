"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:questionInfo
 * @description
 * # questionInfo
 */
(function() {

    function controller(scope, Restangular) {
        scope.model = {};
        scope.$watch("uri", function() {
            if (scope.uri) {
                scope.model = Restangular.oneUrl("question", scope.uri).get().$object;
                console.log(scope.model);
            }
        });

    }

    angular.module("elproffApp")
    .directive("questionInfo", function () {
        return {
            templateUrl: "views/directives/question-info.html",
            restrict: "E",
            scope: {
                uri: "@",
                taskid: "@",
                courseid: "@",
            },
            controller: ["$scope", "Restangular", controller]
        };
    });
}).call(null);
