"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:customEditor
 * @description
 * # helpPageContent
 */
(function() {

    function controller(scope, $timeout, $modal, Common, CustomEditor) {

        //scope.model = {};
        if(scope.questionModel) {
            scope.model = scope.questionModel;
        }
        if(!scope.model){
            scope.model = {};
        }
        if(!scope.editorId) {
            scope.editorId = "";
        }
        $timeout(function() {
            var btnsConfiguration;
            if(scope.attachmentRelation === "help") {
                btnsConfiguration = [[CustomEditor.colorsBtns(),
                                      CustomEditor.imageAttachmentBtns("help"),
                                      CustomEditor.equationAttachmentBtn("help")]];

            } else if(scope.attachmentRelation === "course") {
                btnsConfiguration = [[CustomEditor.colorsBtns(),
                                      CustomEditor.imageAttachmentBtns("course", scope.questionModel),
                                      CustomEditor.equationAttachmentBtn("course", scope.questionModel),
                                      CustomEditor.courseElementAttachment(scope.questionModel)]];
            } else if(scope.attachmentRelation === "question") {
                btnsConfiguration = [[CustomEditor.colorsBtns(),
                                      CustomEditor.imageAttachmentBtns("question"),
                                      CustomEditor.equationAttachmentBtn("question")]];                
            }            
            $("#customEditor" + scope.editorId).markdown({
                autofocus:false,
                savable:false,
                onChange: function(e){
                    scope.$apply(function() {
                        scope.model.description = e.getContent();
                    });
                },
                hiddenButtons: ["cmdUrl","cmdImage"],
                additionalButtons: btnsConfiguration
            });
        });
    }

    angular.module("elproffApp")
    .directive("customEditor", function () {
        return {
            templateUrl: "views/directives/custom-editor.html",
            restrict: "E",
            scope: {
                attachmentRelation: "@",
                questionModel: "=",
                editorId: "@",
            },
            controller: [
                "$scope", "$timeout", "$modal","Common", "CustomEditor",
                controller]
        };
    });
}).call(null);
