"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/phone
 * @description
 * # input/phone
 */
(function() {

    function controller(scope) {

    }

    angular.module("elproffApp")
    .directive("inputPhone", function () {
        return {
            templateUrl: "views/directives/input/phone.html",
            restrict: "E",
            controller: ["$scope", controller]
        };
    });
}).call(null);
