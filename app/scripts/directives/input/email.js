"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/email
 * @description
 * # input/email
 */
(function() {

    function controller(scope) {

    }

    angular.module("elproffApp")
    .directive("inputEmail", function () {
        return {
            templateUrl: "views/directives/input/email.html",
            restrict: "E",
            controller: ["$scope", controller]
        };
    });
}).call(null);
