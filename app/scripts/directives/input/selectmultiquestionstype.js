"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/SelectMultiQuestionsType
 * @description
 * # input/SelectMultiQuestionsType
 */
(function() {

    function controller(scope, Service) {
        scope.$on("lang_updated", function() {
            scope.types = QuestionType.getEntities();
        });
        if (!scope.types){
            scope.types = Service.getEntities();
        }
    }
    angular.module("elproffApp")
    .directive("inputSelectMultiQuestionsType", function () {
        return {
            templateUrl: "views/directives/input/select-multi-questions-type.html",
            restrict: "E",
            controller: ["$scope", "QuestionType", controller]
        };
    });
}).call(null);
