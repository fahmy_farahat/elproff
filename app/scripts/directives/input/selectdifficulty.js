"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/selectDifficulty
 * @description
 * # input/selectDifficulty
 */
(function() {

    function controller(scope, Difficulty) {

        scope.$on("lang_updated", function() {
            scope.difficulties = Difficulty.getEntities();
        });

        scope.difficulties = Difficulty.getEntities();
        var watcher = scope.$watch("model", function() {
            if (!scope.model) {
                return;
            }
            if (!scope.model.difficulty) {
                scope.model.difficulty = scope.difficulties[0];
            }
            watcher();
        });
    }

    angular.module("elproffApp")
    .directive("inputSelectDifficulty", function () {
        return {
            templateUrl: "views/directives/input/select-difficulty.html",
            restrict: "E",
            controller: ["$scope", "Difficulty", controller]
        };
    });
}).call(null);
