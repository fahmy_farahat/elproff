"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/gender
 * @description
 * # input/gender
 */
(function() {

    function controller(scope) {

    }

    angular.module("elproffApp")
    .directive("inputGender", function () {
        return {
            templateUrl: "views/directives/input/gender.html",
            restrict: "E",
            controller: ["$scope", controller]
        };
    });
}).call(null);
