"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/selectCoursePrivateTeacher
 * @description
 * # input/selectCoursePrivateTeacher
 */
(function() {

    function controller(scope, Course , PrivateTeacher, CommonCache) {
        function load() {
            PrivateTeacher.getCourse().then(function(result) {
                scope.courses = result.objects;
            });
        }
        load();
    }

    angular.module("elproffApp")
        .directive("inputSelectCoursePrivateTeacher", function () {
            return {
                templateUrl: "views/directives/input/select-course-privateteacher.html",
                restrict: "E",
                controller: ["$scope", "Course", "PrivateTeacher", "CommonCache", controller]
            };
        });
}).call(null);
