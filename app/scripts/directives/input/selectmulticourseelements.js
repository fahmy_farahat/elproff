"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/selectMultiCourseElements
 * @description
 * # input/selectMultiCourseElements
 */
(function() {

    function controller(scope, CourseElement, CommonCache) {
        scope.elements = [];
        scope.responseCourseElementsStatus = "";
        var load = _.debounce(function () {
            if (!scope.model.course && (!scope.model.courses || !scope.model.courses.length)) {
                return;
            }
            // From the controller scope
            var params = {limit: 0};
            if (scope.model.course) {
                params.course = scope.model.course.id;
            } else if (scope.model.courses && _.isArray(scope.model.courses) && scope.model.courses.length) {
                var coursesIDs = _.compact(_.pluck(_.compact(scope.model.courses), "id"));
                if (coursesIDs.length) {
                    params.course__in = coursesIDs.join(",");
                }
            } else if (scope.model.grade) {
                params.course__grade = scope.model.grade.id;
            }
            scope.responseCourseElementsStatus = "loading-result";
            CourseElement.getList(params).then(function(elements) {
                scope.elements = _.map(elements, CommonCache.element);
                if (scope.elements.length) {
                    scope.responseCourseElementsStatus = "success-result";
                } else {
                    scope.responseCourseElementsStatus = "error-result";
                }
            }, function(){
                scope.responseCourseElementsStatus = "error-result";
            });
        }, 1000);
        load();
        scope.$watch("model.courses", load);
        scope.$watch("model.course", load);
    }

    angular.module("elproffApp")
    .directive("inputSelectMultiCourseElements", function () {
        return {
            templateUrl: "views/directives/input/select-multi-course-elements.html",
            restrict: "E",
            controller: ["$scope", "CourseElement", "CommonCache", controller]
        };
    });
}).call(null);
