"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/inputFees
 * @description
 * # input/inputFees
 */
(function() {

    function controller(scope) {

    }

    angular.module("elproffApp")
    .directive("inputFees", function () {
        return {
            templateUrl: "views/directives/input/fees.html",
            restrict: "E",
            controller: ["$scope", controller]
        };
    });
}).call(null);
