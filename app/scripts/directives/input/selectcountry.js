"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/selectCountry
 * @description
 * # input/selectCountry
 */
(function() {

    function controller(scope, Country) {
        scope.responseCountryStatus = "loading-result";
        Country.getList({limit: 0}).then(function(countries){
            scope.countries = countries;
            if (scope.countries.length){
                scope.responseCountryStatus = "success-result";
            } else {
                scope.responseCountryStatus = "error-result";    
            }
        }, function(){
            scope.responseCountryStatus = "error-result";
        });
    }

    angular.module("elproffApp")
    .directive("inputSelectCountry", function () {
        return {
            templateUrl: "views/directives/input/select-country.html",
            restrict: "E",
            controller: ["$scope", "Country", controller]
        };
    });
}).call(null);
