"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/name
 * @description
 * # input/name
 */
(function() {

    function controller(scope) {

    }

    angular.module("elproffApp")
    .directive("inputName", function () {
        return {
            templateUrl: "views/directives/input/name.html",
            restrict: "E",
            controller: ["$scope", controller]
        };
    });
}).call(null);
