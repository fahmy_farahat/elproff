"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/selectCourseElement
 * @description
 * # input/selectCourseElement
 */
(function() {

    function controller(scope, CourseElement, CommonCache) {
        scope.elements = [];
        var load = _.debounce(function () {
            scope.responseCourseElementStatus = "";
            if (!scope.model.course && (!scope.model.courses || !scope.model.courses.length)) {
                return;
            }
            // From the controller scope
            var params = {limit: 0};
            if (scope.model.course) {
                params.course = scope.model.course.id;
            }
            scope.responseCourseElementStatus = "loading-result";
            CourseElement.getList(params).then(function(elements) {
                scope.elements = _.map(elements, CommonCache.element);
                if (scope.elements.length){
                    scope.responseCourseElementStatus = "success-result";
                } else {
                    scope.responseCourseElementStatus = "error-result";
                }
            }, function(){
                scope.responseCourseElementStatus = "error-result";
            });
        }, 1000);
        load();
        scope.$watch("model.course", load);
    }

    angular.module("elproffApp")
    .directive("inputSelectCourseElement", function () {
        return {
            templateUrl: "views/directives/input/select-course-element.html",
            restrict: "E",
            controller: ["$scope", "CourseElement", "CommonCache", controller]
        };
    });
}).call(null);
