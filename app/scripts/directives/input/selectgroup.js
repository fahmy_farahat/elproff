"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/SelectGroup
 * @description
 * # input/SelectGroup
 */
(function () {

    function controller(scope, Group, CommonCache) {

        function load() {
            scope.responseGroupStatus = "";
            if (scope.model.course) {
                scope.responseGroupStatus = "loading-result";
                var data = {limit:0, course:scope.model.course.id};
                Group.getList(data).then(function (groups) {
                    scope.groups = _.map(groups, CommonCache.group);
                    console.log(scope.groups);
                    if (scope.groups.length){
                        scope.responseGroupStatus = "success-result";
                    } else {
                        scope.responseGroupStatus = "error-result";    
                    }
                }, function(){
                    scope.responseGroupStatus = "error-result";
                });
            } 
        }

        // load();
        scope.$watch("model.course", load);
    }

    angular.module("elproffApp")
        .directive("inputSelectGroup", function () {
            return {
                templateUrl: "views/directives/input/select-group.html",
                restrict: "E",
                controller: ["$scope", "Group", "CommonCache", controller]
            };
        });
}).call(null);
