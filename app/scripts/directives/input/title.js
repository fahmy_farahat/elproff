"use strict";

/**
 * @ngdoc directive
 * @title elproffApp.directive:inputTitle
 * @description
 * # inputTitle
 */
(function() {

    function controller(scope) {

    }

    angular.module("elproffApp")
    .directive("inputTitle", function () {
        return {
            templateUrl: "views/directives/input/title.html",
            restrict: "E",
            controller: ["$scope", controller]
        };
    });
}).call(null);
