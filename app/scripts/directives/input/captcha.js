"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/captcha
 * @description
 * # input/captcha
 */
(function() {

    function controller(scope) {

    }

    angular.module("elproffApp")
    .directive("inputCaptcha", function () {
        return {
            templateUrl: "views/directives/input/captcha.html",
            restrict: "E",
            controller: ["$scope", controller]
        };
    });
}).call(null);
