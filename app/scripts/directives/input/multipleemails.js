"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:multipleEmails
 * @description
 * # multipleEmails
 */
(function() {

    function controller(scope) {}
      angular.module("elproffApp")
        .directive("multipleEmails", function () {
            return {
                templateUrl: "views/directives/input/multiple-emails.html",
                restrict: "E",
                controller: ["$scope", controller]
            };
        });
}).call(null);
