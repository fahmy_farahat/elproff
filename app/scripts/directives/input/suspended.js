"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/inputSuspended
 * @description
 * # input/inputSuspended
 */
(function() {

    function controller(scope) {

    }

    angular.module("elproffApp")
    .directive("inputSuspended", function () {
        return {
            templateUrl: "views/directives/input/suspended.html",
            restrict: "E",
            controller: ["$scope", controller]
        };
    });
}).call(null);
