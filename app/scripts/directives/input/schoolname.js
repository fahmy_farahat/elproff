"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/schoolname
 * @description
 * # input/schoolname
 */
(function() {

    function controller(scope) {

    }

    angular.module("elproffApp")
    .directive("inputSchoolname", function () {
        return {
            templateUrl: "views/directives/input/schoolname.html",
            restrict: "E",
            controller: ["$scope", controller]
        };
    });
}).call(null);
