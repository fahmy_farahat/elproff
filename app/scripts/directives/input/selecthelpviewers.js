"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/selecthelpviewers
 * @description
 * # input/selecthelpviewers
 */
(function() {

    function controller(scope, CommonCache) {

        var load = _.debounce(function () {
            // From the controller scope
            var params = {limit: 0};
            if (!scope.model.viewers) {
                scope.model.viewers = [];
            }
            if (!scope.viewers) {
                scope.viewers = ["Author", "SME", "Reviewer","Student"];
            }
        }, 1000);
        load();
    }

    angular.module("elproffApp")
        .directive("inputSelectHelpViewers", function () {
            return {
                templateUrl: "views/directives/input/select-help-viewers.html",
                restrict: "E",
                controller: ["$scope", "CommonCache", controller]
            };
        });
}).call(null);