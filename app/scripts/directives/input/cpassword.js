"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:inputs/cpassword
 * @description
 * # inputs/cpassword
 */
(function() {

    function controller(scope) {
    }

    angular.module("elproffApp")
    .directive("inputCpassword", function () {
        return {
            templateUrl: "views/directives/input/cpassword.html",
            restrict: "E",
            controller: ["$scope", controller]
        };
    });
}).call(null);
