"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/selectMultiEducationalSystems
 * @description
 * # input/selectMultiEducationalSystems
 */
(function() {

    function controller(scope, EducationalSystem, CommonCache) {

        scope.systems = [];
        function load() {
            if (!scope.model.country && (!scope.model.countries || !scope.model.countries.length)) {
                return;
            }
            // From the controller scope
            var params = {limit: 0};
            if (scope.model.country) {
                params = {"country": scope.model.country.id};
            }
            EducationalSystem.getList(params).then(function(systems) {
                scope.systems = systems;
            });
        }

        load();
        scope.$watch("model.country", load);
    }

    angular.module("elproffApp")
    .directive("inputSelectMultiEducationalSystems", function () {
        return {
            templateUrl: "views/directives/input/select-multi-educational-systems.html",
            restrict: "E",
            controller: ["$scope", "EducationalSystem", "CommonCache", controller]
        };
    });
}).call(null);
