"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/selectEducationalSystem
 * @description
 * # input/selectEducationalSystem
 */
(function() {

    function controller(scope, EducationalSystem, CommonCache) {
        scope.responseSystemStatus = "";
        var load = _.debounce(function () {
            if (!scope.model.country) {
                return;
            }
            // From the controller scope
            var params = {limit: 0}
            if (scope.model.country) {
                scope.responseSystemStatus = "loading-result";
                params.country = scope.model.country.id;
            } else {
                scope.model.system = null;
            }
            EducationalSystem.getList(params).then(function(systems) {
                scope.systems = _.map(systems, CommonCache.system);
                if (scope.systems.length){
                    scope.responseSystemStatus = "success-result";
                } else {
                    scope.responseSystemStatus = "error-result";    
                }
            }, function(){
                scope.responseSystemStatus = "error-result";
            });
        }, 1000);
        load();
        scope.$watch("model.country", load);
    }

    angular.module("elproffApp")
    .directive("inputSelectEducationalSystem", function () {
        return {
            templateUrl: "views/directives/input/select-educational-system.html",
            restrict: "E",
            controller: ["$scope", "EducationalSystem", "CommonCache", controller]
        };
    });
}).call(null);
