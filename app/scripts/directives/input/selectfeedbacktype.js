"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/SelectFeedbackType
 * @description
 * # input/SelectFeedbackType
 */

(function() {

    function controller(scope, FeedbackType) {

        scope.$on("lang_updated", function() {
            scope.feedbackTypes = FeedbackType.getEntities();
        });

        function load(){
            if (!scope.model) {
                return;
            }
            if (!scope.feedbackTypes) {
                scope.feedbackTypes = FeedbackType.getEntities();
            }
            if (!scope.model.feedback_type){      
                scope.model.feedback_type = scope.feedbackTypes[0];
            }
        }
        load();
        scope.$watch("model.feedback_type", load);
    }

    angular.module("elproffApp")
    .directive("inputSelectFeedbackType", function () {
        return {
            templateUrl: "views/directives/input/select-feedback-type.html",
            restrict: "E",
            controller: ["$scope", "FeedbackType", controller]
        };
    });
}).call(null);

