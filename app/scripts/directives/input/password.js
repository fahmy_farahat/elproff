"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:inputs/password
 * @description
 * # inputs/password
 */
(function() {

    function controller(scope) {
    }

    angular.module("elproffApp")
    .directive("inputPassword", function () {
        return {
            templateUrl: "views/directives/input/password.html",
            restrict: "E",
            controller: ["$scope", controller]
        };
    });
}).call(null);
