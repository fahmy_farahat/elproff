"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/inputSelectTerm
 * @description
 * # input/inputSelectTerm
 */
(function() {

    function controller(scope, Service) {
        var load = _.debounce(function () {
            var params = {limit: 0};
            if (!scope.model.term) {
                scope.model.term = null;
            }
            if (!scope.terms) {
                scope.terms = Service.getList(params).$object;
            }
        }, 1000);
        load();
    }

    angular.module("elproffApp")
    .directive("inputSelectTerm", function () {
        return {
            templateUrl: "views/directives/input/select-term.html",
            restrict: "E",
            controller: ["$scope", "SubscriptionTerm", controller]
        };
    });
}).call(null);
