"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:inputs/fullname
 * @description
 * # inputs/fullname
 */
(function() {

    function controller(scope) {
    }

    angular.module("elproffApp")
    .directive("inputFullname", function () {
        return {
            templateUrl: "views/directives/input/fullname.html",
            restrict: "E",
            controller: ["$scope", controller]
        };
    });
}).call(null);
