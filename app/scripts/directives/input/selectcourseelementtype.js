"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/selectCourseElementType
 * @description
 * # input/selectCourseElementType
 */
(function() {

    function controller(scope, Common, CourseElementType) {
        var load = _.debounce(function () {
            // From the controller scope
            var params = {limit: 0};
            if (scope.model.grade) {
                params.grade = scope.model.grade.id;
            }
            if (!scope.model.course){
                params.course = null;
            }
            if (!scope.types){
                scope.types = CourseElementType.getList(params).$object;
            }
        }, 1000);
        load();
        scope.$watch("model.course", load);
    }

    angular.module("elproffApp")
    .directive("inputSelectCourseElementType", function () {
        return {
            templateUrl: "views/directives/input/select-course-element-type.html",
            restrict: "E",
            controller: ["$scope", "Common", "CourseElementType", controller]
        };
    });
}).call(null);
