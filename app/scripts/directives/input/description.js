"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/description
 * @description
 * # input/description
 */
(function() {

    function controller(scope) {

    }

    angular.module("elproffApp")
    .directive("inputDescription", function () {
        return {
            templateUrl: "views/directives/input/description.html",
            restrict: "E",
            controller: ["$scope", controller]
        };
    });
}).call(null);
