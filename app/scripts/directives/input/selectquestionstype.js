"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/selectQuestionsType
 * @description
 * # input/selectQuestionsType
 */
(function() {

    function controller(scope, QuestionType) {
        scope.$on("lang_updated", function() {
            scope.types = QuestionType.getEntities();
        });

        scope.types = QuestionType.getEntities();

        var watcher = scope.$watch("model", function() {
            if (!scope.model) {
                return;
            }
            if (!scope.model.difficulty) {
                scope.model.questions_type = scope.types[0];
            }
            watcher();
        });
    }

    angular.module("elproffApp")
    .directive("inputSelectQuestionsType", function () {
        return {
            templateUrl: "views/directives/input/select-questions-type.html",
            restrict: "E",
            controller: ["$scope", "QuestionType", controller]
        };
    });
}).call(null);
