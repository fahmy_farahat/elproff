"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/selectDifficultyGauge
 * @description
 * # input/selectDifficultyGauge
 */
(function() {

    var opts = {
        lines: 12,        // The number of lines to draw
        angle: 0,         // The length of each line
        lineWidth: 0.190, // The line thickness
        pointer: {
            length: 0.36,     // The radius of the inner circle
            strokeWidth: 0.1, // The rotation offset
            color: "#dadada"  // Fill color
        },
        limitMax: false,        // If true, the pointer will not go past the end of the gauge
        colorStart: "#a3c300",  // Colors
        colorStop: "#ff3d2f",   // just experiment with them
        strokeColor: "#E0E0E0", // to see which ones work best for you
        generateGradient: true
    };

    function controller(scope, Difficulty, $timeout) {

        scope.$on("lang_updated", function() {
            scope.difficulties = Difficulty.getEntities();
        });

        scope.difficulties = Difficulty.getEntities();
        scope.model.difficulty = scope.difficulties[0];

        $timeout(function() {
            var target = document.getElementById("gauge");  // your canvas element
            var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
            gauge.maxValue = 332;                           // set max gauge value
            gauge.animationSpeed = 20;                      // set animation speed (32 is default value)
            gauge.set(20);                                  // set actual value
            $("#canvas-overlay path").click(function (e) {
                var parentOffset = $(this).offset();
                //or $(this).offset(); if you really just want the current element"s offset
                var relX = e.pageX - parentOffset.left +20;
                if(relX >= 278) {
                    relX = 332;
                }
                gauge.set(relX);
            });
        }, 100);
    }

    angular.module("elproffApp")
    .directive("inputSelectDifficultyGauge", function () {
        return {
            templateUrl: "views/directives/input/select-difficulty-gauge.html",
            restrict: "E",
            controller: ["$scope", "Difficulty", "$timeout", controller]
        };
    });
}).call(null);
