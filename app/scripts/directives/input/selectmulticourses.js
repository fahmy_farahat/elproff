"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/selectMultiCourses
 * @description
 * # input/selectMultiCourses
 */
(function() {

    function controller(scope, Course, CommonCache) {

        scope.courses = [];
        scope.responseCoursesStatus = "";
        var load = _.debounce(function () {
            // From the controller scope
            var params = {limit: 0};
            if (scope.model.grades) {
                params.grade__in = _.pluck(scope.model.grades, "id");
            } 
            if (scope.model.grade) {
                params.grade = scope.model.grade.id;
            }
            if (!scope.model.courses) {
                scope.model.courses = [];
            }
            if (!scope.courses.length) {
                scope.responseCoursesStatus = "loading-result";
                Course.getList(params).then(function(courses) {
                    scope.courses = _.map(courses, CommonCache.course);
                    if (scope.courses.length) {
                        scope.responseCoursesStatus = "success-result";
                    } else {
                        scope.responseCoursesStatus = "error-result";
                    }
                });
            }
        }, 1000);
        load();
        scope.$watch("model.grade", load);
        scope.$watch("model.grades", load);
    }

    angular.module("elproffApp")
        .directive("inputSelectMultiCourses", function () {
            return {
                templateUrl: "views/directives/input/select-multi-courses.html",
                restrict: "E",
                controller: ["$scope", "Course", "CommonCache", controller]
            };
        });
}).call(null);
