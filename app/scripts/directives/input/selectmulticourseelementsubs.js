"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/selectMultiCourseElementSubs
 * @description
 * # input/selectMultiCourseElementSubs
 */
(function() {

    function controller(scope, params, CourseElement, CommonCache) {
        scope.levels = [];
        if(!scope.selectedLevels) {
            scope.selectedLevels = [];    
        }
        var watcher = scope.$watch("course", function() {
            if (!scope.course) {
                return;
            }
            var query = {parent: "None", course: params.courseid, limit: 0};
            CourseElement.getList(query).then(function(elements) {
                scope.levels.unshift(_.map(elements, CommonCache.element));
            });
            watcher();
        });

        scope.levelChanged = function(index) {
            scope.levels = _.take(scope.levels, index + 1);
            scope.selectedLevels = _.take(scope.selectedLevels, index + 1);
        };

        scope.$watchCollection("selectedLevels", function() {
            _.each(scope.selectedLevels, function(level, index) {
                if (level) {
                    // Add a new one
                    var newIndex = index + 1; 
                    if (!scope.levels.length) {
                        newIndex = scope.levels.length;
                    }
                    scope.levels[newIndex] = [];
                    _.each(level.children, function(childUri) {
                        if (!_.isObject(childUri)) {
                            childUri = CourseElement.getUri(childUri).$object;
                        }
                        scope.levels[newIndex].push(childUri);
                    });
                }
            });
            // Setting the model
            if(scope.selectedLevels && scope.selectedLevels.length) {
                scope.model.elements = [_.last(_.compact(scope.selectedLevels))];    
            }
            
        });
    }

    angular.module("elproffApp")
    // input-select-multi-course-element-subs
    .directive("inputSelectMultiCourseElementSubs", function () {
        return {
            templateUrl: "views/directives/input/selectmulticourseelementsubs.html",
            restrict: "E",
            controller: ["$scope", "$routeParams", "CourseElement",
                         "CommonCache", controller]
        };
    });
}).call(null);
