"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/selectCourseElementParent
 * @description
 * # input/selectCourseElementParent
 */
(function() {

    function controller(scope, CourseElement, CommonCache) {

        var modelWatcher  = scope.$watch("model", handler);

        function handler() {
            if (scope.elements && scope.elements.length) {
                return;
            }
            // Testing that the model is retrieved fully
            if (scope.model && scope.model.name) {
               if (scope.model.parent) {
                    scope.model.parent = _.find(scope.elements, {
                        resource_uri: scope.model.parent
                    });
                }
                modelWatcher();
            }
        }

        var load = _.debounce(function () {
            if (!scope.model.course) {
                return;
            }
            // From the controller scope
            var params = {limit: 0};
            if (scope.model.course) {
                params.course = scope.model.course.id;
            }
            CourseElement.getList(params).then(function(elements) {
                scope.elements = elements;
            });
        }, 1000);
        load();
        scope.$watch("model.course", load);
    }

    angular.module("elproffApp")
    .directive("inputSelectCourseElementParent", function () {
        return {
            templateUrl: "views/directives/input/select-course-element-parent.html",
            restrict: "E",
            controller: ["$scope", "CourseElement", "CommonCache", controller]
        };
    });
}).call(null);
