"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/birthdate
 * @description
 * # input/birthdate
 */
(function() {

    function controller(scope, Student) {

        scope.maxDate = new Date();
        scope.open = function() {
            event.preventDefault();
            event.stopPropagation();
            scope.opened = true;
        };

        scope.format = "dd-MMMM-yyyy";
    }

    angular.module("elproffApp")
    .directive("inputBirthdate", function () {
        return {
            templateUrl: "views/directives/input/birthdate.html",
            restrict: "E",
            controller: ["$scope", "Student", controller]//,
        };
    });
    
}).call(null);
