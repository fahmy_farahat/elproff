"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/selectMultiGrades
 * @description
 * # input/selectMultiGrades
 */
(function() {

    function controller(scope, Grade, CommonCache) {
        scope.grades = [];
        var load = _.debounce(function () {
            
            if (!scope.model.system && (!scope.model.systems || !scope.model.systems.length)) {
                return;
            }
            // From the controller scope
            var params = {limit: 0};
            if (scope.model.system) {
                params.system = scope.model.system.id;
            }
            if (scope.model.systems) {
                params.system__in = _.pluck(scope.model.systems, "id");
            }
            Grade.getList(params).then(function(grades) {
                scope.grades = grades;
            });
        }, 1000);
        load();
        scope.$watch("model.system", load);
        scope.$watch("model.systems", load);
    }

    angular.module("elproffApp")
    .directive("inputSelectMultiGrades", function () {
        return {
            templateUrl: "views/directives/input/select-multi-grades.html",
            restrict: "E",
            controller: ["$scope", "Grade", "CommonCache", controller]
        };
    });
}).call(null);
