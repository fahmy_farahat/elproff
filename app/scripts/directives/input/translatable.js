"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/transName
 * @description
 * # input/transName
 */
(function() {

    function controller(scope) {
        var modelWatcher, parentModelExistanceWatcher;
        scope.setLang = function setLang(lang) {
            if (modelWatcher) {
                modelWatcher();
            }
            scope.lang = lang;
            if (lang != "") {
                scope.$parent.model[scope.modelName] = scope.$parent.model[scope.modelName + "_" + lang];
            }
            function modelWatcherFn() {
                var model = scope.$parent.model;
                var field = scope.modelName;
                if (lang != "") {
                    model[field + "_" + lang] = model[field];
                }
            }
            modelWatcher = scope.$parent.$watch("model." + scope.modelName, modelWatcherFn);
            modelWatcherFn();
        };
        /*
         * Wait until the parent model is populated
         */
        function load() {
            if (!scope.$parent.model) {
                return;
            }
            parentModelExistanceWatcher();
            scope.setLang(scope.$root.USER.lang);
        }
        parentModelExistanceWatcher = scope.$watch("$parent.model", load);
    }

    angular.module("elproffApp")
    .directive("translatable", function () {
        return {
            templateUrl: "views/directives/input/translatable.html",
            restrict: "E",
            transclude: true,
            scope: {
                modelName: "@model"
            },
            controller: ["$scope", controller]
        };
    });
}).call(null);
