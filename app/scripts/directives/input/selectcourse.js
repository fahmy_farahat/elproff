"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/selectCourse
 * @description
 * # input/selectCourse
 */
(function() {

    function controller(scope, Course, CommonCache) {
        scope.responseCourseStatus = "";
        var load =  _.debounce(function () {
            // From the controller scope
            if (scope.model.filter) {
                var params = {limit:0};
                if (scope.model.grade) {
                    params.grade = scope.model.grade.id;
                    scope.model.course = null;
                    scope.responseCourseStatus = "loading-result";
                }
                Course.allCourses(params).then(function(courses){
                    scope.courses = _.map(courses, CommonCache.course);
                    if (scope.courses.length){
                        scope.responseCourseStatus = "success-result";
                    } else {
                        scope.responseCourseStatus = "error-result";    
                    }
                }, function(){
                    scope.responseCourseStatus = "error-result";
                });
            } else {
                var params = {limit:0};
                if (scope.model.grade) {
                    params.grade = scope.model.grade.id;
                    scope.model.course = null;
                    scope.responseCourseStatus = "loading-result";
                }
                Course.getList(params).then(function(courses) {
                    scope.courses = _.map(courses, CommonCache.course);
                    if (scope.courses.length){
                        scope.responseCourseStatus = "success-result";
                    } else {
                        scope.responseCourseStatus = "error-result";    
                    }
                }, function(){
                        scope.responseCourseStatus = "error-result";
                });
            }
        }, 1000);
        // load();
        scope.$watch("model.grade", load);
    }

    angular.module("elproffApp")
    .directive("inputSelectCourse", function () {
        return {
            templateUrl: "views/directives/input/select-course.html",
            restrict: "E",
            controller: ["$scope", "Course", "CommonCache", controller]
        };
    });
}).call(null);
