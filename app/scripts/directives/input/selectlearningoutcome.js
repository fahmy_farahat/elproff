"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/selectLearningOutcome
 * @description
 * # input/selectLearningOutcome
 */
(function() {

    function controller(scope, LearningOutcome) {

        scope.$on("lang_updated", function() {
            scope.outcomes = LearningOutcome.getEntities();
        });

        function load(){
            if (!scope.model) {
                return;
            }
            if (!scope.outcomes) {
                scope.outcomes = LearningOutcome.getEntities();
            }
            if (!scope.model.learning_outcome){      
                scope.model.learning_outcome = scope.outcomes[0];
            }
        }
        load();
        scope.$watch("model.learning_outcome", load);
    }

    angular.module("elproffApp")
    .directive("inputSelectLearningOutcome", function () {
        return {
            templateUrl: "views/directives/input/select-learning-outcome.html",
            restrict: "E",
            controller: ["$scope", "LearningOutcome", controller]
        };
    });
}).call(null);
