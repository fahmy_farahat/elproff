"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/address
 * @description
 * # input/address
 */
(function() {

    function controller(scope) {

    }

    angular.module("elproffApp")
    .directive("inputAddress", function () {
        return {
            templateUrl: "views/directives/input/address.html",
            restrict: "E",
            controller: ["$scope", controller]
        };
    });
}).call(null);
