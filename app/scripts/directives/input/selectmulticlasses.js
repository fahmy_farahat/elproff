"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/selectmulticlasses
 * @description
 * # input/selectmulticlasses
 */
(function() {

    function controller(scope, Classes, CommonCache) {
    	scope.classes = [];
    	//scope.model = {};
        var load = _.debounce(function () {
        
            Classes.getList().then(function(classes) {
                scope.classes = classes;
            });
        
        }, 1000);
        load();
    }

    angular.module("elproffApp")
        .directive("inputSelectMultiClasses", function () {
            return {
                templateUrl: "views/directives/input/select-multi-classes.html",
                restrict: "E",
                controller: ["$scope", "Classes", "CommonCache", controller]
            };
        });
}).call(null);

