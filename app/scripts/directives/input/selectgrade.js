"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/selectGrade
 * @description
 * # input/selectGrade
 */
(function() {

    function controller(scope, Grade, CommonCache) {
        scope.responseGradeStatus = "";
        var load = _.debounce(function () {
            // From the controller scope
            var params = {limit: 0}
            if (scope.model.system) {
                scope.responseGradeStatus = "loading-result";
                params.system = scope.model.system.id;
                if (scope.model.country) {
                    params.system__country = scope.model.country.id;
                }
                Grade.getList(params).then(function(grades) {
                    scope.grades = _.map(grades, CommonCache.grade);
                    if (scope.grades.length){
                        scope.responseGradeStatus = "success-result";
                    } else {
                        scope.responseGradeStatus = "error-result";    
                    }
                }, function(){
                    scope.responseGradeStatus = "error-result";
                });
            }
        }, 1000);
        load();
        scope.$watch("model.system", load);
        scope.$watch("model.country", load);
    }

    angular.module("elproffApp")
    .directive("inputSelectGrade", function () {
        return {
            templateUrl: "views/directives/input/select-grade.html",
            restrict: "E",
            controller: ["$scope", "Grade", "CommonCache", controller]
        };
    });
}).call(null);
