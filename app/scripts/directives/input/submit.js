"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/submit
 * @description
 * # input/submit
 */
(function() {

    function controller(scope) {
    }

    angular.module("elproffApp")
    .directive("inputSubmit", function () {
        return {
            templateUrl: "views/directives/input/submit.html",
            restrict: "E",
            scope: {
                title: "@",
                isDisabled: "=",
                isLoading: "="
            },
            controller: ["$scope", controller]
        };
    });
}).call(null);
