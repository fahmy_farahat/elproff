"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:input/acceptlicence
 * @description
 * # input/acceptlicence
 */
(function() {

    function controller(scope) {
    }

    angular.module("elproffApp")
    .directive("inputAcceptlicence", function () {
        return {
            templateUrl: "views/directives/input/acceptlicence.html",
            restrict: "E",
            controller: ["$scope", controller]
        };
    });
}).call(null);
