"use strict";
/**
 * @ngdoc directive
 * @name elproffApp.directive:reloadcaptcha
 * @description
 * # reloadcaptcha
 */
(function() {

    function controller(scope) {

        scope.isReloadingCaptcha = false;

        var load = _.once(function() {
            Recaptcha._alias_finish_reload = Recaptcha.finish_reload;
            // We can"t find a way to attach events onFinish
            Recaptcha.finish_reload = function (challenge, b, c) {
                Recaptcha._alias_finish_reload(challenge, b, c);
                scope.$apply(function() {
                    scope.isReloadingCaptcha = false;
                });
            };
        });

        scope.reloadCaptcha = _.throttle(function() {
            if (scope.isReloadingCaptcha) {
                return;
            }
            load();
            scope.isReloadingCaptcha = true;
            Recaptcha.reload();
        }, 1000);
    }

    angular.module("elproffApp")
    .directive("reloadcaptcha", function () {
        return {
            templateUrl: "views/directives/reloadcaptcha.html",
            restrict: "E",
            controller: ["$scope", controller]
        };
    });
}).call(null);
