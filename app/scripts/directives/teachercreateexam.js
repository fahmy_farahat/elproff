"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:teacherCreateExam
 * @description
 * # teacherCreateExam
 */
(function() {

    function controller(scope, $log, Common, CommonCache, $location, PrivateTeacher, TeacherBuild, $modal, Group, $filter, Classes) {

        scope.bulks = [];
        scope.courseElements = [];
        scope.elements = [];
        scope.groups = [];
        scope.model = {};
        scope.model.endTime = new Date();
        scope.model.startTime = new Date();
        scope.checkChangeState = true;

        var data = {
            "course": scope.courseid
        };

        scope.closeAlert = function closeAlert(index){
            scope.alerts.splice(index, 1);
        };
        
        scope.addBulk = function addBulk() {
            var modalInstance = $modal.open({
                templateUrl: "views/modals/createquestionsbulk.html",
                controller: "ModalsCreatequestionsbulkCtrl",
                resolve: {}
            });
            modalInstance.result.then(function (bulk) {
                scope.bulks.push(bulk);
            });
        };

        scope.addElements = function addElements(){
            var modalInstance = $modal.open({
                templateUrl: "views/modals/createcourseelement.html",
                controller: "ModalsCreatecourseelementCtrl",
                resolve: {
                    }
            });
            modalInstance.result.then(function (courseElements) {
                _.each(courseElements, function(courseElement){
                    courseElement = _.map(courseElement, CommonCache.element);
                    scope.elements.push(courseElement[0]);
                });
            });
        };

        function load() {
            var data = {limit: 0, course: scope.courseid};
            if(scope.type === "privateteacher") {
                Group.getList(data).then(function(result) {
                    if (result.length) {
                        var groups = [];
                        _.each(result, function(group) {
                            groups.push({id: group.id, name: group.name, checked: false});
                        });
                        scope.groups = groups;
                    }
                });    
            } else if (scope.type === "schoolteacher") {
                // should be filtered according to classes and subjects
                Classes.getList().then(function(result) {
                    if (result.length) {
                        var schoolclasses = [];
                        _.each(result, function(group) {
                            schoolclasses.push({id: group.id, name: group.name, checked: false});
                        });
                        scope.schoolclasses = schoolclasses;
                    }
                });
            }
            
        }
        load();
        
        scope.CheckAllGroups = function CheckAllGroups() {
            _.each(scope.groups, function(group, i) {
                group.checked = scope.checkChangeState;
                scope.groups[i] = group;
            });
            scope.checkChangeState = !scope.checkChangeState;
        };
        scope.CheckAllClasses = function CheckAllClasses() {
            _.each(scope.schoolclasses, function(sch_class, i) {
                sch_class.checked = scope.checkChangeState;
                scope.schoolclasses[i] = sch_class;
            });
            scope.checkChangeState = !scope.checkChangeState;
        };

        scope.onSuccess = function onSuccess(result) {
            var msg;
            if (result.available_questions > 0) {
                // To be moved to the controller of both 
                // private teacher and schoolteacher
                if(scope.type === "privateteacher") {
                    $location.path("/privateteacher/course/" + scope.courseid + "/exam/" + result.build_id + "/review");
                } else if(scope.type === "schoolteacher") {
                    $location.path("/schoolteacher/course/" + scope.courseid + "/exams/" + result.build_id + "/review");

                }
            } else {
                msg = scope.$root.lang.error_no_questions;
                scope.alerts = [{
                    msg: msg,
                    type: "danger"
                }];
            }
            
        };

        var changeTime = function changeTime() {
            if (!isNaN(scope.model.duration)) {
                var currentDate = moment(scope.model.startTime).add(scope.model.duration, "m");
                scope.model.endTime = new Date(currentDate);
            }
        };

        // increase end time based on teacher duration
        scope.$watch("model.duration", changeTime);
        scope.$watch("model.startTime", changeTime);

        scope.getQuestions = function getQuestions() {
            var msg = [];
            scope.alerts = [];
            
            if (_.isEmpty(scope.model.name)) {
                msg.push(scope.$root.lang.error_empty_name);
            }
            if (!scope.model.duration) {
                msg.push(scope.$root.lang.error_empty_teacher_duration);
            }
            // check if exam end time greated than start time + duration of exam
            var examEnd = moment(scope.model.startTime).add(scope.model.duration -1, "m");
            if (examEnd.isAfter(moment(scope.model.endTime))) {
                msg.push(scope.$root.lang.error_time_not_accurate);
            }
            var selectedEntities = [];
            var emptyselection_error = "";
            if(scope.type === "privateteacher") {
                emptyselection_error = scope.$root.lang.error_empty_selected_groups;
                _.each(scope.groups, function(group, i){
                    if (group.checked) {
                        selectedEntities.push(group.id);
                    }
                });
            } else if(scope.type === "schoolteacher"){
                emptyselection_error = scope.$root.lang.error_empty_selected_classes;
                _.each(scope.schoolclasses, function(sch_class, i){
                    if (sch_class.checked) {
                        selectedEntities.push(sch_class.id);
                    }
                });
            }

            if (_.isEmpty(selectedEntities)) {
                msg.push(emptyselection_error);
            }
            if (!scope.model.startTime || !scope.model.endTime) {
                msg.push(scope.$root.lang.error_empty_exam_time);
            }
            if (_.isEmpty(scope.bulks)) {
                msg.push(scope.$root.lang.error_questionstypenotselected);
            }

            if (_.isEmpty(msg)) {
                generateQuestions(selectedEntities);
            } else {
                scope.alerts = _.map(msg, function(message) {
                    return {msg: message, type: "danger"};
                });
            }
           
        };

        function generateQuestions(selectedEntities) {
            var data = {
                name: scope.model.name,
                start_time: scope.model.startTime.toISOString(),
                end_time: scope.model.endTime.toISOString(),
                teacher_time: scope.model.duration,
                course: scope.courseid,
            };
            if(scope.type === "privateteacher") {
                data.groups = selectedEntities;    
            } else if(scope.type === "schoolteacher") {
                data.schoolclasses = selectedEntities;
            }
            
            if(!_.isEmpty(scope.elements)){
                data.elements = _.pluck(scope.elements, "resource_uri");
            }
            var questionGroups = [];
            _.each(scope.bulks, function(bulk){
                var qGroup = {
                    difficulty: bulk.difficulty.name,
                    learning_outcome: bulk.learning_outcome.name,
                    count: bulk.count,
                    question_type: bulk.questions_type.name,
                };
                questionGroups.push(qGroup);
            });
            data.question_groups = questionGroups;
            TeacherBuild.generateBuild(data).then(scope.onSuccess, Common.onError(scope));
        }
    }

    angular.module("elproffApp")
        .directive("teacherCreateExam", function() {
            return {
                templateUrl: "views/directives/teacher-create-exam.html",
                restrict: "E",
                scope: {
                    courseid: "=courseid",
                    type: "@"
                },
                controller: ["$scope", "$log", "Common", "CommonCache",
                "$location", "PrivateTeacher", "TeacherBuild", "$modal", "Group", "$filter",
                "Classes", controller]
            };
        });
}).call(null);
