"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:authorsHighCharts
 * @description
 * # authorsHighCharts
 */
(function() {

    function controller(scope, root, $location) {
        var watcher = scope.$watch("model", function(newValue) {
            if (!scope.model || !_.isArray(scope.model)) {
                return;
            }
            watcher();

            var categories = [];
            var values = [];
            scope.data = _.map(scope.model, function(array, key) {
                categories.push(array[0]);
                values.push(array[2]);
                // Converting server model to useble plugin data
                return {
                    "name": array[0],
                    "email": array[1],
                    "y": array[2],
                    "value": array[2],
                    "id": array[3]
                };
            });

            function config() {
                // Highcharts confgiurations
                scope.chartConfig = {
                    options: {
                        chart: {
                            animation: true,
                            type: "column",
                            renderTo: "questions"
                        },
                        exporting: {
                            enabled: false
                        },
                        navigator: {
                            enabled: false
                        },
                        tooltip: {
                            formatter: function(){
                                var point = this.points[0].point;
                                return "<b>" + point.name + "</b>" + "<br> <b>" + root.lang.questions + "</b>: " + point.y + "<br> <b>" + root.lang.email + "</b>: "+ point.email;
                            },
                            style: {
                                padding: 10,
                            },
                            useHTML: true
                        },
                        rangeSelector: {
                            enabled: false
                        }
                    },
                    series: [{
                        name: root.lang.questions,
                        type: "column",
                        // Sorting data descending by number of questions
                        data: scope.data,
                        pointWidth: 30,
                        cursor: "pointer",
                        point: {
                            events: {
                                click: function () {
                                    var that = this;
                                    scope.$apply(function() {
                                        $location.path("/admin/author/" + that.id + "/question");
                                    });
                                }
                            }
                        },
                        dataLabels: {
                            enabled: true,
                            //rotation: -90,
                            color: "#FFFFFF",
                            format: "{point.y:.0f}", 
                            x: root.USER.lang === "ar" ? 14 : 0,
                            y: 10,
                            style: {
                                fontSize: "11px",
                            }
                        }
                    }],
                    scrollbar: {
                        enabled: true,
                        minWidth: 20,
                        barBackgroundColor: "gray",
                        barBorderRadius: 7,
                        barBorderWidth: 0,
                        buttonBackgroundColor: "gray",
                        buttonBorderWidth: 0,
                        buttonArrowColor: "yellow",
                        buttonBorderRadius: 7,
                        rifleColor: "yellow",
                        trackBackgroundColor: "white",
                        trackBorderWidth: 1,
                        trackBorderColor: "silver",
                        trackBorderRadius: 7
                    },
                    title: {
                        text: root.lang.authors + " " + root.lang.questions,
                        useHTML: Highcharts.hasBidiBug
                    },
                    legend: {
                        useHTML: Highcharts.hasBidiBug
                    },    
                    xAxis: {
                        // Setting max length of question before scrollbar starts
                        type: "category",
                        categories: scope.categories,
                        max: scope.model.length > 10 ? 10 : null,
                        step: 1,
                        title: {
                            text: root.lang.authors,
                            useHTML: true
                        },
                        labels: {
                            rotation: -30,
                            formatter: function(e) {
                                var point = scope.data[this.value];
                                if (_.isString(this.value) && this.value.length > 0) {
                                    return this.value;
                                } else if (point && point.name && point.name.length > 0) {
                                    return point.name;
                                } else {
                                    return "";
                                }
                            }
                        },
                        // RTL SUPPRT
                        reversed: root.USER.lang === "ar",
                    },
                    yAxis: {
                        labels: {
                            x: root.USER.lang === "ar" ? -8 : -2,
                            y: 4
                        },
                        title: {
                            text: root.lang.questions,
                            useHTML: true
                        },
                        // RTL SUPPRT
                        opposite: root.USER.lang === "ar",
                    },
                    useHighStocks: true,
                    credits: {
                        enabled: false
                    }
                };
            }
            config();
            scope.$on("lang_updated", config);
        });

    }

    angular.module("elproffApp")
        .directive("authorsHighChart", function() {
            return {
                templateUrl: "views/directives/authors-high-chart.html",
                restrict: "E",
                scope: {
                    model: "="
                },
                controller: ["$scope", "$rootScope", "$location", controller]
            };
        });
}).call(null);
