"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:ExamItem
 * @description
 * # ExamItem
 */
(function() {

    function controller(scope, Common, Exam) {

        if (!_.isEmpty(scope.model.exam) && !_.isObject(scope.model.exam)) {
            Exam.getUri(scope.model.exam).then(function(exam){
                scope.courseid = Common.uriToID(exam.course);
            });
        } else if (_.isObject(scope.model.exam)) {
            scope.courseid = Common.uriToID(scope.model.exam.course);
        }

    }

    angular.module("elproffApp")
    .directive("examItem", function () {
        return {
            templateUrl: "views/directives/exam-item.html",
            restrict: "E",
            scope: {
                model: "="
            },
            controller: ["$scope", "Common", "Exam", controller]
        };
    });
}).call(null);
