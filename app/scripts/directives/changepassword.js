"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:changePassword
 * @description
 * # changePassword
 */
(function() {

    function controller(scope, User, Common) {

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            scope.alerts = Common.alerts.success.update_password;
        };

        scope.onError = function onError(error) {
            scope.isLoading = false;
            scope.alerts = Common.alerts.error.update_password;
        };

        scope.save = function save() {
            scope.isLoading = true;
            User.updatePassword(this.model).then(this.onSuccess, Common.onError(scope, this.onError));
        };

        scope.closeAlert = function closeAlert(index) {
            scope.alerts = [];
        };

    }

    angular.module("elproffApp")
    .directive("changePassword", function () {
        return {
            templateUrl: "views/directives/change-password.html",
            restrict: "E",
            controller: ["$scope", "User", "Common", controller]
        };
    });
}).call(null);
