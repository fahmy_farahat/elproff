"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:questionTakeFooter
 * @description
 * # questionTakeFooter
 */
(function() {

    function controller(scope, attrs) {
    }

    angular.module("elproffApp")
    .directive("questionTakeFooter", function () {
        return {
            templateUrl: "views/directives/question-take-footer.html",
            restrict: "E",
            controller: ["$scope", "$attrs", controller]
        };
    });
}).call(null);
