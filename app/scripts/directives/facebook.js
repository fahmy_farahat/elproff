"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:facebook
 * @description
 * # facebook
 */
(function() {

    function controller(scope) {
    	scope.baseURL = document.location.origin;
    	scope.fbShare = function fbShare(){
    		if(!scope.name || scope.name === ""){
    			scope.name = "ELProff";
    		}
    		if(!scope.link || scope.link === ""){
    			scope.link = "/";
    		}
    		if(!scope.picture || scope.picture === ""){
    			scope.picture = "http://beta.elproff.com/images/logo.png";
    		}
    		else{
    			scope.picture = scope.baseURL + scope.picture;
    		}
    		if(!scope.caption || scope.caption === "" ){
    			scope.caption = "ELProff the biggest online educational platform in Egypt";
    		}
    		if(!scope.description || scope.description === ""){
    			scope.description = "Shared via ELProff";
    		}
            FB.ui({
                method: "feed",
                name: scope.name,
                link: scope.baseURL + scope.link,
                picture: scope.picture,
                caption: scope.caption,
                description: scope.description,
                message: scope.message,
            });
        };
    }

    angular.module("elproffApp")
    .directive("facebook", function () {
        return {
            template: '<a ng-click="fbShare()" class="fb" title="Facebook"></a>',
            restrict: "E",
            scope: {
                name: "=",
                link: "=",
                picture: "=",
                caption: "=",
                description: "=",
                message: "="
            },
            controller: ["$scope", controller]
        };
    });
}).call(null);
