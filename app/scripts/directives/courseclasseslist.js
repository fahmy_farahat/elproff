"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:courseClassesList
 * @description
 * # courseClassesList
 */
(function() {

  function controller(scope) {

  }

  angular.module("elproffApp")
    .directive("courseClassesList", function () {
      return {
        templateUrl: "views/directives/course-classes-list.html",
        restrict: "E",
        controller: ["$scope", "$rootScope", controller]
      };
    });
}).call(null);