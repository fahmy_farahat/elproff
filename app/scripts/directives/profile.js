"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:profile
 * @description
 * # profile
 */
(function() {

    function controller(scope, Common, Profile) {
    }
    angular.module("elproffApp")
    .directive("profile", function () {
        return {
            templateUrl: "views/directives/profile.html",
            restrict: "E",
            controller: ["$scope", "Common", "Profile", controller]
        };

    });
}).call(null);
