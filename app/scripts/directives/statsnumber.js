"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:StatsNumber
 * @description
 * # StatsNumber
 */
(function() {

    function controller(scope) {

    }

    angular.module("elproffApp")
    .directive("statsNumber", function () {
        return {
            templateUrl: "views/directives/statsnumber.html",
            restrict: "E",
            scope: {
                count: "@",
                title: "@"
            },
            controller: ["$scope", "$rootScope", controller]
        };
    });
}).call(null);
