"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:QuestionPreview
 * @description
 * # QuestionPreview
 */
(function() {

    function controller(scope, $modal) {
        // Making the question preview sticky for the question add/review
        if ($("#question-preivew-container").length){
            $("#question-preivew-container").stick_in_parent();
        }
        scope.addFeedback = function addFeedback(question){
            var modalInstance = $modal.open({
                templateUrl: "views/modals/add-feedback.html",
                controller: "ModalsAddfeedbackCtrl",
                size: "lg",
                resolve: {
                    question: function(){
                      return question;
                    }
                }
            });
        };
    }
        

    angular.module("elproffApp")
    .directive("questionPreview", function () {
        return {
            templateUrl: "views/directives/question-preview.html",
            restrict: "E",
            controller: ["$scope","$modal", controller]
        };
    });
}).call(null);
