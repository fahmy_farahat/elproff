"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:questionMakePreview
 * @description
 * # questionMakePreview
 */
(function() {

    function controller(scope) {
        scope.$on("question_init", function() {
            if (scope.model.question_type !== "truefalse") {
                scope.model.question.choices = [];
            }
            if (["connect", "dragdrop"].indexOf(scope.model.question_type) >= 0) {
                scope.model.question.ends = [];
            }
            scope.model.user.answer = {};
        });
    }

    angular.module("elproffApp")
    .directive("questionMakePreview", function () {
        return {
            templateUrl: "views/directives/question-make-preview.html",
            restrict: "E",
            controller: ["$scope", controller]
        };
    });
}).call(null);
