"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:groupStudentList
 * @description
 * # groupStudentList
 */
(function() {

  function controller(scope) {

  }

  angular.module("elproffApp")
    .directive("groupStudentList", function () {
      return {
        templateUrl: "views/directives/group-student-list.html",
        restrict: "E",
        controller: ["$scope", "$rootScope", controller]
      };
    });
}).call(null);
