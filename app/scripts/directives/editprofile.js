"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:editProfile
 * @description
 * # editProfile
 */
(function() {

    function controller(scope, User, Grade, Common, System, Country) {

        scope.isLoading = false;
        scope.model = User.getUser();

        if (scope.model && scope.model.grade) {
            Grade.get(scope.model.grade).then(function(grade) {
                if (scope.model.type === "student") {
                    System.getUri(grade.system).then(function(system) {
                        scope.model.system = system;
                        scope.model.country = Country.getUri(system.country).$object;
                    });
                    scope.model.grade = grade;
                }
            });
        }
    
        function reloadCountry() {
            if (scope.model && scope.model.country) {
                Country.get(scope.model.country.id).then(function(country){
                    scope.model.country = country;
                });
            }
        }
        reloadCountry();
        scope.$on("user_updated", reloadCountry);
        scope.onSuccess = function(result) {
            scope.isLoading = false;
            User.setUser(result);
            reloadCountry();
            scope.alerts = Common.alerts.success.update_profile;
        };

        scope.onError = function(result) {
            scope.isLoading = false;
            scope.alerts = Common.alerts.error.update_profile;
        };

        scope.save = function save() {
            scope.isLoading = true;
            var data = new FormData();
            var m = scope.model;

            data.append("full_name", m.full_name || "");
            data.append("avatar", this.avatar);
            if (m.grade) {
                data.append("grade", m.grade.resource_uri);
            }
            if (m.gender) {
                data.append("gender", m.gender);
            }
            if (m.school_name) {
                data.append("school_name", m.school_name);
            }
            if (m.phone_number) {
                data.append("phone_number", m.phone_number);
            }
            if (m.country) {
                data.append("country", m.country.resource_uri);
            }
            if (m.state) {
                data.append("state", m.state || "");
            }
            if (m.city) {
                data.append("city", m.city || "");
            }
            if (m.address) {
                data.append("address", m.address || "");
            }
            if (m.birthdate) {
                var bd = moment.utc(m.birthdate);
                data.append("birthdate", bd || "");
            }

            User.updateProfile(data).then(this.onSuccess, Common.onError(scope));
        };

        scope.closeAlert = function closeAlert(index) {
            scope.alerts = [];
        };

        scope.selectFile = function selectFile() {
            setTimeout(function() {
                angular.element("#avatar").trigger("click");
            }, 0);
        };

    }
    angular.module("elproffApp")
    .directive("editProfile", function () {
        return {
            templateUrl: "views/directives/editprofile.html",
            restrict: "E",
            controller: ["$scope", "User", "Grade", "Common", "EducationalSystem",
                         "Country", controller]
        };
    });
}).call(null);
