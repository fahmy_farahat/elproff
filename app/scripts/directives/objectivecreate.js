"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:objectiveCreate
 * @description
 * # objectiveCreate
 */
(function () {

  function controller (scope) {

    console.log("objectiveCreate directive");

  }

  angular.module("elproffApp")
    .directive("objectiveCreate", function () {
      return {
        templateUrl: "views/directives/objective-create.html",
        restrict: "E",
        controller: ["$scope", controller]
      };
    });


}).call(null);
