"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:invitation
 * @description
 * # invitation
 */
(function() {

    function controller(scope, Common, Invitation, reCAPTCHA) {

        scope.model  = {
            courses: []
        };
        scope.isLoading = false;
        scope.onSuccess = function onSuccess() {
            scope.isLoading = false;
            scope.alerts = Common.alerts.success["invite_" + scope.type] ;
            reCAPTCHA.reload();
            scope.model.email = [];
        };

        function loadGrades(){
            if(scope.grades){
                scope.model.grades = scope.grades;
            }

        }

        scope.showCourses = ["sme", "author", "reviewer", "schoolteacher"].indexOf(scope.type) >= 0;
        scope.$watch("grades", loadGrades);

        scope.alerts = [];
        scope.onError = function onError(result) {
            scope.isLoading = false;
            reCAPTCHA.reload();
            if (result.data.length) {
                var msg, msgArr = [];
                for (var i=0;result.data.length > i; i++){
                    msgArr.push(result.data[i].email);

                }
                msg = msgArr +" "+ scope.$root.lang.error_previouslyinvited;
                scope.alerts = [{
                    msg: msg,
                    type: "danger"
                }];
            } else {
                if (result.data.error === "self_invite") {
                    scope.alerts = Common.alerts.error.selfinvite;
                } else if (result.data.error === "captcha_error") {
                    scope.alerts = Common.alerts.error.captcha_error;
                } else if (result.data.error === "no_courses") {
                    scope.alerts = Common.alerts.error.course;
                } else if (result.data.error === "no_class") {
                    scope.alerts = Common.alerts.error.no_class;
                }
            }
        };

        scope.invite = function invite() {
            scope.isLoading = true;
            var courses = [];
            var user = scope.$root.USER;
            var model = _.cloneDeep(this.model);
            var entitiesWithoutCourses = ["student", "parent", "privateteacher",
                                          "manager", "school_student",
                                         ].indexOf(scope.type);
            if (_.isEmpty(model.email)) {
                scope.alerts = Common.alerts.error.email;
                scope.isLoading = false;
            } else if (_.isEmpty(model.courses) && 
                entitiesWithoutCourses < 0 ) {
                scope.alerts = Common.alerts.error.course;
                scope.isLoading = false;
            } else if (_.isEmpty(scope.school) && scope.type === "manager"){
                scope.alerts = Common.alerts.error.no_school;
                scope.isLoading = false;
            } else if(_.isEmpty(scope.schoolClass) && scope.type === "school_student"){
                scope.alerts = Common.alerts.error.no_class;
                scope.isLoading = false;
            } else if( (!model.classes || !model.classes.length || model.classes.length <= 0) && scope.type === "schoolteacher"){
                scope.alerts = Common.alerts.error.no_class;
                scope.isLoading = false;
            } else {
                model.email = _.map(_.map(model.email, "email"), function (mail) {
                    return mail.toLowerCase();
                });
                if (!_.isUndefined(model.courses)) {
                    angular.forEach(model.courses, function(course) {
                        courses.push({id: course.id});
                    });
                    model.courses = courses;
                }

                var promise;
                if (scope.type === "manager") {
                    model.school = scope.school.resource_uri;
                    promise = Invitation.schooladminManager(model);
                } else if (user.type === "schooladmin" && scope.type === "school_student"){
                    model.class = scope.schoolClass.resource_uri;
                    promise = Invitation.schooladminStudent(model);
                } else if (scope.type === "schoolteacher") {
                    model.classes = _.pluck(model.classes, "resource_uri");
                    model.school = scope.school.resource_uri;
                    promise = Invitation.schooladminSchoolteacher(model);
                } else if (scope.type === "student") {
                    if (user.type === "student") {
                        promise = Invitation.studentStudent(model);
                    } else if (user.type === "parent") {
                        promise = Invitation.parentStudent(model);
                    }
                } else if (scope.type ===  "parent") {
                    promise = Invitation.studentParent(model);
                } else if (scope.type ===  "privateteacher") {
                    model.grade = model.course.grade;
                    promise = Invitation.privateteacherStudent(model);
                } else if (scope.type === "sme") {
                    promise = Invitation.adminSME(model);
                } else if (scope.type === "author") {
                    if (user.type === "sme") {
                        promise = Invitation.smeAuthor(model);
                    } else {
                        promise = Invitation.adminAuthor(model);
                    }
                } else if (scope.type === "reviewer") {
                    if (user.type === "sme") {

                        promise = Invitation.smeReviewer(model);
                    } else {
                        promise = Invitation.adminReviewer(model);
                    }
                }
                if (promise) {
                    promise.then(this.onSuccess, Common.onError(scope, scope.onError));
                } else {
                    scope.alerts = Common.alerts.error.incorrect_type;
                }
            }
        };

        scope.closeAlert = function(index) {
            scope.alerts = [];
        };
    }

    angular.module("elproffApp")
    .directive("invitation", function () {
        return {
            templateUrl: "views/directives/invitation.html",
            restrict: "E",
            scope: {
                type: "@",
                school: "=",
                schoolClass: "=",
                grades: "="
            },
            controller: ["$scope", "Common", "Invitation", "reCAPTCHA", controller]
        };
    });
}).call(null);
