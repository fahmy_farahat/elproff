"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:emailAvailableValidator
 * @description
 * # emailAvailableValidator
 */
angular.module("elproffApp")
.directive("emailAvailableValidator", ["User", function (User) {
    return {
        restrict: "A",
        require: "ngModel",
        link: function postLink($scope, element, attrs, ngModel) {
            ngModel.$asyncValidators.emailAvailable = function(email) {
                return User.checkEmail(email);
            };
        }
    };
}]);
