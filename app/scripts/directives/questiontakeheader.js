"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:questionTakeHeader
 * @description
 * # questionTakeHeader
 */
(function() {

    function controller(scope, Restangular) {
        var watcher = scope.$watch("model", function() {
            if (scope.model.attachment && _.isString(scope.model.attachment)) {
                watcher();
                var uri = scope.model.attachment.split("/");
                uri = uri.slice(uri.length - 2).join("/");
                scope.model.attachment = Restangular.oneUrl(uri).get().$object;
            }
        }, true);
    }

    angular.module("elproffApp")
    .directive("questionTakeHeader", function () {
        return {
            templateUrl: "views/directives/question-take-header.html",
            restrict: "E",
            controller: ["$scope", "Restangular", controller]
        };
    });
}).call(null);
