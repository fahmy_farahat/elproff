"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:teacherCoursesList
 * @description
 * # teacherCoursesList
 */
(function() {

    function controller(scope) {

    }

    angular.module("elproffApp")
    .directive("teacherCoursesList", function () {
        return {
            templateUrl: "views/directives/teacher-courses-list.html",
            restrict: "E",
            controller: ["$scope", "$rootScope", controller]
        };
    });
}).call(null);
