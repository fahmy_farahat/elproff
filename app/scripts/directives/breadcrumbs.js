"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:breadcrumbs
 * @description
 * # breadcrumbs
 */
(function() {

    function controller(scope, Common) {

        scope.model = {
            query: ""
        };
        scope.search = function search() {
            if (scope.model.query === "") {
                delete Common.query.q;
            } else {
                Common.query = {
                    q: scope.model.query
                };
            }
            scope.$emit("relist");
        };
    }

    angular.module("elproffApp")
    .directive("breadcrumbs", function () {
        return {
            templateUrl: "views/directives/breadcrumbs.html",
            restrict: "E",
            replace: true,
            scope: {
                crumbs: "=",
                searchable: "="
            },
            controller: ["$scope", "Common", controller]
        };
    });
}).call(null);
