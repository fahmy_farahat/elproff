"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:course
 * @description
 * # course
 */
(function() {

    function controller(scope, Course, Restangular, Common, Grade, System, Country, CommonCache) {

        scope.isdisabled = false;

        var watcher = scope.$watch("course", function() {
            if (scope.course) {
                watcher();
                if (_.isString(scope.course)) {
                    // Loading the course when its a uri
                    Course.getUri(scope.course).then(function(course) {
                        scope.course = course;
                        CommonCache.course(course);
                        if (scope.$root.USER.type === "student") {
                            var query = {courseid: scope.course.id};
                            scope.stats = Course.studentStats(query).$object;
                        }
                        if ((scope.$root.USER.type === "student" || scope.$root.USER.type === "parent") && scope.course.disabled){
                            scope.isdisabled = true;
                        }
                    }, Common.onError(scope));
                } else {
                    CommonCache.course(scope.course);
                    if (scope.$root.USER.type === "student") {
                        var query = {courseid: scope.course.id};
                        scope.stats = Course.studentStats(query).$object;
                    }
                    if ((scope.$root.USER.type === "student" || scope.$root.USER.type === "parent") && scope.course.disabled){
                        scope.isdisabled = true;
                    }
                }
            }
        });
    }

    angular.module("elproffApp")
        .directive("course", function () {
            return {
                templateUrl: "views/directives/course.html",
                restrict: "E",
                scope: {
                    course: "=",
                    student: "=", // Used by parent
                    subscribed: "@", // Used by parent
                    remove:"="
                },
                controller: [
                    "$scope", "Course", "Restangular", "Common", "Grade",
                    "EducationalSystem", "Country", "CommonCache", controller]
            };
        });
}).call(null);
