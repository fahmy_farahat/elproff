"use strict";

/**
 * @ngdoc directive
 * @name elproffApp.directive:kv
 * @description
 * # kv
 */
(function() {

    function compile(tElem, tAttrs){
        return {
            pre: function(scope, iElem, iAttrs){
                if (!scope.limit) {
                    scope.limit = 1000;
                }
            },
            post: _.noop
        };
    }

    angular.module("elproffApp")
    .directive("kv", function () {
        return {
            template: '<div class="dl-horizontal"> ' + 
        '<span class="key" tooltip="{{$root.lang[k]}}" ng-if="$root.lang[k].length > limit">{{$root.lang[k] | charlimit:limit}}</span>' + 
        '<span class="key" ng-if="$root.lang[k].length <= limit">{{$root.lang[k]}}</span>' + 
        '<span class="value" tooltip="{{v}}" ng-if="v.length > limit">{{v | charlimit:limit}}</span>' + 
        '<span class="value" ng-if="v.length <= limit">{{v}}</span>' + 
        "</div>",
            restrict: "E",
            replace:true,
            scope: {
                k: "@",
                v: "@",
                limit: "@",
            },
            compile: compile,
        };
    });
}).call(null);
