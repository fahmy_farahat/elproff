"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ParentProfileCtrl
 * @description
 * # ParentProfileCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("ParentProfileCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
