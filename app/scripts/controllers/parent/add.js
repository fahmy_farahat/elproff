"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ParentAddCtrl
 * @description
 * # ParentAddCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, $timeout, $location, Service) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/parent/"},
            {title: 'addson', link: ""},
        ];

    };

    angular.module("elproffApp")
    .controller("ParentAddCtrl",
        ["$scope", "Common", "Crumbs", "$timeout", "$location", "Parent", controller]);
}).call(null);
