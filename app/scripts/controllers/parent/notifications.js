"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ParentNotificationsCtrl
 * @description
 * # ParentNotificationsCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("ParentNotificationsCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
