"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ParentPaymentCtrl
 * @description
 * # ParentPaymentCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, User) {

    }

    angular.module("elproffApp")
    .controller("ParentPaymentCtrl", ["$scope", "Common", "Crumbs", "User", controller] );
}).call(null);
