"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ParentProfileChangepasswordCtrl
 * @description
 * # ParentProfileChangepasswordCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("ParentProfileChangepasswordCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
