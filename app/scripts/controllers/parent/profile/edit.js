"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ParentProfileEditCtrl
 * @description
 * # ParentProfileEditCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("ParentProfileEditCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
