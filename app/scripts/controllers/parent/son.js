"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ParentSonCtrl
 * @description
 * # ParentSonCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, Parent) {
        scope.searchable = true;

        scope.crumbs = [
            {title: 'dashboard', link: "#/parent"},
            {title: 'sons', link: "#/parent/sons"},
            {title: params.studentid, link: ""},
        ];

        Crumbs.setStudent(scope.crumbs[2], params.studentid, scope);
        scope.courses = [];
        scope.subscribableCourses = [];

        scope.onSubscribableSuccess = function(result) {
            console.log(result);
            scope.subscribableCourses = result.objects;
        };

        scope.onListSuccess = function onListSuccess(result) {
            console.log(result);
            scope.courses = result.objects;
        };

        scope.onError = function onError(result) {
            if (result.error === "not_son") {
                scope.alerts = Common.alerts.error.not_son;
            }
        };

        function load() {
            var query = {studentid: params.studentid};
            Parent.listSonCourses(query).then(scope.onListSuccess, Common.onError(scope, scope.onError));
            Parent.listSonSubscribableCourses(query).then(scope.onSubscribableSuccess, Common.onError(scope, scope.onError));
        }
        load();
        scope.$on("relist", load);
    }

    angular.module("elproffApp")
    .controller("ParentSonCtrl", 
            ["$scope", "Common", "Crumbs", "$routeParams", "Parent", controller]);
}).call(null);
