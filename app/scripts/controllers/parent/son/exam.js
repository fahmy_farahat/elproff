"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ParentSonExamCtrl
 * @description
 * # ParentSonExamCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("ParentSonExamCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
