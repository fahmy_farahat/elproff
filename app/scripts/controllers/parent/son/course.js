"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ParentSonCourseCtrl
 * @description
 * # ParentSonCourseCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, Parent, $location, QuestionAnswer, Reports) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/parent"},
            {title: 'sons', link: "#/parent/son"},
            {title: params.studentid, link: "#/parent/son/" + params.studentid},
            {title: 'course', link: "#/parent/son/" + params.studentid + "/course/"},
            {title: params.courseid, link: "#/parent/son/" + params.studentid + "/course/" + params.courseid},
            {title: 'report', link: ""},
        ];

        Crumbs.setStudent(scope.crumbs[2], params.studentid, scope);
        Crumbs.setCourse(scope.crumbs[4], params.courseid, scope);
        scope.loaded = false;

        var runned = false;
        scope.model = {
            fraction_grade: 1,
            result_grade: 1
        };

        scope.level = "";

        scope.examid = params.examid;
        scope.questionsCounts = {
            skipped: 0,
            wrong: 0,
            correct: 0,
            total: 1
        };

        scope.onSuccess = function onSuccess(result) {
            scope.loaded = true;
            // TODO: Fix the api so we don"t have to get the first array child
            scope.model = result[0];
            QuestionAnswer.getList({exam_instance: params.examid}).then(function(answers) {
                if (!answers.length) {
                    console.log("NO answers");
                    return;
                }
                scope.answers = answers;
                if (scope.model && !scope.model.grade) {
                    scope.model.grade = 100;
                }
                var total = answers.length;
                var wrong = _.filter(answers,  function(ans) {
                          return (ans.degree||0) <0.5;
                        }).length;
                    var correct = _.filter(answers,  function(ans) {
                          return (ans.degree||0) >=0.5;
                        }).length;
                var skipped = total - (wrong + correct);

                scope.questionsCount = {
                    total: total,
                    skipped: skipped,
                    correct: correct,
                    wrong: wrong
                };
                var sum = _.reduce(_.pluck(answers, "degree"), function(a, b) {return a + b;});
                scope.time = _.reduce(_.pluck(answers, "duration"), function(a, b) {return a + b;});
                scope.model.fraction_grade = sum / answers.length;
                scope.model.result_grade = scope.model.grade * scope.model.fraction_grade;
                scope.model.level = Reports.calcLevel(scope.model.fraction_grade);
            });
        };

        scope.onError = function onError(error) {
            console.log(error);
            if (error.data.error === "not_exist") {
                scope.alerts = Common.alerts.error.parent_son_noexams;
            }
        };

        var query = {courseid: params.courseid, studentid: params.studentid, examid:  params.examid};
        Parent.listExamsReport(query).then(scope.onSuccess, Common.onError(scope, scope.onError));
    }

    angular.module("elproffApp")
    .controller("ParentSonCourseCtrl", 
            ["$scope", "Common", "Crumbs", "$routeParams",
            "Parent", "$location", "QuestionAnswer",
            "Reports", controller]);
}).call(null);
