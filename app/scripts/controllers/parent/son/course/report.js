"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ParentSonCourseReportCtrl
 * @description
 * # ParentSonCourseReportCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, Parent, $timeout, CourseElement) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/parent"},
            {title: 'sons', link: "#/parent/son"},
            {title: params.studentid, link: "#/parent/son/" + params.studentid},
            {title: 'course', link: "#/parent/son/" + params.studentid + "/course/"},
            {title: params.courseid, link: "#/parent/son/" + params.studentid + "/course/" + params.courseid},
            {title: 'report', link: ""},
        ];

        scope.studentid = params.studentid;
        scope.courseid = params.courseid;

        Crumbs.setStudent(scope.crumbs[2], params.studentid, scope);
        Crumbs.setCourse(scope.crumbs[4], params.courseid, scope);
        scope.loading = false;
        
        scope.onSuccess = function onSuccess(result) {
            scope.loading = true;
                scope.model = result;
            scope.studentid = result[0].student;
            $(".content").mCustomScrollbar();
        };

        scope.onError = function onError(error) {
          console.log(error);
          if (error.data.error === "not_exist") {
            scope.alerts = Common.alerts.error.parent_son_noexams;
          }
        };

        scope.query = {
          courseid: params.courseid,
          studentid: params.studentid
        };
        Parent.listExamsReport(scope.query).then(scope.onSuccess, Common.onError(scope, scope.onError));
    }

    angular.module("elproffApp")
    .controller("ParentSonCourseReportCtrl",
    ["$scope", "Common", "Crumbs", "$routeParams", "Parent", "$timeout",
      "CourseElement", controller] );
}).call(null);
