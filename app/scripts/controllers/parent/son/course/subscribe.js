"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ParentSonCourseSubscribeCtrl
 * @description
 * # ParentSonCourseSubscribeCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Parent, Plan, $modal) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/parent"},
            {title: 'sons', link: "#/parent/son"},
            {title: params.studentid, link: "#/parent/son/" + params.studentid},
            {title: 'course', link: "#/parent/son/" + params.studentid + "/course/"},
            {title: params.courseid, link: "#/parent/son/" + params.studentid + "/course/" + params.courseid},
            {title: 'subscribe', link: ""},
        ];

        Crumbs.setStudent(scope.crumbs[2], params.studentid, scope);
        Crumbs.setCourse(scope.crumbs[4], params.courseid, scope);
        scope.plans = [];

        /*
           cope.onSuccess = function onSuccess(result) {
           console.log(result);
           scope.alerts = Common.alerts.success.subscription;
           };

           scope.onError = function onError(error) {
           console.log(error);
           };

           scope.subscribe = function subscribe(plan) {
           var query = {studentid: params.studentid, planid: plan.id};
           Parent.subscribe(query).then(this.onSuccess, Common.onError(scope, this.onError));
           };
           */

        scope.studentid = params.studentid;
        scope.subscribe = function(plan) {
            scope.planid = plan.id;
            var modalInstance = $modal.open({
                templateUrl: "views/modals/add-coupon.html",
                controller: "ModalsAddcouponCtrl",
                scope: scope
            });
        };

        scope.onListSuccess = function onListSuccess(result) {
            console.log(result);
            scope.plans = result.objects;
        };

        function load() {
            var query = {studentid: params.studentid};
            Plan.listForStudent(query).then(scope.onListSuccess, Common.onError(scope, scope.onError));
        }
        load();
        scope.$on("relist", load);
    }

    angular.module("elproffApp")
        .controller("ParentSonCourseSubscribeCtrl",
                ["$scope", "Common", "Crumbs", "$routeParams", "$location",
                "Parent", "SubscriptionPlan", "$modal", controller]);
}).call(null);
