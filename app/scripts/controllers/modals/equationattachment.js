'use strict';

/**
 * @ngdoc function
 * @name elproffApp.controller:ModalsHelpequationattachmentCtrl
 * @description
 * # ModalsequationattachmentCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, $modalInstance, $timeout, attachment_relation, element) {
        scope.isLoading = false;
        scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            scope.attachment = "";
            $modalInstance.close(result.attachment);
        };        
        scope.selectFile = function selectFile() {
            setTimeout(function() {
                angular.element("#attachment").trigger("click");
            }, 0);
        };
        scope.model = {};
        scope.save = function saveImage(){
            var data = new FormData();
            var m = scope.model;
            data.append("attachment_type", "equation");
            if(scope.mathEditor.isFormulaEmpty()){
                scope.isLoading = false;
                scope.$root.alerts = Common.alerts.error.empty_choices;
                return;
            }
            data.append("mml", scope.mathEditor.getMathML());
            data.append("name", m.name);
            scope.isLoading = true;
            if(attachment_relation === "help") {
                Common.customRestangular("help_attachment", data, "submit_attachment")
                                    .then(scope.onSuccess, Common.onError(scope));    
            } else if (attachment_relation === "course") {
                data.append("element_id", element.id);
                Common.customRestangular("course_element_attachment", data, "submit_attachment")
                                    .then(scope.onSuccess, Common.onError(scope));
            } else if (attachment_relation === "question") {
                Common.customRestangular("question_attachment", data)
                                    .then(scope.onSuccess, Common.onError(scope));
            }
            
        }                
        $timeout(function() {
            function loadMathEditor() {
                if (_.isUndefined(com)) {
                    scope.alerts = "Please, reload the page. Bad Internet Connection.";
                    return;
                }
                scope.mathEditor = com.wiris.jsEditor.JsEditor.newInstance({"language": "ar"});
                var mathContainer = document.getElementById("mathEditorInModal");
                scope.mathEditor.insertInto(mathContainer);
            }
            if (_.isUndefined(com) || !com ) {
                $timeout(loadMathEditor, 1000);
            } else {
                loadMathEditor();
            }
        });
    }

    angular.module("elproffApp")
    .controller("ModalsequationattachmentCtrl",
        ["$scope", "Common", "$modalInstance",
         "$timeout", "attachment_relation", "crsElement", controller]);
}).call(null);