"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ModalsAddcouponCtrl
 * @description
 * # ModalsAddcouponCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller (scope, Common, Coupon, $modalInstance, $location){

        scope.alerts = [];
        scope.isLoading = false;

        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            if(result.status == 200){
              scope.alerts = Common.alerts.success.subscription;
            }
            setTimeout(function() {
                $modalInstance.close(scope.model);
                $location.path("/");
            }, 1000);
        };

        scope.onError = function onError(error) {
            scope.isLoading = false;
            //$modalInstance.close(scope.model);
            var err = error.data.error;
            if(err === "Invalid Plan" || err === "object_not_found"){
              scope.alerts = Common.alerts.error.invalidcoupon;
            }
        };

        scope.subscribe = function subscribe () {

            scope.isLoading = true;
            var formModel = scope.model;
            var model = {
                "plan_id": scope.planid,
                "student_id": scope.studentid,
                "coupon_code": formModel.couponcode
            };
            Coupon.couponConsume(model).then(this.onSuccess, this.onError);
        };
    }

    angular.module("elproffApp")
        .controller("ModalsAddcouponCtrl",
                ["$scope", "Common", "Coupon", "$modalInstance", "$location",
                controller]);

}).call(null);
