"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ModalsListchildcourseelementCtrl
 * @description
 * # ModalsListchildcourseelementCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Objective, modal, $modalInstance, courseElementNode) {

        var objectivesQuery = {
            course_element: courseElementNode.id
        };

        Objective.getList(objectivesQuery).then(function(result) {
            scope.loading = false;
            scope.objectives = result;
            scope.loading = true;
        });

        scope.createObjective = function createObjective() {
            var modalInstance = modal.open({
                templateUrl: "views/modals/create-objective.html",
                controller: "ModalsCreateobjectiveCtrl",
                size: "lg",
                resolve: {
                    parentCourseElement: function() {
                        return courseElementNode
                    }
                }
            });
        };

        scope.viewObjective = function viewObjective(indexVal) {
            scope.indexVal = indexVal;
            var modalInstance = modal.open({
                templateUrl: "views/modals/view-objective.html",
                controller: "ModalsViewobjectiveCtrl",
                size: "lg",
                resolve: {
                    objectives: function() {
                        return scope.objectives;
                    },
                    objectiveIndex: function() {
                        return scope.indexVal;
                    }
                }
            });
            scope.cancel();
        };

        scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };

    }

    angular.module("elproffApp")
        .controller("ModalsListchildcourseelementCtrl", ["$scope",
            "Objective", "$modal", "$modalInstance", "courseElementNode",
            controller
        ]);

}).call(null);
