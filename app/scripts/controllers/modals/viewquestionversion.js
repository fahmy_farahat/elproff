"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ModalsViewquestiongroupCtrl
 * @description
 * # ModalsViewquestiongroupCtrl
 * Controller of the elproffApp
 */

(function () {
    function controller (scope,  modal, $modalInstance, index, allquestions,courseid, Review, Common, $location) {
        //buttons: accept, cancel, next, previous, edit. 
        scope.model = {};
        scope.q = {};
        scope.alerts = [];
        scope.current_index = index;
        scope.allquestions = allquestions;
        scope.courseid = courseid;
        function load(current_index){
            //_.inRange(current_index, allquestions.length) not working ??
            if(current_index && current_index < allquestions.length && current_index>=0){
                scope.current_index =current_index;
            }
            scope.q = JSON.parse(scope.allquestions[scope.current_index]['version'])[0];
            scope.reviewid = scope.allquestions[scope.current_index]['id']
            scope.model = _.cloneDeep(scope.q['fields']);
            scope.model.id = scope.q.pk;
        };
        load();
        scope.cancel = function(){

          $modalInstance.close();
        }
        scope.next = function(){
            load(scope.current_index +1);
        }
        scope.previous = function(){
            load(scope.current_index -1);
        }
        scope.accept = function() {
            Review.question_accept({"questionid":scope.model.id}).then(
                this.onSuccess, Common.onError);

        };
        scope.onSuccess = function onSuccess(){
            var loc = "reviewer/course/" + scope.courseid + "/" + "task/" + scope.model.task;
            $modalInstance.close();
            $location.path(loc);
        };
        scope.edit = function () {
            var loc = "reviewer/course/" + scope.courseid + "/" + "task/" + scope.model.task;
            loc = loc + "/question/" + scope.reviewid;
            $modalInstance.close();
            $location.path(loc);
        };
        scope.onError = function onError(result){
            var msg = scope.$root.lang["error_"+result.data.error];
            scope.alerts.push({msg: msg, type: "danger"});
        };
    }

  angular.module("elproffApp")
        .controller("ModalsViewquestionversionCtrl", ["$scope", "$modal",
            "$modalInstance", "index", "allquestions", "courseid",
            "Review","Common", "$location", controller]);
}).call(null);