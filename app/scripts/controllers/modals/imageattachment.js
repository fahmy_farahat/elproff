"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ModalsImageattachmentCtrl
 * @description
 * # ModalsImageattachmentCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, $modalInstance, attachment_relation, element) {
        scope.isLoading = false;
        scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            scope.attachment = "";
            $modalInstance.close(result.attachment);
        };        
        scope.selectFile = function selectFile() {
            setTimeout(function() {
                angular.element("#attachment").trigger('click');
            }, 0);
        };
        scope.model = {};
        scope.saveImage = function saveImage(){
            var data = new FormData();
            scope.model.type = "image";
            var m = _.cloneDeep(scope.model);
            m.attachment = _.cloneDeep(scope.attachment);
            data.append("attachment_type", scope.model.type);
            if(_.isUndefined(m.attachment)) {
                    scope.isLoading = false;
                    scope.$root.alerts = Common.alerts.error.empty_choices;
                    return;
            }
            data.append("attachment", m.attachment);
            data.append("name", m.attachment);
            scope.isLoading = true;
            if(attachment_relation === "help") {
                Common.customRestangular("help_attachment", data, "submit_attachment")
                                    .then(scope.onSuccess, Common.onError(scope));    
            } else if (attachment_relation === "course") {
                data.append("element_id", element.id);
                Common.customRestangular("course_element_attachment", data, "submit_attachment")
                                    .then(scope.onSuccess, Common.onError(scope));
            } else if (attachment_relation === "question") {
                Common.customRestangular("question_attachment", data)
                                    .then(scope.onSuccess, Common.onError(scope));
            }            
        }                

/*
        function load() {
            scope.attachmentsElements = HelpAttachment.getList().$object;

        }
        load();
        scope.remove = function remove(entity) {
            scope.isLoading = true;
            HelpAttachment.one(""+entity.id).remove().then(this.onSuccess, Common.onError(scope));
        };


        scope.use = function use(element) {
            $modalInstance.close(element.attachment);

        };
*/
    }

    angular.module("elproffApp")
    .controller("ModalsImageattachmentCtrl",
        ["$scope", "Common", "$modalInstance", "attachment_relation", "crsElement", controller]);
}).call(null);