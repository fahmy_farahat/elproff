"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ModalsCreateobjectiveCtrl
 * @description
 * # ModalsCreateobjectiveCtrl
 * Controller of the elproffApp
 */
(function () {

    function controller(scope, Common, params, Objective, $modal, $modalInstance,
                        parentCourseElement) {

        scope.courseid = params.courseid;
        scope.isLoading = false;
        scope.model = {};
        scope.model.course_element = parentCourseElement;

        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            scope.model = result;
            $modalInstance.dismiss("cancel");
        };

        scope.bulks = [];
        scope.addBulk = function addBulk() {
            var modalInstance = $modal.open({
                templateUrl: "views/modals/createquestionsbulk.html",
                controller: "ModalsCreatequestionsbulkCtrl",
                size: "lg",
                resolve: {}
            });
            modalInstance.result.then(function (bulk) {
                scope.bulks.push(bulk);
            });
        };

        scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };

        scope.save = function save() {
            if (!scope.model.learning_outcome) {
                scope.alerts = Common.alerts.errors.learning_outcome_required;
                return;
            }
            scope.isLoading = true;
            var model = _.cloneDeep(scope.model);
            model.learning_outcome = model.learning_outcome.name;
            model.course_element = scope.model.course_element;
            model.questions_bulks = _.map(scope.bulks, function(originalBulk) {
            var bulk = _.cloneDeep(originalBulk);
                bulk.questions_type = bulk.questions_type.name;
                bulk.difficulty = bulk.difficulty.name;
                if (!bulk.tasks) {
                    bulk.tasks = [];
                    var savable = {
                        duration: bulk.count * 5,
                        description: scope.$root.lang.createquestions,
                        description_en: scope.$root.lang.trans_obj.en.createquestions,
                        description_ar: scope.$root.lang.trans_obj.ar.createquestions,
                        count: bulk.count,
                        course_element: scope.model.course_element.resource_uri
                    };
                    bulk.tasks.push(savable);
                }
                return bulk;
            });
            Objective.post(model)
                .then(this.onSuccess, Common.onError(scope, this.onError));
        };

    }
  angular.module("elproffApp")
        .controller("ModalsCreateobjectiveCtrl",
            ["$scope", "Common", "$routeParams", "Objective",
                  "$modal", "$modalInstance", "parentCourseElement",
                controller]);
}).call(null);
