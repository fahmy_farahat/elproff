"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ModalsAddedusysschoolCtrl
 * @description
 * # ModalsAddedusysschoolCtrl
 * Controller of the elproffApp
 */

(function(){

    function controller (scope, Common, $modalInstance, grades, system){
    	scope.model = {};
        scope.model.grades = grades;
        scope.model.systems = [system]; 
		scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $modalInstance.close("cancel");
        };
        scope.save = function save() {
        	
        };

    }

    angular.module("elproffApp")
        .controller("ModalsAddedusysschoolCtrl",
                ["$scope", "Common", "$modalInstance", "grades", "system", controller]);

}).call(null);
