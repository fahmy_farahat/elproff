"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ModalsViewquestiongroupCtrl
 * @description
 * # ModalsViewquestiongroupCtrl
 * Controller of the elproffApp
 */

(function () {
    function controller (scope,  modal, $modalInstance, Question, question, allQuestions, index, QuestionGroup, Common) {
    	scope.model = {};
        scope.q = question;
        scope.currentIndex = index;
        scope.questions = allQuestions;
        scope.alerts = [];
        function load(){
            scope.model = _.cloneDeep(scope.q.question);
        };
        load();
        scope.ok = function ok(){
        	$modalInstance.close();
        }
        scope.reject = function reject(){
            var data = {
                group_id: scope.q.group_id,
                question_id: scope.model.id,
            };
            QuestionGroup.reject(data).then(scope.onSuccess, scope.onError);
        }
        scope.change = function(newIndex){
            scope.q = scope.questions[newIndex];
            load();
        }
        scope.next = function next(){
            scope.currentIndex = scope.currentIndex+1;
            scope.change(scope.currentIndex);
        }


        scope.previous = function previous(){
            scope.currentIndex = scope.currentIndex-1;
            scope.change(scope.currentIndex);
        };
        scope.onSuccess = function onSuccess(result){
            scope.q.question = result.question;
            load();
        };
        scope.onError = function onError(result){
            var msg = scope.$root.lang["error_"+result.data.error];
            scope.alerts.push({msg: msg, type: "danger"});
        };
        scope.closeAlert = function closeAlert(index){
            scope.alerts.splice(index, 1);
        };


    }

  angular.module("elproffApp")
        .controller("ModalsViewquestiongroupCtrl", ["$scope", "$modal",
            "$modalInstance", "Question", "question", "allQuestions", "index", "QuestionGroup","Common", controller]);
}).call(null);