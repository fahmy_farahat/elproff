"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ModalsAssignstaffmembercourseCtrl
 * @description
 * # ModalsAssignstaffmembercourseCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, $modalInstance, $window, Service, user, type) {

        scope.$on("lang_updated", function() {
            scope.difficulties = Difficulty.getEntities();
            scope.outcomes = LearningOutcome.getEntities();
        });
        scope.alerts = [];
        scope.model = {};
        if(user.grades) {
            scope.model.grades = user.grades;   
        }
        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $modalInstance.close(scope.model.courses);
            //$window.location.reload();
        };
        scope.onError = function onError(result){
            var msg = scope.$root.lang["error_"+result.data.error];
            scope.alerts = [];
            scope.alerts.push({msg: msg, type: "danger"});
        };
        scope.closeAlert = function closeAlert(index){
            scope.alerts.splice(index, 1);
        };

        scope.ok = function () {
            scope.isLoading = true;
            var ids = _.pluck(scope.model.courses, "id");

            if (!ids || !ids.length) {
                scope.alerts = Common.alerts.error.emptycourses;
                return;
            }
            var model = {
                courses: ids,
                target_user: user.id
            }
            if(type === "author"){
                Service.assignAuthor(model).then(this.onSuccess, Common.onError(scope, scope.onError));    
            }
            else if(type === "sme"){
                Service.assignSME(model).then(this.onSuccess, Common.onError(scope, scope.onError));    
            }
            else if(type === "reviewer"){
                Service.assignReviewer(model).then(this.onSuccess, Common.onError(scope, scope.onError));    
            } 
            else if(type === "schoolteacher"){
                Service.assignTeacher(model).then(this.onSuccess, Common.onError(scope, scope.onError));    
            }

            //Service.assign(model).then(this.onSuccess, Common.onError(scope));
        };

        scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }

    angular.module("elproffApp")
    .controller("ModalsAssignstaffmembercourseCtrl",
            ["$scope", "Common", "Crumbs", "$modalInstance", "$window", "Course", "targetUser", "userType", controller] );
}).call(null);
