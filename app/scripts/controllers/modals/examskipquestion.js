"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ModalsExamSkipQuestion
 * @description
 * # ModalsExamSkipQuestion
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, $modalInstance) {
        scope.ok = function () {
            $modalInstance.close(scope.model);
        };

        scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }

    angular.module("elproffApp")
    .controller("ModalsExamSkipQuestion",
            ["$scope", "Common", "$modalInstance",
            controller] );
}).call(null);
