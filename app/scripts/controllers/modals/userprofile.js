"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ModalsUserprofileCtrl
 * @description
 * # ModalsUserprofileCtrl
 * Controller of the elproffApp
 */
(function() {
    function controller(scope, modalInstance, userlist) {
        scope.userlist = userlist;
        scope.cancel = function () {
            modalInstance.dismiss("cancel");
        };
    }
  angular.module("elproffApp")
    .controller("ModalsUserprofileCtrl", ["$scope", "$modalInstance", "userlist", controller]);
}).call(null);
