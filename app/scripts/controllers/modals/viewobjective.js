"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ModalsViewobjectiveCtrl
 * @description
 * # ModalsViewobjectiveCtrl
 * Controller of the elproffApp
 */
(function () {
    function controller (scope,  modal, $modalInstance, objectives, objectiveIndex) {
        if (objectives && objectiveIndex) {
          scope.model = objectives[objectiveIndex];
        }
   /*     scope.save = function save () {
            console.log("am Save :)");
        };*/
        scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };

    }

  angular.module("elproffApp")
        .controller("ModalsViewobjectiveCtrl", ["$scope", "$modal",
            "$modalInstance", "objectives", "objectiveIndex", controller]);
}).call(null);

