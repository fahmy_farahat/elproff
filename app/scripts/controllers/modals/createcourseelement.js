"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ModalsCreatecourseelementCtrl
 * @description
 * # ModalsCreatecourseelementCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, $rootScope, $modalInstance, CourseElement, currentElement) {

        scope.course = {};
        scope.elements = {};
        scope.model = {};
        scope.selectedLevels = [];

        if (currentElement){
            var selected = [];
            var generateLevels = function generateLevels(currentElement){
                CourseElement.getUri(currentElement).then(function(parentElement){
                    selected.push(parentElement);
                    if (parentElement.parent && !_.isEmpty(parentElement.parent)) {
                        generateLevels(parentElement.parent);
                    } else {
                        scope.$watch('selected', function(){
                            selected.reverse();
                            scope.selectedLevels = selected;
                        });
                    }
                });
            };
            generateLevels(currentElement);
        } 

        scope.onSuccess = function onSuccess(result) {
            var data = scope.model;
            scope.isLoading = false;
            $modalInstance.close(data);
        };
        scope.ok = function () {
            var element_type = scope.model;

            if (_.isUndefined(element_type.elements[0])) {
                var msg = scope.$root.lang.course_element_required;
                scope.alerts = [{
                    msg: msg,
                    type: "danger"
                }];
                    
                scope.model.selectedLevels = scope.selectedLevels;
                $rootScope.elements = scope.model;
            } else {
                $modalInstance.close(element_type);
            }
        };

        scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }

    angular.module("elproffApp")
        .controller("ModalsCreatecourseelementCtrl",
            ["$scope", "$rootScope", "$modalInstance", 
            "CourseElement", "currentElement", controller])
        .value("currentElement", null);
}).call(null);
