"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ModalsAddcourseelementresourceCtrl
 * @description
 * # ModalsAddcourseelementresourceCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, $modalInstance, CourseElementAttachment, element, $timeout, Restangular) {
        scope.model = {};
        scope.selected = {};
        scope.activeTab = 0;
        scope.previousActiveTab = -1; // use to reduce requests and conflicts
        scope.activeSubTab = 0;
        scope.filterName = "";
        scope.topTabs = [true, false, false];
        scope.subTabs = [[true, false], [true, false], [true, false]];


        scope.ok = function () {
            $modalInstance.close(scope.selected.selected);
        };

        scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };

        function load() {
            if (element) {
                var type = "unknown";
                switch(scope.activeTab) {
                    case 0: type = "image"; 
                            break;
                    case 1: type = "formula"; 
                            break;
                    case 2: type = "text";
                            break;
                }
                var query = {
                    course_element: element.id,
                    attachment_type: type
                };
                if (scope.filterName !== "") {
                    query.q = scope.filterName;
                }
                CourseElementAttachment.getList(query).then(function(resources) {
                    scope.resources = resources;
                });
            }
        }
        scope.$on("relist", load);

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            scope.alerts = Common.alerts.success.add;
            scope.subTabs[scope.activeTab][0] = false;
            scope.subTabs[scope.activeTab][1] = true;
            scope.clearForm();
            scope.previousActiveTab = -1;
            load();
        };
        scope.clearForm = function clearForm(){
            scope.model.name = "";
            scope.model.description = "";
            switch(scope.activeTab) {
                    case 0: scope.model.attachment = ""; 
                            break;
                    case 1: scope.mathEditor.setMathML("<math></math>");
                            break;
                    case 2: scope.model.text = "";
                            break;
                }
        };
        var lastSearchKeyword = "";
        scope.search = function search() {
            if (scope.filterName === lastSearchKeyword) {
                return;
            }
            scope.$root.$broadcast("relist");
        };

        scope.$watch("subTabs", function() {
            scope.activeSubTab = scope.subTabs[scope.activeTab].indexOf(true);
        }, true);

        scope.$watch("topTabs", function() {
            if (Common.pagination) {
                Common.pagination.page = 1;
            }
            scope.activeTab = scope.topTabs.indexOf(true);
        }, true);

        scope.$watch("activeTab", function() {
            if (scope.previousActiveTab !== scope.activeTab) {
                scope.previousActiveTab = scope.activeTab;
                load();
            }
        });

        scope.remove = function remove(entity) {
            scope.isLoading = true;
            scope.previousActiveTab = -1;
            entity.remove().then(load, _.noop);
        };

        scope.save = function save(type) {
            scope.isLoading = true;
            var m = _.cloneDeep(scope.model);
            m = Common.copyTranslation(m);
            var data = new FormData();
            data.append('name', m.name || "");
            data.append('description', m.description || "");
            if (type === "image") {
                data.append("attachment", m.attachment);
            } else if (type === "text") {
                data.append("text", m.text);
            } else if (type === "formula") {
                data.append("mml", scope.mathEditor.getMathML());
            }
            data.append("attachment_type", type);
            // reviewer controller passes element as uri not object
            if(element.hasOwnProperty("resource_uri")){
                data.append("course_element", element.resource_uri);
            }
            else{
                data.append("course_element", element);
            }
            Restangular.all("course_element_attachment")
                .withHttpConfig({transformRequest: angular.identity})
                .post(data, {}, {"Content-Type": undefined})
                .then(this.onSuccess, Common.onError(scope));
        };

        $timeout(function() {
            $(".addCourseElementResourceBody #text").markdown({
                autofocus:false,
                savable:false,
                onChange: function(e){
                    scope.$apply(function() {
                        scope.model.text = e.getContent();
                    });
                },
                hiddenButtons: ["cmdUrl","cmdImage"],
                additionalButtons: [
                    [{
                        name: "groupColors",
                        data: [{
                            name: "cmdRed",
                            toggle: false, // this param only take effect if you load bootstrap.js
                            title: scope.$root.lang.red,
                            icon: "fa fa-tint red",
                            callback: function(e){
                            // Replace selection with some drinks
                            var chunk, cursor,
                                selected = e.getSelection(), content = e.getContent();

                            if (!selected) {
                                return;
                            }
                            chunk = '<span style="color:red">' + selected.text + '</span>';

                            // transform selection and set the cursor into chunked text
                            e.replaceSelection(chunk);
                            cursor = selected.start;

                            // Set the cursor
                            e.setSelection(cursor,cursor+chunk.length);
                            }
                        }, {
                            name: "cmdBlue",
                            toggle: false, // this param only take effect if you load bootstrap.js
                            title: scope.$root.lang.blue,
                            icon: "fa fa-tint blue",
                            callback: function(e){
                            // Replace selection with some drinks
                            var chunk, cursor,
                                selected = e.getSelection(), content = e.getContent();

                            if (!selected) {
                                return;
                            }
                            chunk = '<span style="color:blue">' + selected.text + '</span>';

                            // transform selection and set the cursor into chunked text
                            e.replaceSelection(chunk);
                            cursor = selected.start;

                            // Set the cursor
                            e.setSelection(cursor, cursor+chunk.length);
                            }
                        }, {
                            name: "cmdGreen",
                            toggle: false, // this param only take effect if you load bootstrap.js
                            title: scope.$root.lang.green,
                            icon: "fa fa-tint green",
                            callback: function(e){
                            // Replace selection with some drinks
                            var chunk, cursor,
                                selected = e.getSelection(), content = e.getContent();

                            if (!selected) {
                                return;
                            }
                            chunk = '<span style="color:green">' + selected.text + '</span>';

                            // transform selection and set the cursor into chunked text
                            e.replaceSelection(chunk);
                            cursor = selected.start;

                            // Set the cursor
                            e.setSelection(cursor,cursor+chunk.length);
                            }
                        }]
                    }]
                ]

            });
            function loadMathEditor() {
                if (_.isUndefined(com)) {
                    scope.alerts = "Please, reload the page. Bad Internet Connection.";
                    return;
                }
                scope.mathEditor = com.wiris.jsEditor.JsEditor.newInstance({"language": "ar"});
                var mathContainer = document.getElementById("mathEditorInModal");
                scope.mathEditor.insertInto(mathContainer);
            }
            if (_.isUndefined(com)) {
                $timeout(loadMathEditor, 1000);
            } else {
                loadMathEditor();
            }
        });

        scope.closeAlert = function closeAlert(index) {
            scope.alerts = [];
        };
    }

    angular.module("elproffApp")
    .controller("ModalsAddCourseElementResourceCtrl",
            ["$scope", "Common", "$modalInstance", "CourseElementAttachment",
            "element", "$timeout", "Restangular", controller] );
}).call(null);
