"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ModalsPrivateteacheraddsubjectCtrl
 * @description
 * # ModalsPrivateteacheraddsubjectCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, $modalInstance, $window, Restangular, PrivateTeacher) {


        scope.model = {};
        var data = scope.model;
        scope.isLoading = false;
        scope.model.filter = true;

        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $modalInstance.close(data);
        };

        scope.onError = function onError(result) {
            scope.isLoading = false;
            if (result.data.error == "course_already_listed") {
                scope.alerts = [{
                    msg: scope.$root.lang.error_course_already_listed,
                    type: "danger"
                }];
            }
        };

        scope.ok = function () {
            scope.isLoading = true;
            var data =
            {
                "course" : scope.model.course.id
            };
            PrivateTeacher.addCourse(data).then(this.onSuccess, scope.onError);

        };

        scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }

    angular.module("elproffApp")
    .controller("ModalsPrivateteacheraddsubjectCtrl",
            ["$scope", "Common", "Crumbs", "$modalInstance", "$window","Restangular","PrivateTeacher", controller] );
}).call(null);
