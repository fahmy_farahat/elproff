"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ModalsUpdatequestionsbulkCtrl
 * @description
 * # ModalsCreatequestionsbulkCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, $modalInstance, bulk, QuestionsBulk, QuestionType, Difficulty, Task) {

        // Viewing objective
        scope.types = QuestionType.getEntities();
        scope.difficulties = Difficulty.getEntities();

        QuestionsBulk.get(bulk.id).then(function(bulk) {
            bulk.questions_type = _.find(scope.types, {name: bulk.questions_type});
            bulk.difficulty = _.find(scope.difficulties, {name: bulk.difficulty});
            scope.model =  bulk;
        });
        function done() {
            $modalInstance.close(scope.model);
        }
        scope.ok = function () {
            var bulk = scope.model.clone();
            bulk.questions_type = bulk.questions_type.name;
            bulk.difficulty = bulk.difficulty.name;
            bulk.put().then(function() {
                Task.getList({questions_bulk: bulk.id}).then(function(tasks) {
                    if (!tasks.length) {
                        return;
                    }
                    var afterAll = _.after(tasks.length, done);
                    _.each(tasks, function(task) {
                        task.count = Math.ceil(bulk.count / tasks.length);
                        task.put().then(afterAll);
                    });
                });
            });
            $modalInstance.close();
        };

        scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }

    angular.module("elproffApp")
    .controller("ModalsUpdateQuestionsbulkCtrl",
            ["$scope", "Common", "Crumbs", "$modalInstance", "bulk",
            "QuestionsBulk", "QuestionType", "Difficulty", "Task", controller] );
}).call(null);
