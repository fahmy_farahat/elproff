"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ModalsElementtypeCtrl
 * @description
 * # ModalsElementtypeCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, modalInstance, elementtype, CourseElement) {
        scope.loaded = false;
        scope.elementtype = elementtype;

        scope.onElementSuccess = function onElementSuccess(result){
            scope.elements = result;
            scope.loaded = true;
        };

        scope.cancel = function () {
            modalInstance.dismiss("cancel");
        };

        scope.remove = function () {
            scope.isLoading = true;
            elementtype.remove().then(function() {
                scope.$root.$broadcast("relist");
                modalInstance.dismiss("cancel");
            }, Common.onError(scope));
        };

        function load() {
            scope.loaded = false;
            var query = {"element_type": scope.elementtype.id, limit: 0};
            CourseElement.getList(query).then(scope.onElementSuccess, Common.onError(scope));
        }
        load();
    }

    angular.module("elproffApp")
    .controller("ModalsElementtypeCtrl",
        ["$scope", "Common", "$modalInstance", "elementtype", "CourseElement", controller]);
}).call(null);

