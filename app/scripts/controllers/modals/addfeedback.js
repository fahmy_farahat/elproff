"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ModalsAddfeedbackCtrl
 * @description
 * # ModalsAddfeedbackCtrl
 * Controller of the elproffApp
 */

(function(){

    function controller (scope, Common, QuestionFeedback, $modalInstance, question){
    	scope.model = {};
        scope.question = question;
		scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $modalInstance.close("cancel");
        };
        scope.save = function save() {
            if (!scope.model.feedback_type) {
                scope.alerts = Common.alerts.errors.feedback_title_required ;
                return;
            }
            scope.isLoading = true;
            var model = _.cloneDeep(scope.model);
            model.feedback_type = model.feedback_type.name;
            model.question = scope.question;
            QuestionFeedback.post(model)
                .then(scope.onSuccess, Common.onError(scope));
        };

    }

    angular.module("elproffApp")
        .controller("ModalsAddfeedbackCtrl",
                ["$scope", "Common", "QuestionFeedback", "$modalInstance", "question",
                controller]);

}).call(null);
