"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ModalsCreatequestionsbulkCtrl
 * @description
 * # ModalsCreatequestionsbulkCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, $modalInstance) {

        scope.model = {
            count: 10
        };
        scope.alerts = [];
        scope.ok = function () {
            var bulk = scope.model;
            var msg;
            if (_.isUndefined(bulk.questions_type)) {
                msg = scope.$root.lang.questions_type_required;
            } else if (_.isUndefined(bulk.count)) {
                msg = scope.$root.lang.count_required;
            } else {
                $modalInstance.close(bulk);
            }
            scope.alerts = [{
                msg: msg,
                type: "danger"
            }];
        };
        scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }

    angular.module("elproffApp")
    .controller("ModalsCreatequestionsbulkCtrl",
            ["$scope", "Common", "$modalInstance", controller] );
}).call(null);
