"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ModalsExamSkippedQuestionsCtrl
 * @description
 * # ModalsExamSkippedQuestionsCtrl
 * Controller of the elproffApp
 */
(function(){

  function controller(scope, Common, skippedQuestionsList, $location, $modalInstance) {
    scope.skippedQuestionsList = skippedQuestionsList;
    console.log(skippedQuestionsList);
    function changeQuestion(questionIndex) {
      var path = $location.path().split("/");
      path[path.length-1] = questionIndex;
      $location.path(path.join("/"));
    }

    scope.jump = function(questionIndex) {
      changeQuestion(questionIndex);
      $modalInstance.dismiss("cancel");
    }

    scope.ok = function () {
      changeQuestion(scope.skippedQuestionsList[0]);
      $modalInstance.close();
    };

    scope.cancel = function () {
      $modalInstance.dismiss("cancel");
    };
  }

  angular.module("elproffApp")
  .controller("ModalsExamSkippedQuestions",
          ["$scope", "Common", "skippedQuestionsList", "$location", "$modalInstance", 
          controller] );
}).call(null);
