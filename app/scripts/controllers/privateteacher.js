"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:PrivateteacherCtrl
 * @description
 * # PrivateteacherCtrl
 * Controller of the elproffApp
 */
(function () {


    function controller(scope, Common, Crumbs, $modal, Course, CommonCache) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/privateteacher"},
        ];

        scope.course = [];

        function load() {
            Course.getList({limit:0}).then(function (result) {
                scope.courses = _.map(result, CommonCache.course);
            });
        }
        load();
        scope.$on("relist", load);

        scope.addCourse = function (size) {
            var modalInstance = $modal.open({
                templateUrl: "views/modals/privateteacheraddsubject.html",
                controller: "ModalsPrivateteacheraddsubjectCtrl",
                resolve: {}
            });

            modalInstance.result.then(function (course) {
                Course.clearCache();
                scope.$emit("relist");
            });
        };
    }

    angular.module("elproffApp")
           .controller("PrivateteacherCtrl",
                   ["$scope", "Common", "Crumbs", "$modal", "Course",
                   "CommonCache", controller]);

}).call(null);
