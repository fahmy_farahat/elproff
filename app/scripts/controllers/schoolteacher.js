"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchoolteacherCtrl
 * @description
 * # SchoolteacherCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, Course, CommonCache) {

        scope.crumbs = [
            {title: "dashboard", link: "#/schoolteacher/"},
        ];

        function load() {
            Course.getList({limit:0}).then(function (result) {
                scope.courses = _.map(result, CommonCache.course);
            });
        }
        load();
        scope.$on("relist", load);

    }

    angular.module("elproffApp")
    .controller("SchoolteacherCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "Course", "CommonCache", controller]);
}).call(null);
