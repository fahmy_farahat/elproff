"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:RegisterCtrl
 * @description
 * # RegisterCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, User, $location) {
      // if logged in go home
        if (User.getUser()) {
            $location.path("/" + User.getUser().type);
        }

        scope.registered = false;
        scope.isLoading = false;
        scope.onSuccess = function onSuccess() {
            scope.isLoading = false;
            scope.registered = true;
            scope.model = {};
        };

        scope.alerts = [];
        scope.model = {};
        scope.onError = function onError(result) {
            scope.isLoading = false;
            scope.registered = false;
            if (result.data.error_message === "captcha_error") {
                scope.alerts = Common.alerts.error.captcha;
            } else if (result.data.error === "Unsupported user type") {
                scope.alerts = Common.alerts.error.unsupported_user_type;
            } else {
                scope.alerts = Common.alerts.error.register;
            }
        };

        scope.register = function register() {
            scope.isLoading = true;
            var model = _.cloneDeep(this.model);
            model.email = model.email.toLowerCase();
            model.address = {};
            //model.address.country = model.country;
            model.country = model.country.resource_uri;
            model.systems = _.pluck(model.systems, "resource_uri");
            User.create(model).then(this.onSuccess, Common.onError(scope, this.onError));
        };

        scope.closeAlert = function(index) {
            scope.alerts = [];
        };
    }

    angular.module("elproffApp")
    .controller("RegisterCtrl",
            ["$scope", "Common", "Crumbs", "User", "$location", controller]);

}).call(null);
