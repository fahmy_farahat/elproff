"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, User, $location) {

        function redirection() {
            var user = User.getUser();
            if (user) {
                $location.path("/" + User.getUser().type);
            } else {
                $location.path("/login");
            }
        }

        //scope.$on("user_updated", redirection);
        redirection();
    }

    angular.module("elproffApp")
    .controller("MainCtrl", ["$scope", "Common", "Crumbs", "User", "$location", controller] );
}).call(null);
