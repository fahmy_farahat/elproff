"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentCtrl
 * @description
 * # StudentCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, Student, CommonCache) {
        scope.crumbs = [
            {title: "dashboard", link: ""},
        ];
        scope.courses = [];
        scope.subscribableCourses = [];

        scope.onSubscribableSuccess = function(result) {
            scope.subscribableCourses = result.objects;
            scope.subscribableCourses = _.map(result.objects, CommonCache.course);
        };

        scope.onListSuccess = function onListSuccess(result) {
            scope.courses = _.map(result.objects, CommonCache.course);
        };

        function load() {
            Student.listCourses({}).then(scope.onListSuccess, Common.onError(scope));
            Student.listSubscribableCourses({}).then(scope.onSubscribableSuccess, Common.onError(scope));
        }
        load();
        scope.$on("relist", load);
    }

    angular.module("elproffApp")
    .controller("StudentCtrl",
            ["$scope", "Common", "Crumbs", "Student", "CommonCache", controller]);
}).call(null);
