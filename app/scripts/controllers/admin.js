"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminCtrl
 * @description
 * # AdminCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(root, scope, User) {

        scope.crumbs = [
        {title: "dashboard", link: "#/admin/"},
        ];

    }

    angular.module("elproffApp")
        .controller("AdminCtrl", ["$rootScope", "$scope", "User", controller] );
}).call(null);
