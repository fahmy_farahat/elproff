"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ManagerProfileEditCtrl
 * @description
 * # ManagerProfileEditCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("ManagerProfileEditCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
