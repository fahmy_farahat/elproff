"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ManagerProfileChangepasswordCtrl
 * @description
 * # ManagerProfileChangepasswordCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("ManagerProfileChangepasswordCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
