"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ManagerProfileCtrl
 * @description
 * # ManagerProfileCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("ManagerProfileCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
