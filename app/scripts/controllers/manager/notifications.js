"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ManagerNotificationsCtrl
 * @description
 * # ManagerNotificationsCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("ManagerNotificationsCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
