"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ResetpasswordCtrl
 * @description
 * # ResetpasswordCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, params, $location, User) {

        scope.alerts = [];
        scope.closeAlert = function(index) {
            scope.alerts = [];
        };
        scope.isLoading = false;
        scope.model = {};
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            scope.alerts = Common.alerts.success.passwordreset;
            $location.path("/login");
        };

        scope.onError = function onError(error) {
            scope.isLoading = false;
            scope.alerts = Common.alerts.error.passwordreset;
        };

        scope.sendMail = function sendMail() {
            scope.isLoading = true;
            var data = {
                password: scope.model.password,
                token: params.token
            };
            User.resetPassword(data).then(this.onSuccess, Common.onError(scope, this.onError));
        };
    }

    angular.module("elproffApp")
    .controller("ResetpasswordCtrl", 
            ["$scope", "Common", "$routeParams", "$location", "User", controller]);
}).call(null);
