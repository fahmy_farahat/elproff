"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchooladminSchoolGradeAddCtrl
 * @description
 * # SchooladminSchoolGradeAddCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("SchooladminSchoolGradeAddCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
