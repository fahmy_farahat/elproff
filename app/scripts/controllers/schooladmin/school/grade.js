"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchooladminSchoolGradeCtrl
 * @description
 * # SchooladminSchoolGradeCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("SchooladminSchoolGradeCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
