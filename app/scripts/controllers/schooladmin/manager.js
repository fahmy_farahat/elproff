"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchooladminManagerCtrl
 * @description
 * # SchooladminManagerCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Manager, School) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/schooladmin/"},
            {title: 'managers', link: ""},
        ];
        scope.model = {};
        function load() {
            School.getList().then(function(schools){
                scope.model.school = schools[0];
                Manager.getList({school: scope.model.school.id }).then(function(entities) { 
                    scope.managers = entities; 
                });
            });
        }
        load();
        scope.searchable = true;

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            scope.alerts = Common.alerts.success.remove;
        };

        scope.onError = function onError(error) {
            scope.isLoading = false;
            scope.alerts = Common.alerts.error.remove;
        };

        scope.remove = function remove(entity) {
            scope.isLoading = true;
            entity.remove().then(this.onSuccess, Common.onError(scope, this.onError));
        };
    }

    angular.module("elproffApp")
    .controller("SchooladminManagerCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "Manager", "School", controller]);
}).call(null);
