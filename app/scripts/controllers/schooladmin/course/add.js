"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchooladminCourseAddCtrl
 * @description
 * # SchooladminCourseAddCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service) {

        scope.model = {};
        scope.crumbs = [
            {title: 'dashboard', link: "#/schooladin/"},
            {title: 'classes', link: "#/schooladmin/classes/"},
            {title: 'create', link: ""},
        ];

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            scope.alerts = Common.alerts.success.createclass;
            $location.path(scope.crumbs[1].link.substring(1));
        };

        scope.onError = function onError(error) {
            scope.alerts = Common.alerts.error.createclass;
        };

        scope.save = function save() {
            scope.isLoading = true;
            var model = _.cloneDeep(scope.model);
            delete model.system;
            delete model.country;
            Service.post(model).then(this.onSuccess, Common.onError(scope, this.onError));
        };

    }

    angular.module("elproffApp")
    .controller("SchooladminCourseAddCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "Course", controller]);
}).call(null);
