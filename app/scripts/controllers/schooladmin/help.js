"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchooladminHelpCtrl
 * @description
 * # SchooladminHelpCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, $location,  Service) {
    	$(".faq-navigation a").click(function(){
            var faqId = $(this).attr("data-faq");
            if(!$(this).hasClass("active")) {
                $(".faq-content .faq").removeClass("active").fadeOut();
                $("#"+faqId).fadeIn(1500).addClass("active");
                $(".faq-navigation a").removeClass("active");
                $(this).addClass("active");
            }
            return false;
        });
    }

    angular.module("elproffApp")
    .controller("SchooladminHelpCtrl", ["$scope", "Common", "Crumbs", "$routeParams", "$location", "User", controller] );
}).call(null);
