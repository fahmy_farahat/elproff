"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchooladminStudentCtrl
 * @description
 * # SchooladminStudentCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, School, Grade, EduSystem) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/schooladmin/"},
            {title: 'edu_system', link: ""},
        ];
        scope.model = {};
        function load() {

            School.getList().then(function(school){
                scope.model = school[0];
                scope.model.systems = [];
                _.each(scope.model.grades, function(gradeUri, index){
                    Grade.getUri(gradeUri).then(function(grade){
                        scope.model.grades[index] = grade;
                        EduSystem.getUri(grade.system).then(function(system){
                            var isThere = _.some(scope.model.systems, function(sys){
                                return sys.id === system.id;
                            });
                            if(!isThere){
                                scope.model.systems.push(system);
                            }
                        });
                    });

                });
            });
        }
        load();
        scope.$on("relist", load);

        scope.searchable = true;
        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            scope.alerts = Common.alerts.success.remove;
        };

        scope.onError = function onError(error) {
            scope.isLoading = false;
            scope.alerts = Common.alerts.error.remove;
        };

        scope.remove = function remove(entity) {
            scope.isLoading = true;
            entity.remove().then(this.onSuccess, Common.onError(scope, this.onError));
        };


    }

    angular.module("elproffApp")
    .controller("SchooladminStudentCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location",
         "School", "Grade", "EducationalSystem", controller]);
}).call(null);
