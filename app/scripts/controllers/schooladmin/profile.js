"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchooladminProfileCtrl
 * @description
 * # SchooladminProfileCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, User) {

    }

    angular.module("elproffApp")
    .controller("SchooladminProfileCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "User", controller]);
}).call(null);
