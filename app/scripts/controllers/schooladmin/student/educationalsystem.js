"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchooladminStudentEducationalsystemCtrl
 * @description
 * # SchooladminStudentEducationalsystemCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, modal, School, Grade, EduSystem) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/schooladmin/"},
            {title: 'edu_system', link: "#/schooladmin/educationalsystems"},
            {title: params.systemid, link: ""},
        ];
        scope.model = {};
        scope.systemid = params.systemid;
        Crumbs.setSystem(scope.crumbs[2], params.systemid, scope);
        function load() {
            School.getList().then(function(school){
                scope.model = school[0];
                EduSystem.get(params.systemid).then(function(system){
                	scope.model.system = system;
	                _.each(scope.model.grades, function(gradeUri, index){
	                    Grade.getUri(gradeUri).then(function(grade){
	                    	if(grade.system === system.resource_uri){
	                    		scope.model.grades[index] = grade;	
	                    	}
	                    	else {
	                    		scope.model.grades.splice(index, 1);
	                    	}
	                        
	                    });

	                });
                });
            });
        }
        load();
        scope.$on("relist", load);

        scope.searchable = true;
        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            scope.alerts = Common.alerts.success.remove;
        };

        scope.onError = function onError(result) {
            scope.isLoading = false;
            scope.alerts = Common.alerts.error[result.data.error];
        };

        scope.remove = function remove(entity) {
            scope.isLoading = true;
            var data = {grade: entity.id};
            School.deleteGrade(data).then(this.onSuccess, Common.onError(scope, scope.onError));
        };

        scope.addGrade = function addGrade(){
            var modalInstance = modal.open({
                templateUrl: "views/modals/addedusysschool.html",
                controller: "ModalsAddedusysschoolCtrl",
                resolve: {
                    grades: function(){
                        return scope.model.grades;
                    },
                    system: function(){
                        return scope.model.system;
                    }
                },
            });
        };
 

    }

    angular.module("elproffApp")
    .controller("SchooladminStudentEducationalsystemCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "$modal",
         "School", "Grade", "EducationalSystem", controller]);
}).call(null);