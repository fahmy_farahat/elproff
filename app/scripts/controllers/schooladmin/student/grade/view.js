"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchooladminStudentGradeViewCtrl
 * @description
 * # SchooladminStudentGradeViewCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, modal, School, Grade, EduSystem, Classes) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/schooladmin/"},
            {title: 'edu_system', link: "#/schooladmin/educationalsystems"},
            {title: params.systemid, link: "#/schooladmin/educationalsystems/" + params.systemid},
            {title: 'grade', link: "#/schooladmin/educationalsystems/" + params.systemid},
            {title: params.gradeid, link: ""},
            {title: "classes", link: ""},
        ];
        scope.model = {};
        scope.systemid = params.systemid;
        scope.gradeid = params.gradeid;
        Crumbs.setSystem(scope.crumbs[2], params.systemid, scope);
        Crumbs.setGrade(scope.crumbs[4], params.gradeid, scope);
        function load(){
            School.getList().then(function(schools){
                scope.model.school = schools[0];
                Classes.getList({school:scope.model.school.id, grade:scope.gradeid}).then(function(classes){
                    scope.model.classes = classes;
                });
            });
        }
        load();
        scope.$on("relist", load);

        scope.searchable = true;
        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            scope.alerts = Common.alerts.success.remove;
        };

        scope.onError = function onError(error) {
            scope.isLoading = false;
            scope.alerts = Common.alerts.error.remove;
        };

        scope.remove = function remove(entity) {
            scope.isLoading = true;
            entity.remove().then(this.onSuccess, Common.onError(scope, this.onError));
        };
 

    }

    angular.module("elproffApp")
    .controller("SchooladminStudentGradeViewCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "$modal",
         "School", "Grade", "EducationalSystem", "Classes", controller]);
}).call(null);