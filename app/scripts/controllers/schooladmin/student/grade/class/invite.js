"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchooladminStudentGradeClassInviteCtrl
 * @description
 * # SchooladminStudentGradeClassInviteCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, School, Classes) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/schooladmin/"},
            {title: 'edu_system', link: "#/schooladmin/educationalsystems"},
            {title: params.systemid, link: "#/schooladmin/educationalsystems/" + params.systemid},
            {title: 'grade', link: "#/schooladmin/educationalsystems/" + params.systemid},
            {title: params.gradeid, link: "#/schooladmin/educationalsystems/" + params.systemid + "/grade/" + params.gradeid},
            {title: "classes", link: ""},
            {title: params.classid, link: ""},
            {title: "invitestudent", link: ""},
        ];
        scope.model = {};
        scope.systemid = params.systemid;
        scope.gradeid = params.gradeid;
        scope.classid = params.classid;
        Crumbs.setSystem(scope.crumbs[2], params.systemid, scope);
        Crumbs.setGrade(scope.crumbs[4], params.gradeid, scope);
        School.getList().then(function(schools){
                scope.model.school = schools[0];
                Classes.get(scope.classid).then(function(classObj){
                    scope.model.class_obj = classObj;    
                });
        });

    }

    angular.module("elproffApp")
    .controller("SchooladminStudentGradeClassInviteCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams",
         "$location", "School", "Classes", controller]);
}).call(null);
