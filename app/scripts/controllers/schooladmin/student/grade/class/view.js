"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchooladminStudentGradeClassViewCtrl
 * @description
 * # SchooladminStudentGradeClassViewCtrl
 * Controller of the elproffApp
 */

(function() {

    function controller(scope, Common, Crumbs, params, $location, Student) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/schooladmin/"},
            {title: 'edu_system', link: "#/schooladmin/educationalsystems"},
            {title: params.systemid, link: "#/schooladmin/educationalsystems/" + params.systemid},
            {title: 'grade', link: "#/schooladmin/educationalsystems/" + params.systemid},
            {title: params.gradeid, link: "#/schooladmin/educationalsystems/" + params.systemid + "/grade/" + params.gradeid},
            {title: "classes", link: "#/schooladmin/educationalsystems/" + params.systemid + "/grade/" + params.gradeid},
            {title: params.classid, link: ""},
        ];
        scope.model = {};
        scope.systemid = params.systemid;
        scope.gradeid = params.gradeid;
        scope.classid = params.classid;
        Crumbs.setSystem(scope.crumbs[2], params.systemid, scope);
        Crumbs.setGrade(scope.crumbs[4], params.gradeid, scope);
        Crumbs.setClass(scope.crumbs[6], params.classid, scope);
        function load(){
            Student.getList({classes:params.classid}).then(function(students){
                scope.model.students = students;
            });
        }
        load();

        scope.searchable = true;
        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            scope.alerts = Common.alerts.success.remove;
        };

        scope.onError = function onError(error) {
            scope.isLoading = false;
            scope.alerts = Common.alerts.error.remove;
        };

        scope.remove = function remove(entity) {
            scope.isLoading = true;
            entity.remove().then(this.onSuccess, Common.onError(scope, this.onError));
        };
 

    }

    angular.module("elproffApp")
    .controller("SchooladminStudentGradeClassViewCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "Student", controller]);
}).call(null);