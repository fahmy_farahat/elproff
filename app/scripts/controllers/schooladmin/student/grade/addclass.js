"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchooladminStudentGradeAddclassCtrl
 * @description
 * # SchooladminStudentGradeAddclassCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, Classes, $location, School) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/schooladmin/"},
            {title: 'edu_system', link: "#/schooladmin/educationalsystems"},
            {title: params.systemid, link: "#/schooladmin/educationalsystems/" + params.systemid},
            {title: 'grade', link: "#/schooladmin/educationalsystems/" + params.systemid},
            {title: params.gradeid, link: "#/schooladmin/educationalsystems/" + params.systemid + "/grade/" + params.gradeid},
            {title: "addclass", link: ""},
        ];
        scope.model = {};
        scope.systemid = params.systemid;
        scope.gradeid = params.gradeid;
        Crumbs.setSystem(scope.crumbs[2], params.systemid, scope);
        Crumbs.setGrade(scope.crumbs[4], params.gradeid, scope);
        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $location.path(scope.crumbs[1].link.substring(1));
        };
        
        scope.onError = function onError(error) {
            scope.alerts = Common.alerts.error.createclass;
        };
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $location.path(scope.crumbs[4].link.substring(1));
        };

        scope.save = function save() {
            scope.isLoading = true;
            var model = _.cloneDeep(scope.model);
            model = Common.copyTranslation(model);
            model.grade = scope.model.grade.resource_uri;
            delete model.system;
            Classes.post(model).then(this.onSuccess, Common.onError(scope));
        };

    }

    angular.module("elproffApp")
    .controller("SchooladminStudentGradeAddclassCtrl",
            ["$scope", "Common", "Crumbs", "$routeParams", "Classes",
            "$location", "School", controller] );
}).call(null);