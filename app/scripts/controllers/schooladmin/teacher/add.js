"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchooladminTeacherAddCtrl
 * @description
 * # SchooladminTeacherAddCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, School, Grade) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/schooladmin/"},
            {title: 'teachers', link: "#/schooladmin/teacher/"},
            {title: 'add', link: ""},
        ];
        scope.model = {};
        School.getList().then(function(schools){
            scope.model.school = schools[0];
            scope.model.grades = _.map(schools[0].grades, function(g){
                return Grade.getUri(g).$object;
            });
        });

    }

    angular.module("elproffApp")
    .controller("SchooladminTeacherAddCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "School", "Grade", controller]);
}).call(null);
