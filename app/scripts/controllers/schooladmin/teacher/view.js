"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchooladminTeacherViewCtrl
 * @description
 * # SchooladminTeacherViewCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, CommonCache, SchoolTeacher, School, Grade) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/schooladmin/"},
            {title: 'teachers', link: "#/schooladmin/teacher/"},
            {title: 'manage', link: ""},
        ];

        scope.model = { };
        SchoolTeacher.one(params.teacherid).get().then(function(obj) {
            scope.model = obj;
            CommonCache.staffMember(scope.model);
            School.getUri(obj.school).then(function(school){
                scope.model.school = school;
                scope.model.grades = _.map(school.grades, function(g){
                    return Grade.getUri(g).$object;
                });
            });
        });

    }

    angular.module("elproffApp")
    .controller("SchooladminTeacherViewCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams"
         , "CommonCache", "SchoolTeacher", "School", "Grade", controller]);
}).call(null);
