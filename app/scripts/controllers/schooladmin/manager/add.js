"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchooladminManagerAddCtrl
 * @description
 * # SchooladminManagerAddCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, School) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/schooladmin/"},
            {title: 'managers', link: "#/schooladmin/manager/"},
            {title: 'add', link: ""},
        ];
        scope.model = {};
        School.getList().then(function(schools){
                scope.model.school = schools[0];
        });
    }

    angular.module("elproffApp")
    .controller("SchooladminManagerAddCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "School", controller]);
}).call(null);
