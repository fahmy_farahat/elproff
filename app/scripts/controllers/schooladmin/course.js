"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchooladminCourseCtrl
 * @description
 * # SchooladminCourseCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service) {

        scope.model = {};
        scope.crumbs = [
            {title: 'dashboard', link: "#/schooladmin/"},
            {title: 'courses', link: ""},
        ];

        function load() {
            Service.courses().then(function(entities) {
                scope.entities = entities;
            });
        }
        load();
        scope.$on("relist", load);
        scope.searchable = true;

    }

    angular.module("elproffApp")
    .controller("SchooladminCourseCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "School", controller]);
}).call(null);
