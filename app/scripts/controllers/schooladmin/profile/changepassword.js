"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchooladminProfileChangepasswordCtrl
 * @description
 * # SchooladminProfileChangepasswordCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service) {

        scope.links = [{
            name: "school",
            title: scope.$root.lang.school,
            href: "#/schooladmin/profile/school"
        }, {
            name: "changepassword",
            title: scope.$root.lang.changepassword,
            href: "#/schooladmin/profile/changepassword"
        }, {
            name: "editprofile",
            title: scope.$root.lang.editprofile,
            href: "#/schooladmin/profile/edit"
        }, {
            name: "view",
            title: scope.$root.lang.profile,
            href: "#/schooladmin/profile"
        }];

    }

    angular.module("elproffApp")
    .controller("SchooladminProfileChangepasswordCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "User", controller]);
}).call(null);
