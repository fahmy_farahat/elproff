"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchooladminProfileSchoolCtrl
 * @description
 * # SchooladminProfileSchoolCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, Country, Address, Service, CommonCache, EducationalSystem, Grade, Restangular) {

        scope.model = {};
        Service.one(params.schoolid).get().then(function(school) {
            scope.model = school;
            var gradesUris = _.cloneDeep(school.grades)
            scope.model.systems = [];
            scope.model.grades = [];
            _.each(gradesUris, function(gradeUri){
                Grade.getUri(gradeUri).then(function(grade){
                    scope.model.grades.push(grade);
                    EducationalSystem.getUri(grade.system).then(function(system){
                        var isThere = _.some(scope.model.systems, function(sys){
                            return sys.id === system.id;
                        });
                        if(!isThere){
                            scope.model.systems.push(system);
                        }
                    });    
                });
            });
            if (school.address) {
                Address.getUri(school.address).then(function(address){
                    Country.getUri(address.country).then(function(country){
                        scope.model.country = country;
                    });
                    scope.model.city = address.city;
                    scope.model.street_address = address.address;
                    scope.model.state = address.state;    
                });
                
            }
        });

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            scope.alerts = Common.alerts.success.update_school;
        };

        scope.onError = function onError(error) {
            scope.isLoading = false;
            scope.alerts = Common.alerts.error.update_school;
        };

        scope.save = function save(form) {
            scope.isLoading = true;
            var data = new FormData();
            var model = _.cloneDeep(scope.model);
            model = Common.copyTranslation(model);
            data.append("id", model.id);
            data.append("name", model.name);
            data.append("name_en", model.name_en || "");
            data.append("name_ar", model.name_ar || "");
            data.append("phone_number", model.phone_number || "");
            data.append("state", model.state || "");
            data.append("city", model.city || "");
            if(model.country){
                data.append("country", model.country.resource_uri);    
            }
            data.append("street_address", model.street_address);
            if(_.isString(model.grades[0])){
                data.append("grades", angular.toJson(model.grades));
            }
            else {
                data.append("grades", angular.toJson(_.pluck(model.grades, "resource_uri")));    
            }
            
            data.append("image", scope.image);
            Common.customRestangular("school", data, "update").then(this.onSuccess, Common.onError(scope, this.onError));
        };

        scope.closeAlert = function closeAlert(index) {
            scope.alerts = [];
        };

        scope.selectFile = function selectFile() {
            setTimeout(function() {
                angular.element("#schoolimage").trigger('click');
            }, 0);
        };

    }

    angular.module("elproffApp")
    .controller("SchooladminProfileSchoolCtrl",
            ["$scope", "Common", "Crumbs", "$routeParams", "Country", "Address", "School",
           "CommonCache", "EducationalSystem", "Grade", "Restangular", controller]);
}).call(null);
