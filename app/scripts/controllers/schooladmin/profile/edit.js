"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchooladminProfileEditCtrl
 * @description
 * # SchooladminProfileEditCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("SchooladminProfileEditCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
