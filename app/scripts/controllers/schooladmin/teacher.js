"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchooladminTeacherCtrl
 * @description
 * # SchooladminTeacherCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, SchoolTeacher, School) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/schooladmin/"},
            {title: 'teachers', link: ""},
        ];

        function load() {
            School.getList().then(function(school){
                SchoolTeacher.getList({school: school[0].id}).then(function(entities) {
                    scope.entities = entities;
                });    
            });
            
        }
        load();
        scope.$on("relist", load);
        scope.searchable = true;

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            scope.alerts = Common.alerts.success.remove;
        };

        scope.onError = function onError(error) {
            scope.isLoading = true;
            scope.alerts = Common.alerts.error.remove;
        };

        scope.remove = function remove(entity) {
            scope.isLoading = true;
            entity.remove().then(this.onSuccess, Common.onError(scope, this.onError));
        };
    }

    angular.module("elproffApp")
    .controller("SchooladminTeacherCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "SchoolTeacher", "School", controller]);
}).call(null);
