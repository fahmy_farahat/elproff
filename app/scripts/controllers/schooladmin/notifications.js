"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchooladminNotificationsCtrl
 * @description
 * # SchooladminNotificationsCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("SchooladminNotificationsCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
