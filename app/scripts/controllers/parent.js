"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ParentCtrl
 * @description
 * # ParentCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, Parent) {

        scope.crumbs = [
            {title: "dashboard", link: ""},
        ];

        scope.sons = [];
        function load() {
            Parent.listSons().then(function(result) {
                scope.sons = result.objects;
            });
        }
        load();
        scope.$on("relist", load);

    }

    angular.module("elproffApp")
    .controller("ParentCtrl",
        ["$scope", "Common", "Crumbs", "Parent", controller] );
}).call(null);
