"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ForgotpasswordCtrl
 * @description
 * # ForgotpasswordCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, User) {

        scope.alerts = [];
        scope.closeAlert = function(index) {
            scope.alerts = [];
        };
        scope.isLoading = false;
        scope.model = {};
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            scope.alerts = Common.alerts.success.forgotpasswordemailsent;
        };

        scope.sendMail = function sendMail() {
            scope.isLoading = true;
            var email =  scope.model.email.toLowerCase();
            User.forgotPassword({email: email}).then(
                this.onSuccess, Common.onError(scope));
        };
    }

    angular.module("elproffApp")
    .controller("ForgotpasswordCtrl", ["$scope", "Common", "User", controller]);
}).call(null);
