"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:PrivateteacherHelpCtrl
 * @description
 * # PrivateteacherHelpCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, $location,  Service) {

        scope.crumbs = [
            {title: "help"},
        ];

        scope.helpTopics = [{
            title: "subject 1",
            content: "<h4>Sub-subject</h4><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vestibulum urna eu vestibulum consectetur. Nullam pellentesque dictum sapien, a sodales sem luctus at. Quisque gravida tincidunt velit, a consectetur odio tempor eget. In ut massa vitae purus ullamcorper sodales at at ipsum. Ut eu lorem nulla. Pellentesque vitae nibh mauris. Sed ornare fermentum turpis quis tincidunt. Morbi finibus est quis eros mattis, a egestas quam semper. Suspendisse tempus lorem id eros congue, non eleifend purus dictum. Maecenas volutpat mi eu euismod sodales.</p>",
        },
        {
            title: "subject 2",
            content: "<h4>Sub-subject #2</h4><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vestibulum urna eu vestibulum consectetur. Nullam pellentesque dictum sapien, a sodales sem luctus at. Quisque gravida tincidunt velit, a consectetur odio tempor eget. In ut massa vitae purus ullamcorper sodales at at ipsum. Ut eu lorem nulla. Pellentesque vitae nibh mauris. Sed ornare fermentum turpis quis tincidunt. Morbi finibus est quis eros mattis, a egestas quam semper. Suspendisse tempus lorem id eros congue, non eleifend purus dictum. Maecenas volutpat mi eu euismod sodales.</p>",
        }];

        scope.changeHelpTopic = function changeHelpTopic(index) {
            if(!$(this).hasClass("active")) {
                $(".faq-content .faq").removeClass("active").hide();
                $($(".faq-content .faq")[index]).fadeIn(1000).addClass("active");
                $(".faq-navigation a").removeClass("active");
                $($(".faq-navigation a")[index]).addClass("active");
            }
        }

        $(".faq-navigation li:first-child, .faq-content .faq:first-child").addClass("active");
    }

    angular.module("elproffApp")
    .controller("PrivateteacherHelpCtrl", ["$scope", "Common", "Crumbs", "$routeParams", "$location", "User", controller] );
}).call(null);
