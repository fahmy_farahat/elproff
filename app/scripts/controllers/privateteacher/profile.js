"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:PrivateteacherProfileCtrl
 * @description
 * # PrivateteacherProfileCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("PrivateteacherProfileCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
