"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:PrivateteacherInvitationCtrl
 * @description
 * # PrivateteacherInvitationCtrl
 * Controller of the elproffApp
 */
(function () {

  function controller(scope, Common, Crumbs, params, Course, Group) {
    scope.crumbs = [
      {title: "dashboard", link: "#/privateteacher/"},
      {title: "invite", link: ""},
    ];

  }

  angular.module("elproffApp")
    .controller("PrivateteacherInvitationCtrl",
    ["$scope", "Common", "Crumbs", "$routeParams", "Course", "Group", controller]);
}).call(null);
