"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:PrivateteacherCourseCtrl
 * @description
 * # PrivateteacherCourseCtrl
 * Controller of the elproffApp
 */
(function () {

    function controller(scope, Common, Crumbs, params, $location, prompt, root, PrivateTeacher, CommonCache, Group, Course) {
        scope.crumbs = [
            {title: 'dashboard', link: "#/privateteacher"},
            {title: 'course', link: "#/privateteacher/"},
            {title: params.courseid, link: "#/privateteacher/course/" + params.courseid},
            {title: 'view', link: ""}
        ];

        scope.model = {};
        scope.groups = [];

        scope.courseid = params.courseid;
        Crumbs.setCourse(scope.crumbs[2], scope.courseid, scope);

        var data = {
            "course": params.courseid
        };
        function load() {
            Group.getList(data).then(function (result) {
                if (result.length) {
                    scope.groups = _.map(result, CommonCache.group);
                } else {
                    scope.groups = [];
                }
            });
        }
        load();
        scope.$on("relist", load);

        scope.onSuccess = function onSuccess() {
            PrivateTeacher.clearCache();
            Group.clearCache();
            $location.path(scope.crumbs[1].link.substring(1));
        };

        scope.unassignCourse = function () {
            prompt({
                title: root.lang.unassigncourse,
                message: root.lang.unassigncoursemessage,
                buttons: [{
                    "label": root.lang.cancel,
                    "cancel": true
                }, {
                    "label": root.lang.unassignbutton,
                    "primary": true
                }]
            }).then(function () {
                var data = {
                    "course": scope.course.id
                };
                PrivateTeacher.unassignCourse(data).then(scope.onSuccess, Common.onError(scope));
            });
        };
    }

    angular.module("elproffApp")
        .controller("PrivateteacherCourseCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "prompt",
        "$rootScope", "PrivateTeacher", "CommonCache", "Group", "Course", controller]);
}).call(null);
