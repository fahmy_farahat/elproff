"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:PrivateteacherCourseGroupExamlistCtrl
 * @description
 * # PrivateteacherCourseGroupExamlistCtrl
 * Controller of the elproffApp
 */
(function () {

	function controller(scope, $rootScope, Common, Crumbs, params, $location, Exam, User) {
		scope.crumbs = [
		    {title: 'dashboard', link: "#/privateteacher"},
		    {title: 'course', link: "#/privateteacher/course/"},
		    {title: params.courseid, link: "#/privateteacher/course/" + params.courseid},
		    {title: 'groups', link: "#/privateteacher/course/" + params.courseid + "/group/"},
		    {title: params.groupid, link: "#/privateteacher/course/" + params.courseid + "/group/" + params.groupid},
		    {title: 'exams', link: ""},
		];

		scope.courseid = params.courseid;
    	Crumbs.setCourse(scope.crumbs[2], scope.courseid, scope);
    	scope.groupid = params.groupid;
    	Crumbs.setGroup(scope.crumbs[4], scope.groupid, scope);
		function load() {
			Exam.groupExamList({group: scope.groupid}).then(function(result){
				scope.exams = result.exams;
			});	
        }
        load();
	}

  angular.module("elproffApp")
    .controller("PrivateteacherCourseGroupExamlistCtrl",
    ["$scope", "$rootScope", "Common", "Crumbs", "$routeParams", "$location", "Exam", "User", controller]);
}).call(null);