"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:PrivateteacherGroupCreateCtrl
 * @description
 * # PrivateteacherGroupCreateCtrl
 * Controller of the elproffApp
 */
(function(){

  function controller(scope, Common, Crumbs, params, $location, PrivateTeacher, Group) {

    scope.crumbs = [
        {title: 'dashboard', link: "#/privateteacher/"},
        {title: 'courses', link: "#/privateteacher/"},
        {title: params.courseid, link: "#/privateteacher/course/"+params.courseid},
        {title: 'group', link: "#/privateteacher/course/"+params.courseid},
        {title: 'create', link: ""},
    ];

    scope.model = {};

    scope.courseid = params.courseid;
    Crumbs.setCourse(scope.crumbs[2], scope.courseid, scope);

    scope.isLoading = false;
    scope.onSuccess = function onSuccess(result) {
        scope.isLoading = false;
        $location.path(scope.crumbs[3].link.substring(1));
    };

    scope.save = function () {
        scope.isLoading = true;
        var data = {
            "name": scope.model.name,
            "course": scope.course.resource_uri
        };
        Group.post(data).then(scope.onSuccess, Common.onError(scope));
    };

  }

  angular.module("elproffApp")
    .controller("PrivateteacherGroupCreateCtrl",
    ["$scope", "Common", "Crumbs", "$routeParams", "$location",
    "PrivateTeacher", "Group", controller] );
}).call(null);
