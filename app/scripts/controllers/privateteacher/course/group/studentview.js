"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:PrivateteacherCourseGroupStudentviewCtrl
 * @description
 * # PrivateteacherCourseGroupStudentviewCtrl
 * Controller of the elproffApp
 */
(function () {

	function controller(scope, $rootScope, Common, Crumbs, params, $location, Exam, ExamInstance, CommonCache, Restangular) {
		scope.crumbs = [
		    {title: 'dashboard', link: "#/privateteacher"},
		    {title: 'course', link: "#/privateteacher/course/"},
		    {title: params.courseid, link: "#/privateteacher/course/" + params.courseid},
		    {title: 'groups', link: "#/privateteacher/course/" + params.courseid + "/group/"},
		    {title: params.groupid, link: "#/privateteacher/course/" + params.courseid + "/group/" + params.groupid},
		    {title: 'students', link: "#/privateteacher/course/" + params.courseid + "/group/" + params.groupid},
		    {title: params.studentid, link: ""},
		];

		scope.courseid = params.courseid;
    	Crumbs.setCourse(scope.crumbs[2], scope.courseid, scope);
    	scope.groupid = params.groupid;
    	Crumbs.setGroup(scope.crumbs[4], scope.groupid, scope);
    	scope.studentid = params.studentid;
    	Crumbs.setStudent(scope.crumbs[6], scope.studentid, scope)

        function load() {
            ExamInstance.studentGroupExams({group:params.groupid}).then(function(result){
                scope.examInstances = result.exams;
                scope.studentid = Common.uriToID(_.first(result.exams).student);
                scope.examInstances = _.map(scope.examInstances, CommonCache.examInstance);
            });

            Restangular.one("student", params.studentid).get().then(function(student){
                scope.student = student;
            });
        }
        load();
	}

  angular.module("elproffApp")
    .controller("PrivateteacherCourseGroupStudentviewCtrl",
    ["$scope", "$rootScope", "Common", "Crumbs", "$routeParams", "$location", "Exam",
     "ExamInstance", "CommonCache","Restangular", controller]);
}).call(null);