"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:PrivateteacherGroupUpdateCtrl
 * @description
 * # PrivateteacherGroupUpdateCtrl
 * Controller of the elproffApp
 */
(function () {

    function controller(scope, Common, Crumbs, params, Group, Course, $location) {

        scope.crumbs = [
          {title: "dashboard", link: "#/privateteacher/"},
          {title: "courses", link: "#/privateteacher/"},
          {title: params.courseid, link: "#/privateteacher/course/" + params.courseid},
          {title: "groups", link: "#/privateteacher/course/" + params.courseid },
          {title: params.groupid, link: "#/privateteacher/course/" + params.courseid + "/group/" + params.groupid},
          {title: "update", link: ""}
        ];

        scope.model = {};
        scope.courseid = params.courseid;
        scope.groupid = params.groupid;
        //Crumbs.setCourse(scope.crumbs[2], scope.courseid, scope);
        Crumbs.setGroup(scope.crumbs[4], scope.groupid, scope);
        Group.get(params.groupid).then(function (obj) {
            scope.model = obj;
            Course.get(scope.courseid).then(function(course){
                scope.model.course = course;
            });
        });

        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $location.path(scope.crumbs[4].link.substring(1));
        };

        scope.save = function () {
            scope.isLoading = true;
            scope.model.put().then(this.onSuccess, Common.onError(scope));
        };

    }

    angular.module("elproffApp")
      .controller("PrivateteacherGroupUpdateCtrl",
      ["$scope", "Common", "Crumbs", "$routeParams", "Group", "Course", "$location", controller]);
}).call(null);
