"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:PrivateteacherCourseGroupExamsReportCtrl
 * @description
 * # PrivateteacherCourseGroupExamsReportCtrl
 * Controller of the elproffApp
 */
(function(){

  function controller(scope, Common, Crumbs, params, ExamInstance, $timeout, Parent, QuestionAnswer, Exam, Reports) {

    scope.crumbs = [
        {title: 'dashboard', link: "#/privateteacher"},
        {title: 'course', link: "#/privateteacher"},
        {title: params.courseid, link: "#/privateteacher/" + "course/" + params.courseid},
        {title: 'group', link: "#/privateteacher/" + "course/" + params.courseid},
        {title: params.groupid, link: "#/privateteacher/" + "course/" + params.courseid + "/group/" + params.groupid},
        {title: 'student', link: "#/privateteacher/" + "course/" + params.courseid + "/group/" + params.groupid},
        {title: params.studentid, link: "#/privateteacher/" + "course/" + params.courseid + "/group/" + params.groupid + "/student/" + params.studentid},
        {title: 'exam', link: "#/privateteacher/" + "course/" + params.courseid + "/group/" + params.groupid + "/student/" + params.studentid},
        {title: params.examid, link: "#/privateteacher/" + "course/" + params.courseid + "/group/" + params.groupid + "/student/" + params.studentid + "/exam/" + params.examid},
        {title: 'detailedreport', link: ""},
    ];

    scope.courseid = params.courseid;
    scope.groupid = params.groupid;
    scope.studentid = params.studentid;
    scope.examid = params.examid;
    Crumbs.setCourse(scope.crumbs[2], params.courseid, scope);
    Crumbs.setGroup(scope.crumbs[4], params.groupid, scope);
    scope.loaded = false;

    var runned = false;
    scope.model = {
        fraction_grade: 1,
        result_grade: 1
    };

    scope.level = "";

    scope.examid = params.examid;
    scope.questionsCounts = {
        skipped: 0,
        wrong: 0,
        correct: 0,
        total: 1
    };

    scope.onSuccess = function onSuccess(result) {
        scope.loaded = true;
        scope.instanceid = result.id;
        Exam.getUri(result.exam).then(function(examModel){
            scope.model = examModel;
            QuestionAnswer.getList({exam_instance: params.examid}).then(function(answers) {
                if (!answers.length) {
                    return;
                }
                scope.answers = answers;
                if (!scope.model.grade) {
                    scope.model.grade = 100;
                }
                var total = answers.length;

                var wrong = _.filter(answers,  function(ans) {
                          return (ans.degree || 0) < 0.5;
                        }).length;
                var correct = _.filter(answers,  function(ans) {
                      return (ans.degree || 0) >= 0.5;
                    }).length;
                var skipped = total - (wrong + correct);

                scope.questionsCount = {
                    total: total,
                    skipped: skipped,
                    correct: correct,
                    wrong: wrong
                };
                var sum = _.reduce(_.pluck(answers, "degree"), function(a, b) {return a + b;});
                scope.time = _.reduce(_.pluck(answers, "duration"), function(a, b) {return a + b;});
                scope.model.fraction_grade = sum / answers.length;
                scope.model.result_grade = scope.model.grade * scope.model.fraction_grade;
                scope.model.level = Reports.calcLevel(scope.model.fraction_grade);
            }, Common.onError(scope));
            
        });            
    };

    scope.onError = function onError(error) {
        if (error.error === "not_owner") {
            scope.alerts = Common.alerts.error.exam_not_owner;
        }
    };

    var query = {instance_id: params.examid};
    ExamInstance.result(query).then(scope.onSuccess, scope.onError);
  }

  angular.module("elproffApp")
    .controller("PrivateteacherCourseGroupExamsReportCtrl",
    ["$scope", "Common", "Crumbs", "$routeParams", "ExamInstance", "$timeout",
     "Parent", "QuestionAnswer", "Exam", "Reports", controller] );
}).call(null);
