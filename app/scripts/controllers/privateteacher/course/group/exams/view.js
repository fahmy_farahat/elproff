"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:PrivateteacherCourseGroupExamsViewCtrl
 * @description
 * # PrivateteacherCourseGroupExamsViewCtrl
 * Controller of the elproffApp
 */
(function () {

	function controller(scope, $rootScope, Common, Crumbs, params, $location, Exam, Student, ExamInstance) {
		scope.crumbs = [
		    {title: 'dashboard', link: "#/privateteacher"},
		    {title: 'course', link: "#/privateteacher/course/"},
		    {title: params.courseid, link: "#/privateteacher/course/" + params.courseid},
		    {title: 'groups', link: "#/privateteacher/course/" + params.courseid},
		    {title: params.groupid, link: "#/privateteacher/course/" + params.courseid + "/group/" + params.groupid},
		    {title: 'exams', link: ""},
		];

		scope.courseid = params.courseid;
    	Crumbs.setCourse(scope.crumbs[2], scope.courseid, scope);
    	scope.groupid = params.groupid;
    	scope.examid = params.examid;
    	scope.students = [];
    	Crumbs.setGroup(scope.crumbs[4], scope.groupid, scope);
		function load() {
			scope.exam = Exam.get(params.examid).$object;
			ExamInstance.getList({exam: scope.examid}).then(function(result){
				scope.instances = result;
				_.each(scope.instances, function(ei){
					var data = {};
					Student.getUri(ei.student).then(function(student){
						data.user = student.user;
						data.degree = ei.degree;
						scope.students.push(data);	
					});
				});
			});
        }
        load();
	}

  angular.module("elproffApp")
    .controller("PrivateteacherCourseGroupExamsViewCtrl",
    ["$scope", "$rootScope", "Common", "Crumbs", "$routeParams", "$location", "Exam", "Student", "ExamInstance", controller]);
}).call(null);