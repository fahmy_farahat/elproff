"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:PrivateteacherCourseExamReportsCtrl
 * @description
 * # PrivateteacherCourseExamReportsCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/privateteacher/"},
            {title: 'course', link: "#/privateteacher/course/"},
            {title: 'exams', link: "#/privateteacher/course/exams/"},
            {title: 'reports', link: ""},
        ];

        scope.courses = [{
            name: "Arabic",
            grade: "First Elementary",
            groups: [{
            	name: "Group 1",
            	degree: "87",
            },
            {
            	name: "Group 2",
            	degree: "92",
            },
            {
            	name: "Group 3",
            	degree: "63",
            }],
        },
        {
            name: "English",
            grade: "Second Prep",
            groups: [{
            	name: "Group 1",
            	degree: "87",
            },
            {
            	name: "Group 2",
            	degree: "92",
            },
            {
            	name: "Group 3",
            	degree: "63",
            }],
        }];


    }

    angular.module("elproffApp")
    .controller("PrivateteacherCourseExamReportsCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "User", controller]);
}).call(null);
