"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:PrivateteacherCourseExamReviewCtrl
 * @description
 * # PrivateteacherCourseExamReviewCtrl
 * Controller of the elproffApp
 */
(function () {

    function controller(scope, $rootScope, Common, Crumbs, params, $location, QuestionGroup, TeacherBuild, $modal, Question) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/privateteacher/"},
            {title: 'courses', link: "#/privateteacher/course/"},
            {title: params.courseid, link: "#/privateteacher/course/" + params.courseid},
            {title: 'exam', link: "#/privateteacher/course/" + params.courseid + "/exam/create"},
            {title: 'review',link: ""},
        ];
        
        scope.model = {};
        var groups = [];
        scope.alerts = [];
        scope.courseid = params.courseid;
        Crumbs.setCourse(scope.crumbs[2], scope.courseid, scope);
        
        function load() {
            scope.model.questions = [];
            QuestionGroup.getList({build:params.buildid}).then(function(result){
                scope.model.questionsGroup = result;
                _.each(scope.model.questionsGroup, function(qGroup){
                    var groupId = qGroup.id;
                    _.each(qGroup.selected_questions, function(q){
                        var q = {question: q, group_id: groupId};
                        scope.model.questions.push(q);    
                    });
                });
                    
            });
            
        }
        load();
        scope.onSuccess = function onSuccess(result) {
          $location.path(scope.crumbs[2]);
        };

        scope.checkquestions = function checkquestions(q, index){
            var modalInstance = $modal.open({
                templateUrl: "views/modals/viewquestiongroup.html",
                controller: "ModalsViewquestiongroupCtrl",
                resolve: {
                    question: function(){
                        return q;
                    },
                    allQuestions: function(){
                        return scope.model.questions;
                    },
                    index: function(){
                        return index;
                    }
                }
            });
            // modalInstance.result.then(function(newQGroup){
            //     scope.model.questionsGroup[index] = _.cloneDeep(newQGroup);
            // });
        };

        scope.verifyExam = function verifyExam(){
            var data = {build_id: params.buildid};
            TeacherBuild.verifyExam(data).then(scope.onSuccess, Common.onError);
        };

        scope.reject = function reject(q, index){
            var data = {
                group_id: q.group_id,
                question_id: q.question.id,
            };
            QuestionGroup.reject(data).then(function(result){
                scope.model.questions[index].question = _.cloneDeep(result.question);
            }, scope.onError)
        };
        scope.onError = function onError(result){
            var msg = scope.$root.lang["error_"+result.data.error];
            scope.alerts.push({msg: msg, type: "danger"});
        };
        scope.closeAlert = function closeAlert(index){
            scope.alerts.splice(index, 1);
        };

    }
    angular.module("elproffApp")
        .controller("PrivateteacherCourseExamReviewCtrl", ["$scope", "$rootScope", "Common", "Crumbs", "$routeParams",
         "$location", "QuestionGroup","TeacherBuild","$modal", "Question", controller]);
}).call(null);