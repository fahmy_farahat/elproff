"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:PrivateteacherCourseExamCreateCtrl
 * @description
 * # PrivateteacherCourseExamCreateCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, PrivateTeacher, $location, $timeout, Group) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/privateteacher/"},
            {title: 'courses', link: "#/privateteacher/course/"},
            {title: params.courseid, link: "#/privateteacher/course/"+params.courseid},
            {title: 'exam', link: "#/privateteacher/course/" + params.courseid + "/exam/"},
            {title: 'create', link: ""},
        ];
        scope.courseid = params.courseid;
        Crumbs.setCourse(scope.crumbs[2], scope.courseid, scope);

        var data = {
            course: scope.courseid,
            limit: 0

        };

        /**
         * In case he tries to create an exam without having groups,
         * he should be notified and directed to the groups page to add groups
         */
        function load() {
            Group.getList(data).then(function (result) {
                if (!result.length) {
                    scope.alerts = [{
                        msg: "Please, Add a group first",
                        type: "danger"
                    }];
                    // Redirect after two seconds
                    $timeout(function() {
                        $location.path("/privateteacher/course/" + scope.courseid);
                    }, 2000);
                }
            });
        }
        load();
    }

    angular.module("elproffApp")
    .controller("PrivateteacherCourseExamCreateCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "PrivateTeacher",
        "$location", "$timeout", "Group", controller]);
}).call(null);
