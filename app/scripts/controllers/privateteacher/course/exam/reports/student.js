"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:PrivateteacherCourseExamReportsStudentCtrl
 * @description
 * # PrivateteacherCourseExamReportsStudentCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/privateteacher/"},
            {title: 'course', link: "#/privateteacher/course/"},
            {title: 'exams', link: "#/privateteacher/course/exams/"},
            {title: 'reports', link: "#/privateteacher/course/reports/"},
            {title: 'groups', link: "#/privateteacher/course/reports/group"},
            {title: 'student', link: ""},
        ];

    }

    angular.module("elproffApp")
    .controller("PrivateteacherCourseExamReportsStudentCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "User", controller]);
}).call(null);
