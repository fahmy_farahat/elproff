"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:PrivateteacherCourseExamReportsGroupCtrl
 * @description
 * # PrivateteacherCourseExamReportsGroupCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/privateteacher/"},
            {title: 'course', link: "#/privateteacher/course/"},
            {title: 'exams', link: "#/privateteacher/course/exams/"},
            {title: 'reports', link: "#/privateteacher/course/reports/"},
            {title: 'groups', link: ""},
        ];

        scope.students = [{
            name: "Mohamed Atef",
            attendance: "yes",
            time: "22 " + scope.$root.lang.minutes + " 28 " + scope.$root.lang.seconds,
            degree: "83",
        },
        {
            name: "Mahmoud Ahmed",
            attendance: "no",
            time: "12 " + scope.$root.lang.minutes + " 20 " + scope.$root.lang.seconds,
            degree: "",
        },
        {
            name: "Diaa Kassem",
            attendance: "yes",
            time: "23 " + scope.$root.lang.minutes + " 00 " + scope.$root.lang.seconds,
            degree: "93",
        }];


    }

    angular.module("elproffApp")
    .controller("PrivateteacherCourseExamReportsGroupCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "User", controller]);
}).call(null);
