"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:PrivateteacherCourseHomeworkCreateCtrl
 * @description
 * # PrivateteacherCourseHomeworkCreateCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/privateteacher/"},
            {title: 'course', link: "#/privateteacher/course/"},
            {title: 'homeworks', link: "#/privateteacher/course/homeworks/"},
            {title: 'create', link: ""},
        ];

        scope.classes = [{
            id: 1,
            name: "1/1"
        }];

    }

    angular.module("elproffApp")
    .controller("PrivateteacherCourseHomeworkCreateCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "User", controller]);
}).call(null);
