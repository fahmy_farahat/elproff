"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:PrivateteacherGroupCtrl
 * @description
 * # PrivateteacherGroupCtrl
 * Controller of the elproffApp
 */
(function () {

    function controller(scope, Common, Crumbs, params, $location, prompt, root, Group, PrivateTeacher) {
        scope.crumbs = [
            {title: 'dashboard', link: "#/privateteacher"},
            {title: 'course', link: "#/privateteacher/"},
            {title: params.courseid, link: "#/privateteacher/course/" + params.courseid},
            {title: 'groups', link: "#/privateteacher/course/" + params.courseid},
            {title: params.groupid, link: "#/privateteacher/course/" + params.courseid + "/group/" + params.groupid},
            {title: 'view', link: ""}
        ];

        scope.model = {};

        scope.courseid = params.courseid;
        Crumbs.setCourse(scope.crumbs[2], scope.courseid, scope);
        scope.groupid = params.groupid;
        Crumbs.setGroup(scope.crumbs[4], scope.groupid, scope);

        function load() {
            var data = {
                "group": params.groupid
            };
            Group.groupStudents(data).then(function (result) {
                scope.students = result.objects;
            });
        }
        load();
        scope.$on("relist", load);

        scope.onSuccess = function onSuccess(status) {
            if (status === "removeGroup") {
                Group.clearCache();
                $location.path(scope.crumbs[3].link.substring(1));
            } else if (status === "removeStudent") {
                scope.$emit("relist");
            }
        };

        scope.removeStudent = function (student) {
            prompt({
                title: root.lang.removestudent,
                message: root.lang.removestudentmessage,
                buttons: [
                {
                    "label": root.lang.cancel,
                    "cancel": true
                },
                {
                    "label": root.lang.removebutton,
                    "primary": true
                }
                ]
            }).then(function () {
                var data = {
                    "group": scope.groupid,
                    "student": student
                };
                Group.studentRemove(data).then(scope.onSuccess("removeStudent"), Common.onError(scope));
            });
        };

        scope.removeGroup = function () {
            prompt({
                title: root.lang.removegroup,
                message: root.lang.removegroupmessage,
                buttons: [
                    {
                        "label": root.lang.cancel,
                        "cancel": true
                    },
                    {
                        "label": root.lang.removebutton,
                        "primary": true
                    }
                ]
            }).then(function () {
                var data = {
                    "group": scope.groupid
                };
                Group.get(scope.groupid).then(function(group){
                    group.is_deleted = true;
                    group.put().then(scope.onSuccess("removeGroup"), Common.onError(scope))
                });
            });
        };

        scope.viewStudent = function viewStudent(student){
            var studentid = student;
            $location.path("privateteacher/course/" + params.courseid + "/group/" + params.groupid + "/student/" + studentid);
        };
    }

    angular.module("elproffApp")
        .controller("PrivateteacherGroupCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "prompt",
        "$rootScope", "Group", "PrivateTeacher", controller]);
}).call(null);
