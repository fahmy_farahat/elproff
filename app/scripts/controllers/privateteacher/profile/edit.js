"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:PrivateteacherProfileEditCtrl
 * @description
 * # PrivateteacherProfileEditCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("PrivateteacherProfileEditCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
