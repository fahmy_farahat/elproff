"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:PrivateteacherProfileChangepasswordCtrl
 * @description
 * # PrivateteacherProfileChangepasswordCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service) {

        scope.links = [{
            name: "changepassword",
            title: scope.$root.lang.changepassword,
            href: "#/privateteacher/profile/changepassword"
        }, {
            name: "editprofile",
            title: scope.$root.lang.editprofile,
            href: "#/privateteacher/profile/edit"
        }, {
            name: "view",
            title: scope.$root.lang.profile,
            href: "#/privateteacher/profile"
        }];

    }

    angular.module("elproffApp")
    .controller("PrivateteacherProfileChangepasswordCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "User", controller]);
}).call(null);
