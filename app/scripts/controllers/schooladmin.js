"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchooladminCtrl
 * @description
 * # SchooladminCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, School, Grade, EduSystem) {

        scope.crumbs = [
        {title: "dashboard", link: "#/schooladmin/"},
        ];

        scope.model = {};
        function load() {

            School.getList().then(function(school){
                scope.model = school[0];
                scope.model.systems = [];
                _.each(scope.model.grades, function(gradeUri, index){
                    Grade.getUri(gradeUri).then(function(grade){
                        scope.model.grades[index] = grade;
                        EduSystem.getUri(grade.system).then(function(system){
                            var isThere = _.some(scope.model.systems, function(sys){
                                return sys.id === system.id;
                            });
                            if(!isThere){
                                scope.model.systems.push(system);
                            }
                        });
                    });

                });
            });
        }
        load();

    }

    angular.module("elproffApp")
        .controller("SchooladminCtrl",
                ["$scope", "Common", "Crumbs", "$routeParams", "$location",
                "School", "Grade", "EducationalSystem", controller]);
}).call(null);
