"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:PreCtrl
 * @description
 * # PreCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("PreCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
