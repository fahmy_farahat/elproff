"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchoolteacherProfileEditCtrl
 * @description
 * # SchoolteacherProfileEditCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("SchoolteacherProfileEditCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
