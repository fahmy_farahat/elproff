"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchoolteacherProfileChangepasswordCtrl
 * @description
 * # SchoolteacherProfileChangepasswordCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service) {

        scope.links = [{
            name: "changepassword",
            title: scope.$root.lang.changepassword,
            href: "#/schoolteacher/profile/changepassword"
        }, {
            name: "editprofile",
            title: scope.$root.lang.editprofile,
            href: "#/schoolteacher/profile/edit"
        }, {
            name: "view",
            title: scope.$root.lang.profile,
            href: "#/schoolteacher/profile"
        }];
    }

    angular.module("elproffApp")
    .controller("SchoolteacherProfileChangepasswordCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "User", controller]);
}).call(null);
