"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchoolteacherCourseCtrl
 * @description
 * # SchoolteacherCourseCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/schoolteacher/"},
            {title: 'courses', link: ""},
        ];

        scope.courses = [{
            id: 1,
            name: "Arabic",
            image: "images/arabic.png",
            grade: {
                id: 1,
                name: "First Grade"
            }
        }];
        scope.searchable = true;
    }

    angular.module("elproffApp")
    .controller("SchoolteacherCourseCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "User", controller]);
}).call(null);
