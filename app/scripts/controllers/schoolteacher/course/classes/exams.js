"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchoolteacherCourseClassesExamsCtrl
 * @description
 * # SchoolteacherCourseClassesExamsCtrl
 * Controller of the elproffApp
 */
(function () {

	function controller(scope, $rootScope, Common, Crumbs, params, Exam) {
		scope.crumbs = [
		    {title: 'dashboard', link: "#/schoolteacher"},
		    {title: 'course', link: "#/privateteacher"},
		    {title: params.courseid, link: "#/schoolteacher/course/" + params.courseid},
		    {title: 'classes', link: ""},
		    {title: params.classid, link: "#/schoolteacher/course/" + params.courseid + "/class/" + params.classid},
		    {title: 'exams', link: ""},
		];

		scope.courseid = params.courseid;
    	Crumbs.setCourse(scope.crumbs[2], scope.courseid, scope);
    	scope.classid = params.classid;
    	Crumbs.setClass(scope.crumbs[4], scope.classid, scope);
		function load() {
			Exam.classExamList({classes: scope.classid}).then(function(result){
				scope.exams = result.exams;
			});	
        }
        load();
	}

  angular.module("elproffApp")
    .controller("SchoolteacherCourseClassesExamsCtrl",
    ["$scope", "$rootScope", "Common", "Crumbs", "$routeParams", "Exam", controller]);
}).call(null);