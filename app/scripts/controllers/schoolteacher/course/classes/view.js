"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchoolteacherCourseClassesViewCtrl
 * @description
 * # SchoolteacherCourseClassesViewCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, Student) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/schoolteacher/"},
            {title: 'courses', link: "#/schoolteacher/"},
            {title: params.courseid, link: "#/schoolteacher/course/" + params.courseid},
            {title: 'classes', link: ""},
            {title: params.classid, link: ""},
        ];
        scope.courseid = params.courseid;
        scope.classid = params.classid;
        Crumbs.setCourse(scope.crumbs[2], scope.courseid, scope);
        Crumbs.setClass(scope.crumbs[4], scope.classid, scope);
        function load() {
            var data = {
                "classes": params.classid
            };
            Student.getList(data).then(function (result) {
                scope.students = result;
            });
        }
        load();
        scope.$on("relist", load);

    }

    angular.module("elproffApp")
    .controller("SchoolteacherCourseClassesViewCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "Student", controller]);
}).call(null);

