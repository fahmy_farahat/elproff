"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchoolteacherCourseReportsCtrl
 * @description
 * # SchoolteacherCourseReportsCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/schoolteacher/"},
            {title: 'courses', link: "#/schoolteacher/course/"},
            {title: params.courseid, link: "#/schoolteacher/course/" + params.courseid},
            {title: 'reports', link: ""},
        ];

    }

    angular.module("elproffApp")
    .controller("SchoolteacherCourseReportsCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "User", controller]);
}).call(null);
