"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchoolteacherCourseExamsCreateCtrl
 * @description
 * # SchoolteacherCourseExamsCreateCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, Classes) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/schoolteacher/"},
            {title: 'course', link: "#/schoolteacher/course/"},
            {title: params.courseid, link: "#/schoolteacher/course/" + params.courseid},
            {title: 'exams', link: "#/schoolteacher/course/exams/"},
            {title: 'create', link: ""},
        ];

        scope.courseid = params.courseid;
        Crumbs.setCourse(scope.crumbs[2], scope.courseid, scope);

    }

    angular.module("elproffApp")
    .controller("SchoolteacherCourseExamsCreateCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "Classes", controller]);
}).call(null);
