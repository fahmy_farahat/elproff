"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchoolteacherCourseExamsQuestionsCreateCtrl
 * @description
 * # SchoolteacherCourseExamsQuestionsCreateCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/schoolteacher/"},
            {title: 'course', link: "#/schoolteacher/course/"},
            {title: 'exams', link: "#/schoolteacher/course/exams/"},
            {title: 'questions', link: "#/schoolteacher/course/exams/questions/"},
            {title: 'create', link: ""},
        ];

    }

    angular.module("elproffApp")
    .controller("SchoolteacherCourseExamsQuestionsCreateCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "User", controller]);
}).call(null);
