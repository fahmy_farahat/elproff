"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchoolteacherCourseExamsQuestionsCtrl
 * @description
 * # SchoolteacherCourseExamsQuestionsCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("SchoolteacherCourseExamsQuestionsCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
