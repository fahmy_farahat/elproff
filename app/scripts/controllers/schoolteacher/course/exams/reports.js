"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchoolteacherCourseExamsReportsCtrl
 * @description
 * # SchoolteacherCourseExamsReportsCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/schoolteacher/"},
            {title: 'courses', link: "#/schoolteacher/course/"},
            {title: params.courseid, link: "#/schoolteacher/course/" + params.courseid},
            {title: 'exams', link: "#/schoolteacher/course/" + params.courseid + "/exams/"},
            {title: params.examid, link: "#/schoolteacher/course/" + params.courseid + "/exams/" + params.examid},
            {title: 'reports', link: ""},
        ];

    }

    angular.module("elproffApp")
    .controller("SchoolteacherCourseExamsReportsCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "User", controller]);
}).call(null);
