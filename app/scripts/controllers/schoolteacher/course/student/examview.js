"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchoolteacherCourseStudentExamviewCtrl
 * @description
 * # SchoolteacherCourseStudentExamviewCtrl
 * Controller of the elproffApp
 */
(function () {

	function controller(scope, $rootScope, Common, Crumbs, params, $location, Exam, ExamInstance, CommonCache, Restangular) {
		scope.crumbs = [
		    {title: 'dashboard', link: "#/schoolteacher"},
		    {title: 'course', link: ""},
		    {title: params.courseid, link: "#/schoolteacher/course/" + params.courseid},
		    {title: 'classes', link: ""},
		    {title: params.classid, link: "#/schoolteacher/course/" + params.courseid + "/class/" + params.classid},
		    {title: 'students', link: "#/schoolteacher/course/" + params.courseid + "/class/" + params.classid},
		    {title: params.studentid, link: ""},
		];

		scope.courseid = params.courseid;
    	Crumbs.setCourse(scope.crumbs[2], scope.courseid, scope);
    	scope.classid = params.classid;
    	Crumbs.setClass(scope.crumbs[4], scope.classid, scope);
    	scope.studentid = params.studentid;
    	Crumbs.setStudent(scope.crumbs[6], scope.studentid, scope)

        function load() {
        	var data = { classes: params.classid,
        				 student: params.studentid
        			   }
            ExamInstance.studentClassExams(data).then(function(result){
                scope.examInstances = result.exams;
                scope.examInstances = _.map(scope.examInstances, CommonCache.examInstance);
            });

            Restangular.one("student", params.studentid).get().then(function(student){
                scope.student = student;
            });
        }
        load();
	}

  angular.module("elproffApp")
    .controller("SchoolteacherCourseStudentExamviewCtrl",
    ["$scope", "$rootScope", "Common", "Crumbs", "$routeParams", "$location", "Exam",
     "ExamInstance", "CommonCache","Restangular", controller]);
}).call(null);
