"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchoolteacherCourseHomeworksCtrl
 * @description
 * # SchoolteacherCourseHomeworksCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/schoolteacher/"},
            {title: 'courses', link: "#/schoolteacher/course/"},
            {title: params.courseid, link: "#/schoolteacher/course/" + params.courseid},
            {title: 'homeworks', link: ""},
        ];

        scope.entities = [{
            id: 1,
            name: "Arabic",
            image: "images/arabic.png",
            questions: [{
                id: 1,
                name: "First Grade"
            }]
        }];

    }

    angular.module("elproffApp")
    .controller("SchoolteacherCourseHomeworksCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "User", controller]);
}).call(null);
