"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchoolteacherCourseGradeCtrl
 * @description
 * # SchoolteacherCourseGradeCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("SchoolteacherCourseGradeCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
