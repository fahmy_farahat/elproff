"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchoolteacherCourseViewCtrl
 * @description
 * # SchoolteacherCourseViewCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, Classes) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/schoolteacher/"},
            {title: 'courses', link: "#/schoolteacher/"},
            {title: params.courseid, link: "#/schoolteacher/course/" + params.courseid},
            {title: 'classes', link: ""},
        ];
        scope.courseid = params.courseid;
        Crumbs.setCourse(scope.crumbs[2], scope.courseid, scope);
        function load() {
            var data = {
                "course": params.courseid
            };
            Classes.course_classes(data).then(function (result) {
                scope.courseClasses = result.classes;
            });
        }
        load();
        scope.$on("relist", load);

    }

    angular.module("elproffApp")
    .controller("SchoolteacherCourseViewCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "Classes", controller]);
}).call(null);
