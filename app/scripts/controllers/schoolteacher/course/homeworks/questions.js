"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchoolteacherCourseHomeworksQuestionsCtrl
 * @description
 * # SchoolteacherCourseHomeworksQuestionsCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/schoolteacher/"},
            {title: 'course', link: "#/schoolteacher/course/"},
            {title: 'homeworks', link: "#/schoolteacher/course/homeworks/"},
            {title: 'questions', link: "#/schoolteacher/course/homeworks/questions/"},
            {title: 'list', link: ""},
        ];

        scope.classes = [{
            id: 1,
            name: "1/1"
        }];


    }

    angular.module("elproffApp")
    .controller("SchoolteacherCourseHomeworksQuestionsCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "User", controller]);
}).call(null);
