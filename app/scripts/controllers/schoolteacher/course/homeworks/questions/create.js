"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchoolteacherCourseHomeworksQuestionsCreateCtrl
 * @description
 * # SchoolteacherCourseHomeworksQuestionsCreateCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("SchoolteacherCourseHomeworksQuestionsCreateCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
