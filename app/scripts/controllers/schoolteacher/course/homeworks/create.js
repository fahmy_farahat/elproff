"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchoolteacherCourseHomeworksCreateCtrl
 * @description
 * # SchoolteacherCourseHomeworksCreateCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/schoolteacher/"},
            {title: 'course', link: "#/schoolteacher/course/"},
            {title: 'homeworks', link: "#/schoolteacher/course/homeworks/"},
            {title: 'create', link: ""},
        ];

        scope.classes = [{
            id: 1,
            name: "1/1"
        }];

    }

    angular.module("elproffApp")
    .controller("SchoolteacherCourseHomeworksCreateCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "User", controller]);
}).call(null);
