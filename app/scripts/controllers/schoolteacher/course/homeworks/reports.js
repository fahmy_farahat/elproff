"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchoolteacherCourseHomeworksReportsCtrl
 * @description
 * # SchoolteacherCourseHomeworksReportsCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/schoolteacher/"},
            {title: 'course', link: "#/schoolteacher/course/"},
            {title: 'homeworks', link: "#/schoolteacher/course/homeworks/"},
            {title: 'reports', link: ""},
        ];

    }

    angular.module("elproffApp")
    .controller("SchoolteacherCourseHomeworksReportsCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "User", controller]);
}).call(null);
