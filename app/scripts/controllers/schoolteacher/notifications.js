"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchoolteacherNotificationsCtrl
 * @description
 * # SchoolteacherNotificationsCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("SchoolteacherNotificationsCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
