"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SchoolteacherProfileCtrl
 * @description
 * # SchoolteacherProfileCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("SchoolteacherProfileCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
