"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:RegisterThankyouCtrl
 * @description
 * # RegisterThankyouCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, User) {

    }

    angular.module("elproffApp")
    .controller("RegisterThankyouCtrl", ["$scope", "Common", "Crumbs", "User", controller] );
}).call(null);
