"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ReviewerCourseCtrl
 * @description
 * # ReviewerCourseCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, params, $location, Course, CommonCache) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/reviewer/"},
            {title: 'course', link: ""},
        ];

        function load() {
            Course.getList().then(function(courses) {
                scope.entities = _.map(courses, CommonCache.course);
            });
        }
        scope.$on("relist", load);
        scope.searchable = true;
        load();
    }

    angular.module("elproffApp")
    .controller("ReviewerCourseCtrl",
        ["$scope", "Common", "$routeParams", "$location", "Course",
        "CommonCache", controller]);
}).call(null);
