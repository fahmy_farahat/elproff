"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ReviewerProfileCtrl
 * @description
 * # ReviewerProfileCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("ReviewerProfileCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
