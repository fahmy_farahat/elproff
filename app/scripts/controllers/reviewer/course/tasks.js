/**
 * @ngdoc function
 * @name elproffApp.controller:ReviewerCourseTasksCtrl
 * @description
 * # ReviewerCourseTasksCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, $location, Task, CommonCache) {

        scope.courseid = params.courseid;
        scope.crumbs = [
            {title: 'dashboard', link: "#/reviewer/"},
            {title: 'course', link: "#/reviewer/course/"},
            {title: params.courseid, link: "#/reviewer/course/" + params.courseid},
            {title: 'tasks', link: ""},
        ];
        Crumbs.setCourse(scope.crumbs[2], params.courseid, scope);

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            scope.entities = result.objects;
            scope.entities = _.each(scope.entities, function(task){
                task = CommonCache.task(task);
            });
        };

        function load() {
            scope.isLoading = true;
            var query = _.cloneDeep(Common.query);
            query.course = params.courseid;
            Task.my(query).then(scope.onSuccess, Common.onError(scope));
        }
        scope.$on("relist", load);
        scope.searchable = false;
        load();
    }

    angular.module("elproffApp")
    .controller("ReviewerCourseTasksCtrl", 
            ["$scope", "Common", "Crumbs", "$routeParams", "$location", "Task", "CommonCache", controller]);
}).call(null);
