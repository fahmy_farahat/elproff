"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ReviewerTaskQuestionsViewCtrl
 * @description
 * # ReviewerTaskQuestionsViewCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, $location, Course, Task, Question, Restangular) {

        scope.courseid = params.courseid;
        scope.taskid = params.taskid;

        scope.crumbs = [
            {title: 'dashboard', link: "#/reviewer/"},
            {title: 'course', link: "#/reviewer/course/"},
            {title: params.courseid, link: "#/reviewer/course/" + params.courseid},
            {title: 'task', link: "#/reviewer/course/" + params.courseid},
            {title: 'questions', link: ""},
        ];

        Crumbs.setCourse(scope.crumbs[2], params.courseid, scope);
        function load() {
            scope.questions = [];

            scope.onTaskSuccess = function onSuccess(result) {
                scope.task = result;
                scope.loading = false;
            }
            scope.onQuestionSuccess = function onSuccess(result){
                scope.loading = false;
                scope.questions = result;
                if (scope.questions.length == 0){
                    $location.path(scope.crumbs[2].link.substring(1));
                }
            }
            var query = _.cloneDeep(Common.query);
            query.task_id = params.taskid;
            Task.mytask_questions(query).then(scope.onQuestionSuccess, Common.onError(scope));
            Task.one(params.taskid).get().then(scope.onTaskSuccess, Common.onError(scope));
        }
        load();
        scope.searchable = true;
    }

    angular.module("elproffApp")
    .controller("ReviewerTaskQuestionsViewCtrl", 
            ["$scope", "Common", "Crumbs", "$routeParams", "$location", "Course", "Task",
            "Question", "Restangular", controller]);
}).call(null);
