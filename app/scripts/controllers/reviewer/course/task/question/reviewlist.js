"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:QuestionReviewListCtrl
 * @description
 * # QuestionReviewListCtrl
 * Controller of the elproffApp
 */
(function () {

    function controller(scope, $rootScope, Common, Crumbs, params, $location, $modal, Review) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/reviewer/"},
            {title: 'course', link: "#/reviewer/course/"},
            {title: params.courseid, link: "#/reviewer/course/" + params.courseid},
            {title: 'task', link: "#/reviewer/course/" + params.courseid + "/task/" },
            {title: 'Questions', link: "#/reviewer/course/" + params.courseid + "/task/" + params.taskid + "/question/"+params.questionid},
            {title: 'reviewlist', link: ""},
        ];
        
        scope.model = {};
        scope.model.reviews = [];
        scope.alerts = [];
        scope.courseid = params.courseid;
        Crumbs.setCourse(scope.crumbs[2], scope.courseid, scope);
        
        function load() {
            //,"question__is_reviewed":false
            Review.getList({"question__id":params.questionid}).then(function(reviews){
                scope.model.reviews = reviews;
            });
        }
        load();
        scope.onSuccess = function onSuccess(result) {
          $location.path(scope.crumbs[4]);
        };

        scope.view_question = function view_question(index){
            var modalInstance = $modal.open({
                templateUrl: "views/modals/viewquestionversion.html",
                controller: "ModalsViewquestionversionCtrl",
                resolve: {
                    index: function(){
                        return index;
                    },
                    allquestions: function(){
                        return scope.model.reviews;
                    },
                    courseid :function(){
                        return params.courseid;
                    }
                }
            });
        };

        
        scope.onError = function onError(result){
            var msg = scope.$root.lang["error_"+result.data.error];
            scope.alerts.push({msg: msg, type: "danger"});
        };
        

    }
    angular.module("elproffApp")
        .controller("QuestionReviewListCtrl", ["$scope", "$rootScope", "Common", "Crumbs", "$routeParams",
         "$location", "$modal", "Review", controller]);
}).call(null);