/**
 * @ngdoc function
 * @name elproffApp.controller:ReviewerQuestionViewCtrl
 * @description
 * # ReviewerQuestionViewCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service, Task, Restangular, Validate, Review, plumbEndPoint, CommonCache, $timeout) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/reviewer/"},
            {title: 'course', link: "#/reviewer/course/"},
            {title: params.courseid, link: "#/reviewer/course/" + params.courseid},
            {title: 'task', link: "#/reviewer/course/" + params.courseid + "/task/" },
            {title: 'Questions', link: "#/reviewer/course/" + params.courseid + "/task/" + params.taskid + "/questions"},
            {title: 'edit', link: ""},
        ];


        Crumbs.setCourse(scope.crumbs[2], params.courseid);


        scope.onQuestionSuccess = function onSuccess(result) {
            // Getting question review by ID 
            var question = JSON.parse(result.version)[0]['fields']
            scope.model = question;
            scope.model.id = Common.uriToID(result.question);
            scope.model.question = JSON.parse(question.question);
            scope.model.correct_answer = JSON.parse(question.correct_answer);
            // Setting question's answer
            scope.model.user = {};
            scope.model.user.answer = scope.model.correct_answer.answer; 
            scope.courseid = params.courseid;
            scope.taskid = params.taskid;
            // Setting question's time
            scope.model.minutes = Math.floor(question.duration / 60);
            scope.model.seconds = question.duration - (scope.model.minutes * 60);
        }


        function init() {
            scope.objective = scope.task.questions_bulk.objective;
            var task = scope.task;
            _.merge(scope.model, {
                task: task,
                difficulty: task.questions_bulk.difficulty,
                learning_outcome:  scope.objective.learning_outcome,
                course_element: task.course_element,
                question_type: task.questions_bulk.questions_type,
            });

            scope.model.question_type = scope.task.questions_bulk.questions_type;
            $timeout(function(){
                scope.$root.$broadcast("question_init");
            });

            var answer_watch = scope.$watch("model.user.answer", function() {
                if (scope.model.user) {
                    scope.model.correct_answer.answer = scope.model.user.answer;
                    answer_watch();
                }
            });
            
        }

        Task.one(params.taskid).get().then(function(task) {
                scope.task = task;
                CommonCache.task(task);
                task.questions_bulk_promise.then(function(){
                    task.questions_bulk.objective_promise.then(init);
                });
            });

        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            scope.alerts = Common.alerts.success.edit;
            $location.path(scope.crumbs[4].link.substring(1));
        };

        scope.onError = function onError(error) {
            scope.alerts = Common.alerts.error.edit;
        };

        Review.one(params.questionid).get().then(scope.onQuestionSuccess, scope.onError);
        scope.process = function(changed){
            scope.isLoading = true;
            scope.model.correct_answer.answer = scope.model.user.answer;
            var model = _.cloneDeep(scope.model);
            if (model.description.replace(" ", "") === "") {
                scope.alerts = Common.alerts.error.question_desc_required;
                return;
            }
            if (["mcq", "multi_mcq", "connect", "dragdrop", "fillblanks"].indexOf(model.question_type) >= 0) {
                if (!model.question.choices || model.question.choices.length < 2) {
                    scope.alerts = Common.alerts.error.question_choices_required;
                    return;
                }
            }
            if (["connect", "dragdrop"].indexOf(model.question_type) >= 0) {
                if (!model.question.ends || model.question.ends.length < 2) {
                    scope.alerts = Common.alerts.error.question_ends_required;
                    return;
                }
            }
            model.duration = model.minutes * 60 + model.seconds;
            if (!model.duration) {
                scope.alerts = Common.alerts.error.question_duration_required;
                return;
            }
            // Validate that there are no empty or duplicated choices, if choices exists
            if (model.question.choices && !_.isEmpty(model.question.choices)) {                
                var empty_choices = _.filter(model.question.choices, function(answer_choice){
                    // Getting the field type (text || image || equation)
                    var answer_type = answer_choice.type;
                    // Looking if that field is empty
                    // And return that choice if empty
                    return _.isEmpty(answer_choice[answer_type]);
                });
                if (!_.isEmpty(empty_choices)) {
                    // There"s one or more empty choices
                    // Let"s throw an error
                    scope.alerts = Common.alerts.error.empty_choices;
                    scope.isDisabled = false;
                    return;
                }
                // Validate if there is any duplicated choice
                // First, we will remove the duplicates if there"re any
                var choices_without_duplicates = _.uniq(model.question.choices, function(choice){
                    // Uniquiness should be through the text, image, equation
                    return [choice.equation, choice.text, choice.image].join();
                });
                // Compare the two objects (Original choices && Choices without duplicates)
                if (model.question.choices.length !== choices_without_duplicates.length) {
                    // If there"s any difference in length between them, there"s a duplicate
                    // Let"s throw an error and stop the request
                    scope.alerts = Common.alerts.error.duplicated_choices;
                    scope.isDisabled = false;
                    return;
                }
            }
            // Validate that there are no empty or duplicated ends, if ends exists
            if (model.question.ends && !_.isEmpty(model.question.ends)) {                
                var empty_ends = _.filter(model.question.ends, function(answer_end){
                    // Getting the field type (text || image || equation)
                    var answer_type = answer_end.type;
                    // Looking if that field is empty
                    // And return that end if empty
                    return _.isEmpty(answer_end[answer_type]);
                });
                if (!_.isEmpty(empty_ends)) {
                    // There"s one or more empty ends
                    // Let"s throw an error and stop the request
                    scope.alerts = Common.alerts.error.empty_ends;
                    scope.isDisabled = false;
                    return;
                }
                // Future: Check choices duplications
            }
            if (!Validate.answer(model.correct_answer.answer, model.question_type)) {
                scope.alerts = Common.alerts.error.question_answer_required;
                return;
            }
            delete model.user;
            delete model.seconds;
            delete model.minutes;
            delete model.resource_uri;
            Review.question_change(model).then(this.onSuccess, Common.onError(scope, this.onError));
        }
        scope.save = function() {
            scope.process(true);
        };

        scope.cancel = function(){
            $location.path(scope.crumbs[4].link.substring(1));
        };
        function addTerminal(terminal, prefix) {
            return function(withAnswers) {
                var model = {
                    text: "",
                    image: "",
                    equation: "",
                    type: "text"
                };
                // When adding an end, watch its name for a change
                // and hold its value in
                if (terminal === "ends") {
                    model.onchange = function(newValue, oldValue) {
                        //scope.model.question.ends
                        var prevAnswer = scope.model.user.answer[oldValue];
                        scope.model.user.answer[newValue] = prevAnswer;
                        delete scope.model.user.answer[oldValue];
                    };
                }
                var arr = scope.model.question[terminal];
                arr.push(model);
                if (withAnswers) {
                    scope.model.user.answer.push(false);
                }
                // Build jsPlumb
                if (scope.model.question_type === "connect") {
                    // We have to reset end points, to prevent
                    // Duplicates or misarranged objects
                    jsPlumb.selectEndpoints({"source": $("." + prefix)}).remove();
                    // Building end points based on the end model
                    // and its prefix (end || choices)
                    _.each(arr, function(model, index) {
                        plumbEndPoint.create(index, prefix);
                    });
                }
                // Gain focus to the last input
                setTimeout( function(){ 
                    $('input[ng-model="model.text"]:last').focus() 
                }, 100);
            };
        }

        function removeTerminal(terminal, prefix) {
            return function(index) {
                scope.model.question[terminal].splice(index, 1);
                if (scope.model.question_type === "connect") {
                    // Cleaning end points so we could entirly reset them
                    jsPlumb.selectEndpoints({"source": $("." + prefix)}).remove();
                    // Re-Building Based on the end model
                    _.each(scope.model.question[terminal], function(model, index) {
                        plumbEndPoint.create(index, prefix);
                    });
                }
                if (scope.model.user && scope.model.user.answer &&
                    scope.model.user.answer.splice) {
                    scope.model.user.answer.splice(index, 1);
                }
            };
        }

        scope.addEnd = addTerminal("ends", "end");
        scope.addChoice = addTerminal("choices", "choice");

        scope.removeEnd = removeTerminal("ends", "end");
        scope.removeChoice = removeTerminal("choices", "choice");
    }

    angular.module("elproffApp")
    .controller("ReviewerQuestionViewCtrl",
            ["$scope", "Common", "Crumbs", "$routeParams", "$location",
            "Question", "Task", "Restangular", "Validate", "Review", "plumbEndPoint", "CommonCache", "$timeout", controller]);
}).call(null);
