"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ReviewerCourseTaskCreateCtrl
 * @description
 * # ReviewerCourseTaskCreateCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, $location,  Service, root, CommonCache) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/reviewer/"},
            {title: 'course', link: "#/reviewer/course/"},
            {title: params.courseid, link: "#/reviewer/course/" + params.courseid},
            {title: 'task', link: "#/reviewer/course/" + params.courseid + "/task/"},
            {title: 'add', link: ""},
        ];

        scope.alerts = [];
        scope.isLoading = false;
        scope.courseid = params.courseid;
        scope.searchable = true;
        Crumbs.setCourse(scope.crumbs[2], scope.courseid);
        function load() {
            scope.isLoading = true;
            var query = _.cloneDeep(Common.query);
            query.course = params.courseid;
            Service.available(query).then(function(entities) {
                scope.entities = entities.objects;
                scope.entities = _.each(scope.entities, function(task){
                    task = CommonCache.task(task);
                });
                scope.isLoading = false;
            }, Common.onError(scope));
        }
        scope.$on("relist", load);
        load();

        // When reviewer acquires a task he have all the difficulty levels of the task
        scope.afterAssign = function(task){
            _.filter(scope.entities, function(entity) {
                if (entity.objective === task.objective && 
                    entity.course_element.id === task.course_element.id && 
                    entity.questions_bulk.questions_type === task.questions_bulk.questions_type) {
                    Service.selfAssign({task_id: entity.id}).then(function(result) {
                        entity.mine = true;
                        scope.$root.$broadcast("relist");
                        var msg = root.lang.selfassign;
                        scope.alerts = [{
                            msg: msg,
                            type: "success"
                        }];
                    }, Common.onError(scope));
                }
            });
        };
    }

    angular.module("elproffApp")
    .controller("ReviewerCourseTaskCreateCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "Task", "$rootScope", "CommonCache", 
            controller]);
}).call(null);
