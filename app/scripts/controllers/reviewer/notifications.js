"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ReviewerNotificationsCtrl
 * @description
 * # ReviewerNotificationsCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("ReviewerNotificationsCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
