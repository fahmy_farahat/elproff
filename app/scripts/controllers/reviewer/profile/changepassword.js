"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ReviewerProfileChangepasswordCtrl
 * @description
 * # ReviewerProfileChangepasswordCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("ReviewerProfileChangepasswordCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
