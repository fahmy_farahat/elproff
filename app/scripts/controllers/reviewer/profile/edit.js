"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ReviewerProfileEditCtrl
 * @description
 * # ReviewerProfileEditCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("ReviewerProfileEditCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
