"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeAuthorCtrl
 * @description
 * # SmeAuthorCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, Author) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/sme/"},
            {title: 'author', link: ""},
        ];

        function load() {
            scope.entities = Author.getList({order_by: "user__email"}).$object;
        }
        load();
        scope.$on("relist", load);
        scope.searchable = true;

    }

    angular.module("elproffApp")
    .controller("SmeAuthorCtrl",
        ["$scope", "Common", "Crumbs", "Author", controller] );
}).call(null);
