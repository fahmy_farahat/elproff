"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeReviewerCreateCtrl
 * @description
 * # SmeReviewerCreateCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/sme/"},
            {title: 'reviewer', link: "#/sme/reviewer"},
            {title: 'invite', link: ""},
        ];

    }

    angular.module("elproffApp")
    .controller("SmeReviewerCreateCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "Invitation", controller]);
}).call(null);
