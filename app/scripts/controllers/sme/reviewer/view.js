"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeReviewerViewCtrl
 * @description
 * # SmeReviewerViewCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, params, Reviewer, CommonCache) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/sme/"},
            {title: 'reviewer', link: "#/sme/reviewer/"},
            {title: 'view', link: ""},
        ];

        scope.model = { };
        Reviewer.one(params.reviewerid).get().then(function(obj) {
            scope.model = obj;
            CommonCache.staffMember(scope.model);
        });

    }

    angular.module("elproffApp")
    .controller("SmeReviewerViewCtrl",
        ["$scope", "Common", "$routeParams", "Reviewer", "CommonCache", controller]);
}).call(null);
