"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeReviewerCtrl
 * @description
 * # SmeReviewerCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs,  Reviewer) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/sme/"},
            {title: 'reviewer', link: ""},
        ];

        function load() {
            scope.entities = Reviewer.getList().$object;
        }
        load();
        scope.$on("relist", load);
        scope.searchable = true;
    }

    angular.module("elproffApp")
    .controller("SmeReviewerCtrl", 
            ["$scope", "Common", "Crumbs", "Reviewer", controller] );
}).call(null);
