"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeAuthorTaskCtrl
 * @description
 * # SmeAuthorTaskCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("SmeAuthorTaskCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
