"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeAuthorViewCtrl
 * @description
 * # SmeAuthorViewCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, Author, CommonCache) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/sme/"},
            {title: 'author', link: "#/sme/author/"},
            {title: 'view', link: ""},
        ];

        scope.model = { };
        Author.one(params.authorid).get().then(function(obj) {
            scope.model = obj;
            CommonCache.staffMember(scope.model);
        });

    }

    angular.module("elproffApp")
    .controller("SmeAuthorViewCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "Author", "CommonCache", controller]);
}).call(null);
