"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeAuthorQuestionCtrl
 * @description
 * # SmeAuthorQuestionCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, params, Common, Crumbs, Question, Author, CommonCache) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/sme/"},
            {title: 'author', link: "#/sme/author"},
            {title: params.authorid, link: "#/sme/author/" + params.authorid},
            {title: 'questions', link: ""},
        ];
        scope.authorid = params.authorid;

        function load() {
            if (!scope.author) {
                Author.one(scope.authorid).get().then(function(author) {
                    scope.author = author;
                    scope.crumbs[2].title = author.user.full_name;
                    var query = {"created_by": scope.author.user.id};
                    Question.getList(query).then(function(questions) {
                        scope.questions = _.map(questions, CommonCache.question);
                    });
                });
            } else {
                var query = {"created_by": scope.author.user.id};
                Question.getList(query).then(function(questions) {
                    scope.questions = _.map(questions, CommonCache.question);
                });
            }
        }
        load();
        scope.$on("relist", load);
    }

    angular.module("elproffApp")
    .controller("SmeAuthorQuestionCtrl",
        ["$scope", "$routeParams", "Common", "Crumbs", "Question", "Author",
        "CommonCache", controller]);
}).call(null);
