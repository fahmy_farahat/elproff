"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeAuthorTaskViewCtrl
 * @description
 * # SmeAuthorTaskViewCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("SmeAuthorTaskViewCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
