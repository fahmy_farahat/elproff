"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeAuthorCreateCtrl
 * @description
 * # SmeAuthorCreateCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/sme/"},
            {title: 'author', link: "#/sme/author"},
            {title: 'invite', link: ""},
        ];

    }

    angular.module("elproffApp")
    .controller("SmeAuthorCreateCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "Invitation", controller]);
}).call(null);
