"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeCourseElementsCtrl
 * @description
 * # SmeCourseElementsCtrl
 * Controller of the elproffApp
 */
(function() {
    function controller(scope, CourseElement, Crumbs, params, modal, $timeout, Common, CommonCache) {

        scope.alerts = [];
        scope.courseid = params.courseid;
        scope.crumbs = [
            {title: 'dashboard', link: "#/sme/"},
            {title: 'course', link: "#/sme/course/"},
            {title: params.courseid, link: "#/sme/course/" + scope.courseid},
            {title: "Visualization"}
        ];

        // Sets both the scope.course and updates the crumbs
        Crumbs.setCourse(scope.crumbs[2], params.courseid, scope);

        var elementClaasName = ".angular-ui-tree-handle";
        var query = {
            "parent__isnull": "True",
            "course": params.courseid
        };

        function load() {
            CourseElement.getList(query).then(function(result) {
                var elements = _.map(result, CommonCache.element);
                scope.data = _.filter(elements, {parent: null});
            });
        }
        load();
        scope.$on("relist", load);

        scope.toggle = function(scope) {
            scope.toggle();
        };

        scope.onSuccess = function onSuccess(result) {
            // TODO: When error revert back and show the error
            scope.alerts = Common.alerts.success.add;
            $(elementClaasName).addClass("success-flash");
            $timeout(function () {
                $(elementClaasName).removeClass("success-flash");
            }, 1000);
        };

        scope.onError = function onError(result) {
            // TODO: When error revert back and show the error
            scope.alerts = Common.alerts.error.add;
            // Flash the background red
            $(elementClaasName).addClass("error-flash");
            $timeout(function () {
                $(elementClaasName).removeClass("error-flash");
            }, 500);
        };

        scope.treeOptions = {
            dropped: function(event) {
                var source = event.source.nodeScope.$modelValue;
                var newParentUri;
                if (event.dest.nodesScope.$parent.$modelValue){
                    newParentUri = event.dest.nodesScope.$parent.$modelValue.resource_uri;
                }
                if (!event.dest.nodesScope.$parent.$modelValue) {
                    newParentUri = null;
                }
                source.parent = newParentUri;

                CourseElement.one("" + source.id).customPUT(source).then(function() {
                    var surroundingElements = {
                        ids:_.pluck(_.filter(event.dest.nodesScope.$parent.data), "id")
                    };
                    CourseElement.tree_sort(surroundingElements).then(scope.onSuccess, scope.onError);
                }, function(error) {
                    // TODO: When error revert back and show the error
                    if (error.status === 401) {
                        scope.alerts = Common.alerts.error.add;
                    }
                // Flash the background red
                $(elementClaasName).addClass("error-flash");
                $timeout(function () {
                    $(elementClaasName).removeClass("error-flash");
                    }, 500);
                });
            }
        };

        scope.dialogBox = function dialogBox(node) {
            var modalInstance = modal.open({
                templateUrl: "views/modals/list-child-course-element.html",
                controller: "ModalsListchildcourseelementCtrl",
                size: "lg",
                resolve: {
                    courseElementNode: function () {
                        return node;
                    }
                }
            });
        };
    }
  angular.module("elproffApp")
    .controller("SmeCourseElementsCtrl", ["$scope", "CourseElement",
      "Crumbs", "$routeParams", "$modal", "$timeout", "Common", "CommonCache", controller
    ]);
}).call(null);
