"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeObjectiveCreateCtrl
 * @description
 * # SmeObjectiveCreateCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, $location, Objective, Course, CourseElement, $modal, QuestionsBulk, $q, Task) {

        scope.courseid = params.courseid;
        scope.elementid = params.elementid;
        scope.isLoading = false;

        scope.crumbs = [
            {title: 'dashboard', link: "#/sme/"},
            {title: 'courses', link: "#/sme/course/"},
            {title: params.courseid, link: "#/sme/course/" + params.courseid},
            {title: params.elementid, link: "#/sme/course/" + params.courseid + '/element/' + params.elementid},
            {title: ['createobjective'], link: ""},
        ];

        scope.model = { };

        Crumbs.setElement(scope.crumbs[3], params.elementid, scope, function(element) {
            scope.model.course_element = element;
            Crumbs.setCourse(scope.crumbs[2], params.courseid, scope, function(course) {
                scope.model.course_element.course = course;
            });
        });

        scope.onSuccess = function onSuccess(result) {
            //scope.alerts = Common.alerts.success.add;
            scope.isLoading = false;
            $location.path(scope.crumbs[3].link.substring(1));
        };

        scope.bulks = [];
        scope.addBulk = function addBulk() {

            var modalInstance = $modal.open({
                templateUrl: "views/modals/createquestionsbulk.html",
                controller: "ModalsCreatequestionsbulkCtrl",
                size: "lg",
                resolve: { }
            });

            modalInstance.result.then(function (bulk) {
                // Questions Bulks
                scope.bulks.push(bulk);
            });
        };

        scope.save = function save() {
            scope.isLoading = true;
            var model = _.cloneDeep(scope.model);
            // model.questions
            model.learning_outcome = model.learning_outcome.name;
            model.questions_bulks = _.map(scope.bulks, function(originalBulk) {
                var bulk = _.cloneDeep(originalBulk);
                bulk.questions_type = bulk.questions_type.name;
                bulk.difficulty = bulk.difficulty.name;
                bulk.tasks = [];
                //var originalCount = bulk.count;
                //while (originalCount > 0 ) {
                    //var count = originalCount;
                    //if (count > 10) {
                        //count = 10;
                    //}
                    //var savable = {
                        //duration: count * 5,
                        //description: scope.$root.lang.createquestions,
                        //description_en: scope.$root.lang.trans_obj.en.createquestions,
                        //description_ar: scope.$root.lang.trans_obj.ar.createquestions,
                        //count: count,
                        //course_element: scope.element.resource_uri
                    //};
                    //bulk.tasks.push(savable);
                    //originalCount -= count;
                //}
                var savable = {
                    duration: bulk.count * 5,
                    description: scope.$root.lang.createquestions,
                    description_en: scope.$root.lang.trans_obj.en.createquestions,
                    description_ar: scope.$root.lang.trans_obj.ar.createquestions,
                    count: bulk.count,
                    course_element: scope.element.resource_uri
                };
                bulk.tasks.push(savable);
                return bulk;
            });
            Objective.post(model).then(this.onSuccess, Common.onError(scope));
        };

    }

    angular.module("elproffApp")
    .controller("SmeObjectiveCreateCtrl",
            ["$scope", "Common", "Crumbs", "$routeParams", "$location", "Objective",
            "Course", "CourseElement", "$modal", "QuestionsBulk", "$q", "Task",
            controller] );
}).call(null);
