"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeObjectiveViewCtrl
 * @description
 * # SmeObjectiveViewCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, Objective, Course, CourseElement, LearningOutcome, QuestionType, Difficulty, QuestionsBulk, $modal, $q) {

        scope.courseid = params.courseid;
        scope.elementid = params.elementid;
        scope.objectiveid = params.objectiveid;

        scope.crumbs = [
            {title: 'dashboard', link: "#/sme/"},
            {title: 'courses', link: "#/sme/course/"},
            {title: params.courseid, link: "#/sme/course/" + params.courseid},
            {title: params.elementid, link: "#/sme/course/" + params.courseid + "/element/" + params.elementid},
            {title: params.objectiveid, link: ""}
        ];


        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            scope.model = result;
            // TODO: SET LEARNING OUTCOME 
            Crumbs.setElement(scope.crumbs[3], params.elementid, scope, function(element) {
                scope.model.course_element = element;
                Crumbs.setCourse(scope.crumbs[2], params.courseid, scope, function(course) {
                    scope.model.course_element.course = course;
                });
            });
            scope.crumbs[4].title = result.title;
        }

        scope.onError = function onError(error){
            scope.alerts = Common.alerts.error.edit;
        }

        Objective.one(params.objectiveid).get().then(scope.onSuccess, Common.onError(scope, scope.onError));

    }

    angular.module("elproffApp")
    .controller("SmeObjectiveViewCtrl", 
            ["$scope", "Common", "Crumbs", "$routeParams", "Objective", "Course", "CourseElement", 
            "LearningOutcome", "QuestionType", "Difficulty", "QuestionsBulk", "$modal",
            "$q", controller] );
}).call(null);
