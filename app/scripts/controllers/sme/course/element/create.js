"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeCourseElementCreateCtrl
 * @description
 * # SmeCourseElementCreateCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, CourseElementType, Course, CourseElement, $location) {

        scope.courseid = params.courseid;
        scope.model = {};
        scope.crumbs = [
            {title: 'dashboard', link: "#/sme/"},
            {title: 'course', link: "#/sme/course/"},
            {title: params.courseid, link: "#/sme/course/" + params.courseid},
    // No need for element listing, they are being listed in course
            {title: 'elements', link: "#/sme/course/" + params.courseid},
            {title: 'create', link: ""},
        ];

        Crumbs.setCourse(scope.crumbs[2], params.courseid, scope);

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $location.path(scope.crumbs[2].link.substring(1));
        };

        scope.save = function save() {
            scope.isLoading = true;
            var model = _.cloneDeep(scope.model);
            CourseElement.post(model).then(this.onSuccess, Common.onError(scope));
        };
    }

    angular.module("elproffApp")
    .controller("SmeCourseElementCreateCtrl", 
            ["$scope", "Common", "Crumbs", "$routeParams", "CourseElementType", "Course",
            "CourseElement", "$location", controller] );
}).call(null);
