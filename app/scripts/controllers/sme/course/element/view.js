"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeCourseElementViewCtrl
 * @description
 * # SmeCourseElementViewCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, $location, Course, CourseElement, Objective, $modal) {

        scope.courseid = params.courseid;
        scope.elementid = params.elementid;
        scope.crumbs = [
            {title: 'dashboard', link: "#/sme/"},
            {title: 'course', link: "#/sme/course/"},
            {title: params.courseid, link: "#/sme/course/" + params.courseid},
            {title: 'element', link: "#/sme/course/" + params.courseid + "/element/" + params.elementid},
            {title: params.elementid, link: ""}
        ];

        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            load();
        };

        scope.remove = function remove(element) {
            scope.isLoading = true;
            if (!element) {
                return;
            }
            CourseElement.one("" + element.id).remove().then(scope.onSuccess, Common.onError(scope));
        };
        scope.reactivate = function reactivate(element){
            scope.isLoading = true;
            if(!element){
                return;
            }
            CourseElement.reactivate_element({"element":element.id}).then(scope.onSuccess, Common.onError(scope));
        };

        scope.changeCourseElement = function changeCourseElement(objective) {
            scope.obj = objective;

            var modalInstance = $modal.open({
                templateUrl: "views/modals/createcourseelement.html",
                controller: "ModalsCreatecourseelementCtrl",
                size: "md",
                resolve: {
                    currentElement: function () {
                        return scope.obj.course_element;
                    }
                }
            });

            modalInstance.result.then(function (newElement) {
                Objective.get(scope.obj.id).then(function(obj){
                    obj.course_element = _.first(newElement.elements);
                    obj.put().then(scope.onSuccess, Common.onError(scope));
                });
            });

        };

        Crumbs.setCourse(scope.crumbs[2], params.courseid, scope);
        Crumbs.setElement(scope.crumbs[4], params.elementid, scope);
        function load() {
            var objectivesQuery = {element: params.elementid};
            Objective.objective_customlist(objectivesQuery).then(function(objectives){
                scope.objectives = objectives.objects;
            });
            var elementsQuery = {course: params.courseid, parent: params.elementid};
            CourseElement.custom_list(elementsQuery).then(function(result){
                scope.subelements = result.objects;
            });
        };
        load();
        scope.remove_objective = function removeObjective(objective){
            scope.isLoading = true;
            if (!objective) {
                return;
            }
            Objective.one(""+objective.id).remove().then(scope.onSuccess, Common.onError(scope));
        };
        scope.reactivate_objective = function reactivate_objective(objective){
            scope.isLoading = true;
            if(!objective){
                return;
            }
            Objective.reactivate_objective({"objective":objective.id}).then(scope.onSuccess, Common.onError(scope));
        };
        scope.$on("relist", load);
        scope.searchable = true;
    }

    angular.module("elproffApp")
    .controller("SmeCourseElementViewCtrl", 
            ["$scope", "Common", "Crumbs", "$routeParams", "$location", "Course", "CourseElement",
            "Objective", "$modal", controller]);
}).call(null);
