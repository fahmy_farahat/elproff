"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeCourseElementUpdateCtrl
 * @description
 * # SmeCourseElementUpdateCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, CourseElementType, Course, $location,  CourseElement, Restangular) {
        scope.model = {};
        scope.crumbs = [
            {title: 'dashboard', link: "#/sme/"},
            {title: 'course', link: "#/sme/course/"},
            {title: params.courseid, link: "#/sme/course/" + params.courseid},
    // No need for element listing, they are being listed in course
            {title: 'elements', link: "#/sme/course/" + params.courseid},
            {title: params.elementid, link: "#/sme/course/" + params.courseid + "/element/" + params.elementid},
            {title: 'update', link: ""},
        ];
        Crumbs.setCourse(scope.crumbs[2], params.courseid, scope);
        CourseElement.one(params.elementid).get().then(function(obj) {
            scope.model = obj;
            CourseElementType.getUri(obj.element_type).then(function(elementType){
                scope.model.element_type = elementType;
            });
            if(obj.parent && typeof obj.parent !== "undefined"){
                Restangular.oneUrl("course_element", obj.parent).get().then(function(parent){
                    scope.model.parent = parent;    
                });
            }
            scope.crumbs[4].title = scope.$root.lang.i18n(obj, "name");
        });

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $location.path(scope.crumbs[4].link.substring(1));
        };

        scope.save = function save() {
            scope.isLoading = true;
            //delete scope.model.children;
            scope.model.save().then(this.onSuccess, Common.onError(scope));
        };
    }

    angular.module("elproffApp")
    .controller("SmeCourseElementUpdateCtrl",
            ["$scope", "Common", "Crumbs", "$routeParams", "CourseElementType",
            "Course", "$location", "CourseElement", "Restangular", controller] );
}).call(null);
