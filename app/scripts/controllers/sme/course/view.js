"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeCourseViewCtrl
 * @description
 * # SmeCourseViewCtrl
 * Controller of the elproffApp
 */

(function () {

    function controller(scope, Crumbs, params, CourseElement, Common, Question, Course, Grade) {

        scope.courseid = params.courseid;
        scope.crumbs = [
            {title: 'dashboard', link: "#/sme/"},
            {title: 'course', link: "#/sme/course/"},
            {title: params.courseid, link: ""}
        ];

        // Sets both the scope.course and updates the crumbs
        Crumbs.setCourse(scope.crumbs[2], params.courseid, scope);
        function load() {
            CourseElement.custom_list({course: params.courseid}).then(function(result){
                scope.elements = _.map(result.objects, function(element) {
                    Course.getUri(element.course).then(function(course) {
                        element.course = course;
                        element.course.grade = Grade.getUri(course.grade).$object;
                    });
                    return element;
                });
            });
        }
        load();
        scope.$on("relist", load);
        scope.searchable = true;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            load();
        };
        scope.reactivate = function reactivate(element){
            scope.isLoading = true;
            if(!element){
                return;
            }
            CourseElement.reactivate_element({"element":element.id}).then(scope.onSuccess, Common.onError(scope));
        }

        scope.remove = function remove(entity) {
            scope.isLoading = true;
            if (!entity) {
                return;
            }
            // entity is not a restangular object so we cannot use .remove()
            CourseElement.one("" + entity.id).remove().then(scope.onSuccess, Common.onError(scope));
        };
        scope.duplicationsCount = 0;
        Question.duplications({course_id:params.courseid}).then(function(result){
            scope.duplicationsCount = result.meta.total_count;
        }, Common.onError(scope));

    }

    angular.module("elproffApp")
        .controller("SmeCourseViewCtrl",
            ["$scope", "Crumbs", "$routeParams", "CourseElement", "Common",
            "Question", "Course", "Grade", controller]);
}).call(null);
