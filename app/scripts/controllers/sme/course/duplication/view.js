"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeCourseDuplicationViewCtrl
 * @description
 * # SmeCourseDuplicationViewCtrl
 * Controller of the elproffApp
 */
(function () {

    function controller(scope, Crumbs, params, Common, Question, $location) {

        scope.courseid = params.courseid;
        scope.crumbs = [
            {title: 'dashboard', link: "#/sme/"},
            {title: 'course', link: "#/sme/course/"},
            {title: params.courseid, link: "#/sme/course/" + params.courseid},
            {title: 'Duplications', link: ""}
        ];

        // Sets both the scope.course and updates the crumbs
        Crumbs.setCourse(scope.crumbs[2], params.courseid, scope);
        function load() {
            Question.duplications({course_id:params.courseid}).then(function(result){
                scope.questions = result.objects;
                if (scope.questions.length == 0){
                    $location.path(scope.crumbs[2].link.substring(1));
                }
            }, Common.onError(scope));
        }
        load();
        scope.$on("relist", load);
        scope.searchable = true;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            load();
        };
        scope.duplicationsCount = 0;
        scope.deleteQuestion = function deleteQuestion(question){
            scope.isLoading = true;
            Question.one(""+question.id).remove().then(this.onSuccess, Common.onError(scope));
        };

    }

    angular.module("elproffApp")
        .controller("SmeCourseDuplicationViewCtrl",
            ["$scope", "Crumbs", "$routeParams", "Common","Question", "$location", controller]);
}).call(null);
