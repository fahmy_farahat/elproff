"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeCourseDuplicationQuestionViewCtrl
 * @description
 * # SmeCourseDuplicationQuestionViewCtrl
 * Controller of the elproffApp
 */
(function () {

    function controller(scope, Crumbs, params, Common, Question) {

        scope.courseid = params.courseid;
        scope.crumbs = [
            {title: 'dashboard', link: "#/sme/"},
            {title: 'course', link: "#/sme/course/"},
            {title: params.courseid, link: "#/sme/course/" + params.courseid},
            {title: 'Duplications', link: "#/sme/course/" + params.courseid + "/duplications" },
            {title: params.questionid, link: "" },
        ];

        // Sets both the scope.course and updates the crumbs
        Crumbs.setCourse(scope.crumbs[2], params.courseid, scope);
        scope.onQuestionSuccess = function onSuccess(result) {
            // Getting question by ID 
            scope.model = result;
            // Setting question"s answer
            scope.model.user = {};
            scope.model.user.answer = result.correct_answer.answer; 
            // Setting question"s time
            scope.model.minutes = Math.floor(result.duration / 60);
            scope.model.seconds = result.duration - (scope.model.minutes * 60);
        }
        function load() {
        	Question.one(params.questionid).get().then(scope.onQuestionSuccess, Common.onError);
        }
        load();
        scope.$on("relist", load);
        scope.searchable = true;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            load();
        };

    }

    angular.module("elproffApp")
        .controller("SmeCourseDuplicationQuestionViewCtrl",
            ["$scope", "Crumbs", "$routeParams", "Common","Question", controller]);
}).call(null);
