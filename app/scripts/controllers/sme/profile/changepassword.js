"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeProfileChangepasswordCtrl
 * @description
 * # SmeProfileChangepasswordCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("SmeProfileChangepasswordCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
