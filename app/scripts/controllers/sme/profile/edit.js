"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeProfileEditCtrl
 * @description
 * # SmeProfileEditCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("SmeProfileEditCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
