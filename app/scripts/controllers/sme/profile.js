"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeProfileCtrl
 * @description
 * # SmeProfileCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("SmeProfileCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
