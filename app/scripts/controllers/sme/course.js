"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeCourseCtrl
 * @description
 * # SmeCourseCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs,  Course, CommonCache) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/sme/"},
            {title: 'course', link: ""},
        ];

        function load() {
            Course.getList().then(function(courses) {
                scope.entities = _.map(courses, CommonCache.course);
            });
        }
        load();
        scope.$on("relist", load);
        scope.searchable = true;
        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            load();
        };
    }

    angular.module("elproffApp")
    .controller("SmeCourseCtrl", 
        ["$scope", "Common", "Crumbs", "Course", "CommonCache", controller]);
}).call(null);
