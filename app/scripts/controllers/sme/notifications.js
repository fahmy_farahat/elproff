"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeNotificationsCtrl
 * @description
 * # SmeNotificationsCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("SmeNotificationsCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
