"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeInvitationsCtrl
 * @description
 * # SmeInvitationsCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, params, $location, Common, Service) {

    }

    angular.module("elproffApp")
    .controller("SmeInvitationsCtrl",
        ["$scope", "$routeParams", "$location", "Common", "User", controller]);
}).call(null);
