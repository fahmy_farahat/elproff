"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentCourseExamCtrl
 * @description
 * # StudentCourseExamCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, CommonCache, Crumbs, params, ExamInstance) {

        scope.model = {};
        scope.crumbs = [
            {title: "dashboard", link: "#/student/"},
            {title: params.courseid, link: "#/student/course/" + params.courseid},
            {title: "exam", link: ""}
        ];
        scope.courseid = params.courseid;
        Crumbs.setCourse(scope.crumbs[1], scope.courseid);
        scope.exams = ExamInstance.getList({course: scope.courseid}).then(function(exams){
            scope.exams = _.map(exams, CommonCache.examInstance);
        });
    }

    angular.module("elproffApp")
    .controller("StudentCourseExamCtrl",
        ["$scope", "Common", "CommonCache", "Crumbs", "$routeParams", "ExamInstance", controller]);

}).call(null);
