"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentCourseExamCtrl
 * @description
 * # StudentCourseExamCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, ExamInstance) {

        scope.model = {};
        scope.crumbs = [
            {title: "dashboard", link: "#/student/"},
            {title: params.courseid, link: "#/student/course/" + params.courseid},
            {title: "practice", link: ""}
        ];
        scope.courseid = params.courseid;
        Crumbs.setCourse(scope.crumbs[1], scope.courseid);
        scope.exams = ExamInstance.getList().$object;
    }

    angular.module("elproffApp")
    .controller("StudentCoursePracticeCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "ExamInstance", controller]);

}).call(null);
