"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentCoursePracticeLoadCtrl
 * @description
 * # StudentCoursePracticeLoadCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, Exam, $location, Course) {

        scope.courseid = params.courseid;
        scope.crumbs = [
            {title: "dashboard", link: "#/student/"},
            {title: scope.courseid, link: "#/student/course/" + params.courseid},
            {title: "practice", link: ""}
        ];

        scope.model = {};
        Crumbs.setCourse(scope.crumbs[1], params.courseid, scope);
        
        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $location.path(scope.crumbs[1].link.substring(1) + "/exam/" + result.exam_instance + "/take/0");
        };

        scope.onError = function onError(error) {
            scope.isLoading = false;
            if (error.data.error === "no_questions") {
                scope.alerts = Common.alerts.error.exam_no_questions;
            }
        };

        scope.createPractice = function createPractice() {
            scope.isLoading = true;
            var model = _.cloneDeep(scope.model);
            model.difficulty = model.difficulty.name;
            if (model.elements && model.elements.length && _.compact(model.elements).length) {
                model.elements = _.pluck(model.elements, "resource_uri");
            } else {
                model.elements = null;
            }
            if (model.questions_type && model.questions_type.length && _.compact(model.questions_type).length) {
                model.questions_type = _.pluck(model.questions_type, "name");
            } else {
                model.questions_type = null;
            }
            Exam.studentGeneratePractice(model).then(scope.onSuccess, Common.onError(scope, scope.onError));
        };
    }

    angular.module("elproffApp")
    .controller("StudentCoursePracticeGenerateCtrl",
            ["$scope", "Common", "Crumbs", "$routeParams", "Exam", "$location", "Course",
            controller]);

}).call(null);
