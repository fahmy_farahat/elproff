"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentCoursePracticeSuggestedCtrl
 * @description
 * # StudentCoursePracticeSuggestedCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, Course, ExamInstance, CommonCache) {
        scope.courseid = params.courseid;
        scope.crumbs = [
            {title: "dashboard", link: "#/student/"},
            {title: scope.courseid, link: "#/student/course/" + params.courseid},
            {title: "suggestedpractices", link:""}
        ];
        Crumbs.setCourse(scope.crumbs[1], params.courseid, scope);

        scope.onSuccess = function onSuccess(result) {
            scope.practices = _.map(result, CommonCache.examInstance)
        };

        var query = {
                exam_type: "practice",
                course_id: params.courseid,
                instance_type: "recommended",
        };

        ExamInstance.list_instances(query).then(scope.onSuccess, Common.onError(scope));
    }

    angular.module("elproffApp")
    .controller("StudentCoursePracticeSuggestedCtrl", 
        ["$scope", "Common", "Crumbs", "$routeParams", "Course","ExamInstance", "CommonCache", controller]);
}).call(null);
