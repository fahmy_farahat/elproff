"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentCourseSubscribeCtrl
 * @description
 * # StudentCourseSubscribeCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Parent, Plan, $modal) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/student"},
            {title: params.courseid, link: "#/student/course/" + params.courseid},
            {title: 'subscribe', link: ""},
        ];

        Crumbs.setCourse(scope.crumbs[1], params.courseid, scope);
        scope.plans = [];

    /*    scope.onSuccess = function onSuccess(result) {
            console.log(result);
            scope.alerts = Common.alerts.success.subscription;
        };

        scope.onError = function onError(error) {
            console.log(error);
        };

       scope.subscribe = function subscribe(plan) {
            var query = {studentid: params.studentid, planid: plan.id};
            Parent.subscribe(query).then(this.onSuccess, Common.onError(scope, this.onError));
        };*/


        scope.showdetails = function(targetPlan) {
            _.forEach(scope.plans, function(scopePlan) {
                scopePlan.planDetails = targetPlan.id === scopePlan.id && !scopePlan.planDetails;
            });
        };

        scope.subscribe = function(plan){
            scope.planid = plan.id;
            var modalInstance = $modal.open({
                templateUrl: "views/modals/add-coupon.html",
                controller: "ModalsAddcouponCtrl",
                scope: scope
            });
        };

        scope.onListSuccess = function onListSuccess(result) {
            console.log(result);
            scope.plans = result.objects;
        };

        function load() {
            Plan.listForStudent({}).then(scope.onListSuccess, Common.onError(scope, scope.onError));
        }
        load();
        scope.$on("relist", load);
    }

    angular.module("elproffApp")
    .controller("StudentCourseSubscribeCtrl",
            ["$scope", "Common", "Crumbs", "$routeParams", "$location",
            "Parent", "SubscriptionPlan", "$modal", controller]);
}).call(null);
