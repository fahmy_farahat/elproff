"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentCourseExamReportCtrl
 * @description
 * # StudentCourseExamReportCtrl
 * Controller of the elproffApp
 */
(function() {


    function controller(scope, Common, Crumbs, params, Course, Exam, ExamInstance, QuestionAnswer, Reports, StudentPrize, FB) {

        scope.crumbs = [
            {title: "dashboard", link: "#/student/"},
            {title: params.courseid, link: "#/student/course/" + params.courseid},
            //{title: "exam", link: "#/student/course/" + params.courseid + "/exam"},
            {title: "examreport", link: ""}
        ];

        scope.courseid = params.courseid;
        Crumbs.setCourse(scope.crumbs[1], params.courseid, scope);
        scope.loaded = false;

        var runned = false;
        scope.model = {
            fraction_grade: 1,
            result_grade: 1
        };

        scope.level = "";

        scope.examid = params.examid;
        scope.questionsCounts = {
            skipped: 0,
            wrong: 0,
            correct: 0,
            total: 1
        };

        scope.onSuccess = function onSuccess(result) {
            scope.loaded = true;
            scope.instanceid = result.id;
            Exam.getUri(result.exam).then(function(examModel){
                scope.model = examModel;
                QuestionAnswer.getList({exam_instance: params.examid}).then(function(answers) {
                    if (!answers.length) {
                        return;
                    }
                    scope.answers = answers;
                    if (!scope.model.grade) {
                        scope.model.grade = 100;
                    }
                    var total = answers.length;
                    var wrong = _.filter(answers,  function(ans) {
                          return (ans.degree || 0) < 0.5;
                        }).length;
                    var correct = _.filter(answers,  function(ans) {
                          return (ans.degree || 0) >= 0.5;
                        }).length;
                    var skipped = total - (wrong + correct);
                    scope.questionsCount = {
                        total: total,
                        skipped: skipped,
                        correct: correct,
                        wrong: wrong
                    };
                    var sum = _.reduce(_.pluck(answers, "degree"), function(a, b) {return a + b;});
                    scope.time = _.reduce(_.pluck(answers, "duration"), function(a, b) {return a + b;});
                    scope.model.fraction_grade = sum / answers.length;
                    scope.model.result_grade = scope.model.grade * scope.model.fraction_grade;
                    scope.model.level = Reports.calcLevel(scope.model.fraction_grade);
                }, Common.onError(scope));
                StudentPrize.studentExamPrizes({"exam_instance":result.resource_uri}).then(function(prizes){
                    scope.studentprizes = prizes;

                }, Common.onError(scope));
            });            
        };

        scope.onError = function onError(error) {
            if (error.error === "not_owner") {
                scope.alerts = Common.alerts.error.exam_not_owner;
            }
        };

        var query = {instance_id: params.examid};
        scope.baseURL = document.location.origin;
        ExamInstance.result(query).then(scope.onSuccess, scope.onError);
        scope.shareMessage = "I've got a new prize from ELProff";
    }

    angular.module("elproffApp")
    .controller("StudentCourseExamReportCtrl", 
            ["$scope", "Common", "Crumbs", "$routeParams", "Course",
            "Exam", "ExamInstance", "QuestionAnswer", 
            "Reports","StudentPrize", "Facebook", controller]);
}).call(null);
