"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentCourseExamLoadCtrl
 * @description
 * # StudentCourseExamLoadCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, $root, Common, Crumbs, params, Exam, $location, Course) {

        scope.crumbs = [
            {title: "dashboard", link: "#/student/"},
            {title: params.courseid, link: "#/student/course/" + params.courseid},
            {title: "exam", link: ""}
        ];

        scope.courseid = params.courseid;
        scope.model = {};
        scope.alerts = [];
        Crumbs.setCourse(scope.crumbs[1], params.courseid, scope);

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $location.path("/student/course/" + params.courseid + "/exam/" + result.exam_instance + "/take");
        };

        scope.onError = function onError(error) {
            if (error.data === "no_questions") {
                var msg = $root.lang.error_exam_no_questions;
            } else if (error.data === "empty_question_size") {
                var msg = $root.lang.error_empty_exam_size;
            }
            scope.alerts.push({msg:msg, type:"danger"});
        };

        scope.createExam = function createExam() {
            var model = _.cloneDeep(scope.model);
            model.difficulty = model.difficulty.name;
            if (model.elements && model.elements.length && _.compact(model.elements).length) {
                model.elements = _.pluck(model.elements, "resource_uri");
            } else {
                model.elements = null;
            }
            if (!model.count) {
                var msg = $root.lang.error_empty_exam_size;
                scope.alerts.push({msg:msg, type:"danger"});
            } else {
                Exam.studentGenerate(model).then(this.onSuccess, Common.onError(scope, this.onError));
            }
        };
    }

    angular.module("elproffApp")
    .controller("StudentCourseExamGenerateCtrl",
            ["$scope", "$rootScope", "Common", "Crumbs", "$routeParams", "Exam", "$location",
                 controller]);

}).call(null);