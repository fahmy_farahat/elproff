"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentCourseExamSchoolteachersuggestedCtrl
 * @description
 * # StudentCourseExamSchoolteachersuggestedCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, Course, ExamInstance, CommonCache) {
        scope.model = {};
        scope.courseid = params.courseid;
        scope.crumbs = [
            {title: "dashboard", link: "#/student/"},
            {title: scope.courseid, link: "#/student/course/" + params.courseid},
            {title: "suggestedteacherexams", link: ""}
        ];
        Crumbs.setCourse(scope.crumbs[1], params.courseid, scope);

        scope.onSuccess = function onSuccess(result) {
            scope.exams = _.map(result, CommonCache.examInstance)
        };

        var query = {
                exam_type: "exam",
                course_id: params.courseid,
                instance_type: "schoolteacherassigned",
        };

        ExamInstance.list_instances(query).then(scope.onSuccess, Common.onError(scope));
    }
    angular.module("elproffApp")
    .controller("StudentCourseExamSchoolteachersuggestedCtrl", 
        ["$scope", "Common", "Crumbs", "$routeParams", "Course","ExamInstance", "CommonCache", controller]);
}).call(null);
