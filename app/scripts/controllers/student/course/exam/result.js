"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentCourseExamResultCtrl
 * @description
 * # StudentCourseExamResultCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, Course, Exam, ExamInstance, $location, $timeout, QuestionAnswer, Reports) {
        scope.crumbs = [
            {title: "dashboard", link: "#/student/"},
            {title: params.courseid, link: "#/student/course/" + params.courseid},
            //{title: "exam", link: "#/student/course/" + params.courseid + "/exam"},
            {title: "result", link: ""}
        ];
        scope.courseid = params.courseid;
        Crumbs.setCourse(scope.crumbs[1], scope.courseid);
        var runned = false;
        scope.model = {
            fraction_grade: 1,
            result_grade: 1
        };

        scope.showAnswers = function() {
            $location.path("/student/course/" + params.courseid + "/exam/" + params.examid + "/answers");
        };
        scope.showReport = function() {
            $location.path("/student/course/" + params.courseid + "/exam/" + params.examid + "/report");
        };

        scope.onSuccess = function onSuccess(result) {
            Exam.getUri(result.exam).then(function(examModel){
                scope.model = examModel;
                QuestionAnswer.getList({exam_instance: params.examid}).then(function(answers) {
                    if (!answers.length) {
                        console.log("NO answers");
                        return;
                    }
                    scope.answers = answers;
                    if (!scope.model.grade) {
                        scope.model.grade = 100;
                    }
                    var sum = _.reduce(_.pluck(answers, "degree"), function(a, b) {return a + b;});
                    scope.time = _.reduce(_.pluck(answers, "duration"), function(a, b) {return a + b;});
                    scope.model.fraction_grade = sum / answers.length;
                    scope.model.result_grade = scope.model.grade * scope.model.fraction_grade;
                    scope.model.level = Reports.calcLevel(scope.model.fraction_grade);
                });
            });
        };

        scope.onError = function onError(error) {
            console.log(error);
            if (error.error === "not_owner") {
                scope.alerts = Common.alerts.error.exam_not_owner;
            }
        };

        var query = {instance_id: params.examid};
        ExamInstance.result(query).then(scope.onSuccess, scope.onError);
    }

    angular.module("elproffApp")
        .controller("StudentCourseExamResultCtrl",
                ["$scope", "Common", "Crumbs", "$routeParams", "Course", "Exam", "ExamInstance", "$location",
                "$timeout", "QuestionAnswer", "Reports", controller]);

}).call(null);
