"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentCourseExamAnswersCtrl
 * @description
 * # StudentCourseExamAnswersCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, Course, ExamInstance, $location, $timeout, QuestionAnswer, CommonCache, $q, Exam, CourseElement) {
        scope.crumbs = [
            {title: "dashboard", link: "#/student/"},
            {title: params.courseid, link: "#/student/course/" + params.courseid},
            //{title: "exam", link: "#/student/course/" + params.courseid + "/exam"},
            {title: "answers", link: ""}
        ];

        scope.examid = params.examid;
        scope.courseid = params.courseid;
        scope.model = {};

        scope.questionIndex = parseInt(params.questionIndex);

        scope.onSuccess = function onSuccess(examInstance) {
            Exam.getUri(examInstance.exam).then(function(e){
                CommonCache.examSync(e).then(function(exam){
                    scope.exam = exam;
                    scope.questions = exam.questions;
                    scope.exam.type = "perfectanswers";
                    scope.model = scope.questions[scope.questionIndex];
                    var query = {
                        question: scope.model.id,
                        exam_instance: examInstance.id
                    };
                    QuestionAnswer.getList(query).then(function(QuestionAnswer){
                        scope.questionAnswer = _.first(QuestionAnswer);
                    });
                    scope.isLast = (scope.questionIndex === scope.questions.length - 1);
                    scope.model.user = {
                        answer: scope.model.correct_answer.answer
                    };
                    
                }); 
            });
        };

        scope.onError = function onError(error) {
            console.log(error);
            if (error.error === "not_owner") {
                scope.alerts = Common.alerts.error.exam_not_owner;
            }
        };

        scope.onSuccessLoad = function(nextQuestion) {
                scope.isLoading = false;
                // Change URL to question number
                var path = $location.path().split("/");
                path[path.length-1] = nextQuestion;
                $location.path(path.join("/"));
        };

        scope.jump = function jump(nextIndex) {
            scope.isLoading = true;
            scope.onSuccessLoad(nextIndex);
        };

        scope.nextQuestionAnswer = function next() {
            scope.isLoading = true;
            scope.onSuccessLoad(scope.questionIndex + 1);
        };

        scope.finish = function finish() {
            $location.path("/student/course/" + params.courseid + "/exam/" + params.examid + "/result");
        };

        ExamInstance.get(params.examid).then(scope.onSuccess, Common.onError(scope, scope.onError));
    }

    angular.module("elproffApp")
        .controller("StudentCourseExamAnswersCtrl",
                ["$scope", "Common", "Crumbs", "$routeParams", "Course",
                "ExamInstance", "$location", "$timeout",
                "QuestionAnswer", "CommonCache", "$q", "Exam", "CourseElement",
                controller]);

}).call(null);
