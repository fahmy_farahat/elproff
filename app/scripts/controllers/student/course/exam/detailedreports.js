"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentCourseExamDetailedreportsCtrl
 * @description
 * # StudentCourseExamDetailedreportsCtrl
 * Controller of the elproffApp
 */
(function(){

  function controller(scope, Common, Crumbs, params, Parent, $timeout) {

    scope.crumbs = [
        {title: 'dashboard', link: "#/student"},
        {title: 'course', link: "#/student/" + "course/" + params.courseid},
        {title: 'detailedreport', link: ""},
    ];

    scope.courseid = params.courseid;
    Crumbs.setCourse(scope.crumbs[1], params.courseid, scope);
    scope.loaded = false;

    scope.onSuccess = function onSuccess(result) {
        scope.loaded = true;
        scope.model = result;
        scope.studentid = result[0].student;
        $(".content").mCustomScrollbar();
    };

    scope.onError = function onError(error) {
        console.log(error);
        if (error.data.error === "not_exist") {
            scope.alerts = Common.alerts.error.parent_son_noexams;
        }
    };

    scope.query = {courseid: params.courseid};
    Parent.listExamsReport(scope.query).then(scope.onSuccess, Common.onError(scope, scope.onError));
  }

  angular.module("elproffApp")
    .controller("StudentCourseExamDetailedreportsCtrl",
    ["$scope", "Common", "Crumbs", "$routeParams", "Parent", "$timeout",
      "QuestionAnswer", "Reports", controller] );
}).call(null);
