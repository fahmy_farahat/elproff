"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentCourseExamQuestionCtrl
 * @description
 * # StudentCourseExamQuestionCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("StudentCourseExamQuestionCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
