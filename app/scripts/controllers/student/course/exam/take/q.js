"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentCourseExamTakeQCtrl
 * @description
 * # StudentCourseExamTakeQCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, Course, ExamInstance, $location, $timeout, $modal, Question, QuestionAnswer, Validate, prompt, root, CommonCache, Restangular, Exam) {
        scope.crumbs = [
            {title: "dashboard", link: "#/student/"},
            {title: params.courseid, link: "#/student/course/" + params.courseid},
            //{title: "exams", link: "#/student/course/" + params.courseid + "/exam"},
            {title: "question", link: ""}
        ];
        scope.courseid = params.courseid;
        Crumbs.setCourse(scope.crumbs[1], scope.courseid);
        scope.questionIndex = parseInt(params.questionIndex);
        scope.model = {};
        scope.$root.timeValue;

        var arRegex = /[\u0600-\u06ff]|[\u0750-\u077f]|[\ufb50-\ufc3f]|[\ufe70-\ufefc]/g;
        function questionDirection(description) {
            // Set question direction (rtl/ltr)
            scope.questionDirection = arRegex.test(description) ? "questionRTL" :  "questionLTR";
            console.log("direction", scope.questionDirection);
        }

        // On Question Answer Start 
        scope.onQASuccess = function onQASuccess(result) {
            // Starting countdown Timer
            if (scope.exam.type === "exam") {
                scope.isLoading = false;
                scope.questionAnswer = result;
                // Applying student last answer to the question
                if (result.answer || _.isNumber(result.answer)) {
                    scope.model.user.answer = result.answer;
                }
                // Execute timer only if we"re inside the exam
                // Or else it will make an error
                if (scope.$root) {
                    scope.$root.$broadcast("timer-start");
                    scope.$root.$on("timer-stopped", finishExam);
                    scope.$root.$on("timer-tick",function(event, data){
                        scope.currentTime = data.millis / 1000;
                    });
                }
                questionDirection(scope.exam.questions[scope.questionIndex].description);
            } else {
                Question.getUri(result.question).then(function(practiceQuestion){
                    scope.isLoading = false;
                    scope.model = practiceQuestion;
                    scope.questionAnswer = practiceQuestion;
                    questionDirection(scope.model.description);
                });
            }
            // console.log("questions", result.exam.questions[scope.questionIndex].description);
            // console.log("current", scope.questionIndex);
        };

        // Getting answered questions list (Only Exam)
        scope.onAnsweredQSuccess = function(result) {
            // Answered Questions array of answered questions
            scope.answeredQuestions = [];
            scope.unAnsweredQuestions = [];
            scope.examInstance.answers = result;
            // Categorizing questions by their answered status
            // And calculating Countdown Timer in exam
            _.forEach(scope.allQuestions, function(examQuestion, questionIndex) {
                // Finding this question index in Answered questions list
                var thisQuestionisAnswerdIndex = _.findIndex(scope.examInstance.answers, function(answeredQuestion) {
                    // When exam question and this question id(s) are matched
                    return answeredQuestion.question === examQuestion.resource_uri;
                });
                // Checking if we had a result of finding this question in answered questions list
                if (thisQuestionisAnswerdIndex >= 0) {
                    var thisQuestion = scope.examInstance.answers[thisQuestionisAnswerdIndex];
                }
                // Checking if this question is correctly answered:
                if (thisQuestion && Validate.answer(thisQuestion.answer)) {
                    // It"s answered, we add its index to answered questions
                    // And adding its duration to the total elapsed time
                    if (_.indexOf(scope.answeredQuestions, questionIndex) === -1) {
                        scope.answeredQuestions.push(questionIndex);
                    }
                } 
                // Otherwise, it"s unanswered question,
                // Chcking if it"s not already Exists in unanswered questions list
                else if  (thisQuestion && !Validate.answer(thisQuestion.answer)) {
                    if (_.indexOf(scope.unAnsweredQuestions, questionIndex) === -1) {
                        // We add its index to unanswered questions
                        scope.unAnsweredQuestions.push(questionIndex);
                    }
                    scope.examTimeElapsed += thisQuestion.duration;
                }
            });

            // Exam time left equal Exam"s Duration by milliseconds
            // Minus the sum of answered questions" time elapsed
            if (!scope.$root.timeValue) {
                scope.$root.timeValue = scope.exam.time;
            }
            scope.examTimeLeft = scope.$root.timeValue;

            // Finish the exam if there"s no time left
            if (scope.examTimeLeft <= 0 || !_.isNull(scope.examInstance.end_time)) {
                finishExam();
            }
            scope.answeredQuestions.sort();
            scope.unAnsweredQuestions.sort();
        };

        scope.onQAError = function(error) {
            scope.isLoading = false;
        };

        scope.onSuccess = function(ExamResult) {
            if (!ExamResult || !ExamResult.exam) {
                // TODO: Show an alert , error loading exam
                return;
            }
            CommonCache.examInstanceSync(ExamResult).then(function(ExamInstanceResult){
                scope.exam = ExamInstanceResult.exam;
                console.log(scope.exam.time, ExamInstanceResult.exam.time);
                scope.examInstance = ExamInstanceResult;
                // Question request query
                var query = {
                    instance: ExamInstanceResult.resource_uri
                };
                scope.isLoading = true;
                // Using the right api to start the question
                if (scope.exam.type === "exam") {
                    scope.allQuestions = ExamInstanceResult.exam.questions;
                    scope.isLast = (scope.questionIndex === scope.allQuestions.length - 1);
                    // Reversing questions to the right order
                    scope.exam.questions = ExamInstanceResult.exam.questions.reverse();
                    // Getting answered questions
                    QuestionAnswer.list_answers(query).then(scope.onAnsweredQSuccess, Common.onError(scope, scope.onError));
                    // Getting the right question
                    scope.model = scope.allQuestions[scope.questionIndex];
                    // Starting new question
                    query.question = scope.model.resource_uri;
                    QuestionAnswer.start(query).then(scope.onQASuccess, Common.onError(scope, scope.onQAError));
                } else if (scope.exam.type === "practice") {
                    QuestionAnswer.next(query).then(scope.onQASuccess, Common.onError(scope, scope.onQAError));
                }
            });            
        };

        scope.onError = function onError(error) {
            scope.isLoading = false;
            if (error.error === "not_owner") {
                scope.alerts = Common.alerts.error.exam_not_owner;
            }
        };

        var query = {instance_id: params.examid};

        // Finishing exam function
        scope.finishExam = function finishExam() {
            scope.isLoading = false;

            // Redirect to exam report
            ExamInstance.finish(query).then(scope.onSuccess, Common.onError(scope, scope.onError));
            
            // clear all cache related to exam
            ExamInstance.clearCache();
            Question.clearCache();
            QuestionAnswer.clearCache();
            Exam.clearCache();
            delete scope.$root.timeValue;
            
            if (scope.exam.type === "practice") {
                $location.path(scope.crumbs[1].link.substring(1));
            } else {
                $location.path("/student/course/" + params.courseid + "/exam/" + params.examid + "/result");
            }
        };

        ExamInstance.get(params.examid).then(scope.onSuccess, Common.onError(scope, scope.onError));

        scope.onSuccessLoad = function(nextQuestion) {
            return function(result) {
                // Change URL to question number
                var path = $location.path().split("/");
                path[path.length-1] = nextQuestion;
                $location.path(path.join("/"));
            };
        };

        // Applying finish exam function for quick callbacks
        function finishExam(){
            scope.finishExam();
            if(!scope.$$phase) {
                scope.$apply();
            }
        }

        // Update Local Scopre(not updated) with last question status after validation
        function lastQuestionAnswer (success){
            var indexInAnswered   = _.indexOf(scope.answeredQuestions, scope.questionIndex);
            var indexInUnAnswered = _.indexOf(scope.unAnsweredQuestions, scope.questionIndex);
            if (indexInAnswered < 0) {
                scope.answeredQuestions.push(scope.questionIndex);
            }
            if (indexInUnAnswered > -1) {
                scope.unAnsweredQuestions.splice(indexInUnAnswered, 1);
            }
            success();
        }

        function solveQuestion(success, isLast) {
            // If practice, just get the next question
            if (scope.exam.type === "practice" && isLast) {
                success();
            }
            if ( Validate.answer(scope.model.user.answer, scope.model.question_type) ) {
                // The question is solved correctly
                scope.isLoading = true;
                var error = Common.onError(scope, scope.onError);
                // If exam end question by sending the answer
                if (scope.exam.type === "exam" && scope.questionAnswer) {
                    scope.$root.timeValue = scope.currentTime;
                    var answer = {
                        question_answer_id: scope.questionAnswer.resource_uri,
                        answer: scope.model.user.answer
                    };
                    // Ending the current question with sending the answer to API
                    if (isLast) {
                        QuestionAnswer.end(answer).then(lastQuestionAnswer(success), error);
                    } else {
                        QuestionAnswer.end(answer).then(success, error);
                    }
                } 
                // If practice, just get the next question
                if (scope.exam.type === "practice") {
                    success();
                }
            } else {
                // The questions isn"t solved
                skipQuestion(success);
            }
        }

        function skipQuestion(success) {
            scope.$root.timeValue = scope.currentTime;
            var modalInstance = $modal.open({
                templateUrl: "views/modals/examskipquestion.html",
                controller: "ModalsExamSkipQuestion",
                resolve: {}
            });
            modalInstance.result.then(success, _.noop);
        }

        function listSkippedQuestions() {
            // If All Questions more than Answered Questions
            // Answered Questions array of answered questions
            if (scope.allQuestions.length > scope.answeredQuestions.length) {
                scope.skippedQuestionsList = [];
                _.forEach (scope.allQuestions, function(question, i){
                    if (_.indexOf(scope.answeredQuestions, i) === -1){
                        scope.skippedQuestionsList.push(i);
                    }
                });
                // Determins the first skipped question
                // So the user can quickly jump to it
                var modalInstance = $modal.open({
                    templateUrl: "views/modals/examskippedquestions.html",
                    controller: "ModalsExamSkippedQuestions",
                    resolve: {
                        skippedQuestionsList: function () {
                            // Adding skipped question to unanswered questions
                            return _.sort(scope.skippedQuestionsList);
                        }
                    }
                });
                modalInstance.result.then(scope.finishExam, _.noop);
            } 
            // There"re no skipped questions, finish the exam
            else {
                scope.finishExam();
            }
        }

        scope.nextExamQuestion = function () {
            solveQuestion(scope.onSuccessLoad(scope.questionIndex + 1), false);
        };

        scope.nextPracticeQuestion = function () {
            solveQuestion(scope.onSuccessLoad(scope.questionIndex + 1), false);
        };

        scope.skip = function skip() {
            skipQuestion(scope.onSuccessLoad(scope.questionIndex + 1));
        };

        scope.jump = function jump(nextIndex) {
            solveQuestion(scope.onSuccessLoad(nextIndex), false);
        };

        scope.onLastAnswer = function(result) {
            listSkippedQuestions();
        };

        scope.finish = function () {
            if (scope.exam.type === "exam") {
                solveQuestion(scope.onLastAnswer, true);
            } else {
                scope.finishExam();
            }
        };

        scope.toggleHint = function toggleHint() {
            angular.element(".examHint .quest_answer").toggleClass("hidden");
        };

        var confirming = true;

        scope.$on("$locationChangeStart",function (event, next, current){
            if(scope.exam.type === "exam"){
                if (next.search("take") > 0 || next.search("result") > 0) {
                    return;
                } else {
                    if (confirming) {
                        confirming = false;
                        event.preventDefault();
                        prompt({
                            title: root.lang.examend,
                            message: root.lang.examendmessage,
                            buttons: [
                                {
                                    "label": root.lang.cancel,
                                    "cancel": true
                                },
                                {
                                    "label": root.lang.ok,
                                    "primary": true
                                }
                            ]
                        }).then(
                            function () {
                                var host = document.location.origin + "/ui/#";
                                var nextPath = next.replace(host,"");
                                ExamInstance.finish(query).then(scope.onSuccess, Common.onError(scope, scope.onError));
                                $location.path(nextPath);
                            }, 
                            function (){
                                confirming = true;
                            }
                        );
                    }
                }
            }
        });
    }

    angular.module("elproffApp")
    .controller("StudentCourseExamTakeQCtrl",
            ["$scope", "Common", "Crumbs", "$routeParams", "Course", "ExamInstance", "$location", "$timeout", "$modal",
            "Question", "QuestionAnswer", "Validate", "prompt", "$rootScope", "CommonCache", "Restangular", "Exam", controller]);

}).call(null);
