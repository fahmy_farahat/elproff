"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentCourseExamTakeCtrl
 * @description
 * # StudentCourseExamTakeCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller($q, scope, Common, Crumbs, params, Course, CourseElement, ExamInstance, Exam, $location, CommonCache, $timeout) {
        scope.crumbs = [
            {title: "dashboard", link: "#/student/"},
            {title: params.courseid, link: "#/student/course/" + params.courseid},
            {title: "exam", link: ""}
        ];
        scope.courseid = params.courseid;
        Crumbs.setCourse(scope.crumbs[1], scope.courseid);

        scope.model = {};

        scope.onSuccess = function onSuccess(result) {
            var deffered = $q.defer();
            scope.model = result;
            Exam.getUri(result.exam).then(function(exam){
                scope.model.exam = exam;
                Course.getUri(exam.course).then(function(course){
                    scope.model.exam.course = course;
                });
                if(!_.isEmpty(scope.model.exam.course_element)){
                    var afterAllCourseElements = _.after(scope.model.exam.course_element.length, function(){
                        deffered.resolve();
                    });
                    _.each(scope.model.exam.course_element, function(courseElementUri, i){
                        CourseElement.getUri(courseElementUri).then(function(courseElement){
                            scope.model.exam.course_element[i] = courseElement;
                            afterAllCourseElements();
                        });
                    });
                } else{
                    deffered.resolve();
                }
            });
            deffered.promise.then(function(){
                scope.loaded = true;                    
                ExamInstance.get(params.examid).then(function(ExamResult) {
                    CommonCache.examInstanceSync(ExamResult).then(function(ExamInstanceResult){
                        $location.path($location.path() + "/0");
                    });
                }, Common.onError(scope, scope.onError));
            });
        };

        scope.onError = function onError(error) {
            if (error.error === "not_owner") {
                scope.alerts = Common.alerts.error.exam_not_owner;
            } else if (error.error === "exam_ended") {
                scope.alerts = Common.alerts.error.exam_ended;
            } else if (error.data.error === "exam_not_started") {
                scope.alerts = Common.alerts.error.exam_not_started;
            } else if (error.data.error === "exam_ended") {
                scope.alerts = Common.alerts.error.exam_ended;
            }
            $timeout(function(){
                $location.path(scope.crumbs[1].link.substring(1));
            }, 5000);
        };

        ExamInstance.take({instance_id: params.examid})
            .then(scope.onSuccess, Common.onError(scope, scope.onError));
    }

    angular.module("elproffApp")
    .controller("StudentCourseExamTakeCtrl",
            ["$q", "$scope", "Common", "Crumbs", "$routeParams", "Course",
            "CourseElement", "ExamInstance", "Exam", "$location", "CommonCache", "$timeout",
            controller]);

}).call(null);
