"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentCourseExamCtrl
 * @description
 * # StudentCourseExamCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, Restangular, $location, Course) {

        Course.load(params.courseid).then(function(c) {
            scope.course = c;
            scope.title = c.title;
            scope.crumbs = [
                {title: "لوحة التحكم ", link: "#/student/"},
                {title: scope.title.name, link: "#/student/course/" + scope.course.id},
                {title: "   إمتحن", link: ""}
            ];
        });

        var opts = {
            lines: 12,        // The number of lines to draw
            angle: 0,         // The length of each line
            lineWidth: 0.190, // The line thickness
            pointer: {
                length: 0.36,     // The radius of the inner circle
                strokeWidth: 0.1, // The rotation offset
                color: "#dadada"  // Fill color
            },
            limitMax: false,        // If true, the pointer will not go past the end of the gauge
            colorStart: "#a3c300",  // Colors
            colorStop: "#ff3d2f",   // just experiment with them
            strokeColor: "#E0E0E0", // to see which ones work best for you
            generateGradient: true
        };
        var target = document.getElementById("guage");  // your canvas element
        var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
        gauge.maxValue = 332;                           // set max gauge value
        gauge.animationSpeed = 20;                      // set animation speed (32 is default value)
        gauge.set(20);                                  // set actual value
        $("#canvas-overlay path").click(function (e) {
            var parentOffset = $(this).offset();
            //or $(this).offset(); if you really just want the current element"s offset
            var relX = e.pageX - parentOffset.left +20;
            if(relX >= 278) {
                relX = 332;
            }
            gauge.set(relX);
        });

        scope.questionsCount = 0;
        scope.takeExam = function takeExam() {
            var difficulity = Math.floor((gauge.value / 332) * 100);
            var path = "student/course/" + params.courseid + "/exam/load/" + difficulity + "/" + scope.questionsCount;
            $location.path(path);
        };
    }

    angular.module("elproffApp")
    .controller("StudentCourseExamDifficultyCtrl", ["$scope", "Common", "Crumbs", "$routeParams",
            "Restangular", "$location", "Course", controller]);
}).call(null);
