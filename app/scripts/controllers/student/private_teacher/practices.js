"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentPrivateTeacherPracticesCtrl
 * @description
 * # StudentPrivateTeacherPracticesCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("StudentPrivateTeacherPracticesCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
