"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentPrivateTeacherExamsCtrl
 * @description
 * # StudentPrivateTeacherExamsCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("StudentPrivateTeacherExamsCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
