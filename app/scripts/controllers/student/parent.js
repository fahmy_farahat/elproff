"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentParentCtrl
 * @description
 * # StudentParentCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, User) {
        // Start Here

    }

    angular.module("elproffApp")
    .controller("StudentParentCtrl", ["$scope", "Common", "Crumbs", "User", controller]);
}).call(null);
