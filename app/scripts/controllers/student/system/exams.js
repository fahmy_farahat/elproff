"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentSystemExamsCtrl
 * @description
 * # StudentSystemExamsCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, Lang, User) {

    }

    angular.module("elproffApp")
    .controller("StudentSystemExamsCtrl", ["$scope", "Common", "Crumbs", "Lang", "User", controller] );
}).call(null);
