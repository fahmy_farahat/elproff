"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentSystemPracticesCtrl
 * @description
 * # StudentSystemPracticesCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("StudentSystemPracticesCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
