"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentSchoolTeacherPracticesCtrl
 * @description
 * # StudentSchoolTeacherPracticesCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("StudentSchoolTeacherPracticesCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
