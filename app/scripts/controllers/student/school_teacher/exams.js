"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentSchoolTeacherExamsCtrl
 * @description
 * # StudentSchoolTeacherExamsCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("StudentSchoolTeacherExamsCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
