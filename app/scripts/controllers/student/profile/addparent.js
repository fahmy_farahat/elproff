"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentProfileAddparentCtrl
 * @description
 * # StudentProfileAddparentCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs) {
    }

    angular.module("elproffApp")
    .controller("StudentProfileAddparentCtrl", ["$scope", "Common", "Crumbs", controller]);
}).call(null);
