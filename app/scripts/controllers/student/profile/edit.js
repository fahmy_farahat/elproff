"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentProfileEditCtrl
 * @description
 * # StudentProfileEditCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, User) {
        // Start Here

    }

    angular.module("elproffApp")
    .controller("StudentProfileEditCtrl", ["$scope", "Common", "Crumbs", "User", controller]);
}).call(null);
