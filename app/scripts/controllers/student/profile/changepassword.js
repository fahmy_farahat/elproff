"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentProfileChangepasswordCtrl
 * @description
 * # StudentProfileChangepasswordCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs) {
    }

    angular.module("elproffApp")
    .controller("StudentProfileChangepasswordCtrl", ["$scope", "Common", "Crumbs", controller]);
}).call(null);
