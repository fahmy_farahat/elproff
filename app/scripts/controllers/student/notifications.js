"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentNotificationsCtrl
 * @description
 * # StudentNotificationsCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs) {

    }

    angular.module("elproffApp")
    .controller("StudentNotificationsCtrl", 
        ["$scope", "Common", "Crumbs", controller]);

}).call(null);
