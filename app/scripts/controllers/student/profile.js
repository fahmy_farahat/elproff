"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentProfileCtrl
 * @description
 * # StudentProfileCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, User) {
        scope.profile = User.getUser();
    }

    angular.module("elproffApp")
    .controller("StudentProfileCtrl", ["$scope", "Common", "Crumbs", "User", controller]);
}).call(null);
