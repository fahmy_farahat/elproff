"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentCourseCtrl
 * @description
 * # StudentCourseCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, Course, ExamInstance) {
        scope.course = {"name": "..."};
        scope.courseid = params.courseid;
        Course.one(params.courseid).get().then(function(c) {
            scope.course = c;
            scope.title = c.title;
        });
        scope.crumbs = [
            {title: 'dashboard', link: "#/student/"},
            {title: params.courseid, link: ""},
        ];
        Crumbs.setCourse(scope.crumbs[1], params.courseid, scope);

        scope.onError = function onError(error) {
            scope.isLoading = false;
        };

        scope.onExamsListSuccess = function onSuccess(result) {
            scope.recommendedExams = result;
        }

        scope.onPracticesListSuccess = function onSuccess(result) {
            scope.recommendedPractices = result;
        }

        scope.onTotalsSuccess = function onSuccess(result) {

            scope.heighestdegree = result.max_degree * 100;
        }
        scope.onPrivateTeacherListSuccess = function onPrivateTeacherListSuccess(result){
            scope.private_teacher_exams = result;
        }
        scope.onSchoolListSuccess = function onSchoolListSuccess(result){
            scope.school_teacher_exams = result;
        }



        var queries = {
            exams: {                
                exam_type: "exam",
                course_id: params.courseid,
                instance_type: "recommended",
            }, 
            practices: {
                exam_type: "practice",
                course_id: params.courseid,
                instance_type: "recommended",
            },
            private_teacher: {
                exam_type: "exam",
                course_id: params.courseid,
                instance_type: "privateassigned",
            },
            school_teahcer: {
                exam_type: "exam",
                course_id: params.courseid,
                instance_type: "schoolteacherassigned",
            },
            totals: {
                course_id: params.courseid
            }
        };

        ExamInstance.totals(queries.totals).then(scope.onTotalsSuccess, Common.onError(scope, scope.onError));
        ExamInstance.list_instances(queries.exams).then(scope.onExamsListSuccess, Common.onError(scope, scope.onError));
        ExamInstance.list_instances(queries.practices).then(scope.onPracticesListSuccess, Common.onError(scope, scope.onError));
        ExamInstance.list_instances(queries.private_teacher).then(scope.onPrivateTeacherListSuccess, Common.onError(scope, scope.onError));
        ExamInstance.list_instances(queries.school_teahcer).then(scope.onSchoolListSuccess, Common.onError(scope, scope.onError));
    }

    angular.module("elproffApp")
    .controller("StudentCourseCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "Course", "ExamInstance", controller]);
}).call(null);
