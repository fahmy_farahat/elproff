"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:StudentPracticeCtrl
 * @description
 * # StudentPracticeCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, User) {
        // Start Here

    }

    angular.module("elproffApp")
    .controller("StudentPracticeCtrl", ["$scope", "Common", "Crumbs", "User", controller]);
}).call(null);
