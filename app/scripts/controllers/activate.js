"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ActivateCtrl
 * @description
 * # ActivateCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, User) {

        scope.registered = false;
        scope.alerts = [];
        scope.model = {};


        scope.onSuccess = function onSuccess() {
            scope.registered = true;
        };

        scope.onError = function onError(error) {
            scope.alerts = Common.alerts.error.emailactivation;
        };

        User.confirmEmail({"token": params.token}).then(scope.onSuccess, scope.onError);
        scope.closeAlert = function(index) {
            scope.alerts = [];
        };

    }

    angular.module("elproffApp")
    .controller("ActivateCtrl",
            ["$scope", "Common", "Crumbs", "$routeParams", "$location", "User", controller]);
}).call(null);
