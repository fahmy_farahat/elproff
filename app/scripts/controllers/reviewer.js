"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:ReviewerCtrl
 * @description
 * # ReviewerCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, params) {

        scope.crumbs = [
            {title: "dashboard", link: ""},
        ];

    }

    angular.module("elproffApp")
    .controller("ReviewerCtrl",
        ["$scope", "Common", "$routeParams", controller]);
}).call(null);
