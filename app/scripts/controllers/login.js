"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, root, cookies, Common, Crumbs, User, $location, Facebook, Restangular, $timeout) {

        scope.alerts = [];
        scope.isLoading = false;
        scope.user = {};
        scope.data = {};
        var isConnected = false;
        var dialogOpened = false;

        scope.onSuccess = function onSucces(result) { 
            scope.isLoading = false;
            User.setUser(result);
            root.hookRouteChange();
            // TODO: CHANGE THAT TO BE FIXED TYPES MATCHING ROUTING 
            if (result.type === "profile") {
                console.error("Please, call administrator, because a new table has been added, and initial_data_.json should be updated");
            }
            rememberMe();
            $location.path("/" + result.type);
        };

        scope.onError = function onError(error) { 
            scope.isLoading = false;
            var msg = "Incorrect username and/or password."; 
            if (error.data.reason === "disabled") {
                msg = root.lang.loginactivationneeded;
            }
            scope.alerts = [{
                msg: msg,
                type: "danger"
            }];
        };

        scope.login = function login() {
            scope.isLoading = true;
            scope.data = _.cloneDeep(this.model);
            scope.data.email = scope.data.email.toLowerCase();
            User.login(scope.data).then(this.onSuccess, Common.onError(scope, this.onError));
        };

        scope.closeAlert = function(index) {
            scope.alerts = [];
        };

        var rememberMe = function rememberMe() {
            var currentDate = new Date();
            var interval = 1;
            if (!_.isEmpty(scope.data) && scope.data.rememberme) {
                cookies.email = scope.data.email;
                cookies.password = scope.data.password;
            }
        };

        scope.fbLogin = function() {
            Facebook.login(function(response) {
                console.log("start fbLogin");
                if (response.status === "connected") {
                    scope.me();
                    console.log("FB Connected");
                }
            }, {"scope": "email"});
        };

        // scope.getLoginStatus = function() {
        //     Facebook.getLoginStatus(function(response) {
        //         if (response.status === "connected") {
        //             isConnected = true;
        //             scope.me();
        //         } else {
        //             scope.fbLogin();
        //         }
        //     });
        // };

        scope.me = function() {
            Facebook.api("/me", function(response) {
                console.log("meee");
                scope.$apply(function() {
                    var users = Restangular.all("user/fblogin");
                    users.post({"full_name": response.name,
                                 "email": response.email,
                                 "id": response.id}).then(
                               scope.onSuccessFB, scope.onErrorFB);
                });
            });
        };

        scope.onSuccessFB = function(response) {
            if (response.status === "set password") {
                $location.path("/fbregister");
            } else if (response.status === "logged in") {
                scope.isLoading = false;
                scope.user = response.user;
                User.setUser(scope.user);
                $location.path(response.url);
            } else {
                $location.path("/login");
            }
        };

        scope.onErrorFB = function(response) {
            // This should display an error message.
            console.log("error" + response);
        };

        function userLoggedIn() { 
            if (scope.$root.USER) {
                var lastPath = localStorage.getItem("last_path");
                if (lastPath && lastPath !== "/login") {
                    $location.path(lastPath);
                }
            }
        }
        scope.$on("user_updated", userLoggedIn);
        $timeout(userLoggedIn, 1000);
    }

    angular.module("elproffApp")
    .controller("LoginCtrl",
            ["$scope", "$rootScope", "$cookies", "Common", "Crumbs", "User", "$location",
            "Facebook", "Restangular", "$timeout", controller]);
}).call(null);
