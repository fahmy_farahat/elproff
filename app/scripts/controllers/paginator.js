"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:PaginatorCtrl
 * @description
 * # PaginatorCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, params, $location) {
        //Common.pagination = null;
        function init() {
            scope.pagination = _.cloneDeep(Common.pagination);
            if (scope.pagination) {
                scope.pagination.limit = 20;
                if (_.isUndefined(scope.pagination.page) || _.isNaN(scope.pagination.page)) {
                    scope.pagination.offset = scope.pagination.offset || 0 ;
                    scope.pagination.page = 1 + (scope.pagination.offset / scope.pagination.limit);
                    Common.pagination.page = scope.pagination.page;
                }
            }
        }
        scope.$on("list_response", init);
        var paginationWatcher = scope.$watch("pagination", function() {
            if (!scope.pagination) {
                return;
            }
            paginationWatcher();
            scope.$watch("pagination.page", function() {
                if (!scope.pagination) {
                    return;
                }
                if (scope.pagination.page === Common.pagination.page) {
                    return;
                }
                scope.pagination.offset = (scope.pagination.page - 1) * scope.pagination.limit;
                Common.query.offset = scope.pagination.offset;
                Common.query.page = scope.pagination.page;
                scope.$root.$broadcast("relist");
            });
        });
        init();
    }

    angular.module("elproffApp")
    .controller("PaginatorCtrl",
        ["$scope", "Common", "$routeParams", "$location", controller]);
}).call(null);
