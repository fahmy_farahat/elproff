"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminSmeCtrl
 * @description
 * # AdminSmeCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, SME, CommonCache) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'smes', link: ""},
        ];

        function load() {
            SME.getList().then(function(smes) {
                scope.entities = _.map(smes, CommonCache.staffMember);
            });
        }
        load();
        scope.$on("relist", load);
        scope.searchable = true;
    }

    angular.module("elproffApp")
    .controller("AdminSmeCtrl",
        ["$scope", "Common", "Crumbs", "SME", "CommonCache", controller]);
}).call(null);
