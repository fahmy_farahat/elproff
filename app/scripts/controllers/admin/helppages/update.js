"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminHelppagesUpdateCtrl
 * @description
 * # AdminHelppagesUpdateCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, $location, params, Help) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'Help Pages', link: "#/admin/help_pages/"},
            {title: 'update', link: ""},
        ];
        scope.model = {};
        scope.model.viewers = [];
        scope.isLoading = false;
        scope.onSuccess = function onSuccess(){
            scope.isLoading = false;
            $location.path(scope.crumbs[1].link.substring(1));
        };
        scope.save = function save(){
            scope.isLoading = true;
            var data = new FormData();
            var model = _.cloneDeep(scope.model);
            data.append('name', model.name || "");
            data.append('name_ar', model.name_ar || "");
            data.append('name_en', model.name_en || "");
            data.append('description', model.description || "");
            data.append('description_ar', model.description_ar || "");
            data.append('description_en', model.description_en || "");
            data.append("viewers", model.viewers);
            Common.customRestangular("help",data).then(scope.onSuccess,Common.onError)

        };
        Help.one(params.pageid).get().then(function(obj) {
            scope.model = obj;
        });
    }
    angular.module("elproffApp")
    .controller("AdminHelppagesUpdateCtrl",
            ["$scope", "Common", "Crumbs", "$location", "$routeParams", "Help", controller]);
}).call(null);
