"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminHelppagesSmeViewCtrl
 * @description
 * # AdminHelppagesSmeViewCtrl
 * Controller of the elproffApp
 */

(function(){

    function controller(scope, params, Common, Crumbs, Help) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'help', link: "#/admin/help_pages/"},
            {title: 'SMEs', link: ""},
        ];

        //scope.countryid = params.countryid;
        //Crumbs.setCountry(scope.crumbs[2], params.countryid, scope);

        function load() {
            Help.list_help({entity:"SME"}).then(function(result){
                scope.pages = result.objects;     
            }, Common.onError);
        }

        load();
        scope.searchable = true;
        scope.isLoading = false;

        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            load();
        };

        scope.remove = function remove(entity) {
            scope.isLoading = true;
            Help.one(""+entity.id).remove().then(this.onSuccess, Common.onError(scope));
        };

    }

    angular.module("elproffApp")
    .controller("AdminHelppagesSmeViewCtrl",
            ["$scope", "$routeParams", "Common", "Crumbs", "Help",
            controller]);
}).call(null);
