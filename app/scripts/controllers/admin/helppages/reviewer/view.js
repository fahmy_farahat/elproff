"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminHelppagesReviewerViewCtrl
 * @description
 * # AdminHelppagesReviewerViewCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, params, Common, Crumbs, Help) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'help', link: "#/admin/help_pages/"},
            {title: 'reviewers', link: ""},
        ];

        function load() {
            Help.list_help({entity:"Reviewer"}).then(function(result){
                scope.pages = result.objects;     
            }, Common.onError);
        }

        load();
        scope.searchable = true;
        scope.isLoading = false;

        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            load();
        };

        scope.remove = function remove(entity) {
            scope.isLoading = true;
            Help.one(""+entity.id).remove().then(this.onSuccess, Common.onError(scope));
        };

    }

    angular.module("elproffApp")
    .controller("AdminHelppagesReviewerViewCtrl",
            ["$scope", "$routeParams", "Common", "Crumbs","Help",
            controller]);
}).call(null);

