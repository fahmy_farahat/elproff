"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminNotificationsCtrl
 * @description
 * # AdminNotificationsCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, root, $location,  Service) {

    }

    angular.module("elproffApp")
    .controller("AdminNotificationsCtrl",
            ["$scope", "Common", "Crumbs", "$routeParams", "$rootScope", "$location",
            "Notifications", controller] );
}).call(null);
