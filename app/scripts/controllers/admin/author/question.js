"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminAuthorQuestionCtrl
 * @description
 * # AdminAuthorQuestionCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, params, Common, Crumbs, Question, Author, CommonCache) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'author', link: "#/admin/author"},
            {title: params.authorid, link: "#/admin/author/" + params.authorid},
            {title: 'questions', link: ""},
        ];
        scope.authorid = params.authorid;

        function loadQuestions(query) {
            Question.getList(query).then(function(questions) {
                scope.questions = _.map(questions, CommonCache.question);
            });
        }

        function load() {
            if (!scope.author) {
                Author.one(scope.authorid).get().then(function(author) {
                    scope.author = author;
                    scope.crumbs[2].title = author.user.full_name;
                    var query = {"created_by": scope.author.user.id};
                    loadQuestions(query);
                });
            } else {
                var query = {"created_by": scope.author.user.id};
                loadQuestions(query);
            }
        }
        load();
        scope.$on("relist", load);
    }

    angular.module("elproffApp")
    .controller("AdminAuthorQuestionCtrl",
        ["$scope", "$routeParams", "Common", "Crumbs", "Question", "Author",
        "CommonCache", controller]);
}).call(null);
