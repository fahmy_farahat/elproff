"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminAuthorQuestionViewCtrl
 * @description
 * # AdminAuthorQuestionViewCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, params, Common, Crumbs, Question, Author) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'author', link: "#/admin/author"},
            {title: params.authorid, link: "#/admin/author/" + params.authorid},
            {title: 'questions', link: "#/admin/author/" + params.authorid + "/question"},
            {title: params.questionid, link: ""},
        ];
        scope.authorid = params.authorid;
        scope.questionid = params.questionid;
        Crumbs.setAuthor(scope.crumbs[2], scope.authorid, scope);

        function load() {
            Question.one(params.questionid).get().then(function(model) {
                scope.model = model;
                scope.model.user = {
                    answer: scope.model.correct_answer.answer
                };
            });
        }
        load();
        scope.$on("relist", load);
    }

    angular.module("elproffApp")
    .controller("AdminAuthorQuestionViewCtrl",
        ["$scope", "$routeParams", "Common", "Crumbs", "Question", "Author", controller]);
}).call(null);
