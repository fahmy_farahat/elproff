"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminAuthorTaskCtrl
 * @description
 * # AdminAuthorTaskCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("AdminAuthorTaskCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
