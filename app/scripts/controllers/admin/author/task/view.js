"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminAuthorTaskViewCtrl
 * @description
 * # AdminAuthorTaskViewCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("AdminAuthorTaskViewCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
