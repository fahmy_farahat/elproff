"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminAuthorCreateCtrl
 * @description
 * # AdminAuthorCreateCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'author', link: "#/admin/author"},
            {title: 'invite', link: ""},
        ];

    }

    angular.module("elproffApp")
    .controller("AdminAuthorCreateCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "Invitation", controller]);
}).call(null);
