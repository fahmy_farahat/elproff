"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminReviewerCtrl
 * @description
 * # AdminReviewerCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, Reviewer, CommonCache) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'reviewers', link: ""},
        ];

        function load() {
            Reviewer.getList().then(function(reviewers) {
                scope.entities = _.map(reviewers, CommonCache.staffMember);
            });
        }
        load();
        scope.$on("relist", load);
        scope.searchable = true;

    }

    angular.module("elproffApp")
    .controller("AdminReviewerCtrl",
        ["$scope", "Common", "Crumbs", "Reviewer", "CommonCache", controller]);
}).call(null);
