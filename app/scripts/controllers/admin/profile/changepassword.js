"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminProfileChangepasswordCtrl
 * @description
 * # AdminProfileChangepasswordCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("AdminProfileChangepasswordCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
