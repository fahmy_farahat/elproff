"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminProfileEditCtrl
 * @description
 * # AdminProfileEditCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, User) {
        scope.model = User.getUser();
    }

    angular.module("elproffApp")
    .controller("AdminProfileEditCtrl", ["$scope", "User", controller]);
}).call(null);
