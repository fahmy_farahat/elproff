"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeCourseElementTypeCtrl
 * @description
 * # SmeCourseElementTypeCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, root, $location, modal, CourseElementType) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'courseelementtypes', link: ""}
        ];
        function load() {
            scope.entities = CourseElementType.getList().$object;
        }
        load();
        scope.$on("relist", load);
        scope.searchable = true;

        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            load();
        };
        scope.deleteType = function deleteType(elementtypeid){
        };
        scope.isLoading = false;
        scope.openElementType = function openElementType(data) {
            var modalInstance = modal.open({
                templateUrl: "views/modals/element-type.html",
                controller: "ModalsElementtypeCtrl",
                resolve: {
                    elementtype: function () {
                        return data;
                    }
                }
            });
        };
    }

    angular.module("elproffApp")
        .controller("AdminCourseElementTypeCtrl",
                ["$scope", "Common", "Crumbs", "$routeParams", "$rootScope",
                "$location", "$modal", "CourseElementType", controller] );
}).call(null);
