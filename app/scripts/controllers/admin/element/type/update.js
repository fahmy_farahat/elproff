"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeCourseElementTypeUpdateCtrl
 * @description
 * # SmeCourseElementTypeUpdateCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, $location, CourseElement) {

        scope.model = {};
        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'courseelementtype', link: "#/admin/element_type/"},
            {title: params.id, link: "#/admin/element_type/"},
            {title: 'update', link: ""},
        ];
        Crumbs.setElementType(scope.crumbs[2], params.id, scope);
        CourseElement.one(params.id).get().then(function(obj) {
            scope.model = obj;
        });

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $location.path(scope.crumbs[2].link.substring(1));
        };

        scope.save = function save() {
            scope.isLoading = true;
            scope.model.save().then(this.onSuccess, Common.onError(scope));
        };

    }

    angular.module("elproffApp")
    .controller("AdminCourseElementTypeUpdateCtrl",
            ["$scope", "Common", "Crumbs", "$routeParams", "$location",
            "CourseElementType", controller] );
}).call(null);
