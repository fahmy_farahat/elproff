"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeCourseElementTypeCreateCtrl
 * @description
 * # SmeCourseElementTypeCreateCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, $location, CourseElement) {

        scope.model = {};
        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'courseelementtype', link: "#/admin/element_type/"},
            {title: 'create', link: ""},
        ];

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $location.path(scope.crumbs[1].link.substring(1));
        };

        scope.save = function save() {
            scope.isLoading = true;
            var model = _.cloneDeep(scope.model);
            CourseElement.post(model).then(this.onSuccess, Common.onError(scope));
        };

    }

    angular.module("elproffApp")
    .controller("AdminCourseElementTypeCreateCtrl",
            ["$scope", "Common", "Crumbs", "$routeParams", "$location", "CourseElementType", controller] );
}).call(null);
