"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminInvitationsCtrl
 * @description
 * # AdminInvitationsCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, params, $location, Common, Service) {

    }

    angular.module("elproffApp")
    .controller("AdminInvitationsCtrl",
        ["$scope", "$routeParams", "$location", "Common", "User", controller]);
}).call(null);
