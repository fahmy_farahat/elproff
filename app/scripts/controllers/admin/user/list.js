"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminUserListCtrl
 * @description
 * # AdminUserListCtrl
 * Controller of the elproffApp
 */
(function(){

  function controller(scope, $root, Common, Crumbs, Restangular, modal) {

        scope.alerts = [];
        scope.searchable = true;
        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'users', link: ""}
        ];
        scope.model = {};
        scope.model.types = ["allusers", "admin", "author", "reviewer", "parent",
            "privateteacher", "schooladmin", "schoolteacher", "student", "sme",];
        scope.model.users_type = "allusers";
        scope.loadUsers = function() {
            scope.isLoading = true;
            if (scope.model.users_type && scope.model.users_type !== "allusers"
            && _.isString(scope.model.users_type)) {
                Restangular.all("user/users_profiles")
                .getList({profile: scope.model.users_type})
                .then(function(result) {
                    scope.users = result;
                    scope.isLoading = false;
                });
            } else {
                Restangular.all("user").getList().then(function(result) {
                    scope.users = result;
                    scope.isLoading = false;
                });
            }
        };
        scope.loadUsers();
        scope.$on("relist", scope.loadUsers);
        scope.$watch("model.users_type", scope.loadUsers);
        /**
         * User Activate
         * @param {Object} user
         */
        scope.activateSuccess = function activateSuccess(){
            scope.isLoading = false;
            scope.alerts = Common.alerts.success.activate;
            scope.$root.$broadcast("relist");
        };
        scope.activate = function activate(user) {
            user.is_active = true;
            user.put().then(this.activateSuccess, Common.onError(scope));
        };

        /**
        * User Deactivate
        * @param {Object} user
        */
        scope.deactivateSuccess = function deactivateSuccess(){
            scope.alerts = Common.alerts.success.deactivate;
            scope.$root.$broadcast("relist");
        };
        scope.deactivate = function deactivate(user) {
            user.is_active = false;
            user.put().then(this.deactivateSuccess, Common.onError(scope));
        };
        scope.openProfile = function openProfile(data) {
            var modalInstance = modal.open({
                templateUrl: "views/modals/user-profile.html",
                controller: "ModalsUserprofileCtrl",
                resolve: {
                    userlist: function(){
                      return data;
                    }
                }
            });
        };
        /**
        * Users Export
        * @param {array} users
        */
        scope.exportusers = function exportreport(){
            scope.exporting = true;
            var headerTitles = [
                $root.lang.username,
                $root.lang.email,
                $root.lang.usertype,
            ];
            if (scope.model.users_type && scope.model.users_type !== "allusers"
            && _.isString(scope.model.users_type)) {
                Restangular.all("user/users_profiles")
                .getList({profile: scope.model.users_type, limit: 0})
                .then(function(reportData) {
                    _.forEach(reportData, function(user, index) {
                        reportData[index] = [user.full_name, user.email, user.user_type];
                    });
                    console.log(reportData);
                    Common.exportCSV(headerTitles, reportData, "Users List");
                    scope.exporting = false;
                });
            } else {
                Restangular.all("user").getList({limit: 0}).then(function(reportData) {
                    _.forEach(reportData, function(user, index) {
                        reportData[index] = [user.full_name, user.email, user.user_type]
                    });
                    console.log(reportData);
                    Common.exportCSV(headerTitles, reportData, "Users List");
                    scope.exporting = false;
                });
            }
        };

    }
  angular.module("elproffApp")
        .controller("AdminUserListCtrl",
            ["$scope", "$rootScope", "Common", "Crumbs", "Restangular", "$modal",
              controller]);
}).call(null);
