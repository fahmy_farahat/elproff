"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminCountryCtrl
 * @description
 * # AdminCountryCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, Country) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'countries', link: ""},
        ];

        function load () {
            scope.entities = Country.getList().$object;
        }
        load();
        scope.$on("relist", load);
        scope.searchable = true;

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            load();
        };

        scope.remove = function remove(entity) {
            scope.isLoading = true;
            entity.remove().then(this.onSuccess, Common.onError(scope));
        };

        scope.deactivate = function deactivate(entity) {
            if (!entity) {
                return;
            }
            var query = {
                "country": entity.id
            };
            scope.isLoading = true;
            Country.disable(query).then(scope.onSuccess, Common.onError(scope));
        };
        scope.activate = function activate(entity) {
            if (!entity) {
                return;
            }
            var query = {
                "country": entity.id
            };
            scope.isLoading = true;
            Country.enable(query).then(scope.onSuccess, Common.onError(scope));
        };
    }

    angular.module("elproffApp")
    .controller("AdminCountryCtrl",
            ["$scope", "Common", "Crumbs", "Country", controller]);
}).call(null);
