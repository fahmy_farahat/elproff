"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminHelpCtrl
 * @description
 * # AdminHelpCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs) {

        $(".faq-navigation a").click(function(){
            var faqId = $(this).attr("data-faq");
            if(!$(this).hasClass("active")) {
                $(".faq-content .faq").removeClass("active").fadeOut();
                $("#"+faqId).fadeIn(1500).addClass("active");
                $(".faq-navigation a").removeClass("active");
                $(this).addClass("active");
            }
            return false;
        });

    }

    angular.module("elproffApp")
    .controller("AdminHelpCtrl", ["$scope", "Common", "Crumbs", controller]);
}).call(null);
