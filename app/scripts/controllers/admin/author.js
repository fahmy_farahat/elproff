"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminAuthorCtrl
 * @description
 * # AdminAuthorCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, Author, root, CommonCache) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'author', link: ""},
        ];

        function load() {
            Author.getList({order_by: "user__email"}).then(function(authors) {
                scope.entities = _.map(authors, CommonCache.author);
            });
        }
        Author.authorsQuestions().then(function(res) {
            scope.questions = res;
        });
        scope.isLoading = false;
        load();
        scope.$on("relist", load);
        scope.searchable = true;
        scope.exportreport = function exportreport(){
            scope.isLoading = true;
            var headerTitles = [
                root.lang.authorname,
                root.lang.authoremail,
                root.lang.questionscount,
            ];
            Author.authorsQuestions({"limit":0}).then(function(reportData) {
                reportData.forEach(function(item){
                    item.splice(item.length-1, 1);
                });
                Common.exportCSV(headerTitles, reportData, "Author Report");
                scope.isLoading = false;
            });
        };
    }

    angular.module("elproffApp")
    .controller("AdminAuthorCtrl",
        ["$scope", "Common", "Crumbs", "Author", "$rootScope", "CommonCache",
        controller] );
}).call(null);
