"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminProfileCtrl
 * @description
 * # AdminProfileCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, User) {

    }

    angular.module("elproffApp")
    .controller("AdminProfileCtrl", ["$scope", "Common", "Crumbs", "User", controller] );
}).call(null);
