"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminEducationalsystemCtrl
 * @description
 * # AdminEducationalsystemCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, User, System, CommonCache) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'educationalsystems', link: ""},
        ];

        function load() {
            System.getList().then(function(systems) {
                scope.entities = _.map(systems, CommonCache.system);
            });
        }
        load();
        scope.$on("relist", load);
        scope.searchable = true;

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            load();
        };

        scope.remove = function remove(entity) {
            scope.isLoading = true;
            entity.remove().then(this.onSuccess, Common.onError(scope));
        };

    }

    angular.module("elproffApp")
    .controller("AdminEducationalsystemCtrl",
            ["$scope", "Common", "Crumbs", "User", "EducationalSystem",
            "CommonCache", controller]);
}).call(null);
