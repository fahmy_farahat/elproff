"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminCountryCreateCtrl
 * @description
 * # AdminCountryCreateCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, Country, $location) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'country', link: "#/admin/country/"},
            {title: 'create', link: ""},
        ];
        scope.model = {};

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $location.path(scope.crumbs[1].link.substring(1));
        };

        scope.save = function save() {
            scope.isLoading = true;
            var model = _.cloneDeep(scope.model);
            model = Common.copyTranslation(model);
            Country.post(model).then(this.onSuccess, Common.onError(scope));
        };

    }

    angular.module("elproffApp")
    .controller("AdminCountryCreateCtrl",
            ["$scope", "Common", "Crumbs", "$routeParams", "Country",
            "$location", controller] );
}).call(null);
