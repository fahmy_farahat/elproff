"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminCountryViewCtrl
 * @description
 * # AdminCountryViewCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, params, Common, Crumbs, EducationalSystem, CommonCache) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'country', link: "#/admin/country/"},
            {title: params.countryid, link: ""},
        ];

        scope.countryid = params.countryid;
        Crumbs.setCountry(scope.crumbs[2], params.countryid, scope);

        function load() {
            EducationalSystem.getList({country: params.countryid}).then(function(systems) { 
                scope.entities = _.map(systems, CommonCache.system);
            });
        }

        load();
        scope.$on("relist", load);
        scope.searchable = true;
        scope.isLoading = false;

        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            load();
        };

        scope.remove = function remove(entity) {
            scope.isLoading = true;
            entity.remove().then(this.onSuccess, Common.onError(scope));
        };

    }

    angular.module("elproffApp")
    .controller("AdminCountryViewCtrl",
            ["$scope", "$routeParams", "Common", "Crumbs", "EducationalSystem",
            "CommonCache", controller]);
}).call(null);
