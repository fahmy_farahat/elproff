"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminCountryUpdateCtrl
 * @description
 * # AdminCountryUpdateCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, $location, Country) {
        
        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'country', link: "#/admin/country/"},
            {title: params.id, link: "#/admin/country/" + params.id},
            {title: 'update', link: ""},
        ];
        Crumbs.setCountry(scope.crumbs[2], params.id, scope);
        Country.one(params.id).get().then(function(obj) {
            scope.model = obj;
        });

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $location.path(scope.crumbs[2].link.substring(1))
        };

        scope.save = function save() {
            scope.isLoading = true;
            scope.model = Common.copyTranslation(scope.model);
            scope.model.save().then(this.onSuccess, Common.onError(scope));
        };

    }

    angular.module("elproffApp")
    .controller("AdminCountryUpdateCtrl",
            ["$scope", "Common", "Crumbs", "$routeParams", "$location", "Country", controller] );
}).call(null);
