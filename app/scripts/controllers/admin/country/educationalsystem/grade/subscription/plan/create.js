"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminSubscriptionPlanCreateCtrl
 * @description
 * # AdminSubscriptionPlanCreateCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, $location, Service, Course, CommonCache) {

        scope.model = {
            courses: [],
            elements: [],
            grade: {id: params.gradeid}
        };
        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'country', link: "#/admin/country/" + params.countryid},
            {title: params.countryid, link: "#/admin/country/" + params.countryid},
            {title: 'educationalsystem', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: params.systemid, link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: 'grade', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.gradeid},
            {title: 'subscription', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.gradeid + "/subscription"},
            {title: 'plan', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.gradeid + "/subscription/plan"},
            {title: "create", link: ""},
        ];

        scope.countryid = params.countryid;
        scope.systemid = params.systemid;
        scope.gradeid = params.gradeid;
        Crumbs.setCountry(scope.crumbs[2], params.countryid, scope);
        Crumbs.setSystem(scope.crumbs[4], params.systemid, scope);
        Crumbs.setGrade(scope.crumbs[5], params.gradeid, scope);

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $location.path(scope.crumbs[7].link.substring(1));
        };
        function load() {
            Course.getList({grade: params.gradeid}).then(function(courses) {
                scope.model.courses = _.map(courses, CommonCache.course);
            });
        }
        load();

        scope.save = function save() {
            scope.isLoading = true;
            var model = _.cloneDeep(scope.model);
            model = Common.copyTranslation(model);
            model.courses = _.pluck(scope.model.courses, "resource_uri");
            model.courses_elements = _.pluck(scope.model.elements, "resource_uri");
            Service.post(model).then(this.onSuccess, Common.onError(scope));
        };

    }

    angular.module("elproffApp")
    .controller("AdminSubscriptionPlanCreateCtrl",
            ["$scope", "Common", "Crumbs", "$routeParams", "$location",
            "SubscriptionPlan", "Course", "CommonCache", controller] );
}).call(null);
