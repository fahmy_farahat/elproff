"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminSubscriptionTermUpdateCtrl
 * @description
 * # AdminSubscriptionTermUpdateCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, root, $location, Service) {

        scope.model = {};
        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'country', link: "#/admin/country/" + params.countryid},
            {title: params.countryid, link: "#/admin/country/" + params.countryid},
            {title: 'educationalsystem', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: params.systemid, link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: 'grade', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.gradeid},
            {title: 'subscription', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.gradeid + "/subscription"},
            {title: 'term', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.gradeid + "/subscription/term"},
            {title: "update", link: ""}
        ];

        scope.countryid = params.countryid;
        scope.systemid = params.systemid;
        scope.gradeid = params.gradeid;
        Crumbs.setCountry(scope.crumbs[2], params.countryid, scope);
        Crumbs.setSystem(scope.crumbs[4], params.systemid, scope);
        Crumbs.setGrade(scope.crumbs[5], params.gradeid, scope);

        Service.one(params.id).get().then(function(obj) {
            scope.model = obj;
        });

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $location.path(scope.crumbs[7].link.substring(1));
        };

        scope.save = function save() {
            scope.isLoading = true;
            var model = _.cloneDeep(scope.model);
            model = Common.copyTranslation(model);
            Service.post(model).then(this.onSuccess, Common.onError(scope));
        };

        scope.openStartDate = function openEndDate($event) {
            $event.preventDefault();
            $event.stopPropagation();
            scope.startDateOpened = true;
        };

        scope.openEndDate = function openEndDate($event) {
            $event.preventDefault();
            $event.stopPropagation();
            scope.endDateOpened = true;
        };

        scope.dateOptions = {};

    }

    angular.module("elproffApp")
    .controller("AdminSubscriptionTermUpdateCtrl",
            ["$scope", "Common", "Crumbs", "$routeParams", "$rootScope",
            "$location", "SubscriptionTerm", controller] );
}).call(null);
