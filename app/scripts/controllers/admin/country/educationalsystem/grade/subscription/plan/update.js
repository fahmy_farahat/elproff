"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminSubscriptionPlanUpdateCtrl
 * @description
 * # AdminSubscriptionPlanUpdateCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, $location, Service, Course, CourseElement) {

        scope.model = {
            courses: [],
            elements: [],
            grade: {id: params.gradeid}
        };
        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'country', link: "#/admin/country/" + params.countryid},
            {title: params.countryid, link: "#/admin/country/" + params.countryid},
            {title: 'educationalsystem', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: params.systemid, link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: 'grade', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.gradeid},
            {title: 'subscription', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.gradeid + "/subscription"},
            {title: 'plan', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.gradeid + "/subscription/plan"},
            {title: "update", link: ""},
        ];

        scope.countryid = params.countryid;
        scope.systemid = params.systemid;
        scope.gradeid = params.gradeid;
        Crumbs.setCountry(scope.crumbs[2], params.countryid, scope);
        Crumbs.setSystem(scope.crumbs[4], params.systemid, scope);
        Crumbs.setGrade(scope.crumbs[5], params.gradeid, scope);
        scope.isLoading = false;
        // Getting subscription plan
        scope.onGetPlanSuccess = function onSuccess(result) {
            scope.model = result;
            //scope.model.courses_elements = result.courses_elements;
            scope.model.courses = _.map(result.courses, function(e) {
                return Course.getUri(e).$object;
            });
            scope.model.elements = _.map(result.courses_elements, function(e) {
                return CourseElement.getUri(e).$object;
            });
            scope.isLoading = false;
        };

        Service.one(params.planid).get().then(scope.onGetPlanSuccess, Common.onError(scope));

        // Saving subscription update
        scope.onSaveSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $location.path(scope.crumbs[7].link.substring(1));
        };

        scope.save = function save() {
            scope.isLoading = true;
            scope.model = Common.copyTranslation(scope.model);
            scope.model.courses = _.pluck(scope.model.courses, "resource_uri");
            scope.model.courses_elements = _.pluck(scope.model.elements, "resource_uri");
            scope.model.save().then(this.onSaveSuccess, Common.onError(scope));
        };

    }

    angular.module("elproffApp")
    .controller("AdminSubscriptionPlanUpdateCtrl",
            ["$scope", "Common", "Crumbs", "$routeParams", "$location",
            "SubscriptionPlan", "Course", "CourseElement", controller] );
}).call(null);
