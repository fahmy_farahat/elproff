"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminSubscriptionCouponsCreateCtrl
 * @description
 * # AdminSubscriptionCouponsCreateCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, params, Common, Crumbs, Student, Plan, $location, Subscription, Coupon) {

        scope.model = {};
        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'country', link: "#/admin/country/" + params.countryid},
            {title: params.countryid, link: "#/admin/country/" + params.countryid},
            {title: 'educationalsystem', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: params.systemid, link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: 'grade', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.gradeid},
            {title: 'subscription', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.gradeid + "/subscription"},
            {title: 'coupon', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.gradeid + "/subscription/coupons"},
            {title: "create", link: ""},
        ];

        scope.countryid = params.countryid;
        scope.systemid = params.systemid;
        scope.gradeid = params.gradeid;
        Crumbs.setCountry(scope.crumbs[2], params.countryid, scope);
        Crumbs.setSystem(scope.crumbs[4], params.systemid, scope);
        Crumbs.setGrade(scope.crumbs[5], params.gradeid, scope);

        scope.plans = Plan.getList().$object;

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $location.path(scope.crumbs[7].link.substring(1));
        };

        scope.save = function () {
            var planID = scope.model.plan;
            var model = scope.model;
            model = Common.copyTranslation(model);
            if (!_.isUndefined(planID) && (!_.isUndefined(model.name_ar) && !_.isUndefined(model.name_en)) ) {
                var data = {
                    "title": scope.model.name,
                    "plan_id": planID.id,
                    "no_coupons": scope.model.coupons_count
                };
                Coupon.createBulkCoupons(data)
                    .then(this.onSuccess, Common.onError(scope));
            } else {
                scope.alerts = Common.alerts.error.general_data_error;
            }
        };

    }

    angular.module("elproffApp")
    .controller("AdminSubscriptionCouponsCreateCtrl",
            ["$scope", "$routeParams", "Common", "Crumbs", "Student",
                "SubscriptionPlan", "$location", "Subscription",
                "Coupon", controller]);
}).call(null);
