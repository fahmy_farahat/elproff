"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminSubscriptionCouponsCtrl
 * @description
 * # AdminSubscriptionCouponsCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, params, Common, Crumbs, $location, $window, Coupon, CouponBulk) {

        scope.model = {};
        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'country', link: "#/admin/country/" + params.countryid},
            {title: params.countryid, link: "#/admin/country/" + params.countryid},
            {title: 'educationalsystem', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: params.systemid, link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: 'grade', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.gradeid},
            {title: 'subscription', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.gradeid +"/subscription"},
            {title: "Coupons", link: ""},
        ];

        scope.countryid = params.countryid;
        scope.systemid = params.systemid;
        scope.gradeid = params.gradeid;
        Crumbs.setCountry(scope.crumbs[2], params.countryid, scope);
        Crumbs.setSystem(scope.crumbs[4], params.systemid, scope);
        Crumbs.setGrade(scope.crumbs[5], params.gradeid, scope);

        function load() {

            scope.bulks = CouponBulk.getList().$object;
            //console.log(scope.plans)
        }
        load();
        scope.isLoading = false;
        scope.searchable = true;
        scope.onSuccessDownload = function onSuccess(result) {
            scope.isLoading = false;
            $window.open(result.fileurl,"_blank");
        };

        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            load();
        };

        scope.remove = function remove(entity) {
            scope.isLoading = true;
            console.log(entity);
            entity.remove().then(this.onSuccess, Common.onError(scope));
        };

        scope.download = function(bulkid){
            scope.isLoading = true;
            var data = {
                "couponbulk_id": bulkid
            }
            Coupon.downloadCoupons(data).then(this.onSuccessDownload,Common.onError(scope));
        }


    }

    angular.module("elproffApp")
    .controller("AdminSubscriptionCouponsCtrl",
            ["$scope", "$routeParams", "Common", "Crumbs", "$location","$window","Coupon","CouponBulk", controller] );
}).call(null);
