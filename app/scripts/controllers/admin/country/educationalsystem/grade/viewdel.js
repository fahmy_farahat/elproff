"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminCountryEducationalsystemGradeViewdelCtrl
 * @description
 * # AdminCountryEducationalsystemGradeViewdelCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, params, Common, Crumbs, Course) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'country', link: "#/admin/country/"},
            {title: params.countryid, link: "#/admin/country/" + params.countryid},
            {title: 'educationalsystem', link: "#/admin/country/" + params.countryid},
            {title: params.systemid, link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: 'grade', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.gradeid},
            {title: 'Deleted Courses', link: ""}
        ];


        scope.countryid = params.countryid;
        scope.systemid = params.systemid;
        scope.gradeid = params.gradeid;
        Crumbs.setCountry(scope.crumbs[2], params.countryid, scope);
        Crumbs.setSystem(scope.crumbs[4], params.systemid, scope);
        Crumbs.setGrade(scope.crumbs[5], params.gradeid, scope);
        function load() {
            Course.deleted_list({grade: params.gradeid}).then(function(result){
            	scope.entities = result.objects;
            });
        }


        load();
        scope.$on("relist", load);
        scope.searchable = true;
        scope.isLoading = false;

        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            load();
        };

        scope.undoDelete = function undoDelete(course){
            if(!course){
                return;
            }
            Course.undodelete({"course":course.id}).then(scope.onSuccess, Common.onError(scope));
        };



    }

    angular.module("elproffApp")
    .controller("AdminCountryEducationalsystemGradeViewdelCtrl",
            ["$scope", "$routeParams", "Common", "Crumbs", "Course",
            controller] );
}).call(null);