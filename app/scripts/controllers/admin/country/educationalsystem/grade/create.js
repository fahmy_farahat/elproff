"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminGradeCreateCtrl
 * @description
 * # AdminGradeCreateCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, $location, Grade, EducationalSystem) {

        scope.model = {};

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'country', link: "#/admin/country/"},
            {title: params.countryid, link: "#/admin/country/" + params.countryid},
            {title: params.systemid, link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: 'creategrade', link: ""}
        ];

        scope.isLoading = false;
        scope.countryid = params.countryid;
        scope.systemid = params.systemid;
        scope.gradeid = params.gradeid;
        Crumbs.setCountry(scope.crumbs[2], params.countryid, scope);
        Crumbs.setSystem(scope.crumbs[3], params.systemid, scope);

        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $location.path(scope.crumbs[3].link.substring(1));
        };

        scope.save = function save() {
            scope.isLoading = true;
            var model = _.cloneDeep(scope.model);
            model = Common.copyTranslation(model);
            Grade.post(model).then(this.onSuccess, Common.onError(scope));
        };
    }

    angular.module("elproffApp")
    .controller("AdminGradeCreateCtrl",
            ["$scope", "Common", "Crumbs", "$routeParams", "$location", "Grade",
            "EducationalSystem", controller] );
}).call(null);
