"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminCountryEducationalsystemGradeViewCtrl
 * @description
 * # AdminCountryEducationalsystemGradeViewCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, params, Common, Crumbs, User, Course, prompt, root, CommonCache) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'country', link: "#/admin/country/"},
            {title: params.countryid, link: "#/admin/country/" + params.countryid},
            {title: 'educationalsystem', link: "#/admin/country/" + params.countryid},
            {title: params.systemid, link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: 'grade', link: ""}
        ];


        scope.countryid = params.countryid;
        scope.systemid = params.systemid;
        scope.gradeid = params.gradeid;

        Crumbs.setCountry(scope.crumbs[2], params.countryid, scope);
        Crumbs.setSystem(scope.crumbs[4], params.systemid, scope);
        Crumbs.setGrade(scope.crumbs[5], params.gradeid, scope);
        function load() {
            Course.getList({grade: params.gradeid}).then(function(courses) {
                scope.entities = _.map(courses, CommonCache.course);
            });
        }
        load();
        scope.$on("relist", load);
        scope.searchable = true;
        scope.isLoading = false;

        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            load();
        };

        scope.deactivate = function deactivate(entity) {
            scope.isLoading = true;
            if (!entity) {
                return;
            }
            var query = {
                "course": entity.id
            };
            // entity.remove().then(scope.onSuccess, Common.onError(scope));
            Course.disable(query).then(scope.onSuccess, Common.onError(scope));
        };

        scope.activate = function activate(entity) {
            scope.isLoading = true;
            if (!entity) {
                return;
            }
            var query = {
                "course": entity.id
            };
            // entity.remove().then(scope.onSuccess, Common.onError(scope));
            Course.enable(query).then(scope.onSuccess, Common.onError(scope));
        };

        scope.remove = function remove(entity) {
            scope.isLoading = true;
            if (!entity) {
                return;
            }
            prompt({
                title: root.lang.removecourse,
                message: root.lang.removecoursemessage,
                buttons: [
                  {
                    "label": root.lang.cancel,
                    "cancel": true
                  },
                  {
                    "label": root.lang.removebutton,
                    "primary": true
                  }
                ]
            }).then(function () {
                entity.remove().then(scope.onSuccess, Common.onError(scope));
            });
            
        };
    }

    angular.module("elproffApp")
    .controller("AdminCountryEducationalsystemGradeViewCtrl",
            ["$scope", "$routeParams", "Common", "Crumbs", "User", "Course",
            "prompt", "$rootScope", "CommonCache", controller] );
}).call(null);
