"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminCourseCreateCtrl
 * @description
 * # AdminCourseCreateCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, $location, Service, Restangular) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'country', link: "#/admin/country/" + params.countryid},
            {title: params.countryid, link: "#/admin/country/" + params.countryid},
            {title: 'educationalsystem', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: params.systemid, link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: 'grade', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.gradeid},
            {title: 'createcourse', link: ""},

        ];

        scope.countryid = params.countryid;
        scope.systemid = params.systemid;
        scope.gradeid = params.gradeid;
        Crumbs.setCountry(scope.crumbs[2], params.countryid, scope);
        Crumbs.setSystem(scope.crumbs[4], params.systemid, scope);
        Crumbs.setGrade(scope.crumbs[5], params.gradeid, scope);

        scope.model = {
            //grades:[]
        };

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $location.path(scope.crumbs[5].link.substring(1));
        };

        scope.save = function save() {
            scope.isLoading = true;
            var data = new FormData();
            var model = _.cloneDeep(scope.model);
            model = Common.copyTranslation(model);
            data.append('name', model.name || "");
            data.append('name_ar', model.name_ar || "");
            data.append('name_en', model.name_en || "");
            data.append("image", this.image);
            data.append('description', model.description || "");
            data.append('description_ar', model.description_ar || "");
            data.append('description_en', model.description_en || "");
            data.append("grade", model.grade.resource_uri);
            Common.customRestangular("course",data).then(scope.onSuccess, Common.onError(scope));
        };

        scope.clickUpload = function clickUpload() {
            angular.element("#image").trigger("click");
        };

        scope.fileNameChanged = function fileNameChanged() {
          var fileName = document.getElementById("image").files[0].name;
          $("#fielUploadBtn span").text(": "+fileName);
        }
    }

    angular.module("elproffApp")
    .controller("AdminCourseCreateCtrl",
            ["$scope", "Common", "Crumbs","$routeParams","$location", "Course", "Restangular", controller] );
}).call(null);
