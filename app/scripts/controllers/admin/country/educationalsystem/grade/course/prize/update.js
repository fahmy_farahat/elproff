"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminPrizeUpdateCtrl
 * @description
 * # AdminPrizeUpdateCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, Course, $location,  Prize, Restangular) {

        scope.model = {};
        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'country', link: "#/admin/country/" + params.countryid},
            {title: params.countryid, link: ""},
            {title: 'educationalsystem', link: "#/admin/country/" + params.countryid + "/educationalsystem/"},
            {title: params.systemid, link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: 'grade', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.gradeid},
            {title: params.courseid, link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.gradeid + "/course/" + params.courseid},
            {title: 'prize', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.gradeid + "/course/" + params.courseid},
            {title: 'update', link: ""},
        ];
        scope.countryid = params.countryid;
        scope.systemid = params.systemid;
        scope.gradeid = params.gradeid;
        scope.courseid = params.courseid;
        scope.prizeid = params.prizeid;
        
        Crumbs.setPrize(scope.crumbs[7], scope.prizeid, scope, function(obj){
            scope.model = obj;
            scope.image = obj.image;
            Crumbs.setCountry(scope.crumbs[2], params.countryid, scope, false);
            Crumbs.setSystem(scope.crumbs[4], params.systemid, scope, false);
            Crumbs.setGrade(scope.crumbs[5], params.gradeid, scope, false);
            Crumbs.setCourse(scope.crumbs[6], params.courseid, scope, false);
        });


        scope.fileNameChanged = function fileNameChanged(element) {
            scope.$apply(function(){
                scope.imageName = element.files[0].name;    
            });
        }

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $location.path(scope.crumbs[6].link.substring(1));
        };

        scope.save = function save() {
            scope.isLoading = true;
            var data = new FormData();
            var model = _.cloneDeep(scope.model);
            model = Common.copyTranslation(model);
            data.append('name', model.name || "");
            data.append('name_ar', model.name_ar || "");
            data.append('name_en', model.name_en || "");
            data.append("image", this.image);
            data.append('description', model.description || "");
            data.append('description_ar', model.description_ar || "");
            data.append('description_en', model.description_en || "");
            data.append("courses_count", model.courses_count || 0);
            data.append("questions_count", model.questions_count);
            data.append("exams_count", model.exams_count);
            Restangular.one("prize", scope.model.id)
                .withHttpConfig({transformRequest: angular.identity})
                .customPUT(data, undefined, {}, {"Content-Type": undefined})
                .then(this.onSuccess, Common.onError(scope));
        };

    }

    angular.module("elproffApp")
    .controller("AdminPrizeUpdateCtrl", 
            ["$scope", "Common", "Crumbs", "$routeParams", "Course",
            "$location", "Prize", "Restangular", controller] );
}).call(null);
