"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminPrizeCreateCtrl
 * @description
 * # AdminPrizeCreateCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, Course, $location,  Service, Restangular) {

        scope.model = {};
        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'country', link: "#/admin/country/" + params.countryid},
            {title: params.countryid, link: ""},
            {title: 'educationalsystem', link: "#/admin/country/" + params.countryid + "/educationalsystem/"},
            {title: params.systemid, link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: 'grade', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.gradeid},
            {title: params.courseid, link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.gradeid + "/course/" + params.courseid},
            {title: "createprize", link: ""}
        ];
        scope.countryid = params.countryid;
        scope.systemid = params.systemid;
        scope.gradeid = params.gradeid;
        scope.courseid = params.courseid;
        Crumbs.setCountry(scope.crumbs[2], params.countryid, scope);
        Crumbs.setSystem(scope.crumbs[4], params.systemid, scope);
        Crumbs.setGrade(scope.crumbs[5], params.gradeid, scope);
        Crumbs.setCourse(scope.crumbs[6], params.courseid, scope);
        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $location.path(scope.crumbs[6].link.substring(1));
        };

        scope.save = function save() {
            scope.isLoading = true;
            var data = new FormData();
            var model = _.cloneDeep(scope.model);
            model = Common.copyTranslation(model);
            data.append('name', model.name || "");
            data.append('name_ar', model.name_ar || "");
            data.append('name_en', model.name_en || "");
            data.append("avatar", this.image);
            data.append('description', model.description || "");
            data.append('description_ar', model.description_ar || "");
            data.append('description_en', model.description_en || "");
            data.append("courses_count", model.courses_count || 0);
            data.append("questions_count", model.questions_count || 0);
            data.append("exams_count", model.exams_count || 0);
            data.append("course", model.course.resource_uri);
            Restangular.all("prize")
                .withHttpConfig({transformRequest: angular.identity})
                .post(data, {}, {"Content-Type": undefined})
                .then(this.onSuccess, Common.onError(scope));
        };

    }

    angular.module("elproffApp")
    .controller("AdminPrizeCreateCtrl", 
            ["$scope", "Common", "Crumbs", "$routeParams", "Course", "$location", "Prize", 
            "Restangular", controller] );
}).call(null);
