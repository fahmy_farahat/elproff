"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminPrizeCtrl
 * @description
 * # AdminPrizeCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, params, Common, Crumbs, Course, Prize, CommonCache) {
        scope.Math = window.Math;
        scope.model = {};
        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'country', link: "#/admin/country/" + params.countryid},
            {title: params.countryid, link: "#/admin/country/" + params.countryid},
            {title: 'educationalsystem', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: params.systemid, link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: 'grade', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.gradeid},
            {title: params.id, link: ""},
        ];
        scope.countryid = params.countryid;
        scope.systemid = params.systemid;
        scope.gradeid = params.gradeid;
        scope.courseid = params.courseid;
        Crumbs.setCountry(scope.crumbs[2], params.countryid, scope);
        Crumbs.setSystem(scope.crumbs[4], params.systemid, scope);
        Crumbs.setGrade(scope.crumbs[5], params.gradeid, scope);
        Crumbs.setCourse(scope.crumbs[6], params.courseid, scope);

        function load() {
            Prize.getList({course: params.courseid}).then(function(prizes) {
                scope.entities = _.map(prizes, CommonCache.prize);
            });
            scope.stats = Course.stats({courseid: params.courseid}).$object;
        }
        load();
        scope.$on("relist", load);
        scope.searchable = true;
        scope.isLoading = false;
    }

    angular.module("elproffApp")
    .controller("AdminCourseViewCtrl",
            ["$scope", "$routeParams", "Common", "Crumbs", "Course",
            "Prize", "CommonCache", controller] );
}).call(null);
