"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminGradeUpdateCtrl
 * @description
 * # AdminGradeUpdateCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, $location, Grade, System, CommonCache) {

        scope.model = {};
        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'country', link: "#/admin/country/"},
            {title: params.countryid, link: ""},
            {title: 'educationalsystem', link: "#/admin/country/" + params.countryid + "/educationalsystem/"},
            {title: params.systemid, link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: 'grade', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.id},
            {title: params.id, link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.id},
            {title: 'update', link: ""},
        ];

        // No need to wait for the grade to be retrieved, because,
        // we do not need to update country in the model
        // No component ( Drop down ) is showing country, just the crumbs
        Crumbs.setCountry(scope.crumbs[2], params.countryid, scope);

        Grade.one(params.id).get().then(function(obj) {
            scope.model = obj;
            // We need system to wait for grade, to be able to update
            // the system attribute of the - retrieved - grade object.
            Crumbs.setSystem(scope.crumbs[4], params.systemid, scope);
            Crumbs.setGrade(scope.crumbs[6], obj, scope);
        });

        System.getList().then(function(systems) {
            scope.systems = _.map(systems, CommonCache.system);
        });

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $location.path(scope.crumbs[6].link.substring(1));
        };

        scope.save = function save() {
            scope.isLoading = true;
            scope.model = Common.copyTranslation(scope.model);
            scope.model.save().then(this.onSuccess, Common.onError(scope));
        };
    }

    angular.module("elproffApp")
    .controller("AdminGradeUpdateCtrl",
            ["$scope", "Common", "Crumbs", "$routeParams", "$location",
            "Grade", "EducationalSystem", "CommonCache", controller]);
}).call(null);
