"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminSubscriptionCtrl
 * @description
 * # AdminSubscriptionCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, params, Common, Crumbs, Subscription, Student) {

        scope.model = {};
        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'country', link: "#/admin/country/" + params.countryid},
            {title: params.countryid, link: "#/admin/country/" + params.countryid},
            {title: 'educationalsystem', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: params.systemid, link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: 'grade', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid + "/grade/" + params.gradeid},
            {title: "subscription", link: ""}
        ];

        scope.countryid = params.countryid;
        scope.systemid = params.systemid;
        scope.gradeid = params.gradeid;
        Crumbs.setCountry(scope.crumbs[2], params.countryid, scope);
        Crumbs.setSystem(scope.crumbs[4], params.systemid, scope);
        Crumbs.setGrade(scope.crumbs[5], params.gradeid, scope);
        function load() {
            Subscription.listSubscription({grade:params.gradeid}).then(function(result){
                scope.entities = result;
                scope.entities = _.uniq(scope.entities, function(element){
                    return element.student.id;
                }); 
            });
        }
        load();
        scope.$on("relist", load);
        scope.searchable = true;
        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            scope.entities = Subscription.getList().$object;
        };

        scope.remove = function remove(entity) {
            scope.isLoading = true;
            entity.remove().then(this.onSuccess, Common.onError(scope));
        };

    }

    angular.module("elproffApp")
        .controller("AdminSubscriptionCtrl", ["$scope", "$routeParams",
            "Common", "Crumbs", "Subscription", "Student", controller]);
}).call(null);
