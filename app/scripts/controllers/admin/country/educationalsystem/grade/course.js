"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminCourseCtrl
 * @description
 * # AdminCourseCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, User, Course, CommonCache) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'country', link: "#/admin/country/"},
            {title: params.countryid, link: ""},
            {title: 'educationalsystem', link: "#/admin/country/" + params.countryid + "/educationalsystem/"},
            //{title: params.systemid, link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: params.systemid, link: ""}
        ];

        scope.countryid = params.countryid;
        scope.systemid = params.systemid;
        Crumbs.setCountry(scope.crumbs[2], params.countryid, scope);
        Crumbs.setSystem(scope.crumbs[4], params.systemid, scope);

        function load() {
            Course.getList().then(function(courses) {
                scope.entities = _.map(courses, CommonCache.course);
            });
        }
        load();
        scope.$on("relist", load);
        scope.searchable = true;
        scope.isLoading = false;

        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            load();
        };

        scope.remove = function remove(entity) {
            scope.isLoading = true;
            if (!entity) {
                return;
            }
            entity.remove().then(scope.onSuccess, Common.onError(scope));
        };
    }

    angular.module("elproffApp")
    .controller("AdminCourseCtrl",
            ["$scope", "Common", "Crumbs", "User", "Course", "CommonCache",
            controller]);
}).call(null);
