"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminEducationalsystemCreateCtrl
 * @description
 * # AdminEducationalsystemCreateCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, root, User, EducationalSystem, $location) {

        scope.model = {};
        scope.model.name = "";
        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'country', link: "#/admin/country/"},
            {title: params.countryid, link: "#/admin/country/" + params.countryid },
            {title: 'createeducationalsystem', link: ""}
        ];

        Crumbs.setCountry(scope.crumbs[2], params.countryid, scope);

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $location.path(scope.crumbs[2].link.substring(1));
        };
        
        scope.save = function save() {
            scope.isLoading = true;
            var model = _.cloneDeep(scope.model);
            model = Common.copyTranslation(model)
            EducationalSystem.post(model).then(this.onSuccess, Common.onError(scope));
        };

    }

    angular.module("elproffApp")
    .controller("AdminEducationalsystemCreateCtrl",
            ["$scope", "Common", "Crumbs", "$routeParams", "$rootScope", "User", 
            "EducationalSystem", "$location", controller] );
}).call(null);
