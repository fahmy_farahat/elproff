"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminCountryEducationalsystemViewCtrl
 * @description
 * # AdminCountryEducationalsystemViewCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, params, Common, Crumbs, EducationalSystem, Grade, CommonCache) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'country', link: "#/admin/country/"},
            {title: params.countryid, link: "#/admin/country/" + params.countryid},
            {title: 'educationalsystem', link: "#/admin/country/" + params.countryid},
            //{title: params.systemid, link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.systemid},
            {title: params.systemid, link: ""}
        ];

        scope.countryid = params.countryid;
        scope.systemid = params.systemid;
        Crumbs.setCountry(scope.crumbs[2], params.countryid, scope);
        Crumbs.setSystem(scope.crumbs[4], params.systemid, scope);

        function load() {
            Grade.getList({system: params.systemid}).then(function(grades) {
                scope.entities = _.map(grades, CommonCache.grade);
            });
        }
        load();
        scope.$on("relist", load);
        scope.searchable = true;

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            load();
        };

        scope.remove = function remove(entity) {
            scope.isLoading = true;
            entity.remove().then(this.onSuccess, Common.onError(scope));
        };
    }

    angular.module("elproffApp")
    .controller("AdminCountryEducationalsystemViewCtrl",
            ["$scope", "$routeParams", "Common", "Crumbs", "EducationalSystem",
            "Grade", "CommonCache", controller] );
}).call(null);
