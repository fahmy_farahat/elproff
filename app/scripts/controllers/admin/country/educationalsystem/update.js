"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminEducationalsystemUpdateCtrl
 * @description
 * # AdminEducationalsystemUpdateCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, root, $location, EducationalSystem) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'country', link: "#/admin/country/"},
            {title: params.countryid, link: ""},
            {title: 'educationalsystem', link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.id},
            {title: params.id, link: "#/admin/country/" + params.countryid + "/educationalsystem/" + params.id},
            {title: 'update', link: ""},
        ];
        EducationalSystem.one(params.id).get().then(function(obj) {
            scope.model = obj;
            Crumbs.setSystem(scope.crumbs[4], obj, scope);
            // We need country to wait for system, to be able to update
            // the country attribute of the - retrieved - edu-system object.
            Crumbs.setCountry(scope.crumbs[2], params.countryid, scope);
        });

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            $location.path(scope.crumbs[4].link.substring(1));
        };

        scope.save = function save() {
            scope.isLoading = true;
            scope.model = Common.copyTranslation(scope.model);
            scope.model.save().then(this.onSuccess, Common.onError(scope));
        };

    }

    angular.module("elproffApp")
    .controller("AdminEducationalsystemUpdateCtrl", 
        ["$scope", "Common", "Crumbs", "$routeParams", "$rootScope", "$location",
        "EducationalSystem", controller] );
}).call(null);
