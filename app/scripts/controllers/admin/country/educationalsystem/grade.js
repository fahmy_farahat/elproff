"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminGradeCtrl
 * @description
 * # AdminGradeCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Grade, CommonCache) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'grades', link: ""},
        ];

        function load() {
            Grade.getList().then(function(grades) {
                scope.entities = _.map(grades, CommonCache.grade);
            });
        }
        load();
        scope.$on("relist", load);
        scope.searchable = true;

        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            load();
        };

        scope.remove = function remove(entity) {
            scope.isLoading = true;
            entity.remove().then(this.onSuccess, Common.onError(scope));
        };
    }

    angular.module("elproffApp")
    .controller("AdminGradeCtrl",
            ["$scope", "Common", "Grade", "CommonCache", controller]);
}).call(null);
