"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminReviewerViewCtrl
 * @description
 * # AdminReviewerViewCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, CommonCache, params, $location, Reviewer) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'reviewer', link: "#/admin/reviewer/"},
            {title: 'view', link: ""},
        ];

        scope.model = { };
        Reviewer.one(params.id).get().then(function(obj) {
            scope.model = CommonCache.reviewer(obj);
        });
    }

    angular.module("elproffApp")
    .controller("AdminReviewerViewCtrl",
        ["$scope", "Common", "Crumbs", "CommonCache", "$routeParams", "$location", "Reviewer", controller]);
}).call(null);
