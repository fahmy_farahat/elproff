"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminReviewerCreateCtrl
 * @description
 * # AdminReviewerCreateCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'reviewer', link: "#/admin/reviewer"},
            {title: 'invite', link: ""},
        ];

    }

    angular.module("elproffApp")
    .controller("AdminReviewerCreateCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "Invitation", controller]);
}).call(null);
