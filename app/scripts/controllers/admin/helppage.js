"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminHelppageCtrl
 * @description
 * # AdminHelppageCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'Help Pages', link: ""},
        ];
    }

    angular.module("elproffApp")
    .controller("AdminHelppageCtrl",
            ["$scope", "Common", "Crumbs", controller]);
}).call(null);

