"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminSmeCreateCtrl
 * @description
 * # AdminSmeCreateCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'smes', link: "#/admin/sme"},
            {title: 'invite', link: ""},
        ];

    }

    angular.module("elproffApp")
    .controller("AdminSmeCreateCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "Invitation", controller]);
}).call(null);
