"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AdminSmeViewCtrl
 * @description
 * # AdminSmeViewCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/admin/"},
            {title: 'smes', link: "#/admin/sme/"},
            {title: 'view', link: ""},
        ];

        scope.model = { };
        Service.one(params.id).get().then(function(obj) {
            scope.model = obj;
            console.log(scope.model);
            scope.courses = obj.courses;
        });

    }

    angular.module("elproffApp")
    .controller("AdminSmeViewCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "SME", controller]);
}).call(null);
