"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:FBLoginCtrl
 * @description
 * # FBLoginCtrl
 * Controller of the elproffApp
 */
 (function() {

    function controller(scope, User, location, Facebook, Restangular) {
        scope.user = {};
        var isConnected = false;
        var dialogOpened = false;
      
        scope.$watch(function() {
            return Facebook.isReady();

        }, function(newVal) {
            if (newVal) {
                if(! scope.facebookReady){
                    scope.facebookReady = true;
                }
            }
        });

        Facebook.getLoginStatus(function(response) {
            if (response.status === "connected") {
                isConnected = true;
                scope.me();
            } else {
                scope.login();
            }
        });

        scope.login = function() {
            Facebook.login(function(response) {
                console.log("login");
                if (response.status === "connected") {
                    scope.me();
                }
            }, {"scope": "email"});
        };

        scope.me = function() {
            Facebook.api("/me", function(response) {
                console.log("meee");
                scope.$apply(function() {
                    var users = Restangular.all("user/fblogin");
                    users.post({"full_name": response.name,
                                 "email": response.email,
                                 "id": response.id}).then(
                               scope.onSuccess, scope.onError);
                });
            });
        };

        scope.onSuccess = function(response) {
            if (response.status === "set password") {
                location.path("/fbregister");
            } else if (response.status === "logged in") {
                scope.isLoading = false;
                scope.user = response.user;
                User.setUser(scope.user);
                location.path(response.url);
            } else {
                location.path("/login");
            }
        };

        scope.onError = function(response) {
            // This should display an error message.
            console.log("error" + response);
        };

        scope.$on("Facebook:statusChange", function(ev, data) {
            if (data.status !== "connected") {
                scope.$apply(function() {
                    // Not logged in, redirect to login.
                    User.setUser(null);
                    location.path("/login");
                });
            }
        });
    }
    angular.module("elproffApp")
    .controller("FBLoginCtrl", ["$scope", "User", "$location", "Facebook", "Restangular", controller]);
}).call(null);



/**
 * @ngdoc function
 * @name elproffApp.controller:FBRegisterCtrl
 * @description
 * # FBRegisterCtrl
 * Controller of the elproffApp
 */
 (function() {

    function controller(scope, User, location, Facebook, Restangular) {
        scope.onSuccess = function (response) {
            location.path(response.url);
        };

        scope.onError = function(response) {
            // Show error message.
            console.log(response);
        };

        scope.register = function () {
            scope.model.facebook_id = FB.getUserID();
            var register = Restangular.all("user/fbregister");
            register.post(scope.model).then(scope.onSuccess, scope.onError);
        };
    }
    angular.module("elproffApp")
    .controller("FBRegisterCtrl", ["$scope", "User", "$location", "Facebook", "Restangular", controller]);
}).call(null);
