"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AuthorHelpCtrl
 * @description
 * # AuthorHelpCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, $location,  Service, Help) {
    	$(".faq-navigation a").click(function(){
            var faqId = $(this).attr("data-faq");
            if(!$(this).hasClass("active")) {
                $(".faq-content .faq").removeClass("active").fadeOut();
                $("#"+faqId).fadeIn(1500).addClass("active");
                $(".faq-navigation a").removeClass("active");
                $(this).addClass("active");
            }
            return false;
        });
        function load(){
            Help.getList().then(function(pages){
                scope.pages = pages;
                if(params.pageid){
                    scope.currentpage = _.find(pages, function(page){
                        return page.id == parseInt(params.pageid);
                    });
                    scope.pages = _.map(pages,function(page){
                        if(page.id == parseInt(params.pageid)){
                            page.isActive = true;
                        }
                        else {
                            page.isActive = false;
                        }
                        return page;
                    });
                }

            });
            

        };
        load();

    }

    angular.module("elproffApp")
    .controller("AuthorHelpCtrl", ["$scope", "Common", "Crumbs", "$routeParams", "$location", "User", "Help", controller] );
}).call(null);
