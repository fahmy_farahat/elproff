"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AuthorNotificationsCtrl
 * @description
 * # AuthorNotificationsCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("AuthorNotificationsCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
