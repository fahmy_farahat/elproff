"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AuthorCourseCtrl
 * @description
 * # AuthorCourseCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, $location,  Course, CommonCache) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/author/"},
            {title: 'course', link: ""},
        ];

        function load() {
            Course.getList().then(function(courses) {
                scope.entities = _.map(courses, CommonCache.course);
            });
        }
        scope.$on("relist", load);
        scope.searchable = true;
        load();
    }

    angular.module("elproffApp")
    .controller("AuthorCourseCtrl", 
            ["$scope", "Common", "Crumbs", "$routeParams", "$location",
            "Course", "CommonCache", controller] );
}).call(null);
