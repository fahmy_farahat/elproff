"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AuthorProfileEditCtrl
 * @description
 * # AuthorProfileEditCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("AuthorProfileEditCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
