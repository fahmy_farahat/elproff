"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AuthorProfileChangepasswordCtrl
 * @description
 * # AuthorProfileChangepasswordCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("AuthorProfileChangepasswordCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
