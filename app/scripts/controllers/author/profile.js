"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AuthorProfileCtrl
 * @description
 * # AuthorProfileCtrl
 * Controller of the elproffApp
 */
angular.module("elproffApp")
  .controller("AuthorProfileCtrl", function ($scope) {
    $scope.awesomeThings = [
      "HTML5 Boilerplate",
      "AngularJS",
      "Karma"
    ];
  });
