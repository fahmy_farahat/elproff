"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AuthorTasksCtrl
 * @description
 * # AuthorTasksCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, $location,  Task, CommonCache) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/author/"},
            {title: 'courses', link: "#/author/course/"},
            {title: params.courseid, link: "#/author/course/" + params.courseid},
            {title: 'tasks', link: "#/author/course/" + params.courseid},
        ];
        scope.alerts = [];
        scope.courseid = params.courseid;
        Crumbs.setCourse(scope.crumbs[2], scope.courseid);
        scope.isLoading = false;
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            scope.entities = result.objects;
            scope.entities = _.each(scope.entities, function(task){
                task = CommonCache.task(task);
            });
        };
        scope.onError = function onError(result){
            var msg = scope.$root.lang["error_"+result.data.error];
            scope.alerts = [];
            scope.alerts.push({msg: msg, type: "danger"});
            if(result.data.error === "no_tasks"){
                scope.entities = {};
            }
        };
        scope.closeAlert = function closeAlert(index){
            scope.alerts.splice(index, 1);
        };
        function load() {
            scope.isLoading = true;
            var query = _.cloneDeep(Common.query);
            query.course = params.courseid;
            Task.my(query).then(scope.onSuccess, Common.onError(scope, scope.onError));
        }
        scope.$on("relist", load);
        scope.searchable = false;
        load();
    }

    angular.module("elproffApp")
    .controller("AuthorTasksCtrl",
            ["$scope", "Common", "Crumbs", "$routeParams", "$location", "Task", "CommonCache",
            controller] );
}).call(null);
