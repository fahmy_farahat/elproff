"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AuthorTaskCreateCtrl
 * @description
 * # AuthorTaskCreateCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, params, $location,  Task, CommonCache) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/author/"},
            {title: 'course', link: "#/author/course/"},
            {title: params.courseid, link: "#/author/course/" + params.courseid},
            {title: 'tasks', link: "#/author/course/" + params.courseid + "/task/"},
            {title: 'add', link: ""},
        ];

        scope.isLoading = false;
        scope.courseid = params.courseid;
        scope.searchable = true;
        Crumbs.setCourse(scope.crumbs[2], scope.courseid);
        function load() {
            scope.isLoading = true;
            var query = _.cloneDeep(Common.query);
            query.course = params.courseid;
            Task.available(query).then(function(entities) {
                scope.entities = entities.objects;
                scope.entities = _.map(scope.entities, CommonCache.task);
                scope.isLoading = false;
            }, Common.onError(scope));
        }
        scope.$on("relist", load);
        load();

        // When author acquires a task he have all the difficulty levels of the task
        scope.afterAssign = function(task){
            _.filter(scope.entities, function(entity) {
                if (entity.objective === task.objective && 
                    entity.course_element.id === task.course_element.id && 
                    entity.questions_bulk.questions_type === task.questions_bulk.questions_type) {
                    Task.selfAssign({task_id: entity.id}).then(function(result) {
                        entity.mine = true;
                        scope.$root.$broadcast("relist");
                    }, Common.onError(scope));
                }
            });
        };
    }

    angular.module("elproffApp")
    .controller("AuthorTaskCreateCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location", "Task", "CommonCache", controller]);
}).call(null);
