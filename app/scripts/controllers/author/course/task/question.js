"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AuthorQuestionCtrl
 * @description
 * # AuthorQuestionCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Question, Task, Restangular) {

        scope.crumbs = [
            {title: 'dashboard', link: "#/author/"},
            {title: 'courses', link: "#/author/course/"},
            {title: params.courseid, link: "#/author/course/" + params.courseid},
            {title: 'tasks', link: "#/author/course/" + params.courseid + "/task/"},
            {title: 'questions', link: ""}
        ];

        scope.courseid = params.courseid;
        scope.taskid = params.taskid;
        scope.entities = [];
        Crumbs.setCourse(scope.crumbs[2], params.courseid);

        scope.entities = [];
        function init() {
            Task.one(params.taskid).get().then(function(task) {
                scope.entities = task.questions;
                scope.task = task;
            });
        }
        init();
    }

    angular.module("elproffApp")
    .controller("AuthorQuestionCtrl",
        ["$scope", "Common", "Crumbs", "$routeParams", "$location",
        "Question", "Task", "Restangular", controller]);

}).call(null);
