"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AuthorQuestionCreateCtrl
 * @description
 * # AuthorQuestionCreateCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, Crumbs, params, $location, Service, Task, Restangular, Validate, plumbEndPoint, CommonCache, $timeout) {

        scope.model = {};
        scope.crumbs = [
            {title: 'dashboard', link: "#/author/"},
            {title: 'courses', link: "#/author/course/"},
            {title: params.courseid, link: "#/author/course/" + params.courseid},
            {title: 'tasks', link: "#/author/course/" + params.courseid + "/task/"},
            {title: 'questions', link: "#/author/course/" + params.courseid + "/task/" + params.taskid + "/question/create"},
            {title: 'add', link: ""},
        ];

        Crumbs.setCourse(scope.crumbs[2], params.courseid);
        function load() {
            Task.one(params.taskid).get().then(function(task) {
                scope.task = task;
                CommonCache.task(task);
                task.questions_bulk_promise.then(function(){
                    task.questions_bulk.objective_promise.then(function(){
                        init();                    
                    });
                });


            });
        }
        function init() {
            scope.objective = scope.task.questions_bulk.objective;
            var task = scope.task;
            if (scope.task.questions.length >= task.count) {
                $location.path(scope.crumbs[3].link.substring(1));
            }
            delete scope.model.question;
            delete scope.model.correct_answer;
            _.merge(scope.model, {
                description: "",
                task: task,
                difficulty: task.questions_bulk.difficulty,
                learning_outcome:  scope.objective.learning_outcome,
                course_element: task.course_element,
                question_type: task.questions_bulk.questions_type,
                minutes: 0,
                seconds: 0,
                attachment: null,
                question: {},
                correct_answer: {
                    hints: [],
                    answer: {},
                    perfect_answers:[]
                },
                user: {
                    answer: {}
                }
            });

            scope.model.question_type = scope.task.questions_bulk.questions_type;
            if (scope.$root) {
                $timeout(function(){
                    scope.$root.$broadcast("question_init");
                });
                
            }
            scope.$watch("model.user.answer", function() {
                if (scope.model.user) {
                    scope.model.correct_answer.answer = scope.model.user.answer;
                }
            });
            scope.isDisabled = false;
        }
        load();
        scope.onSuccess = function onSuccess(result) {
            scope.isLoading = false;
            scope.isDisabled = true; 
            scope.alerts = Common.alerts.success.add;
            // insert empty object to increase the count of created
            // questions.
            scope.task.questions.push({});
            init();
        };

        scope.onError = function onError(error) {
            scope.alerts = Common.alerts.error.add;
        };

        scope.save = function() {
            scope.isLoading = true;
            scope.isDisabled = true;
            var model = _.cloneDeep(scope.model);
            if (model.description.replace(" ", "") === "") {
                scope.alerts = Common.alerts.error.question_desc_required;
                scope.isDisabled = false;
                return;
            }
            if (["mcq", "multi_mcq", "connect", "dragdrop", "fillblanks"].indexOf(model.question_type) >= 0) {
                if (!model.question.choices || model.question.choices.length < 2) {
                    scope.alerts = Common.alerts.error.question_choices_required;
                    scope.isDisabled = false;
                    return;
                }
            }
            if (["connect", "dragdrop"].indexOf(model.question_type) >= 0) {
                if (!model.question.ends || model.question.ends.length < 2) {
                    scope.alerts = Common.alerts.error.question_ends_required;
                    scope.isDisabled = false;
                    return;
                }
            }
            model.duration = model.minutes * 60 + model.seconds;
            if (!model.duration) {
                scope.alerts = Common.alerts.error.question_duration_required;
                scope.isDisabled = false;
                return;
            }
            // Validate that there are no empty or duplicated choices, if choices exists
            if (model.question.choices && !_.isEmpty(model.question.choices)) {                
                var empty_choices = _.filter(model.question.choices, function(answer_choice){
                    // Getting the field type (text || image || equation)
                    var answer_type = answer_choice.type;
                    if (answer_type === "composite") {
                        return _.isEmpty(answer_choice["description"]);    
                    }
                    // Looking if that field is empty
                    // And return that choice if empty
                    return _.isEmpty(answer_choice[answer_type]);
                });
                if (!_.isEmpty(empty_choices)) {
                    // There"s one or more empty choices
                    // Let"s throw an error
                    scope.alerts = Common.alerts.error.empty_choices;
                    scope.isDisabled = false;
                    return;
                }
                // Future: Check choices duplications
            }
            // Validate that there are no empty or duplicated ends, if ends exists
            if (model.question.ends && !_.isEmpty(model.question.ends)) {                
                var empty_ends = _.filter(model.question.ends, function(answer_end){
                    // Getting the field type (text || image || equation)
                    var answer_type = answer_end.type;
                    if (answer_type === "composite") {
                        return _.isEmpty(answer_end["description"]);    
                    }
                    // Looking if that field is empty
                    // And return that end if empty
                    return _.isEmpty(answer_end[answer_type]);
                });
                if (!_.isEmpty(empty_ends)) {
                    // There"s one or more empty ends
                    // Let"s throw an error and stop the request
                    scope.alerts = Common.alerts.error.empty_ends;
                    scope.isDisabled = false;
                    return;
                }
                // Validate if there is any duplicated choice
                // First, we will remove the duplicates if there"re any
                var ends_without_duplicates = _.uniq(model.question.ends, "description");
                // Compare the two objects (Original ends && Ends without duplicates)
                if (model.question.ends.length !== ends_without_duplicates.length) {
                    // If there"s any difference in length between them, there"s a duplicate
                    // Let"s throw an error and stop the request
                    scope.alerts = Common.alerts.error.duplicated_ends;
                    scope.isDisabled = false;
                    return;
                }
            }
            // Checking if the question is answered
            if (!Validate.answer(model.correct_answer.answer, model.question_type)) {
                scope.alerts = Common.alerts.error.question_answer_required;
                scope.isDisabled = false;
                return;
            }
            delete model.user;
            model.question.ends = _.each(model.question.ends, function(end) {
                delete end.onchange;
                return end;
            });
            delete model.user;
            Service.post(model).then(this.onSuccess, Common.onError(scope, this.onError));
        };

        function addTerminal(terminal, prefix) {
            return function(withAnswers) {
                var model = {
                    text: "",
                    image: "",
                    equation: "",
                    description: "",
                    type: "composite"
                };
                // When adding an end, watch its name for a change
                // and hold its value in
                if (terminal === "ends") {
                    model.onchange = function(newValue, oldValue) {
                        //scope.model.question.ends
                        var prevAnswer = scope.model.user.answer[oldValue];
                        scope.model.user.answer[newValue] = prevAnswer;
                        delete scope.model.user.answer[oldValue];
                    };
                }
                var arr = scope.model.question[terminal];
                arr.push(model);
                if (withAnswers) {
                    scope.model.user.answer.push(false);
                }
                // Build jsPlumb
                if (scope.model.question_type === "connect") {
                    // We have to reset end points, to prevent
                    // Duplicates or misarranged objects
                    jsPlumb.selectEndpoints({"source": $("." + prefix)}).remove();
                    // Building end points based on the end model
                    // and its prefix (end || choices)
                    _.each(arr, function(model, index) {
                        plumbEndPoint.create(index, prefix);
                    });
                }
                // Gain focus to the last input
                setTimeout( function(){ 
                    $('input[ng-model="model.text"]:last').focus() 
                }, 100);
            };
        }

        function removeTerminal(terminal, prefix) {
            return function(index) {
                scope.model.question[terminal].splice(index, 1);
                if (scope.model.question_type === "connect") {
                    // Cleaning end points so we could entirly reset them
                    jsPlumb.selectEndpoints({"source": $("." + prefix)}).remove();
                    // Giving a timeout to prevent duplicated points
                    setTimeout(function() {
                        // Re-Building Based on the end model
                        _.each(scope.model.question[terminal], function(model, index) {
                            plumbEndPoint.create(index, prefix);
                        });
                    }, 100);
                }
                if (scope.model.user && scope.model.user.answer &&
                    scope.model.user.answer.splice) {
                    scope.model.user.answer.splice(index, 1);
                }
            };
        }

        scope.addEnd = addTerminal("ends", "end");
        scope.addChoice = addTerminal("choices", "choice");

        scope.removeEnd = removeTerminal("ends", "end");
        scope.removeChoice = removeTerminal("choices", "choice");
    }

    angular.module("elproffApp")
    .controller("AuthorQuestionCreateCtrl",
            ["$scope", "Common", "Crumbs", "$routeParams", "$location",
            "Question", "Task", "Restangular", "Validate", "plumbEndPoint", "CommonCache", "$timeout", controller]);
}).call(null);
