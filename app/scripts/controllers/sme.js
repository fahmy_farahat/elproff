"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:SmeCtrl
 * @description
 * # SmeCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs) {

        scope.crumbs = [
            {title: "dashboard", link: "#/sme/"},
        ];

    }

    angular.module("elproffApp")
    .controller("SmeCtrl", ["$scope", "Common", "Crumbs", controller] );
}).call(null);
