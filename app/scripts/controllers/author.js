"use strict";

/**
 * @ngdoc function
 * @name elproffApp.controller:AuthorCtrl
 * @description
 * # AuthorCtrl
 * Controller of the elproffApp
 */
(function(){

    function controller(scope, Common, Crumbs, Service) {

        scope.crumbs = [
            {title: "dashboard", link: ""},
        ];

    }

    angular.module("elproffApp")
    .controller("AuthorCtrl", ["$scope", "Common", "Crumbs", "User", controller] );
}).call(null);
