"use strict";
/**
 * @ngdoc function
 * @name elproffApp.controller:InvitationCtrl
 * @description
 * # InvitationCtrl
 * Controller of the elproffApp
 */
(function() {

    function controller(scope, Common, params, Invitation, Grade, Country, System, CommonCache) {

        scope.registered = false;
        scope.isLoading = false;
        scope.onSuccess = function onSuccess() {
            scope.isLoading = false;
            scope.registered = true;
        };
        scope.countries = Country.getList().$object;
        scope.systems = System.getList().$object;
        scope.grades = Grade.getList().$object;

        scope.alerts = [];
        scope.model = {};

        Invitation.load({token: params.token}).then(function(invitation) {
            scope.model.token = invitation.token;
            scope.model.user_type = invitation.user_type;
            
            var load = function () {
                if (!invitation.attrs.grade) {
                    return;
                }
                CommonCache.gradeSync(invitation.attrs.grade).then(function(grade){
                    scope.model.country = grade.system.country;
                    scope.model.system = grade.system;
                    scope.model.grade = grade;
                });
            };

            load();
            scope.$watch("invitation.attrs.grade", load);

        }, function(err) {
            scope.alerts = Common.alerts.error.incorrect_invitation;
        });

        scope.onError = function onError(result) {
            scope.isLoading = false;
            scope.registered = false;
            if (result.error === "captcha_error") {
                scope.alerts = Common.alerts.error.captcha;
            } else if (result.error === "captcha_error") {
                // TODO: work with other error tpes
            } else {
                scope.alerts = Common.alerts.error.register;
            }
        };

        scope.register = function register() {
            scope.isLoading = true;
            var model = _.cloneDeep(this.model);
            Invitation.accept(model).then(this.onSuccess, Common.onError(scope, this.onError));
        };

        scope.closeAlert = function(index) {
            scope.alerts = [];
        };
    }

    angular.module("elproffApp")
    .controller("InvitationCtrl",
            ["$scope", "Common", "$routeParams", "Invitation", "Grade",
             "Country", "EducationalSystem", "CommonCache", controller]);
}).call(null);
