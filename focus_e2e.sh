#!/bin/bash

filename=$1

find test/e2e/ -iname "*.spec.js" | xargs sed -i.orig "s/describe (/describe(/g"
find test/e2e/ -iname "*.spec.js" | xargs sed -i.orig "s/x*describe\s*(/describe(/g"
if test $filename; then
    #find test/e2e/ -iname '*.spec.js' -not -iname "$filename.spec.js" | xargs sed "s/describe(/xdescribe(/g" | grep "describe"
    #find test/e2e/ -iname "$filename.spec.js" | xargs sed "s/xdescribe(/describe(/g" | grep "describe"
    find test/e2e/ -iname '*.spec.js' -not -iname "$filename.spec.js" |\
        xargs sed -i.orig -e "s/describe\s*(/xdescribe(/g"
    find test/e2e/ -iname "$filename.spec.js" |\
        xargs sed -i.orig -e "s/x*describe\s*(/describe(/g"
fi
find test/e2e/ -iregex ".*orig$" -exec rm -f {} \;''
