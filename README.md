## Elproff Front-end Project
Based on _AngularJS_

### Installation
Provided that **Nodejs** is already installed

- npm install -g yo grunt-cli bower karma less jshint
- npm install
- bower install

### e2e Test

The project has many e2e tests, and we are trying to get it all done the right
way. We use protractor to run our tests, with jasmine as the testing framework.

The e2e tests exist in `tests/e2e`
Grunt and Protractor; both generate from the file system their configurations.
As a consequence, to run the e2e tests of a full module ( directory )

`grunt e2e:<directoryname>`

To run the e2e tests of a single suite

`grunt e2e:<directoryname_fileofthesuite>`

_*note* that the underscore _ is the separator in this case_
_*note* that tests don't support for than a single level of depth, hence there
should be no sub-directories after the first level of directories_

### Generate new pages

- Go to the root directory of Angular folder
- yo angular:route **<pagename>**

### Generate Services

- Go to the root directory of Angular folder
- yo angular:service **<servicename>**

