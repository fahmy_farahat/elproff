"use strict";
var timeout = parseInt(process.env.MAX_TIMEOUT || 500000);
var _ = require("lodash");
var fs = require("fs");
var path = require("path");
var grunt = require("grunt");

exports.config = {
    timeout: timeout,
    specs: [ ],
    baseUrl: process.env.BASE_URL || "http://localhost",
    //seleniumAddress: "http://localhost:4444/wd/hub",
    users: {
        admin: { email: process.env.USER_ADMIN || "admin@elproff.com" , password: "123456"},
        sme: { email: process.env.USER_SME || "sme@diaa.me" , password: "123456"},
        author: { email: process.env.USER_AUTHOR || "author@diaa.me" , password: "123456"},
        reviewer: { email: process.env.USER_REVIEWER || "reviewer@diaa.me" , password: "123456"},
        student: { email: process.env.USER_STUDENT || "student@diaa.me" , password: "123456"},
        parent: { email: process.env.USER_PARENT || "parent@diaa.me" , password: "123456"},
        schoolteacher: { email: process.env.USER_SCHOOLTEACHER || "schoolteacher@diaa.me" , password: "123456"},
        privateteacher: { email: process.env.USER_PRIVATETEACHER || "privateteacher@diaa.me" , password: "123456"},
        schooladmin: { email: process.env.USER_SCHOOLADMIN || "schooladmin@diaa.me" , password: "123456"},
        manager: { email: process.env.USER_MANAGER || "manager@diaa.me" , password: "123456"},
    },
    db: {
        protocol: process.env.DB_PROTOCOL || "postgres",
        username: process.env.DB_USER_NAME || "seventy_seven",
        password: process.env.DB_PASSWORD || "123456",
        port: process.env.DB_PORT || "5432",
        host: process.env.DB_HOST || "localhost",
        database: process.env.DB_NAME || "seventy"
    },

    // The timeout in milliseconds for each script run on the browser. This should
    // be longer than the maximum time your application needs to stabilize between
    // tasks.
    allScriptsTimeout: timeout,

    // How long to wait for a page to load.
    getPageTimeout: timeout,
    jasmineNodeOpts: {
        defaultTimeoutInterval: timeout,
        isVerbose: true,
        onComplete: null,
        showColors: true,
        includeStackTrace: true,
        showTiming: true,
        realTimeFailure: true
    },
    framework: "jasmine2",
    capabilities: {
        //"browserName": "firefox" // or "safari", "chrome"
        "browserName": "chrome" // or "safari", "firefox"
            //"browserName": "safari" // or "chrome", "firefox"
    },
};

function addBaseSuites() {
    var suites = {};
    var basePath = path.resolve(path.join("test", "e2e"));
    var files = fs.readdirSync(basePath);
    _.each(files, function(file) {
        // If the directory is python
        // do not add it
        if (file === 'python') {
            return;
        }
        var subPath = path.join(basePath, file);
        if (!fs.lstatSync(subPath).isDirectory()) {
            return;
        }
        var subFiles = fs.readdirSync(subPath);
        suites[file.toLowerCase()] = _.map(subFiles, function(subfile) {
            var basename = file + "_" + subfile.split(".")[0];
            var subfilePath = path.join(subPath, subfile);
            suites[basename.toLowerCase()] = [subfilePath];
            return subfilePath;
        });
    });
    return suites;
}

exports.config.suites = addBaseSuites();

