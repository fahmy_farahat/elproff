"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var utils = new Utils(ptor);

describe('Invitations', function() {

    // Parent invite Son
    var parent = utils.users.parent;
    var isLoggedIn = false;

    it('checks if parent ' + parent.email + ' exists', function(done) {
        utils.db.userExists(parent.email, function(result) {
            parent.exists = !!result;
            done();
        });
    });

    it('should register parent', function(done){
        if (parent.exists) {
            parent.email = faker.internet.email();
        }
        utils.get('/register');
        $('#parent').click();
        ptor.waitForAngular();
        var pr = utils.registercommonfields(parent.email);
        parent.email = pr.email;
        utils.VerificationImage();
        element(By.model('model.accepted')).click();
        utils.submitbutton();
        utils.activateAccount(parent.email).then(function(activated) {
            parent.activated = activated;
            done();
        });
    });

    it('should login', function(done) {
        utils.login(parent.email, '123456');
         $('.user_name').isDisplayed().then(function(result) {
            isLoggedIn = result;
            done();
        });
    });

    it ('should invite son', function(done){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/parent/");
        utils.get("/parent/add");
        utils.testPath("/parent/add");
        utils.uiSelectTest('#country');
        utils.invitecommonfields();
        ptor.waitForAngular();
        utils.uiSelectTest('#system');
        ptor.waitForAngular();
        utils.exists('#parentrelation').sendKeys('Father');
        utils.VerificationImage();
        utils.uiSelectTest('#grade');
        utils.submitbutton();
        utils.successpopup();
        utils.alertpopupmessage('Son');
        done();
    });

    it ('should test invite student and previously invited', function(done){
        utils.get("/parent/");
        utils.get("/parent/add");
        utils.testPath("/parent/add");
        utils.uiSelectTest('#country');
        var email = faker.internet.email();
        utils.invitecommonfields(email);
        ptor.waitForAngular();
        utils.uiSelectTest('#system');
        ptor.waitForAngular();
        utils.exists('#parentrelation').sendKeys('Father');
        utils.VerificationImage();
        utils.uiSelectTest('#grade');
        utils.submitbutton();
        utils.successpopup();
        utils.alertpopupmessage('Son');
        utils.invitecommonfields(email);
        ptor.waitForAngular();
        utils.VerificationImage();
        utils.submitbutton();
        utils.failpopup();
        utils.alertpopupmessage('You have invited him/her before.');
        done();
    });

    var student = utils.users.student;
    var isLoggedIn = false;

    // Son invite Parent
    it ('should register student', function(done) {
        /*if (student.exists) {
            student.email = faker.internet.email();
        }*/
        student.email = faker.internet.email();
        utils.get('/register');
        $('#student').click();
        ptor.waitForAngular();
        var st = utils.registercommonfields(student.email);
        student.email = st.email;
        utils.uiSelectTest('#system');
        utils.uiSelectTest('#grade');
        element(By.model('model.schoolname')).sendKeys(faker.name.findName());
        utils.VerificationImage();
        element(By.model('model.accepted')).click();
        utils.submitbutton();
        utils.activateAccount(st.email).then(function(token) {
            student.activated = token;
            done();
        });
    });

    it('should login', function(done) {
        utils.login(student.email, '123456');
        $('.user_name').isDisplayed().then(function(result) {
            isLoggedIn = result;
            done();
        });
    });

    it ('should test invite parent and succeed', function(done){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/student/");
        element(By.css('#userMenu')).click();
        element(By.css('#userMenu_editProfile')).click();
        element(By.css('#addparent')).click();
        utils.testPath('/student/profile/addparent');
        utils.invitecommonfields();
        element(By.css('#parentrelation')).sendKeys('Father');
        utils.VerificationImage();
        utils.submitbutton();
        utils.successpopup();
        utils.alertpopupmessage('Parent is invited successfully.');
        done();
    });

    it ('should test invite parent and previously invited', function(done){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/student/");
        element(By.css('#userMenu')).click();
        element(By.css('#userMenu_editProfile')).click();
        element(By.css('#addparent')).click();
        utils.testPath('/student/profile/addparent');
        var email = faker.internet.email();
        utils.invitecommonfields(email);
        element(By.css('#parentrelation')).sendKeys('Father');
        utils.VerificationImage();
        utils.submitbutton();
        utils.successpopup();
        utils.invitecommonfields(email);
        utils.VerificationImage();
        utils.submitbutton();
        utils.failpopup();
        utils.alertpopupmessage('You have invited him/her before.');
        done();
    });

    it ('should test that son has data entered by parent in invitation', function(done){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/parent/");
        utils.get("/parent/add");
        utils.testPath("/parent/add");
        utils.uiSelectTest('#country');
        var country = utils.exists('#country').getText();
        var email = faker.internet.email();
        utils.invitecommonfields(email);
        ptor.waitForAngular();
        utils.uiSelectTest('#system');
        ptor.waitForAngular();
        var system = utils.exists('#system');
        utils.exists('#parentrelation').sendKeys('Father');
        utils.VerificationImage();
        utils.uiSelectTest('#grade');
        var grade = utils.exists('#grade');
        utils.submitbutton();
        utils.successpopup();
        utils.alertpopupmessage('Son');
        utils.logout();

        utils.getInvitation(email).then(function(invitation) {
            var token =  invitation;
            utils.get('/invitation/'+ token);
            utils.registercommonfields(email);
            var invCountry = utils.exists('#country').getText();
            var invSystem = utils.exists('#system').getText();
            var invGrade = utils.exists('#grade').getText();
            utils.submitbutton();
            expect(invCountry).toEqual(country);
            expect(invSystem).toEqual(system);
            expect(invGrade).toEqual(grade);
            done();
        });
    });

});
