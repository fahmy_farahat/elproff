"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var utils = new Utils(ptor);

describe('Private Teacher', function() {

    var privateteacher = utils.users.privateteacher;
    var isLoggedIn = false;
    /* Checks if private teacher exists, consist of:
     * -    Check private teacher exist in DB .
     */
    it('checks if Private Teacher ' + privateteacher.email + ' exists', function(done) {
        utils.db.userExists(privateteacher.email, function(result) {
            privateteacher.exists = !!result;
            done();
        });
    });
    /*register private teacher
    *   -check if private teacher exist 
    *   -set privateteacher.email by faker
    *   -get register page
    *   -select private teacher button
    *   -wait for angular
    *   -call function"registercommonfields" and send private teacher email
    *   -set private teacher email value.
    *   -call function "VerificationImage"
    *   -click accept checkbox
    *   -press submit
    *   -set var processed to false
    *   -call function"activateAccount" and send private teacher email.
    */
    it('should register private teacher',function(done){
        if (privateteacher.exists) {
            privateteacher.email = faker.internet.email();
        }
        utils.get('/register');
        $('#privateteacher').click();
        ptor.waitForAngular();
        var pt = utils.registercommonfields(privateteacher.email);
        privateteacher.email = pt.email;
        utils.VerificationImage();
        element(By.model('model.accepted')).click();
        utils.submitbutton();
        utils.activateAccount(privateteacher.email).then(function(result) {
            privateteacher.activated = !!result;
            done();
        });
    });
    /*Login as private teacher, consist of:
     * -    check if private teacher not activated .
     * -    expect private teacher email is activated.
     * -    Call login function in utils and send private teacher credentials.
     * -    Check if user name link is displayed , so private teacher is logged in.
     */    
    it('should login', function(done) {
        if (!privateteacher.activated) {
            expect(privateteacher.email).toBe('activated');
            return done();
        }
        utils.login(privateteacher.email, '123456');
        $('.user_name').isDisplayed().then(function(result) {
            isLoggedIn = result;
            done();
        });
    });
    /*Tests school admin profile, consist of:
     * -    User is logged in to continue.
     * -    Get private teacher dashboard.
     * -    Click user name link.
     * -    Test path of private teacher profile.
     * -    Call function “profileCommonfields”.
     * -    Call function “EditProfile”.
    */
    it('should test profile', function(){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/privateteacher/");
        $('.user_name').click();
        utils.testPath('/privateteacher/profile');
        utils.profileCommonfields('/privateteacher/profile');
        utils.EditProfile('/privateteacher/profile');
    });
    /*test change password with right password, consist of:
           -User logged in to continue.
           -Get private teacher dashboard.
           -Check existence of user name link and click on it.
           -Test path of private teacher profile page”/privateteacher/profile”.
           -Call function” ChangePassword” and send right private teacher password to it.
    */
    it('should change password with right password', function(){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/privateteacher/");
        $('.user_name').click();
        utils.testPath('/privateteacher/profile');
        utils.ChangePassword('/privateteacher/profile','123456');
        utils.successpopup();
    });
    /*test change password with wrong password, consist of:
     * -    User logged in to continue.
     * -    Get private teacher dashboard.
     * -    Check existence of user name link and click on it.
     * -    Test path of private teacher profile page”/privateteacher/profile”.
     * -    Call function” ChangePassword” and send wrong private teacher password to it.
     * -    Call function” failpopup” from utils.
    */
    it('should change password with wrong current password', function(){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/privateteacher/");
        $('.user_name').click();
        utils.testPath('/privateteacher/profile');
        utils.ChangePassword('/privateteacher/profile','675449');
        utils.failpopup();
    });
    /*Logout private teacher, consist of:
     * -    User is logged in to continue.
     * -    Call function logout from utils.
    */
    it('should logout', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.logout();
    });

});
