"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var testTemplate = require('../test_template.js');
var proxy = testTemplate.proxy;
var utils = new Utils(ptor);
var admin = utils.users.admin;

describe('Admin', proxy(admin.email, admin.password, function() {

    /**
     * Creates course and delete course, consist of:
     * -    User is logged in to continue.
     * -    Get admin/country page.
     * -    Click first country item exists.
     * -    Test path of clicked country.
     * -    Check there is educational system into selected country.
     * -    If no education system, create new educational system by calling “createElement” function.
     * -     Click first educational system item exist.
     * -    Test path of clicked educational system.
     * -    Check there is grade into selected educational system.
     * -    If no grade, created new grade by calling “createElement” function.
     * -    Click first grade item exist.
     * -    Test path of clicked grade.
     * -    Check Create course button existence and clicking on it.
     * -    Test path of create course page.
     * -    Fill course name.
     * -    Attach image to course.
     * -    Fill description.
     * -    Press submit.
     * -    Test path of grade page after adding new course.
     * -    Switch system language to English and wait.
     * -    Click first course item pull right flip button.
     * -    Click button remove course.
     * -    Check appeared popup message text is match “This action cannot be undone, Do you want to continue?”
     * -    Click remove button into popup message.
     * -    Check deleted courses button existence and clicking on it.
     * -    Test path of view deleted courses.
     * -    Check existence of deleted course into page.
     */
    it('should create courses and delete courses', function(done) {
        utils.get('/admin/country');
        var e = utils.linkitemFirst();
        e.getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            e.click();
            utils.testPath('/admin/country/' + ids[0]);
            var e2 = utils.linkitemFirst();
            $('.linkitem a').isPresent().then(function(present){
                if (present === false) {
                    utils.createElement('edusys');
                }
            });
            e2.getAttribute('href').then(function(href) {
                var ids = utils.idsOfLink(href);
                e2.click();
                utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1]);
                var e3 = utils.linkitemFirst();
                $('.linkitem a').isPresent().then(function(present){
                    if (present === false) {
                        utils.createElement('grade');
                    }
                });
                e3.getAttribute('href').then(function(href){
                    var ids = utils.idsOfLink(href);
                    e3.click();
                    utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1] + '/grade/' +ids[2]);
                    utils.exists('#create-course').click();
                    utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1] + '/grade/' +ids[2] + '/course/create');
                    var name = faker.name.findName();
                    $('#image').sendKeys(utils.getImageFile('math.png'));
                    element(By.model('model.name')).sendKeys(name);
                    element(By.model('model.description')).sendKeys(faker.lorem.sentence());
                    utils.submitbutton();
                    utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1] + '/grade/' +ids[2]);
                    /**
                     * [Should test deleted course exist in viewdeleted.]
                     */
                    utils.switchLanguage('english');
                    ptor.waitForAngular();
                    var e4 = utils.linkitemFirst();
                    e4.getAttribute('href').then(function(href){
                        var ids = utils.idsOfLink(href);
                        utils.exists('.menu .btn.btn-success.btn-lg.pull-right.flip').click();
                        utils.exists('#remove-course-0').click();
                        expect($('.modal .modal-body p').getText()).toMatch('This action cannot be undone, Do you want to continue?');
                        utils.exists('.modal .btn-primary').click();
                        ptor.waitForAngular();
                        utils.exists('#delete-courses').click();
                        utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1] + '/grade/' +ids[2] + '/viewdeleted');
                        utils.exists('.course-' + ids[3]);
                        done();
                    });
                });
            });
        });
    });    

}));
