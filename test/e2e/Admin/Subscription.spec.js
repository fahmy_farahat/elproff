"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var utils = new Utils(ptor);

describe('Admin', function() {

    var admin = utils.users.admin;
    var isLoggedIn = false;
    
    /* 
    * Test login once, by calling login function in utils and send
    * admin credentials.
    */
    it ('should login once', function(done) {
        utils.login(admin.email, admin.password);
        $('.user_name').isDisplayed().then(function(result) {
            isLoggedIn = result;
            done();
        });
    });


    /**
     * Tests subscription, consist of:
     * -    User is logged in to continue.
     * -    Get admin/country page.
     * -    Click first country item.
     * -    Test path of selected country page.
     * -    Click first educational system item.
     * -    Test path of selected educational system page.
     * -    Click first grade item→ var allDone = _.after(3, done);
     * -    Test path of selected grade.
     * -    Click subscription button.
     * -    Test path of subscription”/subscription”.
     * -    Click manage plan button.
     * -    Test path of manage plan”/subscription/plan”.
     * -    Check existence of create plan button and click on it.
     * -    Test path of create plan”/subscription/plan/create”.
     * -    Select term value.
     * -    Select courses.
     * -    Fill plan name.
     * -    Fill value of plan fees.
     * -    Press submit.
     * -    Test path of plan page”/subscription/plan”.
     * -    allDone();
     * -    Get subscription page”/subscription”.
     * -    Check existence of manage term button and click on it
     * -    Check existence of create term button and click on it.
     * -    Test path of create term” /subscription/term/create”.
     * -    Check existence of term name and fill it.
     * -    Click start date button.
     * -    ('#startdate .btn.btn-default.btn-sm.pull-left').click();
     * -    ('#startdate .btn.btn-default.btn-sm.pull-left').click();
     * -    $('#startdate .btn.btn-default.btn-sm.active').click();
     * -    Click end date button.
     * -    $('#enddate .btn-default.active').click();
     * -    Press submit.
     * -    Test path of subscription term” /subscription/term”.
     * -    allDone();
     * -    Get subscription page”/subscription”.
     * -    Check existence of manage coupons button and click on it.
     * -    Count number of coupon bulks exist in page.
     * -    Check existence of coupon create button and click on it.
     * -    Test path of create coupon”/subscription/coupons/create”.
     * -    Check existence of coupon name and fill it.
     * -    Check existence of coupon number and fill it by key up.
     * -    Select plan.
     * -    Press submit.
     * -    Test path of subscription coupon page” /subscription/coupons”.
     * -    Expect coupon bulks count is increased by one.
     * -    →allDone(); 
     */
    it('should test subscription', function(done) {
        if (!isLoggedIn) {
            return done();
        }
        utils.get('/admin/country');
        var e = utils.linkitemFirst();
        e.getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            e.click();
            utils.testPath('/admin/country/' + ids[0]);
            var e2 = utils.linkitemFirst();
            e2.getAttribute('href').then(function(href) {
                var ids = utils.idsOfLink(href);
                e2.click();
                utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1]);
                var e3 = utils.linkitemFirst();
                e3.getAttribute('href').then(function(href){
                    var ids = utils.idsOfLink(href);
                    e3.click();
                    var allDone = _.after(3, done);
                    utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1] + '/grade/' +ids[2]);
                    utils.exists('#manage-subsription').click();
                    utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1] + '/grade/' +ids[2] + '/subscription');
                    // Test Plan Creations.
                    utils.exists('#manage-plans').click().then(function(){
                        utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1] + '/grade/' +ids[2] + '/subscription/plan');
                        utils.exists('#create-plan').click();
                        utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1] + '/grade/' +ids[2] + '/subscription/plan/create');
                        utils.uiSelectTest('#termid');
                        ptor.waitForAngular();
                        utils.uiSelectTest('#elements div .ui-select-match'); 
                        utils.exists('#name').sendKeys(faker.name.findName());
                        //utils.exists('#academic_year').sendKeys(_.random(1, 10) + '');
                        utils.exists('#fees').sendKeys(_.random(10, 100) + '');
                        utils.submitbutton();
                        utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1] + '/grade/' +ids[2] + '/subscription/plan');
                        allDone();
                    });
                    // Test Term creation.
                    utils.get('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1] + '/grade/' +ids[2] + '/subscription');
                    utils.exists('#manage-terms').click().then(function(){
                        utils.exists('#create-term').click();
                        utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1] + '/grade/' +ids[2] + '/subscription/term/create');
                        utils.exists('#name').sendKeys(faker.name.findName());
                        $('#btn-startdate').click();
                        $('#startdate .btn.btn-default.btn-sm.pull-left').click();
                        $('#startdate .btn.btn-default.btn-sm.pull-left').click();
                        $('#startdate .btn.btn-default.btn-sm.active').click();
                        $('#btn-enddate').click();
                        $('#enddate .btn-default.active').click();
                        ptor.waitForAngular();
                        //ptor.executeScript("$('#startdate input').value('1/1/15');");
                        //ptor.executeScript("$('#enddate input').value('6/1/15');");
                        utils.submitbutton();
                        utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1] + '/grade/' +ids[2] + '/subscription/term');
                        allDone();
                    });
                    //manage coupons..
                    utils.get('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1] + '/grade/' +ids[2] + '/subscription');
                    utils.exists('#manage-coupons').click();
                    //expect($('table.custom-table > thead > tr > td').first().getText()).toEqual('Name');
                    element.all(By.repeater('bulk in bulks')).count().then(function(count) {
                        var bulks = count + 1  ;
                        utils.exists('#create-coupons').click();
                        utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1] + '/grade/' +ids[2] + '/subscription/coupons/create');
                        utils.exists('#name').sendKeys(faker.name.findName());
                        utils.exists('#couponsNumber').sendKeys(protractor.Key.UP);
                        utils.uiSelectTest('#plan');
                        utils.submitbutton();
                        utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1] + '/grade/' +ids[2] + '/subscription/coupons');
                        expect(element.all(By.repeater('bulk in bulks')).count()).toEqual(bulks);
                        allDone();
                    });
                });
            });
        });
    });

    /**
     * Tests update subscription plan, consist of:
     * -    User is logged in to continue.
     * -    Get country page”/admin/country”.
     * -    Select first country item in page.
     * -    Test path of first country page.
     * -    Select first item of educational system item in page.
     * -    Test path of first educational system page.
     * -    Select first grade item in page.
     * -    Test path of first grade page.
     * -    Check existence of manage subscription button and click on it.
     * -    Test path of subscription page”/subscription”.
     * -    Check existence of manage plan button and click on it
     * -    Test path of plan page”/subscription/plan”.
     * -    Select first plan item in page and click on it.
     * -    Select new term,”missing grade field an issue”
     * -    Select courses.
     * -    Check existence of name and fill it.
     * -    Check existence of fees and fill it.
     * -    Press submit.
     * -    Test path of subscription plan”/subscription/plan”.
     * -    Get subscription page.
     * -    Check existence of manage term button and click on it
     * -    Test path of term page”/subscription/term”.
     * -    Select first term item in page and click on it.
     * -    Press English button.
     * -    Check existence of name field and fill it by faker.
     * -    Press Arabic button.
     * -    Check existence of name and fill it by faker.
     * -    Click start date button.
     * -    utils.exists('#startdate .btn-default.active').click();
     * -    utils.exists('#btn-enddate').click();
     * -    utils.exists('#enddate .btn-default.active').click();
     * -    Press submit.
     * -    Test path of term page”/subscription/term”.
     */
    it('should test Update Subscription Plan', function(done) {
        if (!isLoggedIn) {
            return done();
        }
        utils.get('/admin/country');
        // select first link item
        var e = utils.linkitemFirst();
        e.getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            e.click();
            utils.testPath('/admin/country/' + ids[0]);
            // select first link item
            var e2 = utils.linkitemFirst();
            e2.getAttribute('href').then(function(href) {
                var ids = utils.idsOfLink(href);
                e2.click();
                utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1]);
                // select first link item
                var e3 = utils.linkitemFirst();
                e3.getAttribute('href').then(function(href){
                    var ids = utils.idsOfLink(href);
                    e3.click();
                    utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1] + '/grade/' +ids[2]);
                    utils.exists('#manage-subsription').click();
                    utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1] + '/grade/' +ids[2] + '/subscription');
                    utils.exists('#manage-plans').click();
                    utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1] + '/grade/' +ids[2] + '/subscription/plan');
                    // select first link item
                    var e4 = utils.linkitemFirst();
                    e4.getAttribute('href').then(function() {
                        var ids = utils.idsOfLink(href);
                        e4.click();
                        utils.uiSelectTest('#termid');
                        ptor.waitForAngular();
                        utils.uiSelectTest('#elements div .ui-select-match');
                        utils.uiSelectTest('#elements');
                        utils.exists('#name').sendKeys(faker.name.findName());
                        utils.exists('#fees').sendKeys(_.random(10, 100) + '');
                        utils.submitbutton();
                        utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1] + '/grade/' +ids[2] + '/subscription/plan/');
                        //Update Term
                        utils.get('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1] + '/grade/' +ids[2] + '/subscription');
                        utils.exists('#manage-terms').click();
                        utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1] + '/grade/' +ids[2] + '/subscription/term');
                        var e5 = utils.linkitemFirst();
                        e5.getAttribute('href').then(function() {
                            var ids = utils.idsOfLink(href);
                            e5.click();
                            var langBtnSelector = 'form > translatable > div > button';
                            //select english language input
                            utils.exists(langBtnSelector + ':nth-child(2)').click();
                            utils.exists('#name').sendKeys(faker.name.findName());
                            //select arabic language input
                            utils.exists(langBtnSelector + ':nth-child(3)').click();
                            utils.exists('#name').sendKeys(faker.name.findName());
                            utils.exists('#btn-startdate').click();
                            utils.exists('#startdate .btn-default.active').click();
                            utils.exists('#btn-enddate').click();
                            utils.exists('#enddate .btn-default.active').click();
                            utils.submitbutton();
                            utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1] + '/grade/' +ids[2] + '/subscription/term');
                            done();
                        });
                    });
                });
            });
        });
    });

    /**
     *  Tests admin logout, consist of:
     *  -   User existence to continue.
     *  -   Call function” logout”from utils.
     */
    it ('should logout', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.logout();
    });

});

