"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var testTemplate = require('../test_template.js');
var proxy = testTemplate.proxy;
var utils = new Utils(ptor);
var admin = utils.users.admin;

describe('Admin', proxy(admin.email, admin.password, function() {

    /**
     * tests whole education system cycle, consist of:
     * -    User is logged in to continue.
     * -    Get admin dashboard.
     * -    Country link existence and clicking it.
     * -    Test path redirect to country page.
     * -    Create country by calling “createElement” function in utils.
     * -    Test path country page.
     * -    Click on created country and wait.
     * -    Create educational system by calling “createElement” function in utils.
     * -    Check url contain the country.
     * -    Click on created educational system and wait.
     * -    Create grade by calling “createElement” function in utils.
     * -    Check url contain educational system.
     * -    Click on created grade.
     * -    Create course by calling “createElement” function in utils.
     * -    Click on created course.
     * -    Create prize by calling createElement function in utils
     */
    it('should test educational systems cycle', function() {
         utils.get('/admin');
         utils.exists('a[href="#/admin/country/"]').click();
         utils.testPath('/admin/country');
         var ctry = utils.createElement('country');
         utils.testPath('/admin/country');
         //utils.titleExists(name, false);
         ctry.click();
         ptor.waitForAngular();
         var educ = utils.createElement('edusys');
         expect(ptor.getCurrentUrl()).toContain('country');
         educ.click();
         var grad = utils.createElement('grade');
         expect(ptor.getCurrentUrl()).toContain('educationalsystem');
         grad.click();
         var _crs = utils.createElement('course');
         _crs.click();
         var priz = utils.createElement('prize');
     });

}));
