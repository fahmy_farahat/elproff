"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var testTemplate = require('../test_template.js');
var proxy = testTemplate.proxy;
var utils = new Utils(ptor);
var admin = utils.users.admin;
var PythonShell = require('python-shell');
var path = require('path');

describe('Admin', proxy(admin.email, admin.password, function() {

    /**
     * test user profile, consist of:
     * -    Get admin dashboard.
     * -    Click on User name link.
     * -    Test path of user profile page”/admin/profile”.
     * -    Call function “profileCommonfields”in utils.
     * -    Call function “EditProfile”.
     */
    // Not before he adds countries
    it('should test profile', function(){
        $('.user_name').click();
        utils.testPath('/admin/profile');
        utils.profileCommonfields('/admin/profile');
        utils.EditProfile('/admin/profile');
    });

    /**
     * Tests change password with right password, consist of:
     * -    User logged in to continue.
     * -    Get admin dashboard.
     * -    Check existence of user name link and click on it.
     * -    Test path of admin profile page”/admin/profile”.
     * -    Call function” ChangePassword” and send right admin password to it.
     */
    it('should change password with right password', function() {
        utils.get('/admin/profile');
        utils.ChangePassword('/admin/profile', admin.password);
        utils.successpopup();
    });

    /**
     * Tests change password with wrong password, consist of:
     * -    User logged in to continue.
     * -    Get admin dashboard.
     * -    Check existence of user name link and click on it.
     * -    Test path of admin profile page”/admin/profile”.
     * -    Call function” ChangePassword” and send wrong admin password to it.
     * -    Call function” failpopup” from utils
     */
    it('should change password with wrong current password', function() {
        utils.get('/admin/profile');
        utils.ChangePassword('/admin/profile','675449');
        utils.failpopup();
    });

}));
