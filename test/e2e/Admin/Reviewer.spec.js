"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var testTemplate = require('../test_template.js');
var proxy = testTemplate.proxy;
var utils = new Utils(ptor);
var admin = utils.users.admin;

describe('Admin', proxy(admin.email, admin.password, function() {

    /**
     * Tests admin invite new reviewer, consist of:
     * -    Get admin dashboard page.
     * -    Check existence of reviewer link and click on it.
     * -    Test path of admin reviewer page”/admin/reviewer”.
     * -    Check existence of invite button and click on it.
     * -    Test path of create new reviewer page”/reviewer/create”.
     * -    Call function “invitecommonfields”from utils.
     * -    Select courses value.
     * -    Call function” VerificationImage” from utils.
     * -    Press submit.
     * -    Expect success popup function” successpopup”.
     */
    it('should invite reviewer', function() {
        utils.exists('a[href="#/admin/reviewer/"]').click();
        utils.testPath('/admin/reviewer');
        utils.exists('a[href="#/admin/reviewer/create"]').click();
        utils.testPath('/admin/reviewer/create');
        utils.invitecommonfields();
        utils.uiSelectTest('#courses');
        utils.VerificationImage();
        utils.submitbutton();
        utils.successpopup();
    });

}));
