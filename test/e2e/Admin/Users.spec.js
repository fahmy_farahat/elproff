"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var testTemplate = require('../test_template.js');
var proxy = testTemplate.proxy;
var utils = new Utils(ptor);
var admin = utils.users.admin;
var PythonShell = require('python-shell');
var path = require('path');

describe('Admin', proxy(admin.email, admin.password, function() {

    /**
     * Tests admin activate users, consist of:
     * -    User logged in to continue.
     * -    Get admin dashboard.
     * -    Check existence of users link and click on it.
     * -    Test path of users page.
     * -    Check existence of activate button, and if yes press on it and success popup.
     * -    Else, check existence of deactivate button,and press on it.
     */
    it('should activate users', function(done) {
        utils.exists('a[href="#/admin/users_list/"]').click();
        utils.testPath('/admin/users_list');
        $('.linkitem.opacity').isPresent().then(function(displayed){
            if (displayed) {
                utils.exists('.btn.btn-success.btn-sm.btn-block').click();
                utils.successpopup();
            } else {
                utils.exists('.linkitem .btn.btn-warning.btn-sm.btn-block').click();
            }
            done();
        });
    });

    /**
     * Tests users list page, consist of:
     * -    User is logged in to continue.
     * -    Get admin dashboard.
     * -    User list link existence and clicking on it.
     * -    Test path redirection of user list link.
     * -    User in users by repeater existence.
     * -    Pagination existence. 
     * -    Search testing.
     * -    Pagination test
     * -    Test filter user list by select students as ex.
     * -    Pagination test.
     */
    it ('should test the users list page', function() {
        // TODO: add python user ( Student here )
        utils.get('/admin');
        utils.exists('a[href="#/admin/users_list/"]').click();
        utils.testPath('/admin/users_list');
        ptor.waitForAngular();
        var users_list = element.all(By.repeater('user in users'));
        expect(!!users_list).toBeTruthy();
        utils.pagination('user in users');
        utils.search('a');
        utils.pagination('user in users');
        utils.uiSelectTest('#users_type', 'Student');
        utils.pagination('user in users');
    });

}));
