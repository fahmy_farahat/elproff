"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var testTemplate = require('../test_template.js');
var proxy = testTemplate.proxy;
var utils = new Utils(ptor);
var admin = utils.users.admin;
var PythonShell = require('python-shell');
var path = require('path');

describe('Admin', proxy(admin.email, admin.password, function() {

    /**
     * Tests deleting invitations, consist of:
     * -    User is logged in to continue.
     * -    Get admin dashboard.
     * -    Check existence of invitations link and press on it.
     * -    Test path of invitations page. Check if there are invitations, check existence of button delete and press on it and wait for angular.
     * -    Else get admin dashboard.
     */
    it('should delete invitations', function(done) {
        utils.exists('a[href="#/admin/invitations/"]').click();
        utils.testPath('/admin/invitations');
        $$('.invitation ul li').count().then(function(count){
            if (count > 0) {
                utils.exists('.btn.btn-danger').click();
                ptor.waitForAngular();
            } else {
                utils.get('/admin');
            }
            done();
        });
    });

    /**
     * Tests admin resend invitation, consist of:
     * -    User is logged in to continue.
     * -    Get admin dashboard.
     * -    Check existence of invitation link and press on it.
     * -    Test path of invitation page.
     * -    Check if there are invitations, check existence of button resend and press on it and wait for angular.
     * -    Else get admin dashboard.
     */
    it('should resend invitations', function(done) {
        utils.get("/admin");
        utils.exists('a[href="#/admin/invitations/"]').click();
        utils.testPath('/admin/invitations');
        $$('.invitation ul li').count().then(function(count){
            if (count > 0) {
                utils.exists('.btn.btn-primary').click();
                ptor.waitForAngular();
            } else {
                utils.get('/admin');
            }
            done();
        });
    });
}));
