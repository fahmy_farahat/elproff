"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var testTemplate = require('../test_template.js');
var proxy = testTemplate.proxy;
var utils = new Utils(ptor);
var admin = utils.users.admin;
var PythonShell = require('python-shell');
var path = require('path');

describe('Admin', proxy(admin.email, admin.password, function() {

    /**
     * Tests admin invite new author,consist of:
     * -    Get admin dashboard page.
     * -    Check existence of author link and click on it.
     * -    Test path of admin author page”/admin/author”.
     * -    Check existence of created questions chart.
     * -    Check existence of invite button and click on it.
     * -    Test path of create new author page”/author/create”.
     * -    Call function “invitecommonfields”from utils.
     * -    Select courses value.
     * -    Call function” VerificationImage” from utils.
     * -    Press submit.
     * -    Expect success popup function” successpopup”.
     */
    //it('should invite a new author', function() {
        //utils.exists('a[href="#/admin/author/"]').click();
        //utils.testPath('/admin/author');
        //utils.exists('#questions');
        //utils.exists('a[href="#/admin/author/create"]').click();
        //utils.testPath('/admin/author/create');
        //utils.uiSelectTest('#courses');
        //utils.invitecommonfields();
        //utils.VerificationImage();
        //utils.submitbutton();
        //utils.successpopup();
    //});

    /**
     * Tests adds courses to existing author, consist of:
     * -    Get admin dashboard.
     * -    Check existence of admin author link and click on it.
     * -    Test path of author page”/admin/author”.
     * -    Check existence of questions chart.
     * -    Select first author item and click on it.
     * -    Test path of first author page.
     * -    Count number of existing courses for the author.
     * -    Check existence of add course button and click on it.
     * -    Wait for angular.
     * -    Select course.
     * -    Unfocus courses field by press out field.
     * -    Press ok.
     * -    Expect courses number is increased by one.
     *  
     *  HINT: not pass all time , because sometimes add existing course.
     */
    it('should add courses to existing author', function(done) {

        var scriptPath = path.resolve(__dirname, '..', 'python', 'author');
        var options = {scriptPath: scriptPath};

        PythonShell.run('create.py', options, function (err, results) {
            if (err) {
                throw err;
            }
            console.log(results);
            utils.get("/admin");
            utils.exists('a[href="#/admin/author/"]').click();
            utils.testPath('/admin/author');
            utils.exists('#questions');
            // Works only if there are authors - We need authors 
            var e = utils.linkitemFirst();
            e.getAttribute('href').then(function(href) {
                var ids = utils.idsOfLink(href);
                e.click();
                utils.testPath('/admin/author/' + ids[0]);
                $$('.course').count().then(function(count){
                    var crs_count = count+1;
                    utils.exists('.btn.btn-primary.btn-block').click().then(function(){
                        ptor.waitForAngular();
                        utils.uiSelectTest('#courses');
                        utils.exists('.modal-header').click();
                        utils.exists('#modal_ok').click();
                        done();
                    });
                    ptor.waitForAngular();
                    expect($$('.course').count()).toEqual(crs_count);
                });
            });
        });
    });

    /**
     * test admin review author courses, consist of:
     * •    Get admin dashboard.
     * •    Check existence of author link and click on it.
     * •    Test path of author page”/admin/author”.
     * •    Check existence of questions chart page.
     * •    Select first author item and click on it.
     * •    Test path of first author page.
     * •    Check that number of labels of author tasks is 4.
     * •    Check text of “tasks created” label name.
     * •    Check text of “tasks completed” label name.
     * •    Check text of “questions created” label name.
     * •    Check text of “rejected percentage” label name.
     * •    Expect number of inspect buttons to be 3.
     */
    it('should review author Questions', function(done) {
        utils.get("/admin");
        utils.exists('a[href="#/admin/author/"]').click();
        utils.testPath('/admin/author');
        utils.exists('#questions');
        var e = utils.linkitemFirst();
        e.getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            e.click();
            utils.testPath('/admin/author/' + ids[0]);
            expect($$('div p small').count()).toEqual(4);
            expect($('#taskscreated p').getText()).toBe('Tasks Created');
            expect($('#taskscompleted p').getText()).toBe('Tasks Completed');
            expect($('#questionsCreated p').getText()).toBe('Questions Created');
            expect($('#rejectedprecentage p:nth-child(2)').getText()).toBe('Rejected Precentage');
            expect($$('.btn.btn-info.btn-block').count()).toEqual(3);
            done();
        });
    });
   

}));
