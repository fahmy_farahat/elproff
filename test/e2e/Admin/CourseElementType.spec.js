"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var testTemplate = require('../test_template.js');
var proxy = testTemplate.proxy;
var utils = new Utils(ptor);
var admin = utils.users.admin;

describe('Admin', proxy(admin.email, admin.password, function() {

    /**
     * Creates course element types, consist of:
     * -    User is logged in to continue.
     * -    Get admin dashboard.
     * -    Element types link existence and click on it.
     * -    Create element type by calling createElement function in utils.
     */
    it('should create Course Element Types', function() {
        utils.exists('a[href="#/admin/element_type/"]').click();
        var crs_ele = utils.createElement('course-element-type');
    });

}));
