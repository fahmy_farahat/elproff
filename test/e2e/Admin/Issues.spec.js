"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var utils = new Utils(ptor);
describe('issues in admin side', function() {

    var admin = utils.users.admin;
    var isLoggedIn = false;


    it ('should login once', function(done) {
        utils.login(admin.email, admin.password);
        $('.user_name').isDisplayed().then(function(result) {
            isLoggedIn = result;
            done();
        });
    });

    //there is issue here
    it('should test login form if already LoggedIn', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.get('/admin');
        utils.testPath('/admin');
        utils.get('/login');
        ptor.waitForAngular();
        utils.testPath('/admin');
    });

    it('should test phone field format accept only numbers and not exceed 20 digits in edit profile', function() {
        if (!isLoggedIn) {
            return;
        }
        $('.user_name').click();
        $('#editprofile').click();
        expect($('#telephone').getAttribute('maxlength')).toEqual('20');
        $('#telephone').clear().sendKeys('ab1234567890123456789012');
        expect($('#telephone').getAttribute('value')).toEqual('12345678901234567890');
    });

    it ('should click any link to open in new window and assert that the correct page is open', function() {
        if (!isLoggedIn) {
            return;
        }
        var el = $('a[href="#/admin/reviewer/"]');
        //new Action(window).keyDown(Keys.CONTROL).click(el).keyUp(Keys.CONTROL).build().perform();
    });

    //has issue about manage button redirection
    it('should manage button in sme view redirect to course page', function(done) {
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/admin");
        utils.exists('a[href="#/admin/sme/"]').click();
        utils.testPath('/admin/sme');
        var e = utils.linkitemFirst();
        e.getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            e.click();
            utils.testPath('/admin/sme/' + ids[0]);
            $('div.course').getAttribute('id').then(function(course_id) {
                course_id = course_id.replace("_","/");
                utils.exists('.course-body .buttons .btn.btn-primary.btn-block.view_course').click();
                expect(ptor.getCurrentUrl()).toContain(course_id);
                done();
            });
        });
    });

    it('should keep logging to system while switching language', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.get('/admin');
        //change language to arabic
        utils.switchLanguage('arabic');
        ptor.waitForAngular();
        utils.testPath('/admin');
        //change language to english again
        utils.switchLanguage('english');
        ptor.waitForAngular();
        utils.testPath('/admin');
    });
    it('should test that update country redirect to country page not dashboard page',function(){
        if (!isLoggedIn) {
            return;
        }
        utils.get('/admin/country');
        // select first link item
        var e = utils.linkitemFirst();
        e.getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            e.click();
            utils.testPath('/admin/country/' + ids[0]);
            utils.exists('#update-country').click();
            var newCtryName = faker.name.findName();
            utils.exists('#name').clear().sendKeys(newCtryName);
            utils.submitbutton();
            utils.testPath('/admin/country/' + ids[0]);
            expect($('h3.landing_title').getText()).toMatch(newCtryName);
        });
    });

    it('should test that update educational system redirect to educational system page not dashboard page and sure that all fields updated',function(){
        if (!isLoggedIn) {
            return;
        }
        utils.get('/admin/country');
        // select first link item
        var e = utils.linkitemFirst();
        e.getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            e.click();
            utils.testPath('/admin/country/' + ids[0]);
            var e2 = utils.linkitemFirst();
            $('.linkitem a').isPresent().then(function(present){
                if (present === false) {
                    utils.createElement('edusys');
                }
            });
            e2.getAttribute('href').then(function(href) {
                var ids = utils.idsOfLink(href);
                e2.click();
                utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1]);
                utils.exists('#update-edusys').click();
                var newEduName = faker.name.findName();
                utils.exists('#name').clear().sendKeys(newEduName);
                var newDesc = faker.name.findName();
                utils.exists('#description').clear().sendKeys(newDesc);
                utils.submitbutton();
                utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1]);
                expect($('h3.landing_title').getText()).toMatch(newEduName);
                utils.exists('#update-edusys').click();
                expect($('#description').getAttribute('value')).toEqual(newDesc);
            });
        });
    });


    it('should test change language button of country name field in create country page with switching system lannguage',function(){
        if (!isLoggedIn) {
            return;
        }
        utils.get('/admin/country/create');
        utils.langButton();
    });

    it('should test author view translate', function(done) {
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/admin");
        utils.exists('a[href="#/admin/author/"]').click();
        utils.testPath('/admin/author');
        var e = element.all(By.css('.linkitem a')).first();
        e.getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            utils.get('/admin/author/' + ids[0]);
            utils.switchLanguage('arabic');
            ptor.waitForAngular();
            expect($('h3.landing_title').getText()).toMatch('عرض');
            utils.switchLanguage('english');            
            done();
        });
    });

    it('should test email field reset on success invitation', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.get("/admin");
        utils.exists('a[href="#/admin/author/"]').click();
        utils.testPath('/admin/author');
        utils.exists('a[href="#/admin/author/create"]').click();
        utils.testPath('/admin/author/create');
        utils.uiSelectTest('#courses');
        utils.invitecommonfields();
        utils.VerificationImage();
        utils.submitbutton();
        utils.successpopup();
        expect(element(By.model('model.email')).getText()).toEqual('');
    });

    it('should assert that profiles link redirect to right page',function(){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/admin/");
        $('#userMenu').click();
        utils.exists('#userMenu_profiles').click();
        utils.testPath('/admin/profile');
        utils.exists('.student_info.noborder');
        utils.exists('.right_menu');
    });

    it('should test invite author text in author page', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.get("/admin/");
        utils.exists('a[href="#/admin/author/"]').click();
        utils.testPath('/admin/author');
        utils.switchLanguage('arabic');
        ptor.waitForAngular();
        expect($('h3 a').getText()).toMatch(/دعوة/);
        utils.switchLanguage('english');
    });

    it('should test captcha update on fail invitation', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.get("/admin");
        utils.exists('a[href="#/admin/author/"]').click();
        utils.testPath('/admin/author');
        utils.exists('a[href="#/admin/author/create"]').click();
        utils.testPath('/admin/author/create');
        utils.uiSelectTest('#courses');
        var captcha_before = utils.exists('#recaptcha_challenge_image').getAttribute('src');
        utils.invitecommonfields();
        utils.exists('#recaptcha_response_field').sendKeys('anyWord');
        utils.submitbutton();
        utils.failpopup();
        expect($('#recaptcha_challenge_image').getAttribute('src')).not.toEqual(captcha_before);
    });

    it('should test captcha update on success invitation', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.get("/admin");
        utils.exists('a[href="#/admin/author/"]').click();
        utils.testPath('/admin/author');
        utils.exists('a[href="#/admin/author/create"]').click();
        utils.testPath('/admin/author/create');
        utils.uiSelectTest('#courses');
        var captcha_before = utils.exists('#recaptcha_challenge_image').getAttribute('src');
        utils.invitecommonfields();
        utils.VerificationImage();
        utils.submitbutton();
        utils.successpopup();
        expect($('#recaptcha_challenge_image').getAttribute('src')).not.toEqual(captcha_before);
    });

    it('should test captcha error invite subject Expert', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.get('/admin/sme/create');
        utils.testPath('/admin/sme/create');
        utils.invitecommonfields();
        utils.uiSelectTest('#courses');
        utils.exists('#recaptcha_response_field').sendKeys('test');
        utils.submitbutton();
        utils.failpopup();
    });

    it('shoud test deactivated courses does not appear for students', function(done) {
        if (!isLoggedIn) {
            return;
        }
        utils.get("/admin/");
        utils.exists('a[href="#/admin/country/"]').click();
        utils.testPath('/admin/country');
        var country = '';
        var edusys = '';
        var grade = '';
        var courseId = '';
        utils.linkitemFirst().getText().then(function(co) {
            country = co;
            utils.linkitemFirst().click().getText().then(function(sys) {
                edusys = sys;
                utils.linkitemFirst().click().getText().then(function(gr) {
                    grade = gr;
                    utils.linkitemFirst().click();
                    element(By.css('button.btn-success')).click();
                    element(By.css('#deactivate-course')).click();
                    ptor.waitForAngular();
                    utils.logout();
                    var student = utils.users.student;
                    utils.login(student.email, student.password);
                    element(By.css('#userMenu')).click();
                    element(By.css('#userMenu_editProfile')).click();
                    console.log(country);
                    utils.uiSelectTest('#country', country);
                    element(By.css('#state')).clear().sendKeys(faker.name.findName());
                    console.log(edusys);
                    utils.uiSelectTest('#system', edusys);
                    element(By.css('#city')).clear().sendKeys(faker.name.findName());
                    utils.uiSelectTest('#grade', grade);
                    utils.submitbutton();
                    element(By.css('.dashboard')).click();
                    expect($('.opacity').getAttribute('class')).toEqual('course opacity');
                    utils.logout();
                    utils.login(admin.email, admin.password);
                    done();
                });
            });
        });
    });

    it('should logout', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.logout();
    });

});
