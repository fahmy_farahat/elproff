"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var utils = new Utils(ptor);
describe('Admin', function() {
    var admin = utils.users.admin;
    var isLoggedIn = false;

    it ('should login once', function(done) {
        utils.login(admin.email, admin.password);
        $('.user_name').isDisplayed().then(function(result) {
            isLoggedIn = result;
            done();
        });
    });

    it ('should test country crumb hierarchy',function(done){
        if (!isLoggedIn) {
            return done();
        }
        utils.get('/admin/country');
        var $country = utils.linkitemFirst();
            $country.getAttribute('href').then(function(href) {
            $country.click();
            var ids = utils.idsOfLink(href);
            utils.testPath('/admin/country/' + ids[0]);
            utils.dashboardCrumb();
            utils.testPath('/admin');
            done();
        });
    });

    it('should test country crumb works',function(done){
        if (!isLoggedIn) {
            return done();
        }
        utils.get('/admin/country');
        var $country = utils.linkitemFirst();
            $country.getAttribute('href').then(function(href) {
            $country.click();
            var ids = utils.idsOfLink(href);
            utils.testPath('/admin/country/' + ids[0]);
            var $educational = utils.linkitemFirst();
                $educational.getAttribute('href').then(function(href) {
                $educational.click();
                var ids = utils.idsOfLink(href);
                utils.testPath('/admin/country/' + ids[0] + '/educationalsystem/' + ids[1]);
                utils.crumbs();
                utils.testPath('/admin/country/' + ids[0]);
                done();
            });
        });
    });

    it('should logout', function() {
        if (!isLoggedIn) {
            return ;
        }
        utils.logout();
    });
});
