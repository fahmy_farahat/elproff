"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var testTemplate = require('../test_template.js');
var proxy = testTemplate.proxy;
var utils = new Utils(ptor);
var admin = utils.users.admin;

describe('Admin', proxy(admin.email, admin.password, function() {

    /**
     * Tests admin invite subject expert, consist of:
     * -    Get admin dashboard page.
     * -    Check existence of subject expert link and click on it.
     * -    Test path of admin sme page”/admin/sme”.
     * -    Check existence of invite button and click on it.
     * -    Test path of create new sme page”/sme/create”.
     * -    Call function “invitecommonfields”from utils.
     * -    Select courses value.
     * -    Call function” VerificationImage” from utils.
     * -    Press submit.
     * -    Expect success popup function” successpopup”.
     */
    it('should invite subject Expert', function() {
        utils.exists('a[href="#/admin/sme/"]').click();
        utils.testPath('/admin/sme');
        utils.exists('a[href="#/admin/sme/create"]').click();
        utils.testPath('/admin/sme/create');
        utils.invitecommonfields();
        utils.uiSelectTest('#courses');
        utils.VerificationImage();
        utils.submitbutton();
        utils.successpopup();
    });

}));
