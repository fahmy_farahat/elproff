"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('./utils.js').Utils;
var utils = new Utils(ptor);

exports.proxy = function(email, password, mainTestScenario) {
    return function() {

        /* 
        * Test login once, by calling login function in utils and send
        * admin credentials.
        */
        it ('should login once', function() {
            utils.login(email, password);
        });

        mainTestScenario();

        /**
        *  Tests admin logout, consist of:
        *  -   User existence to continue.
        *  -   Call function” logout”from utils.
        */
        it ('should logout', function() {
            utils.logout();
        });

    };
};
