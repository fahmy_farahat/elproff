#!/usr/local/bin/python
# -*- coding: UTF-8 -*-
import sys
import os
seven_path = os.environ.get("SEVEN_PATH")
sys.path.insert(0, seven_path)
# print seven_path
# print sys.path
if not seven_path:
    sys.exit(0)

from core.models import BLOOM_OUTCOME, DIFFICULTIES
from courses.models import Course, CourseElement
from questions.models import Question, Review
from seven_users.models import Reviewer, SevenUser, Author
from task.controller import reviewer_available_tasks
from task.models import Task, Objective, QuestionsBulk
import collections
import django
django.setup()

# Loading test related data
author_email = os.environ.get("USER_AUTHOR") 
admin_email = os.environ.get("USER_ADMIN")
sme_email = os.environ.get("USER_SME")
author_email = os.environ.get("USER_AUTHOR")
reviewer_email = os.environ.get("USER_REVIEWER")
student_email = os.environ.get("USER_STUDENT")
parent_email = os.environ.get("USER_PARENT")
privateteacher_email = os.environ.get("USER_PRIVATETEACHER")
schoolteacher_email = os.environ.get("USER_SCHOOLTEACHER")
schooladmin_email = os.environ.get("USER_SCHOOLADMIN")
manager_email = os.environ.get("USER_MANAGER")


def exists(email=author_email):
    """
    Tests user existance
    """
    user = Author.objects.filter(user__email=email.lower())
    if len(user):
        print "true"
    else:
        print "false"


if __name__ == "__main__":
    exists()

