#!/usr/local/bin/python
# -*- coding: UTF-8 -*-
from __future__ import with_statement
import sys
import os
seven_path = os.environ.get("SEVEN_PATH")
sys.path.insert(0, seven_path)
# print seven_path
# print sys.path
if not seven_path:
    sys.exit(0)

from core.models import BLOOM_OUTCOME, DIFFICULTIES
from courses.models import Course, CourseElement
from questions.models import Question, Review
from seven_users.models import Reviewer, SevenUser, Author
from task.controller import reviewer_available_tasks
from task.models import Task, Objective, QuestionsBulk
import collections
import django
django.setup()

# Loading test related data
author_email = os.environ.get("USER_AUTHOR") 
admin_email = os.environ.get("USER_ADMIN")
sme_email = os.environ.get("USER_SME")
author_email = os.environ.get("USER_AUTHOR")
reviewer_email = os.environ.get("USER_REVIEWER")
student_email = os.environ.get("USER_STUDENT")
parent_email = os.environ.get("USER_PARENT")
privateteacher_email = os.environ.get("USER_PRIVATETEACHER")
schoolteacher_email = os.environ.get("USER_SCHOOLTEACHER")
schooladmin_email = os.environ.get("USER_SCHOOLADMIN")
manager_email = os.environ.get("USER_MANAGER")


def create_author(email=author_email, name="test_author"):
    """
    Creates an author
    @param  email     The author email
    @param  name      The name of the author
    """
    try:
        user = SevenUser.objects.get(email=email.lower())
        print "Found author"
    except SevenUser.DoesNotExist:
        user = SevenUser.objects.create(
            email=email.lower(), full_name=name, is_active=True)
        user.set_password('123456')
        user.save()
        user.set_profile(Author)
        print "Created a new author"


if __name__ == "__main__":
    create_author()

