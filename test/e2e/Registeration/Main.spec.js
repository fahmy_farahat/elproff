"use strict";
var faker = require('faker');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var _ = require('lodash');
var utils = new Utils(ptor);

describe('Registeration', function(){
    beforeEach(function () {
        utils.get('/register');
    });
    var student = utils.users.student;
    var parent = utils.users.parent;

    it('should test press register withot filling any data', function(done){
        $('[type="submit"]').click();
        browser.waitForAngular();
        utils.exists('input-fullname .validation.error.red');
        utils.exists('input-password .validation.error.red');
        utils.exists('input-cpassword .validation.error.red');
        utils.exists('.form-group.type .validation.error.red');
        utils.exists('input-acceptlicence .validation.error.red');
        expect($$('.validation.error').count()).toEqual(6);
        expect($$('.validation.error').get(0).getText()).toEqual('Field is required');
        done();
    });

    it('should test register withot check licence agreements', function(done){
        utils.get('/register');
        utils.exists('#full_name').sendKeys('test');
        utils.exists('#email').sendKeys(faker.internet.email());
        utils.exists('#password').clear().sendKeys('123456');
        utils.exists('#cpassword').clear().sendKeys('123456');
        utils.exists('#parent').click();
        utils.uiSelectTest('#country');
        utils.exists('#recaptcha_response_field').sendKeys('Pypass');
        $('[type="submit"]').click();
        browser.waitForAngular();
        utils.testPath('/register');
        expect($$('.validation.error').count()).toEqual(1);
        expect($('input-acceptlicence .validation.error.red').getText()).toBe('Field is required');
        done();
    });

    it('should display alert msg incase verification image wrong value', function(done){
        utils.get('/register');
        utils.exists('#full_name').sendKeys('test');
        utils.exists('#email').sendKeys(faker.internet.email());
        utils.exists('#password').clear().sendKeys('123456');
        utils.exists('#cpassword').clear().sendKeys('123456');
        utils.exists('#parent').click();
        utils.uiSelectTest('#country');
        utils.exists('#recaptcha_response_field').sendKeys('test');
        utils.exists('#accept').click();
        $('[type="submit"]').click();
        utils.failpopup();
        expect($('.alert.alert-danger.alert-dismissable div span:nth-child(1)').getText()).toBe('The text you entered does not match the text in image.');
        done();
    });

    it ('should register student', function(done) {
        utils.get('/register');
        $('#student').click();
        ptor.waitForAngular();
        var st = utils.registercommonfields(faker.internet.email());
        student.email = st.email;
        ptor.waitForAngular();
        $('#system_label .success-result').isPresent().then(function(res){
            if(res)
            {
                utils.uiSelectTest('#system');
            }
            else
            {
                utils.uiSelectTest('#country');
            }
        });
        
        utils.uiSelectTest('#system');
        ptor.waitForAngular();
        ptor.waitForAngular();
        $('#grade_label .success-result').isPresent().then(function(result){
            if(result)
            {
                utils.uiSelectTest('#grade');
            }
            else
            {
                utils.uiSelectTest('#system');
            }
        });
        utils.uiSelectTest('#grade');
        
        element(By.model('model.schoolname')).sendKeys(faker.name.findName());
        utils.VerificationImage();
        element(By.model('model.accepted')).click();
        utils.submitbutton();
        utils.activateAccount(student.email).then(function(activated) {
            student.activated = activated;
            done();
        });
    });

    it('should test translation register', function(done){
        $('#lang_ar').click();
        browser.waitForAngular();
        expect($$('#parent').getText()).toMatch('ولي أمر');
        expect($$('#student').getText()).toMatch('طالب');
        expect($$('.check a').getText()).toMatch('اتفاقية الترخيص');
        done();
    });

    it('should test picklist search', function(done){
        utils.get('/register');
        utils.exists('#country').click();
        utils.exists('#country input').sendKeys('EGY');
        utils.uiSelectTest('#country');
        expect($('#country').getText()).toMatch(/EGY/);
        done();
    });

    it('should test password length', function(){
        utils.Passwordsfields();
    });
});
