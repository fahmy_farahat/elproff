"use strict";
var faker = require('faker');
var ptor = browser;
var _ = require('lodash');
var Utils = require('../utils.js').Utils;
var utils = new Utils(ptor);

describe('School Admin',function(){

    var schooladmin = utils.users.schooladmin;
    var isLoggedIn = false;
     /* Checks if school admin exists, consist of:
     * -    Check school admin exist in DB .
     */
    it ('checks if School ' + schooladmin.email + ' exists', function(done) {
        utils.db.userExists(schooladmin.email, function(result) {
            schooladmin.exists = !!result;
            done();
        });
    });
    /*register school admin
    *   -check if school admin exist 
    *   -set schooladmin.email by faker
    *   -get register page
    *   -select school admin button
    *   -wait for angular
    *   -call function"registercommonfields" and send school admin email
    *   -set school admin email value.
    *   -wait for angular.
    *   -fill school name by faker
    *   -call function "VerificationImage"
    *   -click accept checkbox
    *   -select educational system value
    *   -press submit
    *   -set var processed to false
    *   -call function"activateAccount" and send school admin email.
    */
    it ('should register School', function(done){
        if (schooladmin.exists) {
            schooladmin.email = faker.internet.email();
        }
        utils.get('/register');
        $('#schooladmin').click();
        ptor.waitForAngular();
        var sch = utils.registercommonfields(schooladmin.email);
        schooladmin.email = sch.email;
        ptor.waitForAngular();
        element(By.model('model.schoolname')).sendKeys(faker.name.findName());
        utils.VerificationImage();
        element(By.model('model.accepted')).click();
        utils.uiSelectTest('#systems');
        utils.submitbutton();
        var processed = false;
        utils.activateAccount(schooladmin.email).then(function(result) {
            schooladmin.activated = !!result;
            done();
        });
    }, utils.timeout);
    /*Login as school admin, consist of:
     * -    check if school admin not activated .
     * -    expect school admin email is activated.
     * -    Call login function in utils and send school admin credentials.
     * -    Check if user name link is displayed , so school admin is logged in.
     */    
     it('should login', function(done) {
        if (!schooladmin.activated) {
            expect(schooladmin.email).toBe('activated');
            return done();
        }
        utils.login(schooladmin.email, '123456');
        $('.user_name').isDisplayed().then(function(result) {
            isLoggedIn = result;
        });
    });
    /*Tests school admin profile, consist of:
     * -    User is logged in to continue.
     * -    Get school admin dashboard.
     * -    Click user name link.
     * -    Test path of school admin profile.
     * -    Call function “profileCommonfields”.
     * -    Call function “EditProfile”.
    */
    it('should test profile', function(done){
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/schooladmin/");
        $('.user_name').click();
        utils.testPath('/schooladmin/profile');
        utils.profileCommonfields('/schooladmin/profile');
        utils.EditProfile('/schooladmin/profile');
    });
   /*test change password with right password, consist of:
           -User logged in to continue.
           -Get school admin dashboard.
           -Check existence of user name link and click on it.
           -Test path of school admin profile page”/schooladmin/profile”.
           -Call function” ChangePassword” and send right school admin password to it.
    */
    it('should change password with right password', function(done){
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/schooladmin/");
        $('.user_name').click();
        utils.testPath('/schooladmin/profile');
        utils.ChangePassword('/schooladmin/profile','123456');
        utils.successpopup();
    });
    /*test change password with wrong password, consist of:
     * -    User logged in to continue.
     * -    Get school admin dashboard.
     * -    Check existence of user name link and click on it.
     * -    Test path of school admin profile page”/schooladmin/profile”.
     * -    Call function” ChangePassword” and send wrong school admin password to it.
     * -    Call function” failpopup” from utils.
    */
    it('should change password with wrong current password', function(done){
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/schooladmin/");
        $('.user_name').click();
        utils.testPath('/schooladmin/profile');
        utils.ChangePassword('/schooladmin/profile','675449');
        utils.failpopup();
    });
   /*Tests school admin has common links, consist of:
     * -    User is loged in to continue.
     * -    Call function “checkCommonLinks”from utils.
    */
    it('should check common links',function(done){
        if (!isLoggedIn) {
            return done();
        }
        utils.checkCommonLinks();
    });
    /*check user images
    *   -user is logged in to continue.
    *   -set var images 
    *   -expect images count to be 5
    */
    it('should check users images',function(done){
        if (!isLoggedIn) {
            return done();
        }
        var images = $$('img[title="img"]');
        expect(images.count()).toEqual(5);
    });
    /*contain following links
    *   -user is logged in to continue
    *   -get school admindashboard.
    *   -check existence of link managers
    *   -check existence of link teachers
    *   -check existence of link students  
    */
    it('should contain the following link buttons',function(done){
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/schooladmin/");
        utils.exists('a[href="#/schooladmin/manager"]');
        utils.exists('a[href="#/schooladmin/teacher"]');
        utils.exists('a[href="#/schooladmin/student"]');
    });
    /*add manager
    *   -user is logged in to continue
    *   -get school admin dashboard page
    *   -click manager link
    *   -test path of school admin manager page
    *   -press add manager link
    *   -test path of add manager page
    *   -call function "invitecommonfields" from utils
    *   -call function "VerificationImage" from utils.
    *   -press submit button
    *   -call successpopup function.
    */
    it('should add manager',function(done){
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/schooladmin/");
        $('a[href="#/schooladmin/manager"]').click();
        utils.testPath('/schooladmin/manager');
        $('a[href="#/schooladmin/manager/add"]').click();
        utils.testPath('/schooladmin/manager/add');
        utils.invitecommonfields();
        utils.VerificationImage();             
        utils.submitbutton();
        utils.successpopup();
    });
    /*add teacher
    *   -user is logged in to continue.
    *   -get school admin dashboard
    *   -click teachers link
    *   -test path of teacher page
    *   -click add new teacher link
    *   -test path of add teacher page
    *   -call function "invitecommonfields" from utils
    *   -call function "VerificationImage" from utils
    *   -call function "submitbutton" from utils
    *   -call function "successpopup" from utils
    */
    it('should add teacher',function(done){
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/schooladmin/");
        $('a[href="#/schooladmin/teacher"]').click();
        utils.testPath('/schooladmin/teacher');
        $('a[href="#/schooladmin/teacher/add"]').click();
        utils.testPath('/schooladmin/teacher/add');
        utils.invitecommonfields();
        utils.VerificationImage();
        utils.submitbutton();
        utils.successpopup();
    });
    /*add student
    *   -user is logged in to continue
    *   -get school admin dashboard
    *   -click students link
    *   -test path of students page
    *   -
    */
    it('should add student',function(done){
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/schooladmin/");
        element(By.css('a[href="#/schooladmin/educationalsystems"]')).click();

        utils.testPath('/schooladmin/educationalsystems');
        var e = utils.linkitemFirst();
        e.getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            e.click();
            utils.testPath('"/schooladmin/educationalsystems' + ids[0] + '"');
        
        });
    });
    /*
    *
    *
    *
    */
    it('should add course',function(done){
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/schooladmin/");
        element(By.css('a[href="#/schooladmin/course"]')).click();
        utils.testPath('/schooladmin/course');
    });
    /*
    *
    *
    *
    */

    it('should add classes',function(done){
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/schooladmin/");
        element(By.css('a[href="#/schooladmin/classes"]')).click();
        utils.testPath('/schooladmin/classes');
    });
    /*Logout school admin, consist of:
     * -    User is logged in to continue.
     * -    Call function logout from utils.
    */

    it('should logout', function(done) {
        if (!isLoggedIn) {
            return done();
        }
        utils.logout();
    });
});

