"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var utils = new Utils(ptor);

describe('Author', function() {

    var sme = utils.users.sme;
    var author = utils.users.author;
    var isLoggedIn = false;
    /*login as author, consist of:
    *   -call login function and send author credentials.
    *   -test path of author dashboard.
    *   -Check if user name link is displayed , so Author is logged in.
    */

    it('should login', function(done) {
        utils.login(author.email, author.password);
        utils.testPath('/author');
        $('.user_name').isDisplayed().then(function(result) {
            isLoggedIn = result;
            done();
        });
    });
    /*create questions,consist of:
    *   -user is logged in to cintinue.
    *   -get author courses page.
    *   -select first course item in page and press on its tasks button.
    *   -wait for angular.
    *   -test path of first course tasks page.
    *   -set question types in array.
    *   -each type:{
    *   -count
    *   -if count =0
    *   -press add task button.
    *   -wait for angular.
    *   -self assign of type.
    *   -press tasks link in crumbs.
    *   -test path of couurse tasks.
    *   -wait for angular.
    *   -check existence of add questions button and press it.
    *   -call function "QuestionBlockComponants" from utils.
    *   -get count value.
    *   -get questions count value.
    *   -get text of total count value and convert it to intger.
    *   -get text of questions value and convert it to intger.
    *   -expect question value to be less than total count value.
    *   -for loop works as long question value less than total count value.
    *   -set avariable "isfillblanks" to be false.
    *   -->>question type"true/false".
    *   -call function"QcreateCommonfields" from utils and send var "isfillblanks".
    *   -call function"TrueFalseQuestion" from utils and send var"questions_val".
    *   -->>question type"mcq" or "multi_mcq".
    *   -call function"QcreateCommonfields" from utils and send var "isfillblanks".
    *   -call function"Addchoices"from utils and send vars"'addMoreChoices','question-choice-0','useranswer-0'".
    *   -call function"removeChoices"from utils.
    *   -->>question type"dragdrop".
    *   -call function"QcreateCommonfields" from utils and send var "isfillblanks".
    *   -call function"DragAndDrop" from utils.
    *   -call function"removeChoices"from utils.
    *   -->>question type"fillblanks".
    *   -var "isfillblanks" to be true.
    *   -call function"QcreateCommonfields" from utils and send var "isfillblanks".
    *   -call function"fillBlanks" from utils.
    *   -call function"removeChoices"from utils.}
    *   -check existence of save and continue button and click it.
    *   -call function"failpopup" from utils.
    *   -call function"alertpopupmessage" and send"Please, solve the question to save the correct answer."
    *   -check if total count value minus questions value equal one.
    *   -test path of tasks page.
    *   -otherwise,call function"successpopup" from utils.
    */
    it ('should create questions', function(done) {
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/author/course");
        utils.itemInList(true).getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            var id = ids[0];
            ptor.waitForAngular();
            utils.exists('#course_' + id + ' .buttons a').click();
            ptor.waitForAngular();
            utils.testPath('/author/course/' + id + '/task');
            var Questions_Types = ['dragdrop','fillblanks' , 'mcq', 'multi_mcq','truefalse'];//
            _.each(Questions_Types,function(Questions_Type){
                //check if Question type exists in tasks and if not add one from add to my tasks.
                $$('[data-type="' + Questions_Type + '"]').count().then(function(count){
                    if (count === 0) {
                        $('a[href="#/author/course/' + id + '/task/create"]').click();
                        ptor.waitForAngular();
                        $('[data-type=' + Questions_Type +'] #selfassign').click();
                        $('#crumb_3').click();
                        ptor.waitForAngular();
                        utils.testPath('/author/course/' + id + '/task');
                    }
                });
                //aading question of each type.
                ptor.waitForAngular();
                var _type = '[data-type="' + Questions_Type + '"] #addquestions';
                utils.exists(_type).click();
                utils.QuestionBlockComponants();
                var counts_val = 0 ;
                var Questions_val = 0;
                var counts = $('[k="count"] .value');
                //count required num of the questions
                counts.getText().then(function(counts_String){
                    var counts_val = parseInt(counts_String);
                    //count added questions to compare with required num.
                    var Questions = $('[k="questions"] .value');
                    Questions.getText().then(function(Questions_String) {
                        Questions_val = parseInt(Questions_String);
                        expect(Questions_val).toBeLessThan(counts_val);
                        for(Questions_val; Questions_val < counts_val; Questions_val++) {
                            var isfillBlanks = false;
                            if(Questions_Type === 'truefalse') {
                                utils.QcreateCommonfields(isfillBlanks);
                                utils.TrueFalseQuestion(Questions_val);
                            } else if (Questions_Type === 'mcq' || Questions_Type === 'multi_mcq') {
                                utils.QcreateCommonfields(isfillBlanks);
                                utils.Addchoices('addMoreChoices','question-choice-0','useranswer-0');
                                utils.removeChoices();
                            } else if (Questions_Type === 'dragdrop') {
                                utils.QcreateCommonfields(isfillBlanks);
                                utils.DragAndDrop();
                                utils.removeChoices();
                            } else if (Questions_Type === 'fillblanks') {
                                isfillBlanks = true;
                                utils.QcreateCommonfields(isfillBlanks);
                                utils.fillBlanks();
                                utils.removeChoices();
                            }
                            utils.exists('.btn.btn-success.btn-block').click();
                            utils.failpopup();
                            utils.alertpopupmessage('Please, solve the question to save the correct answer.');
                             if ((counts_val - Questions_val) === 1 ) {
                                utils.testPath('/author/course/' + id + '/task');
                            } else {
                                utils.successpopup();
                            }
                        }
                    });
                });   
            });
         done();
        });
    });
    /*Logout Author, consist of:
     * -    User is logged in to continue.
     * -    Call function logout from utils.
    */
    it('should logout', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.logout();
    });
});