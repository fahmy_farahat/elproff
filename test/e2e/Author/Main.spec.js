"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var utils = new Utils(ptor);

describe('Author', function() {

    var sme = utils.users.sme;
    var author = utils.users.author;
    var isLoggedIn = false;
    /**
     * Checks if Author exists, consist of:
     * -    Check author exist in DB .
     */

    it('checks if author exists', function(done) {
        utils.db.userExists(author.email, function(result) {
            author.exists = !!result;
            done();
        });
    });
    /**
     * Checks if SME exists, consist of:
     * -    Check sme exist in DB .
     *      if not exist diaplay msg to run sme suit first.
     */

    it('checks if sme exists', function(done) {
        utils.db.userExists(sme.email, function(result) {
            sme.exists = !!result;
            if (!sme.exists) {
                console.log("SME user doesn't exist, in order to continue this test you have to run sme suite first");
            }
            done();
        });
    });
    /*login as sme ,consist of
    *  -check existence of author or sme.
    *  -call login function and send sme credentials.
    */
    it('should login as sme', function(done) {
        if (author.exists || !sme.exists) {
            return done();
        }
        utils.login(sme.email, sme.password);
        done();
    });
    /*Sends invitation from SME to Author, consist of:
     * -    Check If author or sme exist, so return.
     * -    Else test path of SME dashboard.
     * -    Check existence of Authors link and click on it.
     * -    wait for angular.
     * -    Test path of author page "/sme/author".
     * -    Check existence of invite button and click on it.
     * -    wait for angular.
     * -    Test path of Author invite page "sme/author/create".
     * -    Call function” invitecommonfields” from utils and send author email to it.
     * -    Select course.
     * -    Call function” VerificationImage”.
     * -    Press submit.
     * -    Success popup function calling.
    *
    *
    */

    it('should get invited by a Subject-Expert', function(done) {
        if (author.exists || !sme.exists) {
            return done();
        }
        utils.testPath('/sme');
        utils.exists('a[href="#/sme/author/"]').click();
        browser.waitForAngular();
        utils.testPath('/sme/author');
        utils.exists('a[href="#/sme/author/create"]').click();
        browser.waitForAngular();
        utils.testPath('/sme/author/create');
        utils.invitecommonfields(author.email);
        utils.uiSelectTest('#courses');
        utils.VerificationImage();
        utils.submitbutton();
        utils.successpopup();
        done();
    });
    /*Logout as SME and get the invitation code, consist of:
     * -    Check if author or SME exists return.
     * -    Else call function logout.
     *      Send invitation to db and deal with it.
    *
    *
    */
    it('should logout from the sme and get the invitation code', function(done) {
        if (author.exists || !sme.exists) {
            return done();
        }
        utils.logout();
        utils.db.getInvitation(author.email, function(invitation) {
            if (invitation) {
                author.invitation = invitation;
            } else {
                console.log("There's no invitation!");
            }
            expect(!!author.invitation).toBeTruthy();
            done();
        });
    });
    /*Accept invitation, consist of:
     * -    Check if Author or SME exist so return.
     * -    Get”/invitation/ +author.invitation”.
     * -    Check existence of full name field and fill it with faker.
     * -    Check existence of password field and fill it.
     * -    Check existence of confim password and fill it.
     * -    Select country.
     * -    Call function “VerificationImage” in utils.
     * -    Check existence of accept checkbox and click it.
     * -    Press submit button.
     * -    Check existence of login button and click on it.
     * -    Test path of login page.
    *
    *
    */
    it('should accept invitation', function(done) {
        if (author.exists || !sme.exists) {
            return done();
        }
        utils.get('/invitation/' + author.invitation);
        element(By.model('model.full_name')).sendKeys(faker.name.findName());
        author.password = '123456';
        element(By.model('model.password')).sendKeys(author.password);
        element(By.model('model.cpassword')).sendKeys(author.password);
        utils.uiSelectTest('#country');
        utils.VerificationImage();
        element(By.id('accept')).click();
        utils.submitbutton();
        $('a[href="#/login"]').click();
        utils.testPath('/login');
    });
     /*Login as Athor, consist of:
     * -    exoect author existence to true.
     * -    if author not exist.
     * -    msg author acooount not exist.
     * -    Call login function in utils and send Author credentials.
     * -    Test path of Author dashboard.
     * -    Check if user name link is displayed , so Author is logged in.
     */
    it('should login', function(done) {
        expect(author.exists).toBeTruthy();
        if (!author.exists) {
            console.log("Author account doesn't exist, please make sure that you have run into sme suite");
            return done();
        }
        utils.login(author.email, author.password);
        utils.testPath('/author');
        $('.user_name').isDisplayed().then(function(result) {
            isLoggedIn = result;
            done();
        });
    });
    /*Tests Author has common links, consist of:
     * -    User is loged in to continue.
     * -    Get Author dashboard.
     * -    Call function “checkCommonLinks”from utils.
    */
    it('should have common links', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.get("/author/");
        utils.checkCommonLinks();
    });
     /*Tests if Author has specific links, consist of:
     * -    User is logged in to continue.
     * -    Get Author dashboard.
    *  -    check existence of courses link.
    */
    it('should have specific links', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.get("/author/");
        utils.exists('a[href="#/author/course/"]');
    });
    /*Tests Author profile, consist of:
     * -    User is logged in to continue.
     * -    Get Author dashboard.
     * -    Click user name link.
     * -    Test path of Author profile.
     * -    Call function “profileCommonfields”.
     * -    Call function “EditProfile”.
    */
    it('should test profile', function(){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/author");
        $('a.user_name').click();
        utils.testPath('/author/profile');
        utils.profileCommonfields('/author/profile');
        utils.EditProfile('/author/profile');
    });
     /*test change password with right password, consist of:
           -User logged in to continue.
           -Get author dashboard.
           -Check existence of user name link and click on it.
           -Test path of author profile page”/author/profile”.
           -Call function” ChangePassword” and send right author password to it.
    */
    it('should change password with right password', function(){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/author");
        $('a.user_name').click();
        utils.testPath('/author/profile');
        utils.ChangePassword('/author/profile',author.password);
        utils.successpopup();
    });
     /*test change password with wrong password, consist of:
     * -    User logged in to continue.
     * -    Get Author dashboard.
     * -    Check existence of user name link and click on it.
     * -    Test path of author profile page”/author/profile”.
     * -    Call function” ChangePassword” and send wrong author password to it.
     * -    Call function” failpopup” from utils.
    */
    it('should change password with wrong current password', function(){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/author");
        $('a.user_name').click();
        utils.testPath('/author/profile');
        utils.ChangePassword('/author/profile','675449');
        utils.failpopup();
    });
     /*list courses, consist of:
    *       -user is logged in to continue.
    *       -get author courses page.
    *       -expect courses count to be greater than 0.
    */
    it('should list courses', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.get("/author/course");
        expect($$('.course').count()).toBeGreaterThan(0);
    });
     /*list course tasks,consist of:
    *       -user is logged in to continue.
    *       -get author courses page.
    *       -select first course and press its tasks button.
    *       -wait for angular.
    *       -check existence of add tasks button and click it.
    *       -wait for angular.
    *       -check count of questions tasks in page.
    *       -if it is greater than 0 .
    *       -press self assign .
    *       -wait for angular.
    *       -check existence of tasks link in crumbs and press it.
    */
    it('should list course tasks', function(done) {
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/author/course");
        utils.itemInList(true).getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            var id = ids[0];
            utils.exists('#course_' + id + ' .buttons a').click();
            ptor.waitForAngular();
            utils.exists('a[href="#/author/course/' + id + '/task/create"]').click();
            ptor.waitForAngular();
            $$('.block-update-card').count().then(function(count){
                if (count > 0)
                {
                    utils.exists('.block-update-card #selfassign').click();
                    browser.waitForAngular();
                }
                utils.exists('a[href="#/author/course/' + id + '/task/"]').click(); 
            });
            //expect(cardsCount).toBeGreaterThan(0); 
            //expect($$('.block-update-card').count()).toBe(cardsCount - 1);
            done();
        });
    });
     /*required fields appearance,consist of:
    *       -user is logged in to continue.
    *       -get author course page"/author/course".
    *       -select first course item on page and press tasks of it.
    *       -wait for angular
    *       -test path of selected course tasks page.
    *       -check if button add questions displayed ,
    *       -if false,press add tasks button.
    *       -press self assign button.
    *       -get course tasks page again.
    *       -otherwise"add questions button exist",press on it.
    *       -check existence of button save and continue and press it.
    *       -check existence of required field msg appearance.
    *       -check error msg text is 'Question description is required.'.
    *       -fill question head with faker.
    *       -check existence of button save and continue and press it.
    *       -check existence of required field msg appearance.
    *       -check error msg text is 'You should set question solution estimted time.'
    *       -fill exam time second field with random value between 0 and 50 .
    *       -fill exam time minute field with random value between 0 and 50 .
    *       -set array of question types.
    *       -each type.
    *       -check existence of save and continue button and press on it.
    *       -expect error msg text is "Please, solve the question to save the correct answer.".
    */  
    
    it ('should check required fields popup message appearance', function(done) {
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/author/course");
        utils.itemInList(true).getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            var id = ids[0];
            ptor.waitForAngular();
            utils.exists('#course_' + id + ' .buttons a').click();
            ptor.waitForAngular();
            utils.testPath('/author/course/' + id + '/task');
            $('#addquestions').isDisplayed().then(function(displayed){
                if(displayed === false)
                {
                    $('a[href="#/author/course/' + id + '/task/create"]').click();
                    $('#selfassign').click();
                    utils.get('/author/course/' + id + '/task');
                }
            });
            $('#addquestions').click();
            utils.exists('.btn.btn-success.btn-block').click();
            utils.exists('.alert.alert-danger.alert-dismissable');
            expect($('.alert.alert-danger.alert-dismissable div span').getText()).toBe('Question description is required.');
            $('#questionHead').sendKeys(faker.lorem.sentence());
            utils.exists('.btn.btn-success.btn-block').click();
            utils.exists('.alert.alert-danger.alert-dismissable');
            expect($('.alert.alert-danger.alert-dismissable div span').getText()).toBe('You should set question solution estimted time.');
            utils.exists('model.seconds','model').clear().sendKeys(_.random(1, 50) + '');
            utils.exists('model.minutes','model').clear().sendKeys(_.random(1, 10) + '');
            var Questions_Types = ['dragdrop','fillblanks' , 'mcq', 'multi_mcq','truefalse'];//
            _.each(Questions_Types,function(Questions_Type){
                $('take-' + Questions_Type);
                utils.exists('.btn.btn-success.btn-block').click();
                expect($('.alert.alert-danger.alert-dismissable div span').getText()).toBe('Please, solve the question to save the correct answer.');
                done();
            });
        });
    });
    /*Logout Author, consist of:
     * -    User is logged in to continue.
     * -    Call function logout from utils.
    */
    it('should logout', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.logout();
    });
});

