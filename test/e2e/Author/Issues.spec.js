"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var utils = new Utils(ptor);

describe('Author', function() {

    var sme = utils.users.sme;
    var author = utils.users.author;
    var isLoggedIn = false;

    it('should login', function(done) {
        utils.login(author.email, author.password);
        utils.testPath('/author');
        $('.user_name').isDisplayed().then(function(result) {
            isLoggedIn = result;
            done();
        });
    });

    it('should test course crumb hierarchy', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.get("/author/course");
        var e = utils.itemInList(true);
        e.getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            e.click();
            utils.testPath('/author/course/' + ids[0] + '/task');
            expect($('#crumb_0').getAttribute('href')).toMatch('#/author/');
            expect($('#crumb_1').getAttribute('href')).toMatch('#/author/course/');
            expect($('#crumb_2').getAttribute('href')).toMatch('#/author/course/' + ids[0]);
        });
    });

    it ('should test number crumb hierarchy', function(done) {
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/author/course");
        utils.itemInList(true).getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            var id = ids[0];
            ptor.waitForAngular();
            utils.exists('#course_' + id + ' .buttons a').click();
            ptor.waitForAngular();
            utils.testPath('/author/course/' + id + '/task');
            var e = $$('.btn.btn-primary.btn-xs').get(0);
            e.getAttribute('href').then(function(href) {
                var ids = utils.idsOfLink(href);
                e.click();
                utils.testPath('/author/course/'+ ids[0] +'/task/'+ ids[1] +'/question/create');
                expect($('#crumb_0').getAttribute('href')).toMatch('#/author/');
                //Check if crumb is string
                element(By.css('#crumb_4')).getText().then(function (val) {
                    var isString = _.isString(val);
                    expect( isString ).toBeTruthy();
                });
                done();
            });
        });
    });
	
	it ('should check create question questions text', function(done) {
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/author/course");
        utils.itemInList(true).getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            var id = ids[0];
            ptor.waitForAngular();
            utils.exists('#course_' + id + ' .buttons a').click();
            ptor.waitForAngular();
            utils.testPath('/author/course/' + id + '/task');
            utils.exists('#addquestions').click();
            $('#userMenu').click();
            ptor.actions().mouseMove($('#userMenu_languages')).perform();
            $('#userMenu_arabic').click();
            expect($('.main-inner button.btn-success.btn-block').getText()).toMatch(/حفظ ومتابعة/);
            expect($('.createTask div.alert-info').getText()).toMatch(/ملحوظة/);
            expect($('#hint h5').getText()).toMatch(/يظهر فى التدريب/);
            $('#userMenu').click();
            ptor.actions().mouseMove($('#userMenu_languages')).perform();
            $('#userMenu_english').click();
            done();
        });
    });

	it('should logout', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.logout();
    });
});