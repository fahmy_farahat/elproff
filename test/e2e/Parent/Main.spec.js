"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var utils = new Utils(ptor);

describe('Parent', function() {

    var parent = utils.users.parent;
    var student = utils.users.student;
    var isLoggedIn = false;
    /**
     * Checks if parent exists, consist of:
     * -    Check parent exist in DB .
     */
    it('checks if parent ' + parent.email + ' exists', function(done) {
        utils.db.userExists(parent.email, function(result) {
            parent.exists = !!result;
            done();
        });
    });
    /*register parent
    *   -check if parent exist 
    *   -set pparent.email by faker
    *   -get register page
    *   -select pparent button
    *   -wait for angular
    *   -call function"registercommonfields" and send parent email
    *   -set parent email value.
    *   -call function "VerificationImage"
    *   -click accept checkbox
    *   -press submit
    *   -call function"activateAccount" and send parent email.
    */
    it('should register parent', function(done){
        if (parent.exists) {
            parent.email = faker.internet.email();
        }
        utils.get('/register');
        $('#parent').click();
        ptor.waitForAngular();
        var pr = utils.registercommonfields(parent.email);
        parent.email = pr.email;
        utils.VerificationImage();
        element(By.model('model.accepted')).click();
        utils.submitbutton();
        utils.activateAccount(parent.email).then(function(activated) {
            parent.activated = activated;
            done();
        });
    });
     /*Login as parent, consist of:
     * -    check if parent not activated .
     * -    expect parent email is activated.
     * -    Call login function in utils and send parent credentials.
     * -    Check if user name link is displayed , so parent is logged in.
     */
    it('should login', function(done) {
        if (!parent.activated) {
            expect(parent.email).toBe('activated');
            return done();
        }
        utils.login(parent.email, '123456');
        $('.user_name').isDisplayed().then(function(result) {
            isLoggedIn = result;
            done();
        });
    });
    /*Tests parent profile, consist of:
     * -    User is logged in to continue.
     * -    Get parent dashboard.
     * -    Click user name link.
     * -    Test path of parent profile.
     * -    Call function “profileCommonfields”.
     * -    Call function “EditProfile”.
    */
    it('should test profile', function(){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/parent/");
        $('.user_name').click();
        utils.testPath('/parent/profile');
        utils.profileCommonfields('/parent/profile');
    });
    /*test change password with right password, consist of:
           -User logged in to continue.
           -Get parent dashboard.
           -Check existence of user name link and click on it.
           -Test path of parent profile page”/parent/profile”.
           -Call function” ChangePassword” and send right parent password to it.
    */
    it('should change password with right password', function(){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/parent/");
        $('.user_name').click();
        utils.testPath('/parent/profile');
        utils.ChangePassword('/parent/profile','123456');
        utils.successpopup();
    });
    /*test change password with wrong password, consist of:
     * -    User logged in to continue.
     * -    Get parent dashboard.
     * -    Check existence of user name link and click on it.
     * -    Test path of parent profile page”/parent/profile”.
     * -    Call function” ChangePassword” and send wrong parent password to it.
     * -    Call function” failpopup” from utils.
    */
    it('should change password with wrong current password', function(){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/parent/");
        $('.user_name').click();
        utils.testPath('/parent/profile');
        utils.ChangePassword('/parent/profile','675449');
        utils.failpopup();
    });
    /*invite son
    *   -user is logged on to continue
    *   -check exitence of parent add button and click it
    *   -test path of parent add page.
    *   -wait for angular.
    *   -select country field value
    *   -call function"invitecommonfields" and send student email
    *   -wait for angular.
    *   -select educational system field value.
    *   -wait for angular.
    *   -select grade field value.
    *   -check existence of parent relation field and fill its value by faker.
    *   -call function"VerificationImage" from utils.
    *   -press submit button
    *   -call successpopup function from utils.
    *   -call function "alertpopupmessage" from utils and send"son"
    */
    it('should invite son', function (done){
        if (!isLoggedIn) {
            return;
        }
        utils.exists(".btn.btn-info.btn-lg.add2.btn-block").click();
        utils.testPath("/parent/add");
        ptor.waitForAngular();
        utils.uiSelectTest('#country');
        utils.invitecommonfields(student.email);
        ptor.waitForAngular();
        utils.uiSelectTest('#system');
        ptor.waitForAngular();
        utils.uiSelectTest('#grade');
        utils.exists('#parentrelation').sendKeys('Father');
        utils.VerificationImage();
        utils.submitbutton();
        utils.successpopup();
        utils.alertpopupmessage('Son');
        done();
    });
    /*logout from parent profile and logiin as admin son profile to accept invitation
    *use is logged in to continue.
    *logout by calling logout function
    *call login function from utils and send student credentials..
    *test path of student dashboard page.
    *check notification by calling function "notifications" from utils.
    *logout by calling logout function.
    *call login function from utils and send parent credentials.
    *check user name link is displayed
    *so user is logged in.
    */
    it('should logout from parent then login as son and accept invitation', function(done) {
        if (!isLoggedIn) {
            return;
        }
        utils.logout();
        utils.login(student.email, student.password);
        utils.testPath("/student");
        utils.notifications('/parent');
        utils.logout();
        //login as parent...
        utils.login(parent.email, '123456');
        $('.user_name').isDisplayed().then(function(result) {
            isLoggedIn = result;
            done();
        });

    });
    /*Checks notifications, consist of:
     * -    User is logged in to continue.
     * -    Call function “notifications” and send parent to it to check notifications.
     */
    it('should check notifications', function (done) {
        if (!isLoggedIn) {
            return;
        }
         utils.notifications('/parent');
        done();
    });
    /*user profile type not repeated.
    *   -user is logged in to continue.
    *   -get parent profile page.
    *   -call function check profile type and send"'schoolteacher', 'privateteacher', 'parent', 'schooladmin'"
    */
    it('should test user profile type to be not repeated', function (done) {
        if (!isLoggedIn) {
            return;
        }
        utils.get("/parent/profile");
        utils.checkProfileType('schoolteacher', 'privateteacher', 'parent', 'schooladmin');
        done();
    });
    /*translation in invite son.
    *   -user is logged in to continue.
    *   -get parent dashboard.
    *   -press add son button.
    *   -test path of invite son page.
    *   -open user menu.
    *   -switch system language to arabic.
    *   -expect page title to match son in arabic word.
    *   -wait for angular.
    *   -press user menu.
    *   -switch system language to english.
    */
    it('should test translation in invite son', function (done){
       if (!isLoggedIn) {
           return;
       }
       utils.get("/parent/");
       utils.exists(".btn.btn-info.btn-lg.add2.btn-block").click();
       utils.testPath("/parent/add");
       $('#userMenu').click();
       ptor.actions().mouseMove($('#userMenu_languages')).perform().then(function(){
            $('#userMenu_arabic').click();
            expect($('.main-inner h3').getText()).toMatch(/ابنك/);
       });
       ptor.waitForAngular();
       $('#userMenu').click();
       ptor.actions().mouseMove($('#userMenu_languages')).perform();
       $('#userMenu_english').click();
       done();
    });
    /*list don courses
    *   -user is logged in to continue.
    *   -get parent dashboard.
    *   -select first son.
    *   -click on it.
    *   -test path of first son item page.
    *   -set var course count.
    *   -expect count length to be greater than 0.
    */
    it('should test list son courses', function (done) {
        if (!isLoggedIn) {
            return;
        }
        utils.get('/parent');
        var e = $$('a.btn.btn-success.btn-lg.btn-block').first();
        e.getAttribute('href').then(function (href) {
            var ids = utils.idsOfLink(href);
            e.click();
            utils.testPath('/parent/son/' + ids[0]);
            var courseCount = $$('.name a');
            courseCount.then(function(elem) {
                expect(elem.length).toBeGreaterThan(0);
            });
        });
        done();
    });
    /*test crumb hierarchy
    *   -user is logged iin too continue.
    *   -get parent add page.
    *   -expect first crumb link to have href match parent word.
    *   -click first crumb.
    *   -test path of parent dashboard.
    *   -select first son.
    *   -test path of first son.
    *   -expect second crumb to have href match parent/son words.
    */
    it('should test crumb hierarchy', function (done) {
        if (!isLoggedIn) {
            return;
        }
        utils.get('/parent/add');
        expect($('#crumb_0').getAttribute('href')).toMatch('#/parent/');
        utils.exists('#crumb_0').click();
        utils.testPath('/parent');
        var e = $$('a.btn.btn-success.btn-lg.btn-block').first();
        e.getAttribute('href').then(function (href) {
            var ids = utils.idsOfLink(href);
            e.click();
            utils.testPath('/parent/son/' + ids[0]);
            expect($('#crumb_1').getAttribute('href')).toMatch('#/parent/sons');
        });
        done();
    });
    /*test son course report exam
    *   -user is logged in to continue.
    *   -get parent dashboard.
    *   -select first son item
    *   -test path of first son item page
    *   -select first course item and press reports button.
    *   -test path of first son first course report page.
    *   -expect minutes field not to be zero.
    *   -expect max degree field not bring null value.
    *   -expect min degree field not bring null value.
    */
    it('should test son course total report exam', function (done) {
        if (!isLoggedIn) {
            return;
        }
        utils.get('/parent');
        var e = $$('a.btn.btn-success.btn-lg.btn-block').first();
        e.getAttribute('href').then(function (href) {
            var ids = utils.idsOfLink(href);
            e.click();
            utils.testPath('/parent/son/' + ids[0]);
            var e2 = $$('.course-report').first();
            e2.getAttribute('href').then(function (href) {
                var ids = utils.idsOfLink(href);
                e2.click();
                utils.testPath('/parent/son/' + ids[0] + '/course/' + ids[1] + '/report');
                expect($('.reportMinutes').getText()).not.toBe(0);
                expect($('.max_degree').getText()).not.toBe(null);
                expect($('.min_degree').getText()).not.toBe(null);
            });
        });
        done();
    });
     /*Logout Author, consist of:
     * -    User is logged in to continue.
     * -    Call function logout from utils.
    */
    it('should logout', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.logout();
    });

});