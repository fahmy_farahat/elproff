"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var utils = new Utils(ptor);

describe('SME', function() {

    var sme = utils.users.sme;
    var admin = utils.users.admin;
    var isLoggedIn = false;

    /**
     * Checks if SME exists, consist of:
     * -    Check sme exist in DB .
     */
    it('checks if sme exists', function(done) {
        utils.db.userExists(sme.email, function(result) {
            sme.exists = !!result;
            done();
        });
    });

    /**
     * Logins as admin, consist of:
     * -    Check if SME exists, then return.
     * -    Else Call login function in utils and send admin credentials.
     *      To start invitation to SME
     */
    it('should login as admin', function() {
        if (sme.exists) {
            return;
        }
        utils.login(admin.email, admin.password);
    });

    /**
     * Sends invitation from admin to sme, consist of:
     * -    Check If sme exist, so return.
     * -    Else test path of admin dashboard.
     * -    Check existence of subject expert link and click on it.
     * -    Test path of sme page "/admin/sme".
     * -    Check existence of invite button and click on it.
     * -    Test path of sme invite page "admin/sme/create".
     * -    Call function” invitecommonfields” from utils and send sme email to it.
     * -    Select course.
     * -    Call function” VerificationImage”.
     * -    Press submit.
     * -    Success popup function calling.
     */
    it('should get invited by an Admin', function() {
       if (sme.exists) {
           return;
       }
       // If doesn't exist
       utils.testPath('/admin');
       utils.exists('[href="#/admin/sme/"]').click();
       utils.testPath('/admin/sme');
       utils.exists('[href="#/admin/sme/create"]').click();
       utils.testPath('/admin/sme/create');
       utils.invitecommonfields(sme.email);
       utils.uiSelectTest('#courses');
       utils.VerificationImage();
       utils.submitbutton();
       utils.successpopup();
    });

    /**
     * Logout as admin and get the invitation code, consist of:
     * -    Check if SME exists return.
     * -    Else call function logout.
     *      Send invitation to db and deal with it.
     */
    it('should logout from the admin and get the invitation code', function(done) {
        if (sme.exists) {
            return done();
        }
        utils.logout();
        utils.db.getInvitation(sme.email, function(invitation) {
            if (invitation) {
                sme.invitation = invitation;
            } else {
                console.log("There's no invitation!");
            }
            expect(sme.invitation).toBeTruthy();
            done();
        });
    });

    /**
     * Accept invitation, consist of:
     * -    Check if SME exist so return.
     * -    Get”/invitation/ +sme.invitation”.
     * -    Check existence of full name field and fill it with faker.
     * -    Check existence of password field and fill it.
     * -    Check existence of confim password and fill it.
     * -    Select country.
     * -    Call function “VerificationImage” in utils.
     * -    Check existence of accept checkbox and click it.
     * -    Press submit button.
     * -    Check existence of login button and click on it.
     * -    Test path of login page.
     */
    it('should accept invitation', function() {
        if (sme.exists) {
            return;
        }
        utils.get('/invitation/' + sme.invitation);
        utils.exists('model.full_name', 'model').sendKeys(faker.name.findName());
        utils.exists('model.password', 'model').sendKeys(sme.password);
        utils.exists('model.cpassword', 'model').sendKeys(sme.password);
        utils.uiSelectTest('#country');
        utils.VerificationImage();
        utils.exists('#accept').click();
        utils.submitbutton();
        utils.exists('a[href="#/login"]').click();
        utils.testPath('/login');
    });

    /**
     * Login as SME, consist of:
     * -    Call login function in utils and send SME credentials.
     * -    Test path of SME dashboard.
     * -    Check if user name link is displayed , so SME is logged in.
     */
    it('should login', function(done) {
        utils.login(sme.email, sme.password);
        utils.testPath('/sme');
        $('.user_name').isDisplayed().then(function(result) {
            isLoggedIn = result;
            done();
        });
    });

    /**
     * Tests SME has common links, consist of:
     * -    User is loged in to continue.
     * -    Get SME dashboard.
     * -    Call function “checkCommonLinks”from utils.
     */
    it('should have common links', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.get("/sme/");
        utils.checkCommonLinks();
    });

    /**
     * Tests if SME has specific links, consist of:
     * -    User is logged in to continue.
     * -    Get SME dashboard.
     * -    Check existence of courses link.
     * -    Check existence of reviewer link.
     * -    Check existence of author link.
     * -    Check existence of invitations link.
     */
    it('should have specific links', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.get("/sme/");
        utils.exists('a[href="#/sme/course/"]');
        utils.exists('a[href="#/sme/reviewer/"]');
        utils.exists('a[href="#/sme/author/"]');
        utils.exists('a[href="#/sme/invitations/"]');
    });

    /**
     * Tests SME profile, consist of:
     * -    User is logged in to continue.
     * -    Get SME dashboard.
     * -    Click user name link.
     * -    Test path of SME profile.
     * -    Call function “profileCommonfields”.
     * -    Call function “EditProfile”.
     */
    it('should test profile', function(){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/sme/");
        $('.user_name').click();
        utils.testPath('/sme/profile');
        utils.profileCommonfields('/sme/profile');
        utils.EditProfile('/sme/profile');
    });

    /**
     * test SME profile, consist of:
     * -    User is logged in to continue.
     * -    Get SME dashboard.
     * -    Click user name link.
     * -    Test path of SME profile.
     * -    Call function “profileCommonfields”.
     * -    Call function “EditProfile”.
     */
    it('should change password with right password', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.get("/sme/");
        $('.user_name').click();
        utils.testPath('/sme/profile');
        utils.ChangePassword('/sme/profile','123456');
        utils.successpopup();
    });

    /**
     * test change password with wrong password, consist of:
     * -    User logged in to continue.
     * -    Get SME dashboard.
     * -    Check existence of user name link and click on it.
     * -    Test path of SME profile page”/sme/profile”.
     * -    Call function” ChangePassword” and send wrong SME password to it.
     * -    Call function” failpopup” from utils.
     */
    it('should change password with wrong current password', function(){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/sme/");
        $('.user_name').click();
        utils.testPath('/sme/profile');
        utils.ChangePassword('/sme/profile','675449');
        utils.failpopup();
    });

    /**
     * Tests SME invite new author, consist of:
     * -    Get SME dashboard page.
     * -    Check existence of author link and click on it.
     * -    Test path of SME author page”/sme/author”.
     * -    Check existence of invite button and click on it.
     * -    Test path of create new author page”/author/create”.
     * -    Call function “invitecommonfields”from utils.
     * -    Select courses value.
     * -    Call function” VerificationImage” from utils.
     * -    Press submit.
     * -     Expect success popup function” successpopup”.
     */
    it('should invite a new author',function(done){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/sme");
        utils.exists('a[href="#/sme/author/"]').click();
        utils.testPath('/sme/author');
        utils.exists('a[href="#/sme/author/create"]').click();
        utils.testPath('/sme/author/create');
        utils.uiSelectTest('#courses');
        utils.invitecommonfields();
        utils.VerificationImage();
        utils.submitbutton();
        utils.successpopup();
        // It just shows a success message, does not go the listing page
        done();
    });

    /**
     * Tests SME invite new reviewer, consist of:
     * -    Get SME dashboard page.
     * -    Check existence of reviewer link and click on it.
     * -    Test path of SME reviewer page”/sme/reviewer”.
     * -    Check existence of invite button and click on it.
     * -    Test path of create new reviewer page”/reviewer/create”.
     * -    Call function “invitecommonfields”from utils.
     * -    Select courses value.
     * -    Call function” VerificationImage” from utils.
     * -    Press submit.
     * -    Expect success popup function” successpopup”.
     */
    it('should invite reviewer',function(done){
        utils.get('/sme');
        utils.exists('a[href="#/sme/reviewer/"]').click();
        utils.testPath('/sme/reviewer');
        utils.exists('a[href="#/sme/reviewer/create"]').click();
        utils.testPath('/sme/reviewer/create');
        utils.invitecommonfields();
        utils.uiSelectTest('#courses');
        utils.VerificationImage();
        utils.submitbutton();
        utils.successpopup();
        done();
    });

    /**
     * Checks invitations, consist of:
     * -    User is logged in to continue.
     * -    Check existence of invitations link and click on it.
     * -    Test path of invitations path.
     * -    Check inviations , if exit ,check existence of button resend and click on it.
     */
    it('should check invitations',function(done){
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/sme");
        utils.exists('a[href="#/sme/invitations/"]').click();
        utils.testPath('/sme/invitations');
        $$('.invitation ul li').count().then(function(count){
            if (count > 0) {
                utils.exists('.btn.btn-primary').click();
                ptor.waitForAngular();
            }
        });
        done();
    });

    /**
     * Checks notifications, consist of:
     * -    User is logged in to continue.
     * -    Call function “notifications” and send SME to it to check notifications.
     */
    it('should check notifications', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.notifications('/sme');
    });

    /**
     * Adds courses to author, consist of:
     * -    User is logged in to continue.
     * -    Get SME dashboard.
     * -    Check existence of author link and click on it.
     * -    Test path of SME author”/sme/author”.
     * -    Select first author item and press on it.
     * -    Press add course button.
     * -    Select course.
     * -    Unfocus courses field by press outside field.
     * -    Get text of selected course and save it into var.
     * -    Press ok.
     * -    Expect that saved course is displayed into author page by check link text.
     */
    it('should add courses to authors', function(done){
        if (!isLoggedIn) {
            return;
        }
        utils.get('/sme/');
        utils.exists('a[href="#/sme/author/"]').click();
        utils.testPath('/sme/author');
        var e = utils.linkitemFirst();
        e.getAttribute('href').then(function(href) {
            e.click();
            utils.exists('.btn.btn-primary.btn-block').click();
            //FIXME: gives error here
            ptor.waitForAngular();
            ptor.waitForAngular();
            utils.uiSelectTest('#courses');
            utils.exists('.modal-header').click();
            var _course = $('#courses span span span span span').getText();
            utils.exists('#modal_ok').click();
            ptor.waitForAngular();
            ptor.waitForAngular();
            expect(ptor.element(protractor.By.linkText(_course)).isDisplayed()).toBeTruthy();
        });
    });

    /**
     * Logout SME, consist of:
     * -    User is logged in to continue.
     * -    Call function logout from utils.
     */
    it('should logout', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.logout();
    });

});
