"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var utils = new Utils(ptor);

describe('SME update course elements and objectives', function() {
	var sme = utils.users.sme;
    var admin = utils.users.admin;
    var isLoggedIn = false;

    /**
     * Login, consist of:
     * -    Calling login function by sending sme credentials.
     * -    Test path sme dashboard.
     * -    Check user name link is displayed.
     */
    it('should login', function(done) {
        utils.login(sme.email, sme.password);
        utils.testPath('/sme');
        $('.user_name').isDisplayed().then(function(result) {
            isLoggedIn = result;
            done();
        });
    });

    /**
     * Updates course element, consist of:
     * -    User is logged in to continue.
     * -    Get sme dashboard.
     * -    Check existence of course link and press on it.
     * -    Test path of course page.
     * -    Select first course by “iteminlist” function and press on it.
     * -    Test path of first course page.
     * -    Select first course element item in course page and click on it.
     * -    Test path of first course element item.
     * -    Check existence of update button and press on it.
     * -    Test path of course element page update.
     * -    Check existence of name field and fill it by faker.
     * -    Select element parent value and save it in var.
     * -    Select element type value and save it in var.
     * -    Press submit.
     * -    Check update button existence again Click it again.
     * -    Expect name field text to be the updated value of name before
     * -    Expect element parent text to be the selected updated value before.
     * -    Expect element type text to be the selected updated before.
     */
    it('should update course element',function(done) {
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/sme");
        utils.exists('a[href="#/sme/course/"]').click();
        utils.testPath('/sme/course');
        // course
        var e = utils.itemInList(true);
        e.getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            e.click();
            utils.testPath('/sme/course/' + ids[0]);
            // course element
            var e2 = utils.itemInList();
            e2.getAttribute('href').then(function(href) {
                var ids = utils.idsOfLink(href);
                e2.click();
                utils.testPath('/sme/course/' + ids[0] + '/element/' + ids[1]);
                utils.exists('#update_element').click();
                utils.testPath('/sme/course/' + ids[0] + '/element/' + ids[1] + '/update');
                var crsEle_name = faker.name.findName(); 
                utils.exists('#name').clear().sendKeys(crsEle_name);
                utils.uiSelectTest('#parent');
                utils.uiSelectTest('#element_type');
                var parent_ele = $('#parent button span span').getText();
                var ele_typ = $('#element_type button span span').getText();
                utils.submitbutton();
                utils.exists('#update_element').click();
                expect($('#name').getText()).toEqual(crsEle_name);
                expect($('#parent button span span').getText()).toEqual(parent_ele);
                expect($('#element_type button span span').getText()).toEqual(ele_typ);
                ////there is issue here
                done();
            });
        });
    });

    /**
     * Updates objective, consist of:
     * -    Use is logged in to continue.
     * -    Get sme dashboard.
     * -    Check existence of course link and click on it.
     * -    Test path of sme course “/sme/course”.
     * -    Select first course item in courses and press on it.
     * -    Test path of first course item.
     * -    Select first course element item in course page and click on it.
     * -    Test path of first course element item.
     * -    Select first objective exsit→(objectives[0].element(By.css('.linkitem a'));)
     * -    Click on first objective.
     * -    Test path of first objective item.
     * -    Check existence of title then clear it and fill new value.
     * -    Check existence of learning outcome and select its value.
     * -    Check existence of description field and clear it then fill new value.
     * -    Press edit bulk in any bulk.
     * -    browser.executeScript("$('.modal').removeClass('fade');");
     * -    select difficulty value =”easy” for ex.
     * -    Select question type =”multiple choice” for ex.
     * -    Check existence of questions count and clear its value and fill 2 for ex.
     * -    Check existence of ok button and press on it.
     * -    Press submit.
     * -    Check that objective title is updated by xpath ,as existed→
     * [expect(element.all(By.xpath("//*[contains(text(),'" + newtitle +
     * "')]")).count()).toBeGreaterThan(0);].
     */
	it('should test update objective', function(done) {
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/sme");
        utils.exists('a[href="#/sme/course/"]').click();
        utils.testPath('/sme/course');
        // course
        var e = utils.itemInList(true);
        e.getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            e.click();
            utils.testPath('/sme/course/' + ids[0]);
            // course element
            var e2 = utils.itemInList();
            e2.getAttribute('href').then(function(href) {
                var ids = utils.idsOfLink(href);
                e2.click();
                utils.testPath('/sme/course/' + ids[0] + '/element/' + ids[1]);
                // objective
                ptor.element.all(By.repeater('objective in objectives')).then(function(objectives){
                var e3 = objectives[0].element(By.css('.linkitem a'));
                e3.getAttribute('href').then(function(href) {
                  var ids = utils.idsOfLink(href);
                  e3.click();
                  utils.testPath('/sme/course/' + ids[0] + '/element/' + ids[1] + '/objective/' + ids[2]);
                  var newtitle = faker.name.findName();
                  utils.exists('#title').clear().sendKeys(newtitle);
                  utils.uiSelectTest('#learning_outcome');
                  utils.exists('#description').clear().sendKeys(faker.lorem.sentence());
                  /////there is issue here
                  utils.exists('.questions_bulk .actions a').click();
                  browser.executeScript("$('.modal').removeClass('fade');");
            	  ptor.waitForAngular();
            	  utils.uiSelectTest('.modal #difficulty', 'Easy');
                  utils.uiSelectTest('.modal #questions_type', 'Multiple Choices');
            	  ptor.waitForAngular();
                  utils.exists('#count').clear().sendKeys(2);
                  utils.exists('#modal_ok').click();
                  utils.submitbutton();
                  expect(element.all(By.xpath("//*[contains(text(),'" + newtitle + "')]")).count()).toBeGreaterThan(0);
                  done();
                });
                });
            });
        });
    });

    /**
     * Check visualization, consist of:
     * -    Use is logged in to continue.
     * -    Get sme dashboard.
     * -    Check existence of course link and click on it.
     * -    Test path of sme course “/sme/course”.
     * -    Select first course item in courses and press on it.
     * -    Test path of first course item.
     * -    Check existence of visualization button and click on it.
     * -    Test path of visualization page.
     * -    Check existence oof objective button and click on it.
     * -    Check existence of popup screen contain objectives.
     * -    Check existence of create objective and click on it.
     * -    Check existence of popup screen of create objective.
     * -    Check existence of title field and fill it.
     * -    Select learning outcome value using uiselect from utils.
     * -    Check existence of description and fill its value.
     * -    Call function “QBulks” in utils and send each difficulty and each question type to create question bulks.
     * -    Expect count to be 18.
     * -    Press submit.
     */
    it('should check visualization',function(done){
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/sme");
        utils.exists('a[href="#/sme/course/"]').click();
        utils.testPath('/sme/course');
        var e = utils.itemInList(true);
        e.getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            //var url = '/sme/course/' + ids[0];
            e.click();
            ptor.waitForAngular();
            //utils.exists('a[href="#' + url + '"]').click();
            utils.testPath('/sme/course/' + ids[0]);
            //utils.exists('a[href="#' + '/sme/course/' + ids[0] + '/elements"]').click();
            //utils.exists('a[href="#/sme/course/' + ids[0] + '/elements"]').click();
            utils.exists('#visualization').click();
            utils.testPath('/sme/course/' + ids[0] + '/elements');
            utils.exists('.btn.btn-success.btn-sm.pull-right.flip').click();
            utils.exists('.modal-content');
            utils.exists('.btn.btn-primary').click().then(function(){
                utils.exists('.modal-content');
                var title = faker.name.findName();
                utils.exists('#title').sendKeys(title);
                utils.uiSelectTest('#learning_outcome');
                utils.exists('#description').sendKeys(faker.lorem.sentence());
                var Difficulties = ['Easy','Medium','Difficult'];
                _.each(Difficulties, function(Diff) {
                    var Types = ['Drag & Drop','Multiple Choices','Multiple Choices with Multiple Answers','True or False','Connect','Fill the blanks'];
                    _.each(Types, function(Type) {
                        utils.QBulks(Diff,Type);
                    });
                });
                expect($$('questions-bulk').count()).toBe(18);
            });
            done();
        });
    });

    /**
     * Logout, consist of:
     * -    User is logged in to continue.
     * -    Calling function logout from utils.
     */
	it('should logout', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.logout();
    });

});
