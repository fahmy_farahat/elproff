"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var utils = new Utils(ptor);

describe('SME Issues', function() {
		var sme = utils.users.sme;
    	var admin = utils.users.admin;
    	var isLoggedIn = false;

		it('should login', function(done) {
        	utils.login(sme.email, sme.password);
        	utils.testPath('/sme');
        	$('.user_name').isDisplayed().then(function(result) {
            	isLoggedIn = result;
            	done();
        	});
    	});
		it('should check reviewers page title to be Reviewers',function(){
        	if (!isLoggedIn) {
            	return;
        	}
        	utils.get("/sme");
        	utils.exists('a[href="#/sme/reviewer/"]').click();
        	utils.testPath('/sme/reviewer');
        	expect($('.main-inner h3').getText()).toMatch(/Reviewer/);
    	});

    	it('should test crumb hierarchy', function (done) {
        	if (!isLoggedIn) {
            	return;
        	}
        	utils.get('/sme/');
        	utils.exists('a[href="#/sme/course/"]').click();
        	utils.testPath('/sme/course');
        	var e = utils.itemInList(true);
        	e.getAttribute('href').then(function(href) {
            	var ids = utils.idsOfLink(href);
            	e.click();
            	utils.testPath('/sme/course/' + ids[0]);
            	expect($('#crumb_0').getAttribute('href')).toMatch('#/sme/');
            	expect($('#crumb_1').getAttribute('href')).toMatch('#/sme/course/');
        	});
        	utils.get('/sme/');
        	utils.exists('a[href="#/sme/reviewer/"]').click();
        	var e2 = utils.linkitemFirst();
        	e2.getAttribute('href').then(function(href) {
            	var ids = utils.idsOfLink(href);
            	e2.click();
            	utils.testPath('/sme/reviewer/' + ids[0]);
            	expect($('#crumb_0').getAttribute('href')).toMatch('#/sme/');
            	expect($('#crumb_1').getAttribute('href')).toMatch('#/sme/reviewer/');
        	});
        	utils.get('/sme/');
        	utils.exists('a[href="#/sme/author/"]').click();
        	var e3 = utils.linkitemFirst();
        	e3.getAttribute('href').then(function(href) {
            	var ids = utils.idsOfLink(href);
            	e3.click();
            	utils.testPath('/sme/author/' + ids[0]);
            	expect($('#crumb_0').getAttribute('href')).toMatch('#/sme/');
            	expect($('#crumb_1').getAttribute('href')).toMatch('#/sme/author/');
        	});
        	done();
    	});
		
		it('should test course tree collapse', function (done) {
        	if (!isLoggedIn) {
            	return;
        	}
        	utils.get('/sme/');
        	utils.exists('a[href="#/sme/course/"]').click();
        	utils.testPath('/sme/course');
        	var e = utils.itemInList(true);
        	e.getAttribute('href').then(function(href) {
            	var ids = utils.idsOfLink(href);
            	e.click();
            	utils.testPath('/sme/course/' + ids[0]);
            	utils.exists('#visualization').click();
            	$$('#collapse').first().click();
            	var e2 = $$('#collapse span').first();
            	var e3 = $$('.angular-ui-tree-handle').first();
            	expect(e2.getAttribute('class')).toMatch('glyphicon-chevron-right');
            	expect(e3.getAttribute('class')).not.toMatch('error-flash');
        	});
        	done();
    	});

		it('should test create/update objective',function(done) {
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/sme");
        utils.exists('a[href="#/sme/course/"]').click();
        utils.testPath('/sme/course');
        var e = utils.itemInList(true);
        e.getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            e.click();
            ptor.waitForAngular();
            utils.testPath('/sme/course/' + ids[0]);
            var e2 = utils.itemInList();
            e2.getAttribute('href').then(function(href) {
                var ids = utils.idsOfLink(href);
                e2.click();
                utils.testPath('/sme/course/' + ids[0] + '/element/' + ids[1]);
                utils.exists('#create_objective').click();
                var title = faker.name.findName();
                utils.exists('#title').sendKeys(title);
                utils.uiSelectTest('#learning_outcome');
                utils.exists('#description').sendKeys(faker.lorem.sentence());
                utils.QBulks('Easy','Multiple Choices');
                utils.submitbutton();
                expect(element.all(By.xpath("//*[contains(text(),'" + title + "')]")).count()).toBeGreaterThan(0);
                element(By.xpath("//*[contains(text(),'" + title + "')]")).click();
                var editTitle = ' edit';
                var newTitle = title + editTitle;
                utils.exists('#title').sendKeys(editTitle);
                utils.submitbutton();
                expect(element.all(By.xpath("//*[contains(text(),'" + newTitle + "')]")).count()).toBeGreaterThan(0);
            });
        });
        done();
    });
    	it('should logout', function() {
        	if (!isLoggedIn) {
            	return;
        	}
        	utils.logout();
    	});
});