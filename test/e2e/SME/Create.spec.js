"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var utils = new Utils(ptor);

describe('SME Create course elemennts and Objectives', function() {

    var sme = utils.users.sme;
    var admin = utils.users.admin;
    var isLoggedIn = false;

    /**
     * Login as SME, consist of:
     * -    Calling login function and send SME credentials.
     * -    Test path SME dashboard.
     * -    Check user name link is exist.
     */
    it('should login', function(done) {
        utils.login(sme.email, sme.password);
        utils.testPath('/sme');
        $('.user_name').isDisplayed().then(function(result) {
            isLoggedIn = result;
            done();
        });
    });

    /**
     * Adds course elements, consist of:
     * -    User is logged in to continue.
     * -    Get sme dashboard.
     * -    Check existence of course link and press on it.
     * -    Test path of course page.
     * -    Select first course by “iteminlist” function and press on it.
     * -    Test path of first course page.
     * -    Check existence of create element button and click on it.
     * -    Test path of create element in first course page.
     * -    Check existence of name field and fill it.
     * -    Select course element parent.
     * -    Select element type.
     * -    Submit button function in utils.
     * -    Test path of first course page.
     */
    it('should add course elements', function(done) {
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/sme");
        utils.exists('a[href="#/sme/course/"]').click();
        utils.testPath('/sme/course');
        var e = utils.itemInList(true);
        e.getAttribute('href').then(function(href) {
            e.click();
            ptor.waitForAngular();
            var ids = utils.idsOfLink(href);
            var url = '/sme/course/' + ids[0];
            //utils.exists('a[href="#' + url + '"]').click();
            utils.testPath(url);
            utils.exists('#create_element').click();
            utils.testPath(url + '/element/create');
            utils.exists('#name').sendKeys(faker.name.findName());
            utils.uiSelectTest('#parent');
            utils.uiSelectTest('#element_type');
            utils.submitbutton();
            utils.testPath(url);
            done();
        });
    });

    /**
     * Add objectives, consist of:
     * -    Use is logged in to continue.
     * -    Get sme dashboard.
     * -    Check existence of course link and click on it.
     * -    Test path of sme course “/sme/course”.
     * -    Select first course item in courses and press on it.
     * -    Test path of first course item.
     * -    Select first course element item in course page and click on it.
     * -    Test path of first course element item.
     * -    Check existence of create objective button and press on it.
     * -    Test path of create objective page.
     * -    Check existence of title field and fill it by faker.
     * -    Check existence of learning outcome field and select its value.
     * -    Check existence of description and fill its value.
     * -    Call function “QBulks” in utils and send each difficulty and each question type to create question bulks.
     * -    Expect count to be 18.
     * -    Press submit.
     * -    Expect that objective is added by path Count is greater than 0.
     */
    it('should add objectives', function(done) {
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/sme");
        utils.exists('a[href="#/sme/course/"]').click();
        utils.testPath('/sme/course');
        // course
        var e = utils.itemInList(true);
        e.getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            e.click();
            utils.testPath('/sme/course/' + ids[0]);
            // course element
            var e2 = utils.itemInList();
            e2.getAttribute('href').then(function(href) {
                var ids = utils.idsOfLink(href);
                e2.click();
                utils.testPath('/sme/course/' + ids[0] + '/element/' + ids[1]);
                // objective
                utils.exists('#create_objective').click();
                utils.testPath('/sme/course/' + ids[0] + '/element/' + ids[1] + '/objective/create');
                var title = faker.name.findName();
                utils.exists('#title').sendKeys(title);
                utils.uiSelectTest('#learning_outcome');
                utils.exists('#description').sendKeys(faker.lorem.sentence());
                var Difficulties = ['Easy','Medium','Difficult'];
                var Types = ['Drag & Drop','Multiple Choices','Multiple Choices with Multiple Answers','True or False','Connect','Fill the blanks'];
                _.each(Types, function(Type) {
                    _.each(Difficulties, function(Diff) {
                        utils.QBulks(Diff,Type);
                    });
                });
                expect($$('questions-bulk').count()).toBe(18);
                utils.submitbutton();
                expect(element.all(By.xpath("//*[contains(text(),'" + title + "')]")).count()).toBeGreaterThan(0);
                done();
            });
        });
    });

    /**
     * Logout, consist of:
     * -    User is logged in to continue.
     * -    Calling function logout from utils.
     */
	it('should logout', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.logout();
    });
});
