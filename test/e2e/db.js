"use strict";
var pg = require('pg');
var faker = require('faker');
var conf = require('../../protractor.' + (process.env.E2E_ENV || 'default') + '.conf.js').config;
(function(module) {

    function DB() {

        this.conObject = {
            user: conf.db.username,
            password: conf.db.password,
            port: conf.db.port || 5432,
            host: conf.db.host,
            database: conf.db.database
        };
        console.log(this.conObject);
        var that = this;
        pg.connect(this.conObject, function(err, client) {
            if(err) {
                console.log(JSON.stringify(err));
                return console.error('could not connect to postgres', err);
            }
            that.client = client;
        });

        this.execute = function(query, email, next) {
            query = query + " WHERE lower(email)=lower('" + email + "')";
            console.log(query);
            that.client.query(query, function(err, result) {
                if(err) {
                    console.error('error running query', err);
                    return next(false);
                }
                console.log(JSON.stringify(result));
                next(result);
            });
        };

        this.userExists = function userExists(email, next) {
            var query = "SELECT email FROM seven_users_sevenuser";
            this.execute(query, email, function(result) {
                if (result) {
                    return next(!!result.rowCount);
                }
                return next(!!result);
            });
        };

        this.activateAccount = function activateAccount(email, next) {
            var query = "UPDATE seven_users_sevenuser SET is_active = true";
            this.execute(query, email, function(result) {
                if (result) {
                    return next(!!result.rowCount);
                }
                return next(!!result);
            });
        };

        this.getInvitation = function getInvitation(email, next) {
            var query = "SELECT token FROM invitations_invitation";
            this.execute(query, email, function(result) {
                if (result && result.rows[0].token) {
                    return next(result.rows[0].token);
                }
                return next(!!result);
            });
        };

        this.getResetPassword = function getResetPassword(email, next) {
            var query = "SELECT token FROM seven_users_sevenuser";
            this.execute(query, email, function(result) {
                if (result) {
                    return next(result.rows[0]);
                }
                return next(!!result);
            });
        };
    }

    module.exports = {
        DB: DB
    };

})(module);
