"use strict";
var faker = require('faker');
var _ = require('lodash');
var path = require('path');
var conf = require('../../protractor.' + (process.env.E2E_ENV || 'default') + '.conf.js').config;
var DB = require('./db.js').DB;
var db = new DB();

(function(module) {

    var resize = _.once(function(width, height) {
        if (!width) {
            width = 1280;
        }
        if (!height) {
            height = 800;
        }
        var window = browser.driver.manage().window();
        window.setSize(width, height);
        window.maximize();
    });

    function Utils(ptor) {

        this.timeout = conf.timeout;
        console.log("Timeout: " + this.timeout);
        this.users = conf.users;
        this.baseUrl = conf.baseUrl;
        this.loginUrl = this.baseUrl + '/ui/#/login';

        this.resize = resize;
        this.resize();

        this.testPath = function (path) {
            ptor.waitForAngular();
            expect(ptor.getCurrentUrl()).toEqual(this.baseUrl + '/ui/#' + path);
        };

        this.db = db;

        this.get = function (path) {
            ptor.get("ui/#" + path);
            ptor.waitForAngular();
        };

        this.profileCommonfields = function(path){
            //expect($('.img-circle.img-responsive').isDisplayed()).toBeTruthy();
            ptor.waitForAngular();
            expect($('h2').getText()).toEqual($('.user_name').getText());
            this.exists('#changepassword');
            this.exists('#editprofile');
            this.exists('#view');
        };

        this.ChangePassword = function(path,currectpassword){
            $('#changepassword').click();
            this.testPath(path + '/changepassword');
            this.exists('#currentcurrent_password').clear().sendKeys(currectpassword);
            this.Passwordsfields();
            this.submitbutton();
        };

        this.Passwordsfields = function(nocpassword){
            var pws = [1,12,123,1234,12345];
            _.each(pws, function(pw) {
                $('#password').clear().sendKeys(pw);
                ptor.actions().sendKeys(protractor.Key.TAB).perform();
                expect($$('.validation.error.red').first().isDisplayed()).toBeTruthy();
                expect($$('.validation.error.red').first().getText()).toEqual('Field is too short');
            });
            this.exists('#password').clear().sendKeys('123456');
            if (!nocpassword) {
                this.exists('#cpassword').clear().sendKeys('123456');
            }
        };

        this.EditProfile = function(path){
            $('#editprofile').click();
            ptor.waitForAngular();
              expect($('#full_name').getAttribute('value')).toEqual($('.user_name').getText());
              this.exists('#full_name').clear().sendKeys(faker.name.findName());
              this.exists('#telephone').clear().sendKeys(faker.phone.phoneNumber());
              this.uiSelectTest('#country');
              this.exists('#state').clear().sendKeys(faker.address.state());
              this.exists('#city').clear().sendKeys(faker.address.city());
              this.exists('#streetaddress').clear().sendKeys(faker.address.streetAddress());
              if(path === '/student/profile')
              {
                this.exists('model.gender','model').click();
                this.exists('model.birthdate','model').clear().sendKeys('01-January-2003');
                //ptor.executeScript("element(by.model('model.birthdate')).val('01-January-2003')");
                this.uiSelectTest('#system');
                this.uiSelectTest('#grade');
              }
              this.submitbutton();
              ptor.waitForAngular();
              this.successpopup();
            //var img = this.getImageFile('img07.png');
            //this.exists('#avatar').sendKeys(img);
            //expect($('.dropdown.drop_down .img-circle.img-responsive').getAttribute('src')).toBe($('.col-md-4.profile_picture.text-center .img-circle.img-responsive').getAttribute('src'));
            //expect($('#full_name').getAttribute('value')).toEqual($('.user_name').getText());
        };

        this.submitbutton = function() {
            this.exists('[type="submit"]').click();
            ptor.waitForAngular();
        };

        this.logout = function() {
            this.exists('#userMenu').click();
            this.exists('#userMenu_logout').click();
            browser.waitForAngular();
        };

        this.login = function(email,password) {
            this.get('/login');
            browser.waitForAngular();
            element(By.model('model.email')).sendKeys(email);
            element(By.model('model.password')).sendKeys(password);
            this.submitbutton();
        };

        this.withModel = function(selectors) {
            return _.map(selectors, function(selector) {
                return 'model.' + selector;
            });
        };

        this.checkAttr = function(selectors, selectBy, whatToCheck, value) {
            _.each(selectors, function(selector) {
                expect(element(By[selectBy || 'css'](selector)).getAttribute(whatToCheck)).toEqual(value);
            });
        };

        this.check = function(selectors, selectBy, whatToCheck) {
            _.each(selectors, function(selector) {
                expect(element(By[selectBy || 'css'](selector))[whatToCheck || 'isDisplayed']()).toBeTruthy();
            });
        };

        this.checkCommonLinks = function() {
            var selectors = [
                'a.user_name', '.dashboard', '.message', '.help', '.user_name',
                'img[src="images/img03.png"]',
                'a.user_name .img-circle'
                    ];
            this.check(selectors, 'css', 'isDisplayed');
            //expect($('.user_img').getAttribute('aria-expanded')).toEqual('true');
        };

        this.VerificationImage = function(){
            browser.waitForAngular();
            this.exists('#recaptcha_table');
            this.exists('#recaptcha_challenge_image');
            this.exists('#recaptcha_response_field').sendKeys('Pypass');
            //this.exists('#recaptcha_reload_btn #recaptcha_reload');
            //this.exists('#recaptcha_switch_audio_btn #recaptcha_switch_audio');
            //this.exists('#recaptcha_whatsthis_btn #recaptcha_whatsthis');
            //this.exists('#recaptcha_logo');
        };

        this.registercommonfields = function(email){
            if (!email) {
                email = faker.internet.email();
            }
            this.uiSelectTest('#country');
            var name = faker.name.findName();
            this.exists('#email').sendKeys(email);
            this.exists('#full_name').sendKeys(faker.name.findName());
            this.Passwordsfields();
            ptor.waitForAngular();
            return { name: name, email: email };
        };

        this.invitecommonfields = function(email){
            if (!email) {
                email = faker.internet.email();
            }
            var name = faker.name.findName();
            this.exists('.input[type="email"]').sendKeys(email);
            this.exists('#title').sendKeys(name);
            this.exists('#message').sendKeys(faker.lorem.sentence());
            return { name: name, email: email };
        };

        this.successpopup = function(){
            ptor.waitForAngular();
            expect ($('.alert-success').isDisplayed()).toBeTruthy();
        };

        this.failpopup = function(){
            ptor.waitForAngular();
            expect ($('.alert-danger').isDisplayed()).toBeTruthy();
        };

        this.alertpopupmessage = function(message){
            ptor.waitForAngular();
            expect ($('.alert div span').getText()).toMatch(message);
        };

        //this.listItemTitleSelector = '.main-inner a.linkitem h3.title';
        this.listItemTitleSelector = '.main-inner .linkitem a  h3.title';
        //this.listItemLinkSelector = '.main-inner a.linkitem';
        this.listItemLinkSelector = 'link-item a';
        this.listItemDeleteButtonSelector = 'link-item button.btn-danger';
        this.courseListItemTitleSelector = '.main-inner .course .course-body h3.name';
        this.ID_REGEX = new RegExp('\\/(\\d+?)($|\\/)', 'g');

        this.itemInList = function(isCourse) {
            var selector = isCourse ? this.courseListItemTitleSelector + ' a' : this.listItemLinkSelector;
            var e = this.exists(selector);
            ptor.waitForAngular();
            return e;
        };
        this.notifications = function(path){
          var that = this ;
          $('a.navItem.message').click().then(function(){
          $('.navItem.message span').isPresent().then(function(result){
            if(result)
            {
                that.testPath(path + '/notifications');
                that.exists('.notification');
                that.exists('a.false');
                that.exists('a.true').click();

            }
            else{
                that.get(path);
            }
        });
        });
        };

        this.clickItemInList = function(isCourse) {
            this.itemInList(isCourse).click();
            ptor.waitForAngular();
        };

        this.idsOfLink = function(href) {
            var IDs = [];
            var id = this.ID_REGEX.exec(href);
            while(id !== null) {
                IDs.push(id[1]);
                id = this.ID_REGEX.exec(href);
            }
            return IDs;
        };

        this.deleteItemInList = function() {
          var selector = this.listItemDeleteButtonSelector;
          this.exists(selector).click();
          ptor.waitForAngular();
        };

        this.elementExistance = function(ele){
          var that = this ;
          $$('link-item').count().then(function(count){
                if (count === 0)
                {
                    that.createElement(ele);
                }
            });
        };
        /**
         * Searches for the passed title in the list
         * works only with linkitem directive list
         * TODO: add pagination support
         */
        this.titleExists = function(title, isCourse) {
            var selector = isCourse ? this.courseListItemTitleSelector : this.listItemTitleSelector;
            var flow = protractor.promise.controlFlow();
            var promising = function() {
                var defer = protractor.promise.defer();
                // Async
                element.all(By.css(selector)).then(function(titles) {
                    var exists = false;
                    // Done method get executed once after being called the
                    // count of titles
                    var done = _.after(titles.length, function() {
                        if (exists) {
                            defer.fulfill("found");
                        } else {
                            defer.reject("not found");
                        }
                    });
                    _.each(titles, function(titleElement) {
                        // Async
                        titleElement.getText().then(function(text) {
                            if (text === title) {
                                exists = true;
                            }
                            // Call once
                            done();
                        });
                    });
                });
                return defer.promise;
            };
            // Waiting for the promise to be fulfilled
            flow.execute(promising);
        };

        this.exists = function(selector, selectBy) {
            browser.waitForAngular();
            var el;
            if (!selectBy) {
                el = $(selector);
            } else {
                el = element(By[selectBy](selector));
            }
            expect(el.isPresent()).toBeTruthy();
            expect(el.isDisplayed()).toBeTruthy();
            return el;
        };

        this.getImageFile = function(filename) {
            var fileToUpload = '../../app/images/' + filename;
            var absolutePath = path.resolve(__dirname, fileToUpload);
            return absolutePath;
        };

        this.uiSelectTest = function(selector, text) {
            var flow = protractor.promise.controlFlow();
            var promising = function() {
                var defer = protractor.promise.defer();
                $(selector).click();
                ptor.waitForAngular();
                $$(selector + ' a.ui-select-choices-row-inner').then(function(elements) {
                    if (text) {
                        var afterAll = _.after(elements.length, function(clicked) {
                            if (clicked) {
                                defer.fulfill("clicked");
                            } else {
                                defer.reject("wasn't found");
                            }
                        });
                        _.each(elements, function(element) {
                            element.getText().then(function(etext) {
                                console.log("Etext : " + etext + " - Text : " + text);
                                if (etext === text) {
                                    console.log("Clicking...");
                                    element.click();
                                    browser.waitForAngular();
                                }
                                afterAll(true);
                            });
                        });
                    } else {
                        var length = elements.length;
                        console.log("Found [ " +  length + " ] Choices.");
                        if (!length) {
                            console.log("HAS NO CHOICES : " + selector + ' a.ui-select-choices-row-inner');
                            return defer.reject("no choices");
                        }
                        elements[_.random(length-1)].click();
                        browser.waitForAngular();
                        return defer.fulfill("clicked");
                    }
                });
                // to wait for others loading ui selector
                browser.waitForAngular();
                return defer.promise;
            };
            flow.execute(promising);
        };

        this.search = function(name) {
          this.exists('form[name="searchform"] input').clear().sendKeys(name);
          this.exists('#searchButton').click();
          //or press enter//ptor.actions().sendKeys(protractor.Key.ENTER).perform();
          ptor.waitForAngular();
          //var ele = ptor.element(protractor.By.linkText(name));
          var ele = $('link-item[title="' + name +'"] .linkitem a .bodycontainer');
           return ele ;
        };

        /**
         *  Checks existence of “create-elementname” button in page and click on it.
         *  Then fills the name of element. Presses submit. Then call search
         *  function and search the entered name.
         *  @param element it takes element name (country, grade, ..etc).
         *  @return the element.
         */
        this.createElement = function(element) {
            this.exists('#create-' + element).click();
            var name = faker.name.findName();
            this.exists('#name').sendKeys(name);
            this.submitbutton();
            var elem = this.search(name);
            return elem ;
        };


        this.QBulks = function(diff,type) {
            this.exists('#addbulk').click();
            browser.executeScript("$('.modal').removeClass('fade');");
            ptor.waitForAngular();
            this.uiSelectTest('.modal #difficulty', diff);
            ptor.waitForAngular();
            this.uiSelectTest('.modal #questions_type', type);
            ptor.waitForAngular();
            this.exists('#count').clear();
            this.exists('#count').sendKeys(_.random(1, 15) + '');
            ptor.waitForAngular();
            this.exists('#modal_ok').click();
            //expect($$('.questions_bulk').count()).toBeGreaterThan(0);
        };

        this.QuestionBlockComponants = function() {
            this.exists('.update-card-body');
            this.exists('[k="count"]');
            this.exists('[k="questions"]');
            this.exists('[k="type"]');
            this.exists('[k="difficulty"]');
            this.exists('[k="objective"]');
            this.exists('[k="description"]');
            this.exists('[k="outcome"]');
            this.exists('[k="country"]');
            this.exists('[k="edu_system"]');
            this.exists('[k="grade"]');
            this.exists('[k="course"]');
            this.exists('[k="elementparent"]');
            this.exists('[k="element"]');
        };

        this.QcreateCommonfields = function(isfillBlanks) {
            this.exists('.alert.alert-info');
            this.exists('.title #q');
            this.exists('model.seconds','model').clear().sendKeys(_.random(1, 50) + '');
            this.exists('model.minutes','model').clear().sendKeys(_.random(1, 10) + '');
            var Question_Head =faker.lorem.sentence();
            this.exists('#questionHead').sendKeys(Question_Head);
            if (isfillBlanks === true) {
                this.exists('#questionHead').sendKeys('....');
                expect($('#bucket-0').getText()).toEqual('....');
            }
            expect($('.title h4  p').getText()).toEqual(Question_Head);
        };

        this.TrueFalseQuestion = function(Qno) {
            if (Qno % 2 === 0 ) {
                this.exists('.true').click();
            } else {
                this.exists('.false').click();
            }
        };

        this.Addchoices = function(buttonselector,Questionselector,useranswerselector) {
            var that = this;
            _.times(4, function() {
                that.exists('#' + buttonselector).click();
                var choice_answer = (faker.lorem.sentence());
                that.exists('#' + Questionselector).sendKeys(choice_answer);
                that.exists('#' + useranswerselector);
                //expect($('.radio label[for="useranswer-0"]').getText()).toEqual(choice_answer);
                expect($('#' + useranswerselector).getText()).toEqual(choice_answer);
            });
        };
        this.removeChoices = function()
        {
          this.exists('#addMoreChoices').click();
          var choice_answer = (faker.lorem.sentence());
          this.exists('#question-choice-0').sendKeys(choice_answer);
          this.exists('#useranswer-0');
          expect($('#useranswer-0').getText()).toEqual(choice_answer);
          this.exists('.btn.btn-danger.btn-xs.btn-block').click();
          expect($('#useranswer-0').isDisplayed()).toBeFalsy();
          expect($('#question-choice-0').getAttribute('value')).toEqual('');
        };

        this.fillBlanks = function() {
            this.Addchoices('addMoreChoices','question-choice','useranswer-0');
            ptor.waitForAngular();
            var answer = $('#useranswer-1');
            var answer_target = $('#bucket-0');
            ptor.actions().mouseMove(answer).mouseDown(answer).mouseUp(answer_target).perform();
            ptor.waitForAngular();
        };

        this.DragAndDrop = function() {
            this.Addchoices('addMoreChoices','question-choice','choice-0');
            this.Addchoices('addMoreEnds','question-end','bucket-0 .title');
            this.exists('.content');
            this.exists('.title');
            _.times(4, function(i) {
                var choice = $('#choice-' + i);
                var box = $('#bucket-' + i);
                ptor.actions().mouseMove(choice).mouseDown(choice).mouseUp(box).perform();
            });
        };

        this.activateAccount = function(email) {
            var flow = protractor.promise.controlFlow();
            var that = this;
            function promising() {
                var actor = protractor.promise.defer();
                that.db.activateAccount(email, function(result) {
                    actor.fulfill(!!result);
                });
                return actor.promise;
            }
            return flow.execute(promising);
        };

        this.switchLanguage = function(lang)
        {
            $('#userMenu').click();
            ptor.actions().mouseMove($('#userMenu_languages')).perform().then(function(){
              ptor.waitForAngular();
              $('#userMenu_' + lang).click();
          });
        };
        this.langButton = function()
        {
            this.switchLanguage('english');
            ptor.refresh();
            expect($('form > translatable > div > button:nth-child(2)').getAttribute('class')).toBe('btn ng-binding btn-success');
            this.switchLanguage('arabic');
            ptor.waitForAngular();
            ptor.refresh();
            expect($('form > translatable > div > button:nth-child(3)').getAttribute('class')).toBe('btn ng-binding btn-success');
        };
        /**
         * Check repeated user profile type.
         * @param {profileType} add multi or one
         *  profile_type to select profile byClass name.
         *
         */
        this.checkProfileType = function (profileType) {
            if (arguments.length > 1) {
                for (profileType in arguments) {
                    var userSelector = $$('.' + arguments[profileType]);
                    userSelector.then(function(count) {
                        expect(count.length).toEqual(1);
                    });
                }
            } else {
                var userSelector = $$('.' + profileType);
                userSelector.then(function(count) {
                    expect(count.length).toEqual(1);
                });
            }
        };

        /**
         * [linkitemFirst get first link item element]
         */
        this.linkitemFirst = function () {
            return $$('.linkitem a').first();
        };

        this.crumbs = function(){
            var crumb = element.all(By.repeater('crumb in crumbs'));
            crumb.count().then(function(count){
                // We need to target the crumb before the last one
                // The last one equals length - 1
                var crumb_id = count - 2;
                $('.crumbs #crumb_' + crumb_id).click();
                ptor.waitForAngular();
            });
        };

        this.dashboardCrumb = function(){
            $('.crumbs #crumb_0').click();       
        };

        /**
         * Test pagination functionality
         * @param  {string} model [The model repeater e.g:"thing in things"]
         * @return {boolean}      [Whether the functionality works fine || not]
         */
        this.pagination = function(model) {
            element.all(By.repeater('page in pages')).then(function(pages){
                if (pages.length > 1) {
                    var model_a = element.all(By.repeater(model));
                    var pagination_el = element.all(By.css('a[ng-click="selectPage(page.number)"]'));
                    pagination_el.get(1).click();
                    ptor.waitForAngular();
                    var model_b = element.all(By.repeater(model));
                    expect(model_a.get(0) == model_b.get(0)).toBeFalsy();
                    pagination_el.get(0).click();
                    ptor.waitForAngular();
                }
            });
        };

        /**
         * Wait for element to be visbile
         * @param  {string} el       [selector string]
         * @param  {string} selector [selector type]
         * @return {object}          [element]
         */
        this.waitFor = function(el, selector) {
            browser.driver.wait(protractor.until.elementIsNotVisible($(el)));
            return this.exists(el, selector);
        };
        /**
         * Used to clean getAttribute(href) to get a valid link href
         * @param  {string} href [getAttribute result]
         * @return {string}      [href that can be called in element selector]
         */
        this.cleanhref = function(href) {
            var splitter = "#";
            return splitter + href.split(splitter).pop();
        };
    }

    module.exports = {
        Utils: Utils
    };

})(module);
