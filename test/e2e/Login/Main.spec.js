"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var utils = new Utils(ptor);

describe('Login Process', function () {

  /**before each it function 
  *   -call get function from utils and send "/login".
  */

  beforeEach(function () {
    utils.get('/login');
  });
  /**check language button existence
  *   -call "exists" function from utils and send selector".btn.btn-default.btn-block"
  */
  it('should check change language button exist',function(){
    utils.exists('.btn.btn-default.btn-block');
  });

  /**show login page fields
  *   -set var array"selectors" hold fields selctors"email,password,remember me,forget pw,login with fb,login button,register now link
  *   -call function "check" from utils and send "selectors".
  */

  it('should shows login page fields', function () {
    var selectors = [
      '#email', '#password', '#rememberme', '.forgot', '.login_fb',
      '.btn.btn-success.btn-lg.btn-block', 'a[href="#/register"]', 'h4'
    ];
    utils.check(selectors);
  });

  /**check mandatory fields
  *   -expect field email to have required attribute .
  *   -expect field password to have required attribute .
  */

  it('should check mandatory fields', function () {
    expect($('#email').getAttribute('required')).toEqual('true');
    expect($('#password').getAttribute('required')).toEqual('true');
  });

  /**redirect to forget password page.
  *   -click on forget password link.
  *   -wait for angular
  *   -test path of forgot password page.
  */

  it('should redirect to forget password page', function () {
    $('.forgot').click().then(function () {
      ptor.waitForAngular();
      utils.testPath('/forgotpassword');
    });
  });

  /** redirect to register page.
  *   -click register now link.
  *   -wait for angular.
  *   -test path of register page.
  *   -check existence of language button in register page.
  */

  it('should redirect to register Page', function () {
    $('a[href="#/register"]').click();
    ptor.waitForAngular();
    utils.testPath('/register');
    utils.exists('.btn.btn-default.btn-block');
  });

  /**authenticate admin with matching crediantials login and redirect to admin dashboard.
  *   -call login function from utils and send right admin crediantials.
  *   -test path of admin dashboard.
  *   -call logout function.
  */

  it('should be autheticate Admin user, when the crediantials are matching and redirect to the admin dashboard page', function () {
    utils.login('admin@elproff.com', '123456');
    utils.testPath('/admin');
    utils.logout();
  });

  /**show required field error msg when press signin without filling data.
  *   -press sign in button by calling function"submitbutton" from utils.
  *   -expect email required field msg appears.
  *   -expect password required field msg appears.
  *   -expect error msg text is "Field is required".
  */
  //FIXME : Incorrect selectors
  it('should show required field error messages when press sign in button without filling data', function () {
    utils.submitbutton();
    expect($('input-email .validation.error.red').isDisplayed()).toBeTruthy();
    expect($('input-password .validation.error.red').isDisplayed()).toBeTruthy();
    expect($('.validation.error.red').getText()).toBe('Field is required');
  });

  /**fail login
  *   -set var array tests of emails and passwords.
  *   -each value in test array:
  *   -call function login and send email,password values from array by order.
  *   -expect email field to have entered email value.
  *   -expect password field to have entered password value.
  *   -fail popup appeared bu calling "failpopup" function from utils
  */

  it('should fail login', function () {
    var tests = [
      {email: faker.internet.email(), password: '5657677'},
      {email: faker.internet.email(), password: '123456'},
      {email: 'admin@elproff.com', password: '687578'}
    ];
    _.each(tests, function (testCase) {
      utils.login(testCase.email, testCase.password);
      expect($('#email').getAttribute('value')).toEqual(testCase.email);
      expect($('#password').getAttribute('value')).toEqual(testCase.password);
      utils.failpopup();
    });
  });

  /**press forget password
  *   -wait for angular.
  *   -click forget password.
  *   -test path of forget password page.
  *   -expect button send exist.
  *   -set var rreset email and give value.
  *   -check existence of email field and fill it with var reset email value.
  *   -press send button.
  *   -exppect success popup appear by calling "successpopup" function in utils.
  *   -set var "processed" to false.
  *   -set var "token" to false.
  *   -call db function "getResetPassword".
  *   -check if result false.
  *   -expect reset email is sent.
  *   -expect token to be true.
  *   -get reset password page.
  *   -call function "Passwordsfields" from utils.
  *   -call function "submitbutton" from utils.
  */

  it('should press forget password', function (done) {
    ptor.waitForAngular();
    $('.forgot').click();
    utils.testPath('/forgotpassword');
    expect($('[type="submit"]').getText()).toBe('Send');
    var reset_email = 'admin@elproff.com';
    utils.exists('#email').sendKeys(reset_email);
    utils.submitbutton();
    utils.successpopup();
    var processed = false;
    var token = false;
    utils.db.getResetPassword(reset_email, function (result) {
      if (!result) {
        return expect(reset_email).toBe('sent');
      }
      token = result.token;
      expect(token).not.toBe(false);
      utils.get('/resetpassword/' + token);
      utils.Passwordsfields();
      utils.submitbutton();
      done();
    });
  }); 

  /**validation msg when forget password and fill not existing email.
  *   -wait for angular.
  *   -click forget password link.
  *   -test path of forget password page.
  *   -check existence of email field and fill it with wrong email.
  *   -press submit button.
  *   -call function "failpopup" from utils.
  *   -expect error msg to be "Email is not registered."
  */

  it('should check validation msg when forget password and fill not existing email',function(){
        ptor.waitForAngular();
        $('.forgot').click();
        utils.testPath('/forgotpassword');
        utils.exists('#email').sendKeys('a@a.x');
        utils.submitbutton();
        utils.failpopup();
        expect($('.alert-danger div span:nth-child(1)').getText()).toBe('Email is not registered.');
  });
});
