"use strict";
var faker = require('faker');
var _ = require('lodash');
var path = require('path');
var conf = require('../../protractor.' + (process.env.E2E_ENV || 'default') + '.conf.js').config;
var DB = require('./db.js').DB;
var db = new DB();
var Utils = require('./utils.js').Utils;
var utils = new Utils(ptor);

(function(module) {

    function Utils(ptor) {

        this.timeout = conf.timeout;
        console.log("Timeout: " + this.timeout);
        this.users = conf.users;
        this.baseUrl = conf.baseUrl;
        this.loginUrl = this.baseUrl + '/ui/#/login';

        this.resize = _.once(function(width, height) {
            if (!width) {
                width = 1280;
            }
            if (!height) {
                height = 800;
            }
            var window = browser.driver.manage().window();
            window.setSize(width, height);
            window.maximize();
        });
        this.resize();
        this.createExam = function(Qnum,Qtype){
            _.map($('.pagination-sm.pagination a'),function() {
                  pages.push($('.pagination-sm.pagination a').getText());      
            });


            _.each(pages, function(pag){
                pag.getText().then(function(text){
                  console.log(text);
                  if (text === 'Previous')
                  {
                    $('.active').gettext().then(function(text){
                      var pg = parseInt(text);
                      var prevPg = pg - 1;
                      pag.click().then(function(){
                        //expect(element.By.linkText(prevPg).getAttribute('class')).toEqual('active');
                      });
                    });
                  }
                  else if (text === 'Next')
                  {
                    $('.active').gettext().then(function(text){
                      var pg = parseInt(text);
                      var nextPg = pg + 1;
                      pag.click().then(function(){
                        //expect(element.By.linkText(nextPg).getAttribute('class')).toEqual('active');
                      });
                    });
                  }
                
                else
                {
                    pag.click().then(function(){
                      ptor.waitForAngular();
                      expect($(selector).isDisplayed()).toBeTruthy();
                    });
                }
                
                    //var el_count = $$(selector).count();

                });
            });
    }
 module.exports = {
        Utils: Utils
    };

})(module);
