"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var utils = new Utils(ptor);

describe('Student', function() {

    var student = utils.users.student;
    var isLoggedIn = false;
    /*register student
    *   -check if student exist 
    *   -if false.
    *   -get register page
    *   -select student button
    *   -wait for angular
    *   -call function"registercommonfields" and send student email
    *   -wait for angular.
    *   -check dropdown loading mark"green mark" of educational system.
    *   -if yes:
    *   -select educational system value
    *   -if not:
    *   -select country again"change its value".
    *   -select educational system value .
    *   -wait for angular
    *   -wait for angular.
    *   -check dropdown loading mark"green mark" of grade.
    *   -if yes:
    *   -select grade value
    *   -if not:
    *   -select educational system value"change its value" .
    *   -select grade value 
    *   -fill school name by faker
    *   -call function "VerificationImage"
    *   -click accept checkbox
    *   -press submit
    *   -set var processed to false
    *   -call function"activateAccount" and send student email.
    */
    it('checks if student ' + student.email + ' exists', function(done) {
        utils.db.userExists(student.email, function(result) {
            //student.exists = !!result;
            if(result === false)
            {
                utils.get('/register');
                $('#student').click();
                ptor.waitForAngular();
                var st = utils.registercommonfields(student.email);
                ptor.waitForAngular();
                $('#system_label .success-result').isPresent().then(function(res){
                    if(res)
                    {
                        utils.uiSelectTest('#system');
                    }
                    else
                    {
                        utils.uiSelectTest('#country');
                    }
                });
        
                utils.uiSelectTest('#system');
                ptor.waitForAngular();
                ptor.waitForAngular();
                $('#grade_label .success-result').isPresent().then(function(result){
                    if(result)
                    {
                        utils.uiSelectTest('#grade');
                    }
                    else
                    {
                        utils.uiSelectTest('#system');
                    }
                });
                utils.uiSelectTest('#grade');
        
                element(By.model('model.schoolname')).sendKeys(faker.name.findName());
                utils.VerificationImage();
                element(By.model('model.accepted')).click();
                utils.submitbutton();
                utils.activateAccount(student.email).then(function(activated) {
                    student.activated = activated;
                });

            }
        done();
        });
    });

    
    //it('should login', function(done) {
    //    if (!student.activated) {
    //       expect(student.email).toBe('activated');
    //       return done();
    //    }
    //    utils.login(student.email, '123456');
    //    $('.user_name').isDisplayed().then(function(result) {
    //        isLoggedIn = result;
    //        done();
    //    });
    //});
   
    it ('should login as default student',function(done){
        utils.login(student.email, student.password);
        utils.testPath('/student');
        $('.user_name').isDisplayed().then(function(result) {
            isLoggedIn = result;
            done();
        });
    });

   
    it('should test course crumb hierarchy', function(done) {
        if (!isLoggedIn) {
            return;
        }
        utils.get("/student");
        var e = element.all(By.css('.btn.btn-danger.btn-block')).get(0);
        e.getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            e.click();
            utils.testPath('/student/course/' + ids[0] + '/exam/create');
            expect($('#crumb_0').getAttribute('href')).toMatch('#/student/');
            //Check if crumb is string
            element( By.css('#crumb_1') ).getText()
              .then(function(val) {
                var isString = _.isString(val);
                expect( isString ).toBeTruthy();
            });
        });
        done();
    });

    it('should test crumb alignment', function(done) {
        if (!isLoggedIn) {
            return;
        }
        utils.get("/student/");
        var crumbs = element(By.css('.crumbs')).getAttribute('class');
        expect(crumbs).toMatch('pull-left');
        done();
    });

    it('should take practice and assert that refresh keep in practice page',function(done){
        if (!isLoggedIn) {
            return; 
        }
        utils.get('/student/');
        var e = utils.itemInList(true);
        e.getAttribute('href').then(function (href) {
            var ids = utils.idsOfLink(href);
            e.click();
            utils.testPath('/student/course/' + ids[0]);
            utils.exists('a[href="#/student/course/'+ ids[0] +'/practice/create"]').click();
            utils.testPath('/student/course/' + ids[0] + '/practice/create');
            ptor.waitForAngular();
            utils.submitbutton();
            expect(ptor.getCurrentUrl()).toContain("/exam/");
            ptor.refresh();
            expect(ptor.getCurrentUrl()).toContain("/exam/");
            done();
        });
    });
    it('should check that no more elements in create exam not has dropdownlist with it',function(){
        if (!isLoggedIn) {
            return; 
        }
        utils.get('/student/');
        var e = utils.itemInList(true);
        e.getAttribute('href').then(function (href) {
            var ids = utils.idsOfLink(href);
            e.click();
            utils.testPath('/student/course/' + ids[0]);
            utils.exists('a[href="#/student/course/'+ ids[0] +'/exam/create"]').click();
            utils.testPath('/student/course/' + ids[0] + '/exam/create');
            var i = 0 ;
            utils.uiSelectTest('#element_' + i);
            i = i+1;
            $('.form-group h3').isPresent().then(function(display){
                if(display === true)
                {
                    expect($('#element_' + i).isPresent()).toBeFalsy();
                }
                else
                {
                    utils.uiSelectTest('#element_' + i);
                }
            });
        });
    });

    // it('should check failpopup exam create',function(done){
    //     utils.get("/student/");
    //     var e = $$('.buttons .student .btn.btn-danger.btn-block').get(0);
    //     e.getAttribute('href').then(function(href) {
    //         var ids = utils.idsOfLink(href);
    //         e.click();
    //         utils.testPath('/student/course/' + ids[0] + '/exam/create');
    //     });
    //     utils.submitbutton();
    //     utils.failpopup();
    //     ptor.waitForAngular();
    //     utils.uiSelectTest('#element_0');
    //     utils.uiSelectTest('#difficulty');
    //     utils.exists('.subs .sub.subject select').click();
    //     $('[value="10"]').click();
    //     utils.submitbutton();
    //     //utils.get()
    //     expect(ptor.getCurrentUrl()).toContain("/exam/");
    //     done();
    // });

    it('should test crumb arabic interface', function(done){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/student/");
        $('#userMenu').click();
        ptor.actions().mouseMove($('#userMenu_languages')).perform().then(function(){
            $('#userMenu_arabic').click();
            var crumbs = element(By.css('#crumb_0')).getText();
            expect(crumbs).toMatch('لوحة التحكم');
        });
        ptor.waitForAngular();
        $('#userMenu').click();
        ptor.actions().mouseMove($('#userMenu_languages')).perform();
        $('#userMenu_english').click();  
        done();
    });

    it('should test crumb subject course name', function (done) {
        utils.get('/student');
        var e = utils.itemInList(true);
        e.getAttribute('href').then(function (href) {
            var ids = utils.idsOfLink(href);
            e.click();
            utils.testPath('/student/course/' + ids[0]);
            var getText = $('#crumb_1').getText();
            getText.then(function (text) {
                expect(text.length).toBeGreaterThan(0);   
            });        
        });
        done();
    });

    it('should test last question answer not skipped in exam', function(done){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/student/");
        element.all(By.css('.btn-danger.btn-block')).get(4).click();
        utils.submitbutton();
        var last_question = element.all(By.css('.progressBar li')).get(-1);
        var questions_count = last_question.getText();
        last_question.click();
        utils.exists('[ng-click="ok()"]').click();
        element(By.css('question-preview .ng-scope')).getAttribute('data-type').then( function(type){
            if(type === 'truefalse'){
                element(By.css('.true')).click();
                element(By.css('.btn-success.btn-lg')).click();
                expect(element.all(By.css('.modal .modal-body ul li')).get(-1)).not.toBe(questions_count);
            } else if (type === 'mcq'){
                element(By.css('#useranswer-0')).click();
                element(By.css('.btn-success.btn-lg')).click();
                expect(element.all(By.css('.modal .modal-body ul li')).get(-1)).not.toBe(questions_count);
            }else {

            }
        });
        done();
    });
    /*exam time in exam detailed report
    *   -user is logged in to continue
    *   -get student dashboard.
    *   -press exam button in first course in student dashboard.
    *   -select exam size(10)question.
    *   -press submit
    *   -each question in progress bar:
    *   -catch question type from question content then:
    *   -check if type"T/F"
    *   -click true
    *   -press next.
    *   -check if type"mcq"
    *   -select first answer
    *   -press next.
    *   -any other type.
    *   -press next
    *   -press skip of warning popup.
    *   -check if warning msg displayed.
    *   -if yes,click skip
    *   -wait for angular
    *   -
    *   -
    */
    it('should test exam time in exam detailed report', function(done){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/student/");
        element.all(By.css('.btn-danger.btn-block')).get(0).click();
        ptor.findElement(protractor.By.css('select option:nth-child(1)')).click();
        utils.submitbutton();
        var questions_count = element.all(By.css('.progressBar li')).each( function(){
            element(By.css('question-preview .ng-scope')).getAttribute('data-type').then( function(type){
                if(type === 'truefalse'){
                    element(By.css('.true')).click();
                    element(By.css('.btn-success.btn-lg')).click();
                }else if (type === 'mcq'){
                    element(By.css('#useranswer-0')).click();
                    element(By.css('.btn-success.btn-lg')).click();
                }else {
                    element(By.css('.btn-success.btn-lg')).click();
                    element(By.css('.modal .btn-primary')).click();
                }
            });
        });
        $$('.modal').isDisplayed().then( function(exist) {
            if ( exist ) {
                utils.exists('.modal .btn-primary').click();
            }
        });
        ptor.waitForAngular();
        var studentTime = utils.exists('.timing .t1').getText();
        utils.exists('.btn-warning').click();
        ptor.waitForAngular();
        expect($('.circle4 .txt1').getText()).toMatch(/\d{1,2}:\d{2}/);
        expect($('.circle4 .txt1').getText()).toEqual(studentTime);
        var examTime = utils.exists('.exam_time h5 span').getText();
        expect($('.exam_time h5 span').getText()).toMatch(/\d{1,2}:\d{2}/);
        $$('.time_data .yellow').isDisplayed().then( function(exist) {
            if ( exist ) {
                expect($('.time_data .yellow').getText()).toMatch(/\d{1,2}:\d{2}\sminutes/); 
            } else {
                expect($('.time_data .red').getText()).toMatch(/\d{1,2}:\d{2}$/);
            }
        });
        done();
    });
    /*end exam dialog
    *   -user is logged in to continue.
    *   -get student dashboard.
    *   -press exam button of first course exist in dashboard.
    *   -wait for angular
    *   -select exam size(10)question.
    *   -press take exam button.
    *   -select first crumb link"dashboard".
    *   -click first crumb link"dashboard".
    *   -text of warning popup of end exam.
    *   -expect text of warning popup to be "Are you sure end exam ?"
    *   -click ok
    *   -test path of student dashboard
    */
    it('should test end exam dialog', function(done){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/student/");
        element.all(By.css('.btn-danger.btn-block')).get(0).click();
        ptor.waitForAngular();
        ptor.findElement(protractor.By.css('select option:nth-child(1)')).click();
        utils.submitbutton();
        var e = utils.exists('#crumb_0');
        var eHref = e.getAttribute('href');
        e.click();
        var modalText = $('.modal .modal-body p').getText();
        expect(modalText).toEqual('Are you sure end exam ?');
        utils.exists('.modal .modal-footer .btn-primary').click();
        utils.testPath('/student');
        done();
    });
    /*test finish practice redirection action not to be result page.
    *   -user is logged in to continue
    *   -get student dashboard.
    *   -click first course in student dashboard.
    *   -click practice link button
    *   -press take practice button
    *   -press next button.
    *   -press skip button in warning popup
    *   -expect that page not contain exam result.
    *   -press finish button.
    *   -expect that page not contain exam result.
    */
    it('should test finish practice redirection action not to be result page', function(done){
        if(!isLoggedIn) {
            return;
        }
        utils.get("/student/");
        element.all(By.css('h3 a')).get(0).click();
        element.all(By.css('a.btn-warning')).get(0).click();
        utils.submitbutton();
        element.all(By.css('button.btn-success')).get(0).click();
        element(By.css('.modal .btn-primary')).click();
        expect($('.main-inner h3').getText()).not.toEqual(' Exam result ');
        element.all(By.css('button.btn-success')).get(1).click();
        expect($('.main-inner h3').getText()).not.toEqual(' Exam result ');
        done();
    });
    /*suggested exams,suuggested practices and highest degree values exist.
    *   -user is logged in to continue.
    *   -get student dashboard.
    *   -click first course item in student dashboard.
    *   -expect suggested exam count number to be more than zero
    *   -expect suggested practice count number to be more than zero.
    *   -expect highest degree text to match % sign.
    */
    it('should test suggested exams, suggested practices and highest degree values exist', function(done){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/student/");
        element.all(By.css('h3 a')).get(0).click();
        expect($('a.btn-danger span').getText()).toBeGreaterThan(0);
        expect($('a.btn-warning span').getText()).toBeGreaterThan(0);
        expect($('.percent').getText()).toMatch(/\d[%]/); // 90%
        done();
    });
    /*detailed report max and min degree
    *   -user is logged in to continue.
    *   -get student dashboard.
    *   -click first course item in student dashboard.
    *   -click detailed reports button.
    *   -expect max degree
    *   -expect min degree
    */

    it('should test detailed report max and min degree', function(done){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/student/");
        element.all(By.css('h3 a')).get(0).click();
        element(By.css('a.btn-success')).click();
        expect($('.taqreerCircle .g2').getText()).toMatch(/\d/);
        expect($('.taqreerCircle.red .g2').getText()).toMatch(/\d/);
        done();
    });
    /*detailed report links
    *   -user is logged in to continue.
    *   -get student dashboard.
    *   -select first course item exist in dashboard.
    *   -click first course item.
    *   -test path of student first course page.
    *   -click button detailed reports.
    *   -test path of student detailed reports.
    */
    it('should test detailed report links', function(done){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/student/");
        var e = element.all(By.css('h3 a')).get(0);
        e.getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            e.click();
            utils.testPath("/student/course/" + ids[0]);
            element(By.css('a.btn-success')).click();
            utils.testPath("/student/course/"+ ids[0] + "/exam/report");
            done();
        });
    });
    /*Tests student profile, consist of:
     * -    User is logged in to continue.
     * -    Get student dashboard.
     * -    Click user name link.
     * -    Test path of student profile.
     * -    Call function “profileCommonfields”.
     * -    Call function “EditProfile”.
    */
     it('should test profile', function(){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/student/profile");
        utils.testPath("/student/profile");
        utils.profileCommonfields('/student/profile');
        utils.EditProfile('/student/profile');
    });
     /*test change password with right password, consist of:
           -User logged in to continue.
           -Get student dashboard.
           -Check existence of user name link and click on it.
           -Test path of student profile page”/student/profile”.
           -Call function” ChangePassword” and send right student password to it.
    */
    it('should change password with right password', function(){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/student/profile");
        utils.testPath("/student/profile");
        utils.ChangePassword('/student/profile','123456');
        utils.successpopup();
    });
    /*test change password with wrong password, consist of:
     * -    User logged in to continue.
     * -    Get student dashboard.
     * -    Check existence of user name link and click on it.
     * -    Test path of student profile page”/student/profile”.
     * -    Call function” ChangePassword” and send wrong student password to it.
     * -    Call function” failpopup” from utils.
    */
    it('should change password with wrong current password', function(){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/student/profile");
        utils.testPath("/student/profile");
        utils.ChangePassword('/student/profile','675449');
        utils.failpopup();
    });

    /*Logout student, consist of:
     * -    User is logged in to continue.
     * -    Call function logout from utils.
    */
    it('should logout', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.logout();
    });

});
