"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var utils = new Utils(ptor);
//var examUtils = require('../examUtils.js').examUtils;
//var examutils = new examUtils(ptor);


describe('Exam cycle', function() {
	var sme = utils.users.sme;
	var author = utils.users.author;
	var isLoggedIn = false;

    it('should login as author',function(done){
        utils.login(author.email, author.password);
        utils.testPath('/author');
        $('.user_name').isDisplayed().then(function(result) {
            isLoggedIn = result;
            done();
        });
    });
    it('should author create exam questions',function(done){
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/author/course"); 
        utils.itemInList(true).getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            var id = ids[0];
            ptor.waitForAngular();
            utils.exists('#course_' + id + ' .buttons a').click();
            ptor.waitForAngular();
            utils.testPath('/author/course/' + id + '/task');
              //utils.get('/author/course/2/task');
            var Difficulties = ['Easy','Medium','Difficult'];
            _.each(Difficulties, function(Diff) { 
                var Questions_Types = ['dragdrop','fillblanks' , 'mcq', 'multi_mcq','truefalse'];//
                _.each(Questions_Types,function(Questions_Type){
                    //$('a[href="#/author/course/2/task/create"]').click();
                        $('a[href="#/author/course/' + id + '/task/create"]').click();
                        ptor.waitForAngular();
                        //($('[v="' + Diff + '"]') && $('[data-type=' + Questions_Type +'] #selfassign')).click();
                        $('[data-type=' + Questions_Type +'] #selfassign').click();
                        utils.get('/author/course/' + id + '/task');
                        //utils.get('/author/course/2/task');
                         ptor.waitForAngular();
                var _type = ($('[v="' + Diff + '"]') && $('[data-type=' + Questions_Type +'] #addquestions'));
                _type.click();
                utils.QuestionBlockComponants();
                var i = 0;
                for(i; i < 1; i++) {
                            var isfillBlanks = false;
                            if(Questions_Type === 'truefalse') {
                                utils.QcreateCommonfields(isfillBlanks);
                                utils.TrueFalseQuestion(i);
                            } else if (Questions_Type === 'mcq' || Questions_Type === 'multi_mcq') {
                                utils.QcreateCommonfields(isfillBlanks);
                                utils.Addchoices('addMoreChoices','question-choice-0','useranswer-0');
                            } else if (Questions_Type === 'dragdrop') {
                                utils.QcreateCommonfields(isfillBlanks);
                                utils.DragAndDrop();
                            } else if (Questions_Type === 'fillblanks') {
                                isfillBlanks = true;
                                utils.QcreateCommonfields(isfillBlanks);
                                utils.fillBlanks();
                            }
                            utils.exists('.btn.btn-success.btn-block').click();
                        }
                        done();
                });   
            });   
        });
    });
    



});