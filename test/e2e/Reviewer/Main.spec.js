"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var utils = new Utils(ptor);

describe('Reviewer', function() {

    var sme = utils.users.sme;
    var reviewer = utils.users.reviewer;
    var isLoggedIn = false;
    /**
     * Checks if reviewer exists, consist of:
     * -    Check reviewer exist in DB .
     */
    it('checks if reviewer exists', function(done) {
        utils.db.userExists(reviewer.email, function(result) {
            reviewer.exists = !!result;
            done();
        });
    });
    /*login as sme ,consist of
    *  -check existence of  reviewer.
    *  -call login function and send sme credentials.
    */
    it('should login as sme', function(done) {
        if (reviewer.exists) {
            return done(); 
        }
        utils.login(sme.email, sme.password);
        $('.user_name').isDisplayed().then(function(result) {
            isLoggedIn = result;
            done();
        });
    });
    /*Sends invitation from SME to reviewer, consist of:
     * -    Check If reviewer exist, so return.
     * -    Else test path of SME dashboard.
     * -    Check existence of reviewers link and click on it.
     * -    wait for angular.
     * -    Test path of reviewer page "/sme/reviewer".
     * -    Check existence of invite button and click on it.
     * -    wait for angular.
     * -    Test path of reviewer invite page "sme/reviewer/create".
     * -    Call function” invitecommonfields” from utils and send reviewer email to it.
     * -    Select course.
     * -    Call function” VerificationImage”.
     * -    Press submit.
     * -    Success popup function calling.
    *
    *
    */
    it('should get invited by subject Expert',function(){
        if (reviewer.exists) {
            return;
        }
        utils.get('/sme');
        $('a[href="#/sme/reviewer/"]').click();
        browser.waitForAngular();
        utils.testPath('/sme/reviewer');
        browser.waitForAngular();
        $('a[href="#/sme/reviewer/create"]').click();
        browser.waitForAngular();
        utils.testPath('/sme/reviewer/create');
        browser.waitForAngular();
        sme = utils.invitecommonfields(reviewer.email);
        utils.uiSelectTest('#courses');
        utils.VerificationImage();
        utils.submitbutton();
        utils.successpopup();
    });
    /*logout ,consist of:
    *   -check if reviewer exist then return.
    *   -call logout function.
    *   -Send invitation to db and deal with it.
    */
    it('should logout', function(done) {
        if (reviewer.exists) {
            return done();
        }
        utils.logout();
        utils.getInvitation(reviewer.email).then(function(invitation) {
            reviewer.invitation =  invitation;
            done();
        });
    });
    /*Accept invitation, consist of:
     * -    Check if Reviewer exist so return.
     * -    Get”/invitation/ +reviewer.invitation”.
     * -    Check existence of full name field and fill it with faker.
     * -    Check existence of password field and fill it.
     * -    Check existence of confim password and fill it.
     * -    Select country.
     * -    Call function “VerificationImage” in utils.
     * -    Check existence of accept checkbox and click it.
     * -    Press submit button.
     * -    Check existence of login button and click on it.
     * -    Test path of login page.
    */
    it('should accept invitation', function() {
        if (reviewer.exists) {
            return;
        }
        utils.get('/invitation/' + reviewer.invitation);
        element(By.model('model.full_name')).sendKeys(faker.name.findName());
        reviewer.password = '123456';
        element(By.model('model.password')).sendKeys(reviewer.password);
        element(By.model('model.cpassword')).sendKeys(reviewer.password);
        utils.uiSelectTest('#country');
        utils.VerificationImage();
        element(By.id('accept')).click();
        utils.submitbutton();
        $('a[href="#/login"]').click();
        utils.testPath('/login');
    });
    /*Login as Athor, consist of:
     * -    Call login function in utils and send Reviewer credentials.
     * -    Test path of Reviewer dashboard.
     * -    Check if user name link is displayed , so Reviewer is logged in.
    */
    it('should login', function(done) {
        utils.login(reviewer.email, reviewer.password);
        utils.testPath('/reviewer');
        $('.user_name').isDisplayed().then(function(result) {
            isLoggedIn = result;
            done();
        });
    });
    /*Tests Reviewer has common links, consist of:
     * -    User is loged in to continue.
     * -    Get Reviewer dashboard.
     * -    Call function “checkCommonLinks”from utils.
    */
    it('should have common links', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.get("/reviewer/");
        utils.checkCommonLinks();
    });
    /*Tests if Reviewer has specific links, consist of:
     * -    User is logged in to continue.
     * -    Get Reviewer dashboard.
    *  -    check existence of courses link.
    */
    it('should have specific links', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.get("/reviewer/");
        utils.exists('a[href="#/reviewer/course/"]');
    });
    /*Tests Reviewer profile, consist of:
     * -    User is logged in to continue.
     * -    Get Reviewer dashboard.
     * -    Click user name link.
     * -    Test path of Reviewer profile.
     * -    Call function “profileCommonfields”.
     * -    Call function “EditProfile”.
    */
     it('should test profile', function(){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/reviewer/");
        $('.user_name').click();
        utils.testPath('/reviewer/profile');
        utils.profileCommonfields('/reviewer/profile');
        utils.EditProfile('/reviewer/profile');
    });
     /*test change password with right password, consist of:
           -User logged in to continue.
           -Get Reviewer dashboard.
           -Check existence of user name link and click on it.
           -Test path of author profile page”/reviewer/profile”.
           -Call function” ChangePassword” and send right Reviewer password to it.
    */
    it('should change password with right password', function(){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/reviewer/");
        $('.user_name').click();
        utils.testPath('/reviewer/profile');
        utils.ChangePassword('/reviewer/profile','123456');
        utils.successpopup();
    });
    /*test change password with wrong password, consist of:
     * -    User logged in to continue.
     * -    Get Reviewer dashboard.
     * -    Check existence of user name link and click on it.
     * -    Test path of author profile page”/Reviewer/profile”.
     * -    Call function” ChangePassword” and send wrong Reviewer password to it.
     * -    Call function” failpopup” from utils.
    */
    it('should change password with wrong current password', function(){
        if (!isLoggedIn) {
            return;
        }
        utils.get("/reviewer/");
        $('.user_name').click();
        utils.testPath('/reviewer/profile');
        utils.ChangePassword('/reviewer/profile','675449');
        utils.failpopup();
    });
    /*list courses, consist of:
    *       -user is logged in to continue.
    *       -check existence of reviewer courses button and click on it.
    *       -test path of reviewer courses page
    *       -expect courses count to be greater than 0.
    */
    it('should list courses', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.exists('[href="#/reviewer/course/"]').click();
        utils.testPath('/reviewer/course');
        expect($$('.course').count()).toBeGreaterThan(0);
    });
    /*assign task and check success and danger popup msg,consist of:
    *   -user is logged in to continue.
    *   -get reviewer courses page.
    *   -select fisrt course item in page and click course tasks button.
    *   -test path of course tasks page.
    *   -check existence of add task button and press it.
    *   -test path of add task page.
    *   -check selfassign buttons existence.
    *   -if greater than one:-
    *   -press first selfassign button.
    *   -wait for angular.
    *   -success popup appear.
    *   -else:-
    *   -press first selfassign button.
    *   -wait for angular.
    *   -error msg appear.
    *   -press tasks link in crumbs.
    *   -test path of course tasks.
    *
    *
    */
    it('should assign task and check success/danger popup msg appearance', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.get("/reviewer/course");
        var e = utils.itemInList(true);
        e.getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            utils.exists('#course_' + ids[0] + ' .buttons a').click();  
            utils.testPath('/reviewer/course/' + ids[0] + '/task');
            utils.exists('[href="#/reviewer/course/'+ ids[0] +'/task/create"]').click();
            utils.testPath('/reviewer/course/' + ids[0] + '/task/create');
            var e2 = $$('#selfassign');
            e2.then(function (tasks) {
                if(e2.length > 1)
                {
                    e2.get(0).click();
                    ptor.waitForAngular();
                    utils.successpopup();
                }
                else
                {
                    e2.get(0).click();
                    ptor.waitForAngular();
                    utils.failpopup();
                }
                utils.exists('#crumb_3').click();
            });
            utils.testPath('/reviewer/course/' + ids[0] + '/task');
        });
    });
    /*unassign task and check success and danger popup msg,consist of:
    *   -user is logged in to continue.
    *   -get reviewer courses page.
    *   -select fisrt course item in page and click course tasks button.
    *   -test path of course tasks page.
    *   -check selfassign buttons existence.
    *   -if greater than one:-
    *   -press first selfassign button.
    *   -wait for angular.
    *   -success popup appear.
    *   -else:-
    *   -press first selfassign button.
    *   -wait for angular.
    *   -error msg appear.
    *   -test path of course tasks.
    */
    it('should unassign task and check success/danger popup msg appearance', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.get("/reviewer/course");
        var e = utils.itemInList(true);
        e.getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            utils.exists('#course_' + ids[0] + ' .buttons a').click();  
            utils.testPath('/reviewer/course/' + ids[0] + '/task');
            var e2 = $$('#selfunassign');
            e2.then(function (tasks) {
                if(e2.length > 1)
                {
                    e2.get(0).click();
                    ptor.waitForAngular();
                    utils.successpopup();
                }
                else
                {
                    e2.get(0).click();
                    ptor.waitForAngular();
                    utils.failpopup();
                    //expect(e2.length).toBeEqual(0);
                }
                
            });
            utils.testPath('/reviewer/course/' + ids[0] + '/task');
        });
    });
    /*Checks notifications, consist of:
     * -    User is logged in to continue. 
     * -    Call function “notifications” and send reviewer to it to check notifications.
    */
    it('should check notifications', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.get("/reviewer/notifications");
        utils.notifications('/reviewerre');
    });
    /*Logout Reviewer, consist of:
     * -    User is logged in to continue.
     * -    Call function logout from utils
    *
    *
    */
    it('should logout', function() {
        if (!isLoggedIn) {
            return;
        }
        utils.logout();
    });
});	
