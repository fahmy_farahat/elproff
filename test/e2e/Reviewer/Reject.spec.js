"use strict";
var faker = require('faker');
var _ = require('lodash');
var ptor = browser;
var Utils = require('../utils.js').Utils;
var utils = new Utils(ptor);

describe('Reviewer Reviews questions and accept them', function() {

    var sme = utils.users.sme;
    var reviewer = utils.users.reviewer;
    var isLoggedIn = false;
    it('should login', function(done) {
        utils.login(reviewer.email, reviewer.password);
        utils.testPath('/reviewer');
        $('.user_name').isDisplayed().then(function(result) {
            isLoggedIn = result;
            done();
        });
    });

    it('should review questions task and reject questions', function(done) {
        if (!isLoggedIn) {
            return done();
        }
        utils.get("/reviewer/course/");
        utils.itemInList(true).getAttribute('href').then(function(href) {
            var ids = utils.idsOfLink(href);
            var id = ids[0];
            utils.exists('#course_' + id + ' .buttons a').click();
            browser.waitForAngular();
            var cardsCount = $$('.block-update-card');
            cardsCount.then(function (e) {
                if(e.length === 0)
                {
                    utils.exists('a[href="#/reviewer/course/' + id + '/task/create"]').click();
                    browser.waitForAngular();
                    utils.exists('.block-update-card #selfassign').click();
                    browser.waitForAngular();  
                }
                $('#crumb_3').click();
                ptor.waitForAngular();
                utils.testPath('/reviewer/course/' + ids[0] + '/task');
            });
                var e = $('a.btn.btn-primary.btn-xs');
                e.getAttribute('href').then(function(href) {
                    var ids = utils.idsOfLink(href);
                    e.click();
                    ptor.waitForAngular();
                    //utils.testPath('/reviewer/course/' + id[0] + '/task/' + id[1] +'/questions');
                    element.all(By.repeater('question in questions')).then(function(questions){
                    	var question = $$('.linkitem a');
                    	_.times(questions.length,function(){
                        	question.click();
                        	ptor.waitForAngular();
                        	$$('.btn-group .btn.btn-success').last().click();
                        });
                    });
                            //utils.testPath('/reviewer/course/' + id + '/task/' + id[1] +'/questions/' +id[2]);
                        //});
                });
            
            done();
        });
    });

});