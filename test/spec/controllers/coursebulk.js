'use strict';

describe('Controller: CoursebulkCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var CoursebulkCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CoursebulkCtrl = $controller('CoursebulkCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
