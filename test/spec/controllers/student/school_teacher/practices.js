'use strict';

describe('Controller: StudentSchoolTeacherPracticesCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var StudentSchoolTeacherPracticesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StudentSchoolTeacherPracticesCtrl = $controller('StudentSchoolTeacherPracticesCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
