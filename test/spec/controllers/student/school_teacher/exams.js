'use strict';

describe('Controller: StudentSchoolTeacherExamsCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var StudentSchoolTeacherExamsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StudentSchoolTeacherExamsCtrl = $controller('StudentSchoolTeacherExamsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
