'use strict';

describe('Controller: StudentSystemExamsCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var StudentSystemExamsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StudentSystemExamsCtrl = $controller('StudentSystemExamsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
