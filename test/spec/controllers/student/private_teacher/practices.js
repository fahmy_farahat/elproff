'use strict';

describe('Controller: StudentPrivateTeacherPracticesCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var StudentPrivateTeacherPracticesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StudentPrivateTeacherPracticesCtrl = $controller('StudentPrivateTeacherPracticesCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
