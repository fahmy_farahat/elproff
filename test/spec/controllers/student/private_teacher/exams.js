'use strict';

describe('Controller: StudentPrivateTeacherExamsCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var StudentPrivateTeacherExamsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StudentPrivateTeacherExamsCtrl = $controller('StudentPrivateTeacherExamsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
