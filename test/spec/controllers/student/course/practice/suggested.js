'use strict';

describe('Controller: StudentCoursePracticeSuggestedCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var StudentCoursePracticeSuggestedCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StudentCoursePracticeSuggestedCtrl = $controller('StudentCoursePracticeSuggestedCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
