'use strict';

describe('Controller: StudentCourseSubscribeCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var StudentCourseSubscribeCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StudentCourseSubscribeCtrl = $controller('StudentCourseSubscribeCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
