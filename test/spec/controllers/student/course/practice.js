'use strict';

describe('Controller: StudentCoursePracticeCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var StudentCoursePracticeCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StudentCoursePracticeCtrl = $controller('StudentCoursePracticeCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
