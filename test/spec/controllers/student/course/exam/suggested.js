'use strict';

describe('Controller: StudentCourseExamSuggestedCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var StudentCourseExamSuggestedCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StudentCourseExamSuggestedCtrl = $controller('StudentCourseExamSuggestedCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
