'use strict';

describe('Controller: StudentCourseExamPrivateteachersuggestedCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var StudentCourseExamPrivateteachersuggestedCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StudentCourseExamPrivateteachersuggestedCtrl = $controller('StudentCourseExamPrivateteachersuggestedCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
