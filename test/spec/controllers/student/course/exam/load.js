'use strict';

describe('Controller: StudentCourseExamLoadCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var StudentCourseExamLoadCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StudentCourseExamLoadCtrl = $controller('StudentCourseExamLoadCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
