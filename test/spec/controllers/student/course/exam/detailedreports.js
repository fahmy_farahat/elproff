'use strict';

describe('Controller: StudentCourseExamDetailedreportsCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var StudentCourseExamDetailedreportsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StudentCourseExamDetailedreportsCtrl = $controller('StudentCourseExamDetailedreportsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
