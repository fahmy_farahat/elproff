'use strict';

describe('Controller: StudentCourseExamSchoolteachersuggestedCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var StudentCourseExamSchoolteachersuggestedCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StudentCourseExamSchoolteachersuggestedCtrl = $controller('StudentCourseExamSchoolteachersuggestedCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
