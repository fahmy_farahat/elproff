'use strict';

describe('Controller: StudentParentCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var StudentParentCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StudentParentCtrl = $controller('StudentParentCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
