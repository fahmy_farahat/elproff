'use strict';

describe('Controller: StudentNotificationsCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var StudentNotificationsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StudentNotificationsCtrl = $controller('StudentNotificationsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
