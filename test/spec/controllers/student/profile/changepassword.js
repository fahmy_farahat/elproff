'use strict';

describe('Controller: StudentProfileChangepasswordCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var StudentProfileChangepasswordCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StudentProfileChangepasswordCtrl = $controller('StudentProfileChangepasswordCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
