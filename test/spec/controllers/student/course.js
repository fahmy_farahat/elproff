'use strict';

describe('Controller: StudentCourseCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var StudentCourseCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StudentCourseCtrl = $controller('StudentCourseCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
