'use strict';

describe('Controller: SmeNotificationsCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeNotificationsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeNotificationsCtrl = $controller('SmeNotificationsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
