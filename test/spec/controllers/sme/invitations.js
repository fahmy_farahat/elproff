'use strict';

describe('Controller: SmeInvitationsCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeInvitationsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeInvitationsCtrl = $controller('SmeInvitationsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
