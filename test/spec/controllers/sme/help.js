'use strict';

describe('Controller: SmeHelpCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeHelpCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeHelpCtrl = $controller('SmeHelpCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
