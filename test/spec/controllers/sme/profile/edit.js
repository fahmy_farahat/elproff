'use strict';

describe('Controller: SmeProfileEditCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeProfileEditCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeProfileEditCtrl = $controller('SmeProfileEditCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
