'use strict';

describe('Controller: SmeCourseCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeCourseCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeCourseCtrl = $controller('SmeCourseCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
