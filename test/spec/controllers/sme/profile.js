'use strict';

describe('Controller: SmeProfileCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeProfileCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeProfileCtrl = $controller('SmeProfileCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
