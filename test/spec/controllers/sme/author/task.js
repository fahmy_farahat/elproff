'use strict';

describe('Controller: SmeAuthorTaskCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeAuthorTaskCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeAuthorTaskCtrl = $controller('SmeAuthorTaskCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
