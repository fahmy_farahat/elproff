'use strict';

describe('Controller: SmeAuthorTaskViewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeAuthorTaskViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeAuthorTaskViewCtrl = $controller('SmeAuthorTaskViewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
