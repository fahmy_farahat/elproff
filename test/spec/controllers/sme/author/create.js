'use strict';

describe('Controller: SmeAuthorCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeAuthorCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeAuthorCreateCtrl = $controller('SmeAuthorCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
