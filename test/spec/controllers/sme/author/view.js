'use strict';

describe('Controller: SmeAuthorViewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeAuthorViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeAuthorViewCtrl = $controller('SmeAuthorViewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
