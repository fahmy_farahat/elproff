'use strict';

describe('Controller: SmeAuthorQuestionViewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeAuthorQuestionViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeAuthorQuestionViewCtrl = $controller('SmeAuthorQuestionViewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
