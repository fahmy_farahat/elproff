'use strict';

describe('Controller: SmeAuthorQuestionCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeAuthorQuestionCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeAuthorQuestionCtrl = $controller('SmeAuthorQuestionCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
