'use strict';

describe('Controller: SmeObjectiveViewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeObjectiveViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeObjectiveViewCtrl = $controller('SmeObjectiveViewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
