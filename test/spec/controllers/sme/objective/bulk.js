'use strict';

describe('Controller: SmeObjectiveBulkCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeObjectiveBulkCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeObjectiveBulkCtrl = $controller('SmeObjectiveBulkCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
