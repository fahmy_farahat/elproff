'use strict';

describe('Controller: SmeObjectiveCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeObjectiveCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeObjectiveCreateCtrl = $controller('SmeObjectiveCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
