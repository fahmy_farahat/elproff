'use strict';

describe('Controller: SmeAuthorCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeAuthorCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeAuthorCtrl = $controller('SmeAuthorCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
