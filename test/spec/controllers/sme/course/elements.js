'use strict';

describe('Controller: SmeCourseElementsCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeCourseElementsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeCourseElementsCtrl = $controller('SmeCourseElementsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
