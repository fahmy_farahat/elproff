'use strict';

describe('Controller: SmeCourseDuplicationViewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeCourseDuplicationViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeCourseDuplicationViewCtrl = $controller('SmeCourseDuplicationViewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
