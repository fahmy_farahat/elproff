'use strict';

describe('Controller: SmeCourseElementTypeCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeCourseElementTypeCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeCourseElementTypeCreateCtrl = $controller('SmeCourseElementTypeCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
