'use strict';

describe('Controller: SmeCourseElementCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeCourseElementCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeCourseElementCreateCtrl = $controller('SmeCourseElementCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
