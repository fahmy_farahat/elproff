'use strict';

describe('Controller: SmeCourseCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeCourseCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeCourseCreateCtrl = $controller('SmeCourseCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
