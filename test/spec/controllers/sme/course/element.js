'use strict';

describe('Controller: SmeCourseElementCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeCourseElementCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeCourseElementCtrl = $controller('SmeCourseElementCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
