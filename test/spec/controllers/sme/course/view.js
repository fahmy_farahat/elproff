'use strict';

describe('Controller: SmeCourseViewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeCourseViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeCourseViewCtrl = $controller('SmeCourseViewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
