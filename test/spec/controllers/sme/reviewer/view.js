'use strict';

describe('Controller: SmeReviewerViewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeReviewerViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeReviewerViewCtrl = $controller('SmeReviewerViewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
