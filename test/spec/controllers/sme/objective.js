'use strict';

describe('Controller: SmeObjectiveCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SmeObjectiveCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SmeObjectiveCtrl = $controller('SmeObjectiveCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
