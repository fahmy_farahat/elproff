'use strict';

describe('Controller: PrivateteacherCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var PrivateteacherCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PrivateteacherCtrl = $controller('PrivateteacherCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
