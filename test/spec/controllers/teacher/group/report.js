'use strict';

describe('Controller: TeacherGroupReportCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var TeacherGroupReportCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TeacherGroupReportCtrl = $controller('TeacherGroupReportCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
