'use strict';

describe('Controller: TeacherGroupEditCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var TeacherGroupEditCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TeacherGroupEditCtrl = $controller('TeacherGroupEditCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
