'use strict';

describe('Controller: TeacherGroupCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var TeacherGroupCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TeacherGroupCreateCtrl = $controller('TeacherGroupCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
