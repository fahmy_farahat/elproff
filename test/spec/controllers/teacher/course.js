'use strict';

describe('Controller: TeacherCourseCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var TeacherCourseCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TeacherCourseCtrl = $controller('TeacherCourseCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
