'use strict';

describe('Controller: TeacherHomeworkCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var TeacherHomeworkCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TeacherHomeworkCreateCtrl = $controller('TeacherHomeworkCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
