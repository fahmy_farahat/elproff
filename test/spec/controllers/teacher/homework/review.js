'use strict';

describe('Controller: TeacherHomeworkReviewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var TeacherHomeworkReviewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TeacherHomeworkReviewCtrl = $controller('TeacherHomeworkReviewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
