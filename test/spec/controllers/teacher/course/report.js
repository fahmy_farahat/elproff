'use strict';

describe('Controller: TeacherCourseReportCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var TeacherCourseReportCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TeacherCourseReportCtrl = $controller('TeacherCourseReportCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
