'use strict';

describe('Controller: TeacherProfileEditCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var TeacherProfileEditCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TeacherProfileEditCtrl = $controller('TeacherProfileEditCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
