'use strict';

describe('Controller: TeacherExamQuestionCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var TeacherExamQuestionCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TeacherExamQuestionCreateCtrl = $controller('TeacherExamQuestionCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
