'use strict';

describe('Controller: TeacherExamReviewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var TeacherExamReviewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TeacherExamReviewCtrl = $controller('TeacherExamReviewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
