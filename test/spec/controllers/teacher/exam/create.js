'use strict';

describe('Controller: TeacherExamCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var TeacherExamCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TeacherExamCreateCtrl = $controller('TeacherExamCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
