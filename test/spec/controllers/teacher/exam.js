'use strict';

describe('Controller: TeacherExamCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var TeacherExamCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TeacherExamCtrl = $controller('TeacherExamCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
