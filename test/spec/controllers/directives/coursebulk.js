'use strict';

describe('Controller: DirectivesCoursebulkCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var DirectivesCoursebulkCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DirectivesCoursebulkCtrl = $controller('DirectivesCoursebulkCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
