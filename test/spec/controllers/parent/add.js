'use strict';

describe('Controller: ParentAddCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ParentAddCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ParentAddCtrl = $controller('ParentAddCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
