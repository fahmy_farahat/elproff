'use strict';

describe('Controller: ParentSubjectCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ParentSubjectCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ParentSubjectCtrl = $controller('ParentSubjectCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
