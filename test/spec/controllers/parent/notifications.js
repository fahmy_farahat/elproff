'use strict';

describe('Controller: ParentNotificationsCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ParentNotificationsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ParentNotificationsCtrl = $controller('ParentNotificationsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
