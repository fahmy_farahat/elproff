'use strict';

describe('Controller: ParentProfileCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ParentProfileCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ParentProfileCtrl = $controller('ParentProfileCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
