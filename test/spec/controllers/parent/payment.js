'use strict';

describe('Controller: ParentPaymentCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ParentPaymentCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ParentPaymentCtrl = $controller('ParentPaymentCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
