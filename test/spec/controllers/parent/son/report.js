'use strict';

describe('Controller: ParentSonReportCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ParentSonReportCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ParentSonReportCtrl = $controller('ParentSonReportCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
