'use strict';

describe('Controller: ParentSonCourseExamCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ParentSonCourseExamCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ParentSonCourseExamCtrl = $controller('ParentSonCourseExamCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
