'use strict';

describe('Controller: ModalsHelpequationattachmentCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ModalsHelpequationattachmentCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModalsHelpequationattachmentCtrl = $controller('ModalsHelpequationattachmentCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
