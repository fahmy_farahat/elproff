'use strict';

describe('Controller: ModalsUserprofileCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ModalsUserprofileCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModalsUserprofileCtrl = $controller('ModalsUserprofileCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
