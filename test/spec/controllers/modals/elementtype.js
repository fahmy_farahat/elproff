'use strict';

describe('Controller: ModalsElementtypeCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ModalsElementtypeCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModalsElementtypeCtrl = $controller('ModalsElementtypeCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
