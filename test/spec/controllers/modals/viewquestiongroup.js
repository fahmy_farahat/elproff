'use strict';

describe('Controller: ModalsViewquestiongroupCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ModalsViewquestiongroupCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModalsViewquestiongroupCtrl = $controller('ModalsViewquestiongroupCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
