'use strict';

describe('Controller: ModalsEditobjectiveCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ModalsEditobjectiveCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModalsEditobjectiveCtrl = $controller('ModalsEditobjectiveCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
