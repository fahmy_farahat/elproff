'use strict';

describe('Controller: ModalsAddcouponCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ModalsAddcouponCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModalsAddcouponCtrl = $controller('ModalsAddcouponCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
