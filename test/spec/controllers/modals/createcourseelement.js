'use strict';

describe('Controller: ModalsCreatecourseelementCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ModalsCreatecourseelementCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModalsCreatecourseelementCtrl = $controller('ModalsCreatecourseelementCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
