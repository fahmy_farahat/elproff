'use strict';

describe('Controller: ModalsHelpattachmentCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ModalsHelpattachmentCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModalsHelpattachmentCtrl = $controller('ModalsHelpattachmentCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
