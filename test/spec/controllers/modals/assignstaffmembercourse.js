'use strict';

describe('Controller: ModalsAssignstaffmembercourseCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ModalsAssignstaffmembercourseCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModalsAssignstaffmembercourseCtrl = $controller('ModalsAssignstaffmembercourseCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
