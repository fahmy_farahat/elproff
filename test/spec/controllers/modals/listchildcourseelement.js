'use strict';

describe('Controller: ModalsListchildcourseelementCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ModalsListchildcourseelementCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModalsListchildcourseelementCtrl = $controller('ModalsListchildcourseelementCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
