'use strict';

describe('Controller: ModalsPrivateteacheraddsubjectCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ModalsPrivateteacheraddsubjectCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModalsPrivateteacheraddsubjectCtrl = $controller('ModalsPrivateteacheraddsubjectCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
