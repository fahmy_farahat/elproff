'use strict';

describe('Controller: ModalsCreatequestionsbulkCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ModalsCreatequestionsbulkCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModalsCreatequestionsbulkCtrl = $controller('ModalsCreatequestionsbulkCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
