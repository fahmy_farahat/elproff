'use strict';

describe('Controller: ModalsAddedusysschoolCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ModalsAddedusysschoolCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModalsAddedusysschoolCtrl = $controller('ModalsAddedusysschoolCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
