'use strict';

describe('Controller: ModalsExamskippedquestionsCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ModalsExamskippedquestionsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModalsExamskippedquestionsCtrl = $controller('ModalsExamskippedquestionsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
