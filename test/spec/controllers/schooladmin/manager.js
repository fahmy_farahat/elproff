'use strict';

describe('Controller: SchooladminManagerCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchooladminManagerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchooladminManagerCtrl = $controller('SchooladminManagerCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
