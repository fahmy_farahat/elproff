'use strict';

describe('Controller: SchooladminStudentAddCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchooladminStudentAddCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchooladminStudentAddCtrl = $controller('SchooladminStudentAddCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
