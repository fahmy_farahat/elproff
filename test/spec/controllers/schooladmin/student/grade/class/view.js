'use strict';

describe('Controller: SchooladminStudentGradeClassViewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchooladminStudentGradeClassViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchooladminStudentGradeClassViewCtrl = $controller('SchooladminStudentGradeClassViewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
