'use strict';

describe('Controller: SchooladminStudentGradeClassInviteCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchooladminStudentGradeClassInviteCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchooladminStudentGradeClassInviteCtrl = $controller('SchooladminStudentGradeClassInviteCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
