'use strict';

describe('Controller: SchooladminStudentGradeViewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchooladminStudentGradeViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchooladminStudentGradeViewCtrl = $controller('SchooladminStudentGradeViewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
