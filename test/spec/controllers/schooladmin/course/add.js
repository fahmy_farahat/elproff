'use strict';

describe('Controller: SchooladminCourseAddCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchooladminCourseAddCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchooladminCourseAddCtrl = $controller('SchooladminCourseAddCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
