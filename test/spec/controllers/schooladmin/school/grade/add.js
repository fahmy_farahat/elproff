'use strict';

describe('Controller: SchooladminSchoolGradeAddCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchooladminSchoolGradeAddCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchooladminSchoolGradeAddCtrl = $controller('SchooladminSchoolGradeAddCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
