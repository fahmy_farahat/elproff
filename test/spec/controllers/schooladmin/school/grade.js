'use strict';

describe('Controller: SchooladminSchoolGradeCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchooladminSchoolGradeCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchooladminSchoolGradeCtrl = $controller('SchooladminSchoolGradeCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
