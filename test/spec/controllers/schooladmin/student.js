'use strict';

describe('Controller: SchooladminStudentCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchooladminStudentCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchooladminStudentCtrl = $controller('SchooladminStudentCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
