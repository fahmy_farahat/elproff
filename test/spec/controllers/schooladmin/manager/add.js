'use strict';

describe('Controller: SchooladminManagerAddCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchooladminManagerAddCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchooladminManagerAddCtrl = $controller('SchooladminManagerAddCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
