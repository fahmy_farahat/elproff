'use strict';

describe('Controller: SchooladminHelpCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchooladminHelpCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchooladminHelpCtrl = $controller('SchooladminHelpCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
