'use strict';

describe('Controller: SchooladminClassesAddCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchooladminClassesAddCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchooladminClassesAddCtrl = $controller('SchooladminClassesAddCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
