'use strict';

describe('Controller: SchooladminProfileSchoolCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchooladminProfileSchoolCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchooladminProfileSchoolCtrl = $controller('SchooladminProfileSchoolCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
