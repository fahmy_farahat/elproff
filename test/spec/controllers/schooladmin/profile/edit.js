'use strict';

describe('Controller: SchooladminProfileEditCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchooladminProfileEditCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchooladminProfileEditCtrl = $controller('SchooladminProfileEditCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
