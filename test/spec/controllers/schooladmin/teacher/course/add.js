'use strict';

describe('Controller: SchooladminTeacherCourseAddCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchooladminTeacherCourseAddCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchooladminTeacherCourseAddCtrl = $controller('SchooladminTeacherCourseAddCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
