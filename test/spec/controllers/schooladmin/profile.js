'use strict';

describe('Controller: SchooladminProfileCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchooladminProfileCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchooladminProfileCtrl = $controller('SchooladminProfileCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
