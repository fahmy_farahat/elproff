'use strict';

describe('Controller: PaginatorCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var PaginatorCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PaginatorCtrl = $controller('PaginatorCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
