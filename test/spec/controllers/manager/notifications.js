'use strict';

describe('Controller: ManagerNotificationsCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ManagerNotificationsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ManagerNotificationsCtrl = $controller('ManagerNotificationsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
