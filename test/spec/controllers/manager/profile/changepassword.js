'use strict';

describe('Controller: ManagerProfileChangepasswordCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ManagerProfileChangepasswordCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ManagerProfileChangepasswordCtrl = $controller('ManagerProfileChangepasswordCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
