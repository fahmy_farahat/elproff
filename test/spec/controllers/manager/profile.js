'use strict';

describe('Controller: ManagerProfileCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ManagerProfileCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ManagerProfileCtrl = $controller('ManagerProfileCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
