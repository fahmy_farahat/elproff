'use strict';

describe('Controller: AuthorTaskMyCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AuthorTaskMyCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AuthorTaskMyCtrl = $controller('AuthorTaskMyCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
