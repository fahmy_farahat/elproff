'use strict';

describe('Controller: AuthorHelpCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AuthorHelpCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AuthorHelpCtrl = $controller('AuthorHelpCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
