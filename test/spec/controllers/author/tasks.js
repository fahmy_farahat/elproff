'use strict';

describe('Controller: AuthorTasksCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AuthorTasksCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AuthorTasksCtrl = $controller('AuthorTasksCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
