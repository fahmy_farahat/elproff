'use strict';

describe('Controller: AuthorProfileChangepasswordCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AuthorProfileChangepasswordCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AuthorProfileChangepasswordCtrl = $controller('AuthorProfileChangepasswordCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
