'use strict';

describe('Controller: AuthorQuestionCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AuthorQuestionCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AuthorQuestionCtrl = $controller('AuthorQuestionCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
