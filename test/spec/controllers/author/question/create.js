'use strict';

describe('Controller: AuthorQuestionCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AuthorQuestionCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AuthorQuestionCreateCtrl = $controller('AuthorQuestionCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
