'use strict';

describe('Controller: AuthorCourseTaskCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AuthorCourseTaskCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AuthorCourseTaskCtrl = $controller('AuthorCourseTaskCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
