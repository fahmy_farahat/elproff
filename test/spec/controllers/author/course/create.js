'use strict';

describe('Controller: AuthorCourseCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AuthorCourseCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AuthorCourseCreateCtrl = $controller('AuthorCourseCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
