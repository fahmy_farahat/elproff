'use strict';

describe('Controller: SchoolmanagerHelpCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchoolmanagerHelpCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchoolmanagerHelpCtrl = $controller('SchoolmanagerHelpCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
