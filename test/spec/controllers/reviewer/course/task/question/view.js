'use strict';

describe('Controller: ReviewerCourseTaskQuestionViewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ReviewerCourseTaskQuestionViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ReviewerCourseTaskQuestionViewCtrl = $controller('ReviewerCourseTaskQuestionViewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
