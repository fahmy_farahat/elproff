'use strict';

describe('Controller: ReviewerCourseTaskCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ReviewerCourseTaskCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ReviewerCourseTaskCreateCtrl = $controller('ReviewerCourseTaskCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
