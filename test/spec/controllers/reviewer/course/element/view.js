'use strict';

describe('Controller: ReviewerCourseElementViewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ReviewerCourseElementViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ReviewerCourseElementViewCtrl = $controller('ReviewerCourseElementViewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
