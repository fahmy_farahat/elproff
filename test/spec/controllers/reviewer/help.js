'use strict';

describe('Controller: ReviewerHelpCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ReviewerHelpCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ReviewerHelpCtrl = $controller('ReviewerHelpCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
