'use strict';

describe('Controller: ReviewerProfileChangepasswordCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ReviewerProfileChangepasswordCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ReviewerProfileChangepasswordCtrl = $controller('ReviewerProfileChangepasswordCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
