'use strict';

describe('Controller: ReviewerProfileEditCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ReviewerProfileEditCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ReviewerProfileEditCtrl = $controller('ReviewerProfileEditCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
