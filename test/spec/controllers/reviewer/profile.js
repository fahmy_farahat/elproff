'use strict';

describe('Controller: ReviewerProfileCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ReviewerProfileCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ReviewerProfileCtrl = $controller('ReviewerProfileCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
