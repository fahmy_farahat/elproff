'use strict';

describe('Controller: PrivateteacherProfileCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var PrivateteacherProfileCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PrivateteacherProfileCtrl = $controller('PrivateteacherProfileCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
