'use strict';

describe('Controller: PrivateteacherCourseHomeworkCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var PrivateteacherCourseHomeworkCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PrivateteacherCourseHomeworkCreateCtrl = $controller('PrivateteacherCourseHomeworkCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
