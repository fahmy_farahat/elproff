'use strict';

describe('Controller: PrivateteacherCourseExamCreateReviewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var PrivateteacherCourseExamCreateReviewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PrivateteacherCourseExamCreateReviewCtrl = $controller('PrivateteacherCourseExamCreateReviewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
