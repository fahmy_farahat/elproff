'use strict';

describe('Controller: PrivateteacherCourseExamCreateQuestionsCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var PrivateteacherCourseExamCreateQuestionsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PrivateteacherCourseExamCreateQuestionsCtrl = $controller('PrivateteacherCourseExamCreateQuestionsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
