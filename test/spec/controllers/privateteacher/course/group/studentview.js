'use strict';

describe('Controller: PrivateteacherCourseGroupStudentviewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var PrivateteacherCourseGroupStudentviewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PrivateteacherCourseGroupStudentviewCtrl = $controller('PrivateteacherCourseGroupStudentviewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
