'use strict';

describe('Controller: PrivateteacherGroupUpdateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var PrivateteacherGroupUpdateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PrivateteacherGroupUpdateCtrl = $controller('PrivateteacherGroupUpdateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
