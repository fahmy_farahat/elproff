'use strict';

describe('Controller: PrivateteacherHelpCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var PrivateteacherHelpCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PrivateteacherHelpCtrl = $controller('PrivateteacherHelpCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
