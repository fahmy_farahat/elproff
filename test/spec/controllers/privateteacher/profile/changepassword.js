'use strict';

describe('Controller: PrivateteacherProfileChangepasswordCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var PrivateteacherProfileChangepasswordCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PrivateteacherProfileChangepasswordCtrl = $controller('PrivateteacherProfileChangepasswordCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
