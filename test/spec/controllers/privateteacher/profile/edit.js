'use strict';

describe('Controller: PrivateteacherProfileEditCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var PrivateteacherProfileEditCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PrivateteacherProfileEditCtrl = $controller('PrivateteacherProfileEditCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
