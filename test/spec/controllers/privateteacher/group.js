'use strict';

describe('Controller: PrivateteacherGroupCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var PrivateteacherGroupCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PrivateteacherGroupCtrl = $controller('PrivateteacherGroupCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
