'use strict';

describe('Controller: PrivateteacherNotificationsCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var PrivateteacherNotificationsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PrivateteacherNotificationsCtrl = $controller('PrivateteacherNotificationsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
