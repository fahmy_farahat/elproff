'use strict';

describe('Controller: AdminUserListCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminUserListCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminUserListCtrl = $controller('AdminUserListCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
