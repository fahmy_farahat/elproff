'use strict';

describe('Controller: AdminEducationalsystemUpdateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminEducationalsystemUpdateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminEducationalsystemUpdateCtrl = $controller('AdminEducationalsystemUpdateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
