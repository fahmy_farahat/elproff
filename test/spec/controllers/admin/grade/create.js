'use strict';

describe('Controller: AdminGradeCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminGradeCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminGradeCreateCtrl = $controller('AdminGradeCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
