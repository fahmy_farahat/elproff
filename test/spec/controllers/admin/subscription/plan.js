'use strict';

describe('Controller: AdminSubscriptionPlanCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminSubscriptionPlanCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminSubscriptionPlanCtrl = $controller('AdminSubscriptionPlanCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
