'use strict';

describe('Controller: AdminSubscriptionUpdateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminSubscriptionUpdateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminSubscriptionUpdateCtrl = $controller('AdminSubscriptionUpdateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
