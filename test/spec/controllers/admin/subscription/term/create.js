'use strict';

describe('Controller: AdminSubscriptionTermCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminSubscriptionTermCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminSubscriptionTermCreateCtrl = $controller('AdminSubscriptionTermCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
