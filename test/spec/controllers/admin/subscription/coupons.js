'use strict';

describe('Controller: AdminSubscriptionCouponsCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminSubscriptionCouponsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminSubscriptionCouponsCtrl = $controller('AdminSubscriptionCouponsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
