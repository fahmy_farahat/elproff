'use strict';

describe('Controller: AdminCourseCourseElementUpdateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminCourseCourseElementUpdateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminCourseCourseElementUpdateCtrl = $controller('AdminCourseCourseElementUpdateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
