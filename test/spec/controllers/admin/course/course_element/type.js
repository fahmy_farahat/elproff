'use strict';

describe('Controller: AdminCourseCourseElementTypeCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminCourseCourseElementTypeCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminCourseCourseElementTypeCtrl = $controller('AdminCourseCourseElementTypeCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
