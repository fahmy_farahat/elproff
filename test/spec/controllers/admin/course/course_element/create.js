'use strict';

describe('Controller: AdminCourseCourseElementCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminCourseCourseElementCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminCourseCourseElementCreateCtrl = $controller('AdminCourseCourseElementCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
