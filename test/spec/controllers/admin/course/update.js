'use strict';

describe('Controller: AdminCourseUpdateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminCourseUpdateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminCourseUpdateCtrl = $controller('AdminCourseUpdateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
