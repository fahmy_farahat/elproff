'use strict';

describe('Controller: AdminCourseCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminCourseCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminCourseCreateCtrl = $controller('AdminCourseCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
