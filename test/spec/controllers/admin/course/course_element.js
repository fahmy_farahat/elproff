'use strict';

describe('Controller: AdminCourseCourseElementCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminCourseCourseElementCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminCourseCourseElementCtrl = $controller('AdminCourseCourseElementCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
