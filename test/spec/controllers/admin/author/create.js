'use strict';

describe('Controller: AdminAuthorCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminAuthorCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminAuthorCreateCtrl = $controller('AdminAuthorCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
