'use strict';

describe('Controller: AdminAuthorTaskCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminAuthorTaskCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminAuthorTaskCtrl = $controller('AdminAuthorTaskCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
