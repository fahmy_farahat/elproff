'use strict';

describe('Controller: AdminAuthorViewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminAuthorViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminAuthorViewCtrl = $controller('AdminAuthorViewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
