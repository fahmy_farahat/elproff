'use strict';

describe('Controller: AdminAuthorTaskViewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminAuthorTaskViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminAuthorTaskViewCtrl = $controller('AdminAuthorTaskViewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
