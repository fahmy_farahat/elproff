'use strict';

describe('Controller: AdminAuthorQuestionCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminAuthorQuestionCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminAuthorQuestionCtrl = $controller('AdminAuthorQuestionCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
