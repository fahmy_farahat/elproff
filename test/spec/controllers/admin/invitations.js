'use strict';

describe('Controller: AdminInvitationsCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminInvitationsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminInvitationsCtrl = $controller('AdminInvitationsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
