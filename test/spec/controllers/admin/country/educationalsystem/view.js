'use strict';

describe('Controller: AdminCountryEducationalsystemViewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminCountryEducationalsystemViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminCountryEducationalsystemViewCtrl = $controller('AdminCountryEducationalsystemViewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
