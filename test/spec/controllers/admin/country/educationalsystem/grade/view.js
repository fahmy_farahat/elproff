'use strict';

describe('Controller: AdminCountryEducationalsystemGradeViewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminCountryEducationalsystemGradeViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminCountryEducationalsystemGradeViewCtrl = $controller('AdminCountryEducationalsystemGradeViewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
