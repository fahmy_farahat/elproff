'use strict';

describe('Controller: AdminCountryEducationalsystemGradeSubscriptionViewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminCountryEducationalsystemGradeSubscriptionViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminCountryEducationalsystemGradeSubscriptionViewCtrl = $controller('AdminCountryEducationalsystemGradeSubscriptionViewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
