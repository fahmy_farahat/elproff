'use strict';

describe('Controller: AdminCountryEducationalsystemGradeViewdelCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminCountryEducationalsystemGradeViewdelCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminCountryEducationalsystemGradeViewdelCtrl = $controller('AdminCountryEducationalsystemGradeViewdelCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
