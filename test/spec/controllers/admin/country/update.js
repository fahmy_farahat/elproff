'use strict';

describe('Controller: AdminCountryUpdateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminCountryUpdateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminCountryUpdateCtrl = $controller('AdminCountryUpdateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
