'use strict';

describe('Controller: AdminCountryViewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminCountryViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminCountryViewCtrl = $controller('AdminCountryViewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
