'use strict';

describe('Controller: AdminCountryCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminCountryCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminCountryCreateCtrl = $controller('AdminCountryCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
