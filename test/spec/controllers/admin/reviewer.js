'use strict';

describe('Controller: AdminReviewerCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminReviewerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminReviewerCtrl = $controller('AdminReviewerCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
