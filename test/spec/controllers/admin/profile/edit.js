'use strict';

describe('Controller: AdminProfileEditCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminProfileEditCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminProfileEditCtrl = $controller('AdminProfileEditCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
