'use strict';

describe('Controller: AdminSmeCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminSmeCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminSmeCtrl = $controller('AdminSmeCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
