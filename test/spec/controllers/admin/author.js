'use strict';

describe('Controller: AdminAuthorCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminAuthorCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminAuthorCtrl = $controller('AdminAuthorCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
