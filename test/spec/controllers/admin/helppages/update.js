'use strict';

describe('Controller: AdminHelppagesUpdateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminHelppagesUpdateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminHelppagesUpdateCtrl = $controller('AdminHelppagesUpdateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
