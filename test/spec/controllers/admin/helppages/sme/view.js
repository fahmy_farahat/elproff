'use strict';

describe('Controller: AdminHelppagesSmeViewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminHelppagesSmeViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminHelppagesSmeViewCtrl = $controller('AdminHelppagesSmeViewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
