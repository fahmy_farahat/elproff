'use strict';

describe('Controller: AdminHelppagesCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminHelppagesCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminHelppagesCreateCtrl = $controller('AdminHelppagesCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
