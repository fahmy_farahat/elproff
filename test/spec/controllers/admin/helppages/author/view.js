'use strict';

describe('Controller: AdminHelppagesAuthorViewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminHelppagesAuthorViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminHelppagesAuthorViewCtrl = $controller('AdminHelppagesAuthorViewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
