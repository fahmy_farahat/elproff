'use strict';

describe('Controller: AdminPrizeCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminPrizeCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminPrizeCreateCtrl = $controller('AdminPrizeCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
