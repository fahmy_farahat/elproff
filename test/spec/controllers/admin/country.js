'use strict';

describe('Controller: AdminCountryCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminCountryCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminCountryCtrl = $controller('AdminCountryCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
