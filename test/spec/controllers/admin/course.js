'use strict';

describe('Controller: AdminCourseCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminCourseCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminCourseCtrl = $controller('AdminCourseCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
