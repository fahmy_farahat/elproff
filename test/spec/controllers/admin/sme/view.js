'use strict';

describe('Controller: AdminSmeViewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminSmeViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminSmeViewCtrl = $controller('AdminSmeViewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
