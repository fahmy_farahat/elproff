'use strict';

describe('Controller: AdminSmeCreateCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminSmeCreateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminSmeCreateCtrl = $controller('AdminSmeCreateCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
