'use strict';

describe('Controller: AdminEducationalsystemCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminEducationalsystemCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminEducationalsystemCtrl = $controller('AdminEducationalsystemCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
