'use strict';

describe('Controller: AdminElementTypeViewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminElementTypeViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminElementTypeViewCtrl = $controller('AdminElementTypeViewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
