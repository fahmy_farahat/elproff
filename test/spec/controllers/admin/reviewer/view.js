'use strict';

describe('Controller: AdminReviewerViewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminReviewerViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminReviewerViewCtrl = $controller('AdminReviewerViewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
