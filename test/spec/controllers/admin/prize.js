'use strict';

describe('Controller: AdminPrizeCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminPrizeCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminPrizeCtrl = $controller('AdminPrizeCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
