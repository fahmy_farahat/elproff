'use strict';

describe('Controller: AdminHelpCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var AdminHelpCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminHelpCtrl = $controller('AdminHelpCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
