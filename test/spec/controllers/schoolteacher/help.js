'use strict';

describe('Controller: SchoolteacherHelpCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchoolteacherHelpCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchoolteacherHelpCtrl = $controller('SchoolteacherHelpCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
