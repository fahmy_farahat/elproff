'use strict';

describe('Controller: SchoolteacherProfileEditCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchoolteacherProfileEditCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchoolteacherProfileEditCtrl = $controller('SchoolteacherProfileEditCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
