'use strict';

describe('Controller: SchoolteacherProfileCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchoolteacherProfileCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchoolteacherProfileCtrl = $controller('SchoolteacherProfileCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
