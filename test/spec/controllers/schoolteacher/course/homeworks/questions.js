'use strict';

describe('Controller: SchoolteacherCourseHomeworksQuestionsCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchoolteacherCourseHomeworksQuestionsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchoolteacherCourseHomeworksQuestionsCtrl = $controller('SchoolteacherCourseHomeworksQuestionsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
