'use strict';

describe('Controller: SchoolteacherCourseHomeworksReportsCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchoolteacherCourseHomeworksReportsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchoolteacherCourseHomeworksReportsCtrl = $controller('SchoolteacherCourseHomeworksReportsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
