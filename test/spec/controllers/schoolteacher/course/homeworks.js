'use strict';

describe('Controller: SchoolteacherCourseHomeworksCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchoolteacherCourseHomeworksCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchoolteacherCourseHomeworksCtrl = $controller('SchoolteacherCourseHomeworksCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
