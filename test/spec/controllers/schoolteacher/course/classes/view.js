'use strict';

describe('Controller: SchoolteacherCourseClassesViewCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchoolteacherCourseClassesViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchoolteacherCourseClassesViewCtrl = $controller('SchoolteacherCourseClassesViewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
