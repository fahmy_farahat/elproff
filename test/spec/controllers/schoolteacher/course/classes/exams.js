'use strict';

describe('Controller: SchoolteacherCourseClassesExamsCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchoolteacherCourseClassesExamsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchoolteacherCourseClassesExamsCtrl = $controller('SchoolteacherCourseClassesExamsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
