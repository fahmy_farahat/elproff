'use strict';

describe('Controller: SchoolteacherNotificationsCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchoolteacherNotificationsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchoolteacherNotificationsCtrl = $controller('SchoolteacherNotificationsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
