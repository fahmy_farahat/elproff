'use strict';

describe('Controller: SchoolAdminCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchoolAdminCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchoolAdminCtrl = $controller('SchoolAdminCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
