'use strict';

describe('Controller: SchoolAdminProfileCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchoolAdminProfileCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchoolAdminProfileCtrl = $controller('SchoolAdminProfileCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
