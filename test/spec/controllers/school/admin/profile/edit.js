'use strict';

describe('Controller: SchoolAdminProfileEditCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchoolAdminProfileEditCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchoolAdminProfileEditCtrl = $controller('SchoolAdminProfileEditCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
