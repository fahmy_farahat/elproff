'use strict';

describe('Controller: SchoolProfileCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchoolProfileCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchoolProfileCtrl = $controller('SchoolProfileCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
