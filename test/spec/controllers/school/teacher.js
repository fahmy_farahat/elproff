'use strict';

describe('Controller: SchoolTeacherCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchoolTeacherCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchoolTeacherCtrl = $controller('SchoolTeacherCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
