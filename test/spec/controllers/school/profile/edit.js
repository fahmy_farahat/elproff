'use strict';

describe('Controller: SchoolProfileEditCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var SchoolProfileEditCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SchoolProfileEditCtrl = $controller('SchoolProfileEditCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
