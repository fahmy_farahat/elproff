'use strict';

describe('Controller: ParentCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ParentCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ParentCtrl = $controller('ParentCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
