'use strict';

describe('Controller: ReviewerCtrl', function () {

  // load the controller's module
  beforeEach(module('elproffApp'));

  var ReviewerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ReviewerCtrl = $controller('ReviewerCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
