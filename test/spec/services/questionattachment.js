'use strict';

describe('Service: QuestionAttachment', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var QuestionAttachment;
  beforeEach(inject(function (_QuestionAttachment_) {
    QuestionAttachment = _QuestionAttachment_;
  }));

  it('should do something', function () {
    expect(!!QuestionAttachment).toBe(true);
  });

});
