'use strict';

describe('Service: exam', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var exam;
  beforeEach(inject(function (_exam_) {
    exam = _exam_;
  }));

  it('should do something', function () {
    expect(!!exam).toBe(true);
  });

});
