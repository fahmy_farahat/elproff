'use strict';

describe('Service: LearningOutcome', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var LearningOutcome;
  beforeEach(inject(function (_LearningOutcome_) {
    LearningOutcome = _LearningOutcome_;
  }));

  it('should do something', function () {
    expect(!!LearningOutcome).toBe(true);
  });

});
