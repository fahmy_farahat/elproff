'use strict';

describe('Service: CommonCache', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var CommonCache;
  beforeEach(inject(function (_CommonCache_) {
    CommonCache = _CommonCache_;
  }));

  it('should do something', function () {
    expect(!!CommonCache).toBe(true);
  });

});
