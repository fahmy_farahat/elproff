'use strict';

describe('Service: Reviewer', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var Reviewer;
  beforeEach(inject(function (_Reviewer_) {
    Reviewer = _Reviewer_;
  }));

  it('should do something', function () {
    expect(!!Reviewer).toBe(true);
  });

});
