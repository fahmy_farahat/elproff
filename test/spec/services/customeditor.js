'use strict';

describe('Service: CustomEditor', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var CustomEditor;
  beforeEach(inject(function (_CustomEditor_) {
    CustomEditor = _CustomEditor_;
  }));

  it('should do something', function () {
    expect(!!CustomEditor).toBe(true);
  });

});
