'use strict';

describe('Service: QuestionsBulk', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var QuestionsBulk;
  beforeEach(inject(function (_QuestionsBulk_) {
    QuestionsBulk = _QuestionsBulk_;
  }));

  it('should do something', function () {
    expect(!!QuestionsBulk).toBe(true);
  });

});
