'use strict';

describe('Service: ExamInstance', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var ExamInstance;
  beforeEach(inject(function (_ExamInstance_) {
    ExamInstance = _ExamInstance_;
  }));

  it('should do something', function () {
    expect(!!ExamInstance).toBe(true);
  });

});
