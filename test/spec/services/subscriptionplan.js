'use strict';

describe('Service: SubscriptionPlan', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var SubscriptionPlan;
  beforeEach(inject(function (_SubscriptionPlan_) {
    SubscriptionPlan = _SubscriptionPlan_;
  }));

  it('should do something', function () {
    expect(!!SubscriptionPlan).toBe(true);
  });

});
