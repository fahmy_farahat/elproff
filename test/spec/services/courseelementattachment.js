'use strict';

describe('Service: CourseElementAttachment', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var CourseElementAttachment;
  beforeEach(inject(function (_CourseElementAttachment_) {
    CourseElementAttachment = _CourseElementAttachment_;
  }));

  it('should do something', function () {
    expect(!!CourseElementAttachment).toBe(true);
  });

});
