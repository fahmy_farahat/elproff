'use strict';

describe('Service: SchoolTeacher', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var SchoolTeacher;
  beforeEach(inject(function (_SchoolTeacher_) {
    SchoolTeacher = _SchoolTeacher_;
  }));

  it('should do something', function () {
    expect(!!SchoolTeacher).toBe(true);
  });

});
