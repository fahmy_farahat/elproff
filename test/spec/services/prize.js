'use strict';

describe('Service: Prize', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var Prize;
  beforeEach(inject(function (_Prize_) {
    Prize = _Prize_;
  }));

  it('should do something', function () {
    expect(!!Prize).toBe(true);
  });

});
