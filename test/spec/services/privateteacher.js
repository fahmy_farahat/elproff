'use strict';

describe('Service: privateteacher', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var privateteacher;
  beforeEach(inject(function (_privateteacher_) {
    privateteacher = _privateteacher_;
  }));

  it('should do something', function () {
    expect(!!privateteacher).toBe(true);
  });

});
