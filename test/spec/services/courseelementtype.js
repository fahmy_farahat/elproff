'use strict';

describe('Service: CourseElementType', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var CourseElementType;
  beforeEach(inject(function (_CourseElementType_) {
    CourseElementType = _CourseElementType_;
  }));

  it('should do something', function () {
    expect(!!CourseElementType).toBe(true);
  });

});
