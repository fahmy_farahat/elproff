'use strict';

describe('Service: AnswersDegree', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var AnswersDegree;
  beforeEach(inject(function (_AnswersDegree_) {
    AnswersDegree = _AnswersDegree_;
  }));

  it('should do something', function () {
    expect(!!AnswersDegree).toBe(true);
  });

});
