'use strict';

describe('Service: Coupon', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var Coupon;
  beforeEach(inject(function (_Coupon_) {
    Coupon = _Coupon_;
  }));

  it('should do something', function () {
    expect(!!Coupon).toBe(true);
  });

});
