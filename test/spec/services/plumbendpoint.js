'use strict';

describe('Service: plumbEndPoint', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var plumbEndPoint;
  beforeEach(inject(function (_plumbEndPoint_) {
    plumbEndPoint = _plumbEndPoint_;
  }));

  it('should do something', function () {
    expect(!!plumbEndPoint).toBe(true);
  });

});
