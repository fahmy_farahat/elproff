'use strict';

describe('Service: SME', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var SME;
  beforeEach(inject(function (_SME_) {
    SME = _SME_;
  }));

  it('should do something', function () {
    expect(!!SME).toBe(true);
  });

});
