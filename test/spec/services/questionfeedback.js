'use strict';

describe('Service: QuestionFeedback', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var QuestionFeedback;
  beforeEach(inject(function (_QuestionFeedback_) {
    QuestionFeedback = _QuestionFeedback_;
  }));

  it('should do something', function () {
    expect(!!QuestionFeedback).toBe(true);
  });

});
