'use strict';

describe('Service: Parent', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var Parent;
  beforeEach(inject(function (_Parent_) {
    Parent = _Parent_;
  }));

  it('should do something', function () {
    expect(!!Parent).toBe(true);
  });

});
