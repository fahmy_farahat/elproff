'use strict';

describe('Service: TeacherBuild', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var TeacherBuild;
  beforeEach(inject(function (_TeacherBuild_) {
    TeacherBuild = _TeacherBuild_;
  }));

  it('should do something', function () {
    expect(!!TeacherBuild).toBe(true);
  });

});
