'use strict';

describe('Service: HTTPCache', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var HTTPCache;
  beforeEach(inject(function (_HTTPCache_) {
    HTTPCache = _HTTPCache_;
  }));

  it('should do something', function () {
    expect(!!HTTPCache).toBe(true);
  });

});
