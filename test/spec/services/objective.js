'use strict';

describe('Service: Objective', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var Objective;
  beforeEach(inject(function (_Objective_) {
    Objective = _Objective_;
  }));

  it('should do something', function () {
    expect(!!Objective).toBe(true);
  });

});
