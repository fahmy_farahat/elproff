'use strict';

describe('Service: Grade', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var Grade;
  beforeEach(inject(function (_Grade_) {
    Grade = _Grade_;
  }));

  it('should do something', function () {
    expect(!!Grade).toBe(true);
  });

});
