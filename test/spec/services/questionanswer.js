'use strict';

describe('Service: QuestionAnswer', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var QuestionAnswer;
  beforeEach(inject(function (_QuestionAnswer_) {
    QuestionAnswer = _QuestionAnswer_;
  }));

  it('should do something', function () {
    expect(!!QuestionAnswer).toBe(true);
  });

});
