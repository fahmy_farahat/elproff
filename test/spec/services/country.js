'use strict';

describe('Service: country', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var country;
  beforeEach(inject(function (_country_) {
    country = _country_;
  }));

  it('should do something', function () {
    expect(!!country).toBe(true);
  });

});
