'use strict';

describe('Service: QuestionType', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var QuestionType;
  beforeEach(inject(function (_QuestionType_) {
    QuestionType = _QuestionType_;
  }));

  it('should do something', function () {
    expect(!!QuestionType).toBe(true);
  });

});
