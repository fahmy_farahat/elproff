'use strict';

describe('Service: Classes', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var Classes;
  beforeEach(inject(function (_Classes_) {
    Classes = _Classes_;
  }));

  it('should do something', function () {
    expect(!!Classes).toBe(true);
  });

});
