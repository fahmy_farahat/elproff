'use strict';

describe('Service: validateAnswer', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var validateAnswer;
  beforeEach(inject(function (_validateAnswer_) {
    validateAnswer = _validateAnswer_;
  }));

  it('should do something', function () {
    expect(!!validateAnswer).toBe(true);
  });

});
