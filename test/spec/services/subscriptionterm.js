'use strict';

describe('Service: SubscriptionTerm', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var SubscriptionTerm;
  beforeEach(inject(function (_SubscriptionTerm_) {
    SubscriptionTerm = _SubscriptionTerm_;
  }));

  it('should do something', function () {
    expect(!!SubscriptionTerm).toBe(true);
  });

});
