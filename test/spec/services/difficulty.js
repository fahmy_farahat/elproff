'use strict';

describe('Service: Difficulty', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var Difficulty;
  beforeEach(inject(function (_Difficulty_) {
    Difficulty = _Difficulty_;
  }));

  it('should do something', function () {
    expect(!!Difficulty).toBe(true);
  });

});
