'use strict';

describe('Service: FeedbackType', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var FeedbackType;
  beforeEach(inject(function (_FeedbackType_) {
    FeedbackType = _FeedbackType_;
  }));

  it('should do something', function () {
    expect(!!FeedbackType).toBe(true);
  });

});
