'use strict';

describe('Service: SchoolManager', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var SchoolManager;
  beforeEach(inject(function (_SchoolManager_) {
    SchoolManager = _SchoolManager_;
  }));

  it('should do something', function () {
    expect(!!SchoolManager).toBe(true);
  });

});
