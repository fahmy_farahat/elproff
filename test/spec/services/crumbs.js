'use strict';

describe('Service: Crumbs', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var Crumbs;
  beforeEach(inject(function (_Crumbs_) {
    Crumbs = _Crumbs_;
  }));

  it('should do something', function () {
    expect(!!Crumbs).toBe(true);
  });

});
