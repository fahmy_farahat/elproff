'use strict';

describe('Service: CourseElement', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var CourseElement;
  beforeEach(inject(function (_CourseElement_) {
    CourseElement = _CourseElement_;
  }));

  it('should do something', function () {
    expect(!!CourseElement).toBe(true);
  });

});
