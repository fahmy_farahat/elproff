'use strict';

describe('Service: CouponBulk', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var CouponBulk;
  beforeEach(inject(function (_CouponBulk_) {
    CouponBulk = _CouponBulk_;
  }));

  it('should do something', function () {
    expect(!!CouponBulk).toBe(true);
  });

});
