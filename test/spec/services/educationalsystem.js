'use strict';

describe('Service: EducationalSystem', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var EducationalSystem;
  beforeEach(inject(function (_EducationalSystem_) {
    EducationalSystem = _EducationalSystem_;
  }));

  it('should do something', function () {
    expect(!!EducationalSystem).toBe(true);
  });

});
