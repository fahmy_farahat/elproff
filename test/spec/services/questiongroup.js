'use strict';

describe('Service: QuestionGroup', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var QuestionGroup;
  beforeEach(inject(function (_QuestionGroup_) {
    QuestionGroup = _QuestionGroup_;
  }));

  it('should do something', function () {
    expect(!!QuestionGroup).toBe(true);
  });

});
