'use strict';

describe('Service: HelpAttachment', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var HelpAttachment;
  beforeEach(inject(function (_HelpAttachment_) {
    HelpAttachment = _HelpAttachment_;
  }));

  it('should do something', function () {
    expect(!!HelpAttachment).toBe(true);
  });

});
