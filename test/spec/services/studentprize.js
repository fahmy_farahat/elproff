'use strict';

describe('Service: StudentPrize', function () {

  // load the service's module
  beforeEach(module('elproffApp'));

  // instantiate service
  var StudentPrize;
  beforeEach(inject(function (_StudentPrize_) {
    StudentPrize = _StudentPrize_;
  }));

  it('should do something', function () {
    expect(!!StudentPrize).toBe(true);
  });

});
