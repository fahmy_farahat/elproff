'use strict';

describe('Filter: durationFormat', function () {

  // load the filter's module
  beforeEach(module('elproffApp'));

  // initialize a new instance of the filter before each test
  var durationFormat;
  beforeEach(inject(function ($filter) {
    durationFormat = $filter('durationFormat');
  }));

  it('should return the input prefixed with "durationFormat filter:"', function () {
    var text = 'angularjs';
    expect(durationFormat(text)).toBe('durationFormat filter: ' + text);
  });

});
