'use strict';

describe('Filter: examinstance', function () {

  // load the filter's module
  beforeEach(module('elproffApp'));

  // initialize a new instance of the filter before each test
  var examinstance;
  beforeEach(inject(function ($filter) {
    examinstance = $filter('examinstance');
  }));

  it('should return the input prefixed with "examinstance filter:"', function () {
    var text = 'angularjs';
    expect(examinstance(text)).toBe('examinstance filter: ' + text);
  });

});
