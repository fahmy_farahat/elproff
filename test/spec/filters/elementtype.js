'use strict';

describe('Filter: elementType', function () {

  // load the filter's module
  beforeEach(module('elproffApp'));

  // initialize a new instance of the filter before each test
  var elementType;
  beforeEach(inject(function ($filter) {
    elementType = $filter('elementType');
  }));

  it('should return the input prefixed with "elementType filter:"', function () {
    var text = 'angularjs';
    expect(elementType(text)).toBe('elementType filter: ' + text);
  });

});
