'use strict';

describe('Filter: charlimit', function () {

  // load the filter's module
  beforeEach(module('elproffApp'));

  // initialize a new instance of the filter before each test
  var charlimit;
  beforeEach(inject(function ($filter) {
    charlimit = $filter('charlimit');
  }));

  it('should return the input prefixed with "charlimit filter:"', function () {
    var text = 'angularjs';
    expect(charlimit(text)).toBe('charlimit filter: ' + text);
  });

});
