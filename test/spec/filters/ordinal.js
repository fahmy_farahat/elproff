'use strict';

describe('Filter: ordinal', function () {

  // load the filter's module
  beforeEach(module('elproffApp'));

  // initialize a new instance of the filter before each test
  var ordinal;
  beforeEach(inject(function ($filter) {
    ordinal = $filter('ordinal');
  }));

  it('should return the input prefixed with "ordinal filter:"', function () {
    var text = 'angularjs';
    expect(ordinal(text)).toBe('ordinal filter: ' + text);
  });

});
