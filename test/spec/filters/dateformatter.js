'use strict';

describe('Filter: dateFormatter', function () {

  // load the filter's module
  beforeEach(module('elproffApp'));

  // initialize a new instance of the filter before each test
  var dateFormatter;
  beforeEach(inject(function ($filter) {
    dateFormatter = $filter('dateFormatter');
  }));

  it('should return the input prefixed with "dateFormatter filter:"', function () {
    var text = 'angularjs';
    expect(dateFormatter(text)).toBe('dateFormatter filter: ' + text);
  });

});
