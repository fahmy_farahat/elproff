'use strict';

describe('Directive: questionTakeFooter', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<question-take-footer></question-take-footer>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the questionTakeFooter directive');
  }));
});
