'use strict';

describe('Directive: questionMakeFooter', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<question-make-footer></question-make-footer>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the questionMakeFooter directive');
  }));
});
