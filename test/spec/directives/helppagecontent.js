'use strict';

describe('Directive: helpPageContent', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<help-page-content></help-page-content>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the helpPageContent directive');
  }));
});
