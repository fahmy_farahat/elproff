'use strict';

describe('Directive: questionInfo', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<question-info></question-info>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the questionInfo directive');
  }));
});
