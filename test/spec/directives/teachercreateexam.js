'use strict';

describe('Directive: teacherCreateExam', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<teacher-create-exam></teacher-create-exam>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the teacherCreateExam directive');
  }));
});
