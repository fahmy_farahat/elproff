'use strict';

describe('Directive: student/exam/suggested', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<student/exam/suggested></student/exam/suggested>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the student/exam/suggested directive');
  }));
});
