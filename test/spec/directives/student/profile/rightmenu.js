'use strict';

describe('Directive: student/profile/rightmenu', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<student/profile/rightmenu></student/profile/rightmenu>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the student/profile/rightmenu directive');
  }));
});
