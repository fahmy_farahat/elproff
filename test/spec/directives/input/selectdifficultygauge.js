'use strict';

describe('Directive: input/selectDifficultyGauge', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<input/select-difficulty-gauge></input/select-difficulty-gauge>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the input/selectDifficultyGauge directive');
  }));
});
