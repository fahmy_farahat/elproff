'use strict';

describe('Directive: inputSelectMultiQuestionsType', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<input/-select-multi-questions-type></input/-select-multi-questions-type>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the input/SelectMultiQuestionsType directive');
  }));
});
