'use strict';

describe('Directive: input/schoolname', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<input/schoolname></input/schoolname>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the input/schoolname directive');
  }));
});
