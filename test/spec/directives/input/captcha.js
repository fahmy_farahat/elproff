'use strict';

describe('Directive: input/captcha', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<input/captcha></input/captcha>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the input/captcha directive');
  }));
});
