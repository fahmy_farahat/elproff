'use strict';

describe('Directive: inputs/fullname', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<inputs/fullname></inputs/fullname>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the inputs/fullname directive');
  }));
});
