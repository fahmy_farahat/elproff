'use strict';

describe('Directive: authorHighChart', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<author-high-chart></author-high-chart>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the authorHighChart directive');
  }));
});
