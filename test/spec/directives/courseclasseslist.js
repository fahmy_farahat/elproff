'use strict';

describe('Directive: courseClassesList', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<course-classes-list></course-classes-list>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the courseClassesList directive');
  }));
});
