'use strict';

describe('Directive: schoolStudentAdd', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<school-student-add></school-student-add>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the schoolStudentAdd directive');
  }));
});
