'use strict';

describe('Directive: staffCourseItem', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<staff-course-item></staff-course-item>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the staffCourseItem directive');
  }));
});
