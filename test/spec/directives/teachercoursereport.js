'use strict';

describe('Directive: teacherCourseReport', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<teacher-course-report></teacher-course-report>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the teacherCourseReport directive');
  }));
});
