'use strict';

describe('Directive: privateteacherRightMenu', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<privateteacher-right-menu></privateteacher-right-menu>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the privateteacherRightMenu directive');
  }));
});
