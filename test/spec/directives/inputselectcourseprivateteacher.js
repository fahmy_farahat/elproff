'use strict';

describe('Directive: inputSelectCoursePrivateTeacher', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<input-select-course-private-teacher></input-select-course-private-teacher>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the inputSelectCoursePrivateTeacher directive');
  }));
});
