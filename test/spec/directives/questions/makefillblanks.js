'use strict';

describe('Directive: questions/makeFillBlanks', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<questions/make-fill-blanks></questions/make-fill-blanks>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the questions/makeFillBlanks directive');
  }));
});
