'use strict';

describe('Directive: questions/takeFillBlanks', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<questions/take-fill-blanks></questions/take-fill-blanks>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the questions/takeFillBlanks directive');
  }));
});
