'use strict';

describe('Directive: questions/makeDragDrop', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<questions/make-drag-drop></questions/make-drag-drop>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the questions/makeDragDrop directive');
  }));
});
