'use strict';

describe('Directive: smallLanguageBtns', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<small-language-btns></small-language-btns>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the smallLanguageBtns directive');
  }));
});
