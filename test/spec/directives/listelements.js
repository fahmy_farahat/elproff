'use strict';

describe('Directive: listelements', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<listelements></listelements>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the listelements directive');
  }));
});
