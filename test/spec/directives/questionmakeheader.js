'use strict';

describe('Directive: questionMakeHeader', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<question-make-header></question-make-header>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the questionMakeHeader directive');
  }));
});
