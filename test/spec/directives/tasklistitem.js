'use strict';

describe('Directive: taskListItem', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<task-list-item></task-list-item>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the taskListItem directive');
  }));
});
