'use strict';

describe('Directive: teacherExamReport', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<teacher-exam-report></teacher-exam-report>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the teacherExamReport directive');
  }));
});
