'use strict';

describe('Directive: report/courseReportExamItem', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<report/course-report-exam-item></report/course-report-exam-item>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the report/courseReportExamItem directive');
  }));
});
