'use strict';

describe('Directive: report/examGrade', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<report/exam-grade></report/exam-grade>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the report/examGrade directive');
  }));
});
