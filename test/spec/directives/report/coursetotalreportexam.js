'use strict';

describe('Directive: report/courseTotalReportExam', function () {

  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<report/course-total-report-exam></report/course-total-report-exam>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the report/courseTotalReportExam directive');
  }));
});
