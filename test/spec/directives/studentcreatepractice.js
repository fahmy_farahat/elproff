'use strict';

describe('Directive: studentCreatePractice', function () {
  // TODO:  MAKE TEST UNIT
  // load the directive's module
  beforeEach(module('elproffApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<student-create-practice></student-create-practice>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the studentCreatePractice directive');
  }));
});
